import Echo from "laravel-echo";

declare global { // расширяем тип window, добавляя туда io для импорта через require
   interface Window {
      io: typeof import('socket.io-client')
   }
}

window.io = require('socket.io-client');


export const echo = new Echo({
   broadcaster: 'socket.io',
   transports: ['websocket'],
   host: process.env.REACT_APP_SOCKET_URL,
})