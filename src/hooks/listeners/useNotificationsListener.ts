import {Dispatch, useEffect} from "react";
import {echo} from "./echo";
import {ObjectId} from "../../api/events-api";
import app from "../../redux/app-reducer/app-reducer";

export const useNotificationsListener = (dispatch: Dispatch<any>, userId?: string) => {
   // прослушивает события получения уведомления
   useEffect(() => {
      if (echo && userId) {
         echo.channel(`vcoons_database_notify.${userId}`)
            .listen('CreateMeet', (e: SocketNotificationT) => {
               dispatch(app.actions.addNotification(e.notify))
            });
         echo.channel(`vcoons_database_notify.${userId}`)
            .listen('CancelMeeting', (e: SocketNotificationT) => {
               console.log(e)
               dispatch(app.actions.addNotification(e.notify))
            });
         echo.channel(`vcoons_database_notify.${userId}`)
            .listen('ConfirmMeeting', (e: SocketNotificationT) => {
               console.log(e)
               dispatch(app.actions.addNotification(e.notify))
            });
      }
      return () => echo.leaveChannel(`vcoons_database_notify.${userId}`)
   }, [])
}


export type NotificationT = {
   title: string
   text: string
   type: "confirm" | "confirmed" | "canceled"
   payload: {
      buttons: Array<ButtonT>
      date: string
      icon: string
      event_id: string
   }
   isConfirmed: boolean
}
export type SocketNotificationT = {
   notify: NotificationT
   specialist: {
      _id: ObjectId
      email: string
      first_name: string
      last_name: string
      user_id: number
   }
   socket: null
}

export type ButtonT = {
   text: string
   color: string
   action: string
}