import {Dispatch, useEffect} from "react";
import meeting, {LinkT} from "../../redux/meeting-reducer/meeting-reducer";
import {echo} from "./echo";
import {useSelector} from "react-redux";
import {RootStateT} from "../../redux/store";

export const useMessageListener = (dispatch: Dispatch<any>, eventId: string) => {
   // прослушивает события отправки/получения сообщения в чате видео-встречи

   useEffect(() => {
      if (echo) {
         echo.channel(`vcoons_database_meet.chat.${eventId}`)
            .listen('ChatMessage', (message: SocketMessageT) => {
               dispatch(meeting.actions.addMessage(message))
               dispatch(meeting.actions.setIsNewMessage(true))
            });
         echo.channel(`vcoons_database_meet.chat.${eventId}`)
            .listen('LinkMessages', (link: SocketLinkT) => {
               dispatch(meeting.actions.setLink(link))
            });
      }

      return () => echo.leaveChannel(`vcoons_database_meet.chat.${eventId}`)
   }, [])
}

export type SocketMessageT = {
   message: string
   event_id: string
   time: number
   user: {
      avatar: string
      first_name: string
      last_name: string
      user_id: number
   }
   socket: null
}
export type SocketLinkT = {
   event_id: string
   link: LinkT
   time: number
   user: {
      avatar: string
      first_name: string
      last_name: string
      user_id: number
   }
   socket: null
}