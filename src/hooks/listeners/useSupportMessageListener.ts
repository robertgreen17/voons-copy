import {Dispatch, useEffect} from "react";
import meeting, {LinkT} from "../../redux/meeting-reducer/meeting-reducer";
import {echo} from "./echo";
import {useSelector} from "react-redux";
import {RootStateT} from "../../redux/store";
import {SupportMessageT} from "../../api/user-api";
import support from "../../redux/support-reducer/support-reducer";

export const useSupportMessageListener = (dispatch: Dispatch<any>, user_id: string, onNewMessage?: () => void) => {
   // прослушивает события отправки/получения сообщения в чате

   useEffect(() => {
      if (echo) {
         echo.channel(`vcoons_database_support.${user_id}`)
            .listen('SendMessageToAdmin', (socketSupportMessage: SocketSupportMessageT) => {
               dispatch(support.actions.addSupportMessages([socketSupportMessage.data]))
               if (onNewMessage) onNewMessage()
            });
      }
      return () => echo.leaveChannel(`vcoons_database_support.${user_id}`)
   }, [])
}

export type SocketSupportMessageT = {
   data: SupportMessageT
   socket: null
}
export type SocketLinkT = {
   event_id: string
   link: LinkT
   time: number
   user: {
      avatar: string
      first_name: string
      last_name: string
      user_id: number
   }
   socket: null
}