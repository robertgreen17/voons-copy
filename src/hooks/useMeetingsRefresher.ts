import {Dispatch, useEffect} from "react";
import {getDayMeetings, THREE_HOURS_MS} from "../utils/calendar";
import {getMeetings} from "../redux/events-reducer/events-reducer";
import {MeetingT} from "../api/events-api";

export const useMeetingsRefresher = (
   meetings: Array<MeetingT>,
   dispatch: Dispatch<any>,
   currentMeetings?: Array<MeetingT>,
) => {
   useEffect(() => {
      if (!currentMeetings) currentMeetings = getDayMeetings(new Date(), meetings)
      // обновление данных о встрече во время начала встречи
      if (meetings && currentMeetings.length > 0) {
         let leftTime = -1;
         let i = 0
         while (leftTime < 0 && i < currentMeetings.length) {
            // время начала ближайшей встречи в timestamp миллисекундах
            const nearestMeetingStart = currentMeetings[i].timestamps.time_start * 1000 - THREE_HOURS_MS
            // время до начала ближайшей встречи в миллисекундах
            const nowDate = new Date().getTime()
            leftTime = nearestMeetingStart - nowDate
            ++i
         }
         if (leftTime < 0) return

         const timeout = setTimeout(() => {
            // ставим таймер, чтобы в момент начала встречи сделать новый запрос
            dispatch(getMeetings({}))
            // это нужно для появления возможность перейти к трансляции, не перезагружая страницу
         }, leftTime)

         return () => {
            clearTimeout(timeout)
         }
      }
   }, [meetings, currentMeetings])
}