import React, {useEffect, useRef} from "react";

export function useHorizontalScroll() {
   // hook for containers
   // add onWheel event listener for horizontal scroll
   const elRef = useRef<any>();
   useEffect(() => {
      const el = elRef.current;
      if (el) {
         const onWheel = (e: React.WheelEvent) => {
            e.preventDefault();
            el.scrollTo({
               left: el.scrollLeft + e.deltaY,
               // behavior: "smooth"
            });
         };
         el.addEventListener("wheel", onWheel);
         return () => el.removeEventListener("wheel", onWheel);
      }
   }, []);
   return elRef;
}