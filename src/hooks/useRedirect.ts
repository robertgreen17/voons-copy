import { useHistory } from "react-router-dom"

export const useRedirect = (condition: boolean | undefined, url: string) => {
   const history = useHistory()

   if (condition) history.push(url)
}