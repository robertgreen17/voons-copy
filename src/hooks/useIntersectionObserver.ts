import {useCallback, useRef} from "react";

export const useIntersectionObserver = (isLoading: boolean,
                                        hasMore: boolean,
                                        fetchData: (offset: number) => void,
                                        offset: number,
                                        setOffset: (offset: number) => void,
                                        limit: number) => {
   const observer = useRef<any>()
   const lastItem = useCallback((node: any) => {
      if (isLoading) return
      if (observer && observer.current) observer.current.disconnect()
      observer.current = new IntersectionObserver(async entries => {
         if (entries[0].isIntersecting && hasMore) {
            await fetchData(offset + limit)
            setOffset(offset + limit)
         }
      })
      if (node) observer.current.observe(node)
   }, [isLoading, offset, observer])
   return lastItem
}