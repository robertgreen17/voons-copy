import {MonthDataT} from "../components/common/Calendar/Calendar";
import {WorkTimeT} from "../api/calendar-api";
import {isEqualObjProps, isObjInArray} from "./objects";
import {MeetingT, TimesT} from "../api/events-api";
import {sortBy} from 'lodash';

enum Month {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
}

export const isEqualDays = (a?: Date, b?: Date) => {
    if (!a || !b) return false
    const aDay = a.getDate()
    const bDay = b.getDate()
    const aMonth = a.getMonth()
    const bMonth = b.getMonth()
    const aYear = a.getFullYear()
    const bYear = b.getFullYear()
    return aDay === bDay && aMonth === bMonth && aYear === bYear
}

export const getMonthData = (year: number, month: number) => {
    const result: MonthDataT = []
    const date = new Date(year, month)
    const daysInMonth = getDaysInMonth(date)
    const monthStartsOn = getDayOfWeek(date)
    const weeksInMonth = (daysInMonth + monthStartsOn) / DAYS_IN_WEEK
    let day = 1

    for (let i = 0; i < weeksInMonth; ++i) {
        result[i] = []
        for (let j = 0; j < DAYS_IN_WEEK; ++j) {
            if (i === 0 && j < monthStartsOn || day > daysInMonth) {
                result[i][j] = null
            } else {
                const date = new Date(year, month, day++)
                result[i][j] = {
                    mod: "disabled",
                    statusMod: "disabled",
                    date: date
                }
            }
        }
    }
    return result
}

export const markSelectedDays = (data: MonthDataT, selectedDays: Array<Date>) => {
    const result: MonthDataT = []
    for (let i = 0; i < data.length; ++i) {
        result[i] = []
        for (let j = 0; j < data[i].length; ++j) {
            let day = data[i][j]
            if (day) {
                for (let k = 0; k < selectedDays.length; ++k) {
                    const selectedDay = selectedDays[k].getDate()
                    const currentDay = day.date.getDate()
                    const selectedMonth = selectedDays[k].getMonth()
                    const currentMonth = day.date.getMonth()
                    const selectedYear = selectedDays[k].getFullYear()
                    const currentYear = day.date.getFullYear()
                    if (selectedDay === currentDay && selectedMonth === currentMonth && selectedYear === currentYear) {
                        day.mod = "active"
                        break
                    } else {
                        day.mod = "disabled"
                    }
                }
            }
            result[i][j] = day
        }
    }
    return result
}
export const markWorkDays = (data: MonthDataT, workDays: Array<WorkTimeT>) => {
    const result: MonthDataT = []
    for (let i = 0; i < data.length; ++i) {
        result[i] = []
        for (let j = 0; j < data[i].length; ++j) {
            let day = data[i][j]
            if (day) {
                for (let k = 0; k < workDays.length; ++k) {
                    const workDay = new Date(workDays[k].timestamps.date * 1000).getDate()
                    const currentDay = day.date.getDate()
                    const workMonth = new Date(workDays[k].timestamps.date * 1000).getMonth()
                    const currentMonth = day.date.getMonth()
                    const workYear = new Date(workDays[k].timestamps.date * 1000).getFullYear()
                    const currentYear = day.date.getFullYear()
                    if (workDay === currentDay
                        && workMonth === currentMonth
                        && day.mod !== "active"
                        && workYear === currentYear
                    ) {
                        day.mod = "default"
                    }
                }
            }
            result[i][j] = day
        }
    }
    return result
}
export const markMeetingDays = (data: MonthDataT, meetings: Array<MeetingT>) => {
    const result: MonthDataT = []
    for (let i = 0; i < data.length; ++i) {
        result[i] = []
        for (let j = 0; j < data[i].length; ++j) {
            let day = data[i][j]
            if (day) {
                for (let k = 0; k < meetings.length; ++k) {
                    const meetingDay = new Date(meetings[k].timestamps.date * 1000).getDate()
                    const currentDay = day.date.getDate()
                    const meetingMonth = new Date(meetings[k].timestamps.date * 1000).getMonth()
                    const currentMonth = day.date.getMonth()
                    const meetingYear = new Date(meetings[k].timestamps.date * 1000).getFullYear()
                    const currentYear = day.date.getFullYear()
                    if (meetingDay === currentDay
                        && meetingMonth === currentMonth
                        && meetingYear === currentYear
                        && !meetings[k].finished
                    ) {
                        day.statusMod = "active"
                    }
                }
            }
            result[i][j] = day
        }
    }
    return result
}

export const filterMeetingsByIsFinished = (meetings: Array<MeetingT>, isFinished: boolean) => {
    let filtered: Array<MeetingT>;
    if (isFinished) {
        filtered = meetings.filter(meeting => meeting.finished && meeting.review);
        return sortBy(filtered, (m) => -m.timestamps.date)
    } else {
        filtered = meetings.filter(meeting => !meeting.finished || !meeting.review);
        return sortBy(filtered, (m) => m.timestamps.date)
    }
}

export const convertMeetingsArrToObj = (meetings: Array<MeetingT>) => {
    if (meetings.length === 0) return {}
    type MeetingsT = {
        [date: string]: Array<MeetingT>
    }
    let result: MeetingsT = {}

    meetings.forEach(meeting => {
        if (result[meeting.date as keyof MeetingT]) {
            result[meeting.date as keyof MeetingT] = [...result[meeting.date as keyof MeetingT], meeting]
        } else {
            result[meeting.date as keyof MeetingT] = [meeting]
        }

    })

    return result
}

export const getMeetingsPrice = (meetings: Array<MeetingT>) => {
    if (meetings.length === 0) return 0
    let res = 0
    meetings.forEach(m => {
        res += m.price.value
    })
    return res
}

export const getDayMeetings = (day: Date, meetings: Array<MeetingT>) => {
    if (!day) return []
    let result: Array<MeetingT> = []
    result = meetings.filter(meeting => {
        const currentDay = day.getDate()
        const currentMonth = day.getMonth()
        const currentYear = day.getFullYear()
        const meetingDay = new Date(meeting.timestamps.date * 1000).getDate()
        const meetingMonth = new Date(meeting.timestamps.date * 1000).getMonth()
        const meetingYear = new Date(meeting.timestamps.date * 1000).getFullYear()
        if (currentDay === meetingDay
            && currentYear === meetingYear
            && currentMonth === meetingMonth
            && !meeting.finished
        ) {
            return meeting
        }
    })
    return sortBy(result, m => m.timestamps.time_start)
}
export const getDayWorkTime = (day: Date, workDays: Array<WorkTimeT>) => {
    if (!day) return []
    type ResultT = {
        time_to: string
        time_from: string
        work_id: string
    }
    let result: Array<ResultT> = []
    workDays.forEach(workDay => {
        if (new Date(workDay.timestamps.date * 1000).getDate() === day.getDate()) {
            result = [...result, {
                time_from: workDay.time_from,
                time_to: workDay.time_to,
                work_id: workDay.work_id
            }]
        }
    })
    return result
}
export const getWorkIdByDate = (date: Date, workDays: Array<WorkTimeT>) => {
    for (let i = 0; i < workDays.length; ++i) {
        const workDay = new Date(workDays[i].timestamps.date * 1000).getDate()
        const currentDay = date.getDate()
        const workMonth = new Date(workDays[i].timestamps.date * 1000).getMonth()
        const currentMonth = date.getMonth()
        const workYear = new Date(workDays[i].timestamps.date * 1000).getFullYear()
        const currentYear = date.getFullYear()
        if (workDay === currentDay && workMonth === currentMonth && workYear === currentYear) {
            return workDays[i].work_id
        }
    }
    return ""
}

export const getSelectedDaysWorkTime = (days: Array<Date>, workDays: Array<WorkTimeT>) => {
    if (days.length === 0) return null
    type TimeT = {
        time_to: string
        time_from: string
    }
    let result: Array<TimeT> = []
    for (let i = 0; i < days.length; ++i) {
        const day = days[i].getDate()
        for (let j = 0; j < workDays.length; ++j) {
            const workDay = new Date(workDays[j].timestamps.date * 1000).getDate()
            if (day === workDay) {
                result.push({
                    time_from: workDays[j].time_from,
                    time_to: workDays[j].time_to,
                })
            }
        }
    }
    return result
}

export const getDaysWithSimilarWorkTime = (currentDay: Date, monthData: MonthDataT, workTimes: Array<WorkTimeT>) => {
    const currentWorkTime = getDayWorkTime(currentDay, workTimes)
    if (!currentWorkTime) return [currentDay]

    const result: Array<Date> = []
    for (let i = 0; i < monthData.length; ++i) {
        for (let j = 0; j < monthData[i].length; ++j) {
            const day = monthData[i][j]
            if (day) {
                const dayWorkTime = getDayWorkTime(day.date, workTimes)
                if (dayWorkTime) {
                    if (isEqualObjProps(currentWorkTime, dayWorkTime, ["time_from", "time_to"])) {
                        result.push(day.date)
                    }
                }
            }
        }
    }
    return result
}

export const MINUTE_SECONDS = 60

export const getAllowedTimeTable = (timeTable: Array<TimesT>, duration: number | null) => {
    let res: Array<TimesT> = []
    let j = 0
    if (!duration) return timeTable
    for (let i = 0; i < timeTable.length; i++) {
        const times = timeTable[i].times
        if (times.length === 0) continue
        const end = times[times.length - 1].timestamp
        const duration_sec = duration * MINUTE_SECONDS
        // const filtered = times.filter(time => time.timestamp + duration_sec <= end)
        const filtered = times.filter(time => {
            let isNotEndOfInterval = false
            for (let k = 0; k < times.length; k++) {
                if (times[k].timestamp === time.timestamp + 5 * MINUTE_SECONDS) isNotEndOfInterval = true
                if (times[k].timestamp === (time.timestamp + duration_sec) && isNotEndOfInterval) return true
            }
            return false
        })
        res[j] = {
            times: filtered,
            date: timeTable[i].date
        }
        ++j
    }
    return res
}

export type SelectedDatesWorkTimeT = {
    days: Array<Date>
    time: Array<TimeT>
}

type TimeT = {
    time_from: string
    time_to: string
    work_id: string
}
export const splitWorkDaysByTime = (workDays: Array<WorkTimeT>) => {
    if (workDays.length === 0) return []

    const result: Array<Array<TimeT>> = []
    for (let i = 0; i < workDays.length; ++i) {

    }
    return result
}

export const getSelectedDatesTimesFrom = (dayTimes: Array<DayTimesT>, selectedDates: Array<Date>) => {
    let res: Array<string> = []
    for (let i = 0; i < dayTimes.length; i++) {
        for (let j = 0; j < dayTimes[i].times.length; j++) {
            if (res.indexOf(dayTimes[i].times[j].time_from) === -1) {
                const date = new Date(dayTimes[i].times[j].timestamps.date * 1000)
                if (selectedDates.filter(d => isEqualDays(d, date)).length > 0) {
                    res = [...res, dayTimes[i].times[j].time_from]
                }
            }
        }
    }
    return res
}
export const getSelectedDatesTimesTo = (dayTimes: Array<DayTimesT>, selectedDates: Array<Date>) => {
    let res: Array<string> = []
    for (let i = 0; i < dayTimes.length; i++) {
        for (let j = 0; j < dayTimes[i].times.length; j++) {
            if (res.indexOf(dayTimes[i].times[j].time_to) === -1) {
                const date = new Date(dayTimes[i].times[j].timestamps.date * 1000)
                if (selectedDates.filter(d => isEqualDays(d, date)).length > 0) {
                    res = [...res, dayTimes[i].times[j].time_to]
                }
                // res = [...res, dayTimes[i].times[j].time_to]
            }

        }
    }
    return res
}

export const getSelectedDatesWorkIdsByTimeFrom = (dayTimes: Array<DayTimesT>, timeFrom: string) => {
    let res: Array<string> = []
    for (let i = 0; i < dayTimes.length; i++) {
        for (let j = 0; j < dayTimes[i].times.length; j++) {
            if (dayTimes[i].times[j].time_from === timeFrom) {
                res = [...res, dayTimes[i].times[j].work_id]
            }

        }
    }
    return res
}

type DayTimesT = {
    day: number
    times: Array<WorkTimeT>
}

export const getSelectedDatesWorkTimes = (dates: Array<Date>, workTimes: Array<WorkTimeT>) => {
    let res: Array<DayTimesT> = []
    const workDays = splitWorkDaysByDays(workTimes)
    res = workDays.filter(d => dates.filter(t => isEqualDays(new Date(d.day), t)).length > 0)
    return res
}

export const getAllWeeksWithSelectedDays = (dates: Array<Date>, monthData: MonthDataT) => {
    let res: Array<Date> = []
    for (let i = 0; i < monthData.length; ++i) {
        const week = monthData[i]
        let isSelected = false
        dates.forEach((day) => {
            if (!isSelected && week.filter(d => isEqualDays(d?.date, day)).length > 0) {
                isSelected = true
                week.forEach((d) => {
                    if (d) {
                        res = [...res, d?.date]
                    }
                })
            }
        })
    }
    return res
}


export const splitWorkDaysByDays = (workDays: Array<WorkTimeT>) => {
    if (workDays.length === 0) return []

    const result: Array<DayTimesT> = []
    for (let i = 0; i < workDays.length; ++i) {
        if (result.filter(d => d.day === workDays[i].timestamps.date * 1000).length === 0) {
            result.push({
                day: workDays[i].timestamps.date * 1000,
                times: [workDays[i]]
            })
        } else {
            result.filter(d => d.day === workDays[i].timestamps.date * 1000).forEach(d => d.times.push(workDays[i]))
        }
    }
    result.forEach(d => d.times = sortBy(d.times, t => t.timestamps.time_from))
    return result
}


export const transformDatesToString = (dates: Array<Date>) => {
    const arr = dates.map(date => date.getDate()).sort((a, b) => {
        if (a < b) return -1
        if (a > b) return 1
        return 0
    })
    if (arr.length === 1) return String(arr[0]);

    let count = 0;
    let first_element;
    let main_string = '';
    let i = 0;

    while (i < arr.length - 1) {
        count = 0;
        first_element = arr[i];

        while (arr[i] === arr[i + 1] - 1) {
            ++count;
            ++i;
        }

        if (count === 0)
            main_string = main_string + first_element + ",";

        else if (count === 1)
            main_string = main_string + first_element + "," + arr[i] + ",";

        else
            main_string = main_string + first_element + "-" + arr[i] + ",";

        i++;
        if (i === arr.length - 1)
            main_string = main_string + arr[i] + ",";

    }
    return main_string.slice(0, -1);
    // return arr.join(", ")
}

// export const getCurrentWeek = (date: Date) => {
//    const monthStartsOn = getDayOfWeek(date)
//    return Math.floor((date.getDate() + monthStartsOn) / DAYS_IN_WEEK) - 1
// }

// @ts-ignore
Date.prototype.getWeekOfMonth = function () {
    var date = new Date(this.getTime());
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    var week1 = new Date(date.getFullYear(), date.getMonth(), 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

export const getCurrentWeek = () => {
    // @ts-ignore
    // console.log(new Date().getWeekOfMonth())
    // @ts-ignore
    return new Date().getWeekOfMonth()
}
// export const getCurrentWeek = () => {
//    var year = new Date().getFullYear();
//    var month = new Date().getMonth();
//    var today = new Date(year, month, 0).getTime();
//    var now = new Date().getTime();
//    var week = Math.ceil((now - today) / (1000 * 60 * 60 * 24 * 7));
//    console.log(week)
//    return week
//
// }

export function getWeeksCount(year: number, month: number) { // Внимание: Месяцы нумеруются с 0, как принято в JS
    const l = new Date(year, month + 1, 0);
    return Math.ceil((l.getDate() - (l.getDay() ? l.getDay() : 7)) / 7) + 1;
}


export const getDaysInMonth = (date: Date) => {
    const month = date.getMonth()
    const year = date.getFullYear()

    if (isLeapYear(year) && month === Month.February) {
        return DAYS_IN_MONTH[month] + 1
    } else {
        return DAYS_IN_MONTH[month]
    }
}

export const getDayOfWeek = (date: Date) => {
    const dayOfWeek = date.getDay()

    if (dayOfWeek === 0) return 6

    return dayOfWeek - 1
}


export const isLeapYear = (year: number) => {
    return !((year % 4) || (!(year % 100) && (year % 400)))
}

export const DAYS_IN_WEEK = 7
export const DAYS_IN_MONTH = [31, 28, 30, 31, 30, 31, 30, 31, 30, 31, 30, 31]
export const WEEKDAYS_RU = ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"]
export const MONTHS_RU = {
    0: "Января",
    1: "Февраля",
    2: "Марта",
    3: "Апреля",
    4: "Мая",
    5: "Июня",
    6: "Июля",
    7: "Августа",
    8: "Сентября",
    9: "Октября",
    10: "Ноября",
    11: "Декабря"
}
export const THREE_HOURS_MS = 10800000
