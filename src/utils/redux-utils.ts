import {Dispatch} from "react";
import {showNotification} from "./notifications";
import app from "../redux/app-reducer/app-reducer";
import {BaseResponseType} from "../api/api";
import {refresh} from "../redux/auth-reducer/auth-reducer";

export const commonThunkHandler = async (operation: any, dispatch: Dispatch<any>, isLoader?: boolean) => {
   // make tryException flow for thunks
   try {
      if (isLoader) dispatch(app.actions.setIsLoader(true))
      await operation() // perform an operation
      if (isLoader) dispatch(app.actions.setIsLoader(false))
   }
   catch (error) {
      console.error(error)
      //showNotification("Произошла ошибка. Перезагрузите страницу и попробуйте снова", "error")
   }
}

export const checkForRefresh = async (resData: BaseResponseType<any>, dispatch: Dispatch<any>) => {
   if (resData.data['refresh'] === true) {
      await dispatch(refresh())
   }
}