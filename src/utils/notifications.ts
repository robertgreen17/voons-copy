import {BaseResponseType} from "../api/api";
import Noty from "noty";

export type AppNotificationT = "alert" | "error" | "info" | "information" | "success" | "warning"

export const checkMessage = (response: BaseResponseType<any>) => {
   if (response.message) {
      switch (response.success) {
         case false:
            showNotification(response.message, "error")
            break
         case true:
            showNotification(response.message, "success")
            break
         default:
            showNotification(response.message, "information")
      }
   }
}

export const showNotification = (message: string, type: AppNotificationT) => {
   if (isAllowedNotification || message !== lastMessage) {
      // если это уведомление разрешено или это уведомление с другим текстом, то показываем
      lastMessage = message
      new Noty({
         text: message,
         layout: "bottomLeft",
         theme: "bootstrap-v4",
         type: type,
         timeout: 3000,
      }).show();
   }
   if (message === lastMessage) {
      // если это уведомление такое же, как и последнее, то
      isAllowedNotification = false // запрещаем его показывать
      setTimeout(() => {
         isAllowedNotification = true // через секунду опять разрешаем
      }, 1000)
      return
   } else {
      return
   }
}

let isAllowedNotification = true // флаг, который нужен для того, чтобы не показывать одно уведомление несколько раз подряд
let lastMessage = "" // текст последнего уведомления