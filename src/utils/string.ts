export const cutter = (maxLen: number, string: string) => {
    if (string.length > maxLen) {
        return string.substr(0, maxLen) + "..."
    }
    return string
}

export function formatMoney(n?: number) {
    if (!n) return "00.00"
    return parseFloat(n.toString()).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").replace('.', '.');
}

export function toProperName(str: string) {
    if (!str) return str
    return str[0].toUpperCase() + str.slice(1)
}

export const filterBySubstr = (arr: Array<string>, substr: string) => {
    return arr.filter(item => item.toLowerCase().indexOf(substr.toLowerCase()) !== -1)
}

export const declReviews = (count: number) => {
    switch (true) {
        case count % 10 === 1:
            return "отзыв"
        case count % 10 > 1 && count % 10 < 5:
            return "отзыва"
        default:
            return "отзывов"
    }
}
export const declWord = (count: number, word: string) => {
    let exceptions: Array<number> = [11, 12, 13, 14];
    switch (true) {
        case count % 10 === 1 && !exceptions.includes(count):
            return count + " " + word
        case count % 10 > 1 && count % 10 < 5:
            return count + " " + word + "a"
        default:
            return count + " " + word + "ов"
    }
}