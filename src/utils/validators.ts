export const emailValidator = (value: string) => {
   const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   return re.test(String(value).toLowerCase()) ? "" : "Введен некорректный email"
}

export const validateRequiredField = (value: string) => {
   return value.length === 0 ? "Это поле не может быть пустым" : undefined
}