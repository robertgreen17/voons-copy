export function sortObjArrayBy<OT>(arr: Array<OT>, prop: keyof OT) {
   // сортирует массив объектов по возрастанию по св-ву prop
   // OT - тип объекта в массиве
   return arr.sort((a, b) => {
      if (a[prop] < b[prop]) {
         return 1;
      }
      if (a[prop] > b[prop]) {
         return -1;
      }
      return 0;
   })
}

export const isEmptyMatrix = (matrix: Array<Array<any>>) => {
   for (let i = 0; i < matrix.length; ++i) {
      for (let j = 0; j < matrix[i].length; ++j) {
         if (matrix[i][j] !== undefined) return false
      }
   }
   return true
}

export function getBestItemByComparison<T>(arr: Array<T>, compareFunc: (a: T, b: T) => T) {
   if (!arr) return undefined
   let res: T = arr[0]
   arr.forEach(item => {
      res = compareFunc(item, res)
   })
   return res
}