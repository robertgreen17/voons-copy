export const getFormatTimeNow = () => {
   const date = new Date();
   const hours = date.getHours();
   const minutes = date.getMinutes();
   return hours + ":" + minutes
}

export const getTimestampNow = () => {
   return + new Date()
}

export const getWeekInterval = () => {
   return [getTimestampNow(), getTimestampNow() + WEEK_MILLISECONDS]
}

export const getWeekDaysArray = (start: number, end: number) => {
   let array = []
   while (start < end) {
      array.push(start)
      start += DAY_MILLISECONDS
   }
   return array
}

export const DAY_MILLISECONDS = 86400000
export const WEEK_MILLISECONDS = DAY_MILLISECONDS * 7
