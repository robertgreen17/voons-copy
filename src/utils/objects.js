export const deleteItemByKey = (obj, key) => {
   // возвращает новый объект с удаленным ключом key
   let res = {}
   Object.keys(obj).forEach((item) => {
      if (item !== key) res[item] = {...obj[item]}
   })
   return res
}

export const isEqualObjProps = (a, b, props) => {
   // делает глубокое сравнение объектов по свойствам из массива props

   if (a === b) return true;

   // if (!props || props.length === 0) {
   //    // если массив свойств пуст, то проверяем все свойства
   //    Object.keys(a).forEach((key) => {
   //       if (key in b && a[key] !== b[key]) return false
   //    })
   //    return true
   // }

   for (let i = 0; i < props.length; ++i) {
      if (props[i] in b && props[i] in b) {
         if (a[props[i]] !== b[props[i]]) return false
      }
   }
   return true
}


export const isEmptyObj = (obj) => {
   // проверяет объект на пустоту
   return Object.keys(obj).length === 0;
}

export const isObjInArray = (arr, obj, props) => {
   // проверяет есть ли в массиве идентичный объект
   for (let i = 0; i < arr.length; ++i) {
      if (isEqualObjProps(obj, arr[i], props)) {
         return true
      }
   }
   return false
}

