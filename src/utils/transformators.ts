import {PriceT} from "../api/events-api";

export type PricesObjT = Record<number, string>

export const transformPricesToObject = (prices: Array<PriceT> | undefined) => {
   if (prices) {
      let obj: PricesObjT = {}
      prices.forEach((item) => {
         obj[item.time] = item.price
      })
      return obj
   }
   return undefined
}