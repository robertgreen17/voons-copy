import {DocT} from "../api/user-api";

export const getFileExtension = (fileName: string) => {
   if (!fileName) return ""
   return fileName.split('.').pop();
}

export const getFileMimeTypeByUrl = (url: string) => {
   const extension = getFileExtension(url)
   switch (extension) {
      case "doc":
         return "application/msword"
      case "jpeg":
      case "jpg":
         return "image/jpeg"
      case "png":
         return "image/png"
      case "pdf":
         return "application/pdf"
      default:
         return "image/jpeg"
   }
}

// export const getPathArrayFromDocs = (docs: Record<string, DocT>) => {
//    let array: Array<string> = []
//    let j = 0
//    Object.keys(docs).forEach((key: string, i) => {
//       const path = docs[key as keyof DocT].path
//       const start = path.search("files")
//       if (docs[key].moderation) {
//          array[j] = path.slice(start)
//          ++j
//       }
//    })
//    return array;
// }
export const getPathArrayFromDocs = (docs: Record<string, DocT>) => {
   let array: Array<string> = []
   let j = 0
   Object.keys(docs).forEach((key: string, i) => {
      const path = docs[key as keyof DocT].path
      if (docs[key].moderation) {
         array[j] = path
         ++j
      }
   })
   return array;
}

export const getPathFromDoc = (doc: DocT) => {
   const path = doc.path
   const start = path.search("files")
   return path.slice(start)
}