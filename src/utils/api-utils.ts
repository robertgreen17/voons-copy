import {BaseResponseType, instance} from "../api/api";

export async function makePostRequest<RD>(body: any, url: string) {
   return instance.post<BaseResponseType<RD>>(url, {...body}).then(res => res.data)
}

export const setTokenInHeaders = () => {

}