import {BaseResponseType, instance} from "./api";
import {ObjectId, PriceT} from "./events-api";
import {ReviewT} from "./user-api";

export const searchApi = {
   // api requests for searching
   find(body: FindReqBodyT) {
      return instance.get<BaseResponseType<FindResDataT>>("/search/search.find", {params: body}).then(res => res.data)
   },
   getCategories() {
      return instance.get<BaseResponseType<Array<CategoryT>>>("/search/search.categories").then(res => res.data)
   },
   predict(body: {name: string}) {
      return instance.get<BaseResponseType<Array<string | PredictionT>>>("/search/search.predict",{params: body}).then(res => res.data)
   },
   predictCountry(body: {name: string}) {
      return instance.get<BaseResponseType<Array<CountryT>>>("/search/search.country",{params: body}).then(res => res.data)
   },
};

// response/requests data types
export type CountryT = {
   code_country: string
   fullname: string
   iso: string
   location: string
   name: string

}
export type PredictionT = {
   ru: string
   en: string
}
export type FindReqBodyT = {
   name?: string
   category?: Array<string>
   limit?: number
   offset?: number
}
export type FindResDataT = Array<SearchUserT>

export type SearchUserT = {
   _id: ObjectId
   user_id: number
   first_name: string
   last_name: string
   avatar: string
   times_price: Array<PriceT>
   work_info: string
   lang: Array<string>
   tags: Array<TagT | string>
   reviews: Array<ReviewT>
   reviews_count: number
   rating: number,
   services: Array<string>,
   uses_free: Array<string>
}
export type CategoryT = {
   width?: string
   height?: string
   name: string
   tags: Array<TagT>
   icon: string
   users_count: number
   big: string
   text: string
   title: string
}

export type TagT = {
   _id: ObjectId
   name_ru: string
   name_en: string
   global: boolean
}