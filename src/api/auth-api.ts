import {BaseResponseType, instance} from "./api";

export const authApi = {
   // api requests for authentication
   authCreate(body: AuthCreateReqBodyT) {
      return instance.put<BaseResponseType<AuthCreateResDataT>>("/auth/auth.create", body).then(res => res.data)
   },
   authLogin(body: AuthLoginReqBodyT) {
      return instance.post<BaseResponseType<AuthLoginResDataT>>("/auth/auth.login", {...body}).then(res => res.data)
   },
   resetPassword(body: {account: string}) {
      return instance.post<BaseResponseType<ResetPasswordResDataT>>("/auth/auth.resetPassword", {...body}).then(res => res.data)
   },
   verifyCode(body: VerifyCodeReqBodyT) {
      return instance.post<BaseResponseType<VerifyCodeResDataT>>("/auth/auth.verifyCode", {...body}).then(res => res.data)
   },
   doPassword(body: DoPasswordReqBodyT) {
      return instance.post<BaseResponseType<AuthLoginResDataT>>("/auth/auth.doPassword", {...body}).then(res => res.data)
   },
   doSocial(body: {type: string}) {
      return instance.post<BaseResponseType<AuthLoginResDataT>>("/auth/auth.doSocial", {...body}).then(res => res.data)
   },
   refresh() {
      return instance.patch<BaseResponseType<AuthLoginResDataT>>("/auth/auth.refresh").then(res => res.data)
   },
};

// response/requests data types
export type AuthCreateReqBodyT = {
   first_name: string
   last_name: string
   email: string
   country: string
   // city: string
   phone: string
   account_type: 0 | 1
   password: string
   fields?: string
   invited_by?: string
}
export type AuthCreateResDataT = {
   mongo_id: string
   user_id: string
   first_name?: string
   last_name?: string
   email?: string
   country?: string
   city?: string
   phone?: string
}
export type AuthLoginReqBodyT = {
   account: string
   password: string
}
export type AuthLoginResDataT = {
   access_token: string
   refresh_token: string
   expires_at: string
   ip: string
}
export type ResetPasswordResDataT = {
   code: any
   user: any
}
export type DoPasswordReqBodyT = {
   temp_token: string
   password: string
   destory_sessions: boolean
}
export type VerifyCodeReqBodyT = {
   code: string
   account: string
}
export type VerifyCodeResDataT = {
   temp_token: string
}