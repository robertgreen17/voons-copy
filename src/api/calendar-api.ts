import {BaseResponseType, instance} from "./api";

export const calendarApi = {
   // api requests for calendar
   createCalendar(body: CreateCalendarReqBodyT) {
      return instance.put<BaseResponseType<[]>>("/calendar/calendar.create", body).then(res => res.data)
   },
   deleteCalendar(body: {work_id: Array<string>}) {
      return instance.delete<BaseResponseType<[]>>("/calendar/calendar.remove", {params: body}).then(res => res.data)
   },
   getWorkTimes(body: GetWorkTimesReqBodyT) {
      return instance.get<BaseResponseType<GetWorkTimesResDataT>>("/calendar/calendar.getWorkTimes", {params: body}).then(res => res.data)
   },
   setTime(body: any[][]) {
      return instance.post<BaseResponseType<GetWorkTimesResDataT>>("/calendar/calendar.setTime", {params: body}).then(res => res.data)
   }
};


// response/requests data types
export type CreateCalendarReqBodyT = {
   time_from: string | Array<string>
   time_to: string | Array<string>
   date: Array<number>
}

export type GetWorkTimesReqBodyT = {
   user_id?: string // mongo_id
   date?: string
}
export type GetWorkTimesResDataT = Array<WorkTimeT>

export type SetWorkTimesModalT = Array<Number>


export type WorkTimeT = {
   time_from: string
   time_to: string
   date: string
   work_id: string
   timestamps: {
      time_from: number
      time_to: number
      date: number
   }
}