import {BaseResponseType, instance} from "./api";

export const meetingApi = {
    // api requests for meeting
    sendMessage(body: SendMessageReqBody) {
        return instance.post<BaseResponseType<SendMessageResData>>("/meeting/meeting.sendMessage", body).then(res => res.data)
    },
    cancelMeeting(body: { event_id: string }) {
        return instance.post<BaseResponseType<[]>>("/meeting/meeting.cancel", body).then(res => res.data)
    },
    confirmMeeting(body: { event_id: string }) {
        return instance.post<BaseResponseType<[]>>("/meeting/meeting.confirm", body).then(res => res.data)
    },
    rate(body: RateReqBodyT) {
        return instance.post<BaseResponseType<[]>>("/meeting/meeting.rating", body).then(res => res.data)
    },

};

// response/requests data types
export type SendMessageReqBody = {
    event_id: string
    by_id: string
    text: string
    attachments?: Array<any>
}
export type RateReqBodyT = {
    specialist_id: string
    rating: number
    review?: string
    event_id: string
}
export type SendMessageResData = {
    with_url: boolean
}