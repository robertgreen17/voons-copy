import axios, {AxiosRequestConfig, AxiosResponse} from "axios";

export const instance = axios.create({
   baseURL: process.env.REACT_APP_API_URL,
   headers: {
      "Content-Type": "application/json",
      'X-SERVER-TOKEN': 'base64:GX1sRxf5YWW0zx26VzymifueXQuVGWbRdZSAryHjhTc=',
   }
});

// middleware which add tokens to every request
instance.interceptors.request.use(
   (config: AxiosRequestConfig) => {
      return {
         ...config,
         headers: {
            ...config.headers,
            'X-ACCESS-TOKEN': accessToken,
            'X-REFRESH-TOKEN': localStorage.getItem('refresh'),
            'X-TIMEZONE': new Date().getTimezoneOffset() / 60,
         }
      }
   }
)

instance.interceptors.response.use((response: AxiosResponse<BaseResponseType<any>>) => {
   // everything went well
   if (response.data.data.access_token) accessToken = response.data.data.access_token // set access token in variable
   return response; // pass data through
}, error => {
   // when error
   return error.response // return error data
});

export type BaseResponseType<D> = {
   success: boolean
   message: string
   data: D
}

// user access token variable
export let accessToken = ""
export const setAccessToken = (value: string | null) => {
   if (!value) return
   accessToken = value
}