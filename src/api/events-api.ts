import {BaseResponseType, instance} from "./api";

export const eventsApi = {
   // api requests for events
   setTimesPrice(body: SetTimesPriceReqBodyT) {
      return instance.post<BaseResponseType<Array<SetTimesPriceReqBodyT>>>("/events/events.setTimesPrice", body).then(res => res.data)
   },
   createEvent(body: CreateEventReqBodyT) {
      return instance.put<BaseResponseType<{isNoMoney?: boolean}>>("/events/events.create", body).then(res => res.data)
   },
   getFreeTimes(body: GetFreeTimesReqBodyT) {
      return instance.get<BaseResponseType<GetFreeTimesResDataT>>("/events/events.getFreeTimes", {params: body}).then(res => res.data)
   },
   getMeetings(body: GetMeetingsReqBody) {
      return instance.get<BaseResponseType<Array<MeetingT>>>("/events/events.getMeetings", {params: body}).then(res => res.data)
   },
};

// response/requests data types
export type SetTimesPriceReqBodyT = {
   time: Array<Number>
   price: Array<String>
}

export type PriceT = {
   time: number
   price: string
}

export type CreateEventReqBodyT = {
   time: number
   date: number
   specialist_id: ObjectId  // mongo_id
   description: string
   free?: boolean
}
export type GetFreeTimesReqBodyT = {
   date_from: number
   date_to: number
   specialist_id: ObjectId  // mongo_id
}
export type GetFreeTimesResDataT = {
   prices: Array<PriceT> // ценовая политика
   times: Array<TimesT>
   available_free: boolean
}

export type TimesT = {
   date: string
   times: Array<TimeT> // доступное время
}

export type TimeT = {
   human: string
   timestamp: number
}

export type GetMeetingsReqBody = {
   event_id?: string
   limit?: number
   offset?: number
}
export type GetMeetingReqBody = {
   event_id: string
}
export type MeetingT = {
   _id: ObjectId
   specialist: {
      _id: ObjectId
      first_name: string
      last_name: string
      user_id: number
   }
   client: {
      _id: ObjectId
      first_name: string
      last_name: string
   }
   time_start: string
   time_end: string
   date: string
   price: {
      format: string
      value: number
   }
   finished: boolean
   review: boolean
   timestamps: {
      date: number
      time_start: number
      time_end: number
   }
   started: boolean
   duration: string
   confirmed: boolean
   description: string
}

export type ObjectId = {
   $oid: string
}