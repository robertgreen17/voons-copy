import {BaseResponseType, instance} from "./api";
import {UserTypeT} from "../redux/user-reducer/user-reducer";
import {ObjectId, PriceT} from "./events-api";
import {TagT} from "./search-api";
import {NotificationT} from "../hooks/listeners/useNotificationsListener";


export const userApi = {
    // api requests for user
    getUsers(body: GetUsersReqBodyT) {
        return instance.get<BaseResponseType<GetUsersResDataT>>("/users/users.get", {params: body ? body : ""}).then(res => res.data)
    },
    getDocs(body: GetDocsReqBodyT) {
        return instance.get<BaseResponseType<GetDocsResDataT>>("/users/users.getDocs", {params: body}).then(res => res.data)
    },
    setDocs(body: FormData | Array<File>) {
        return instance.post<BaseResponseType<GetDocsResDataT>>("/users/users.setDocs", body).then(res => res.data)
    },
    removeDocs(body: RemoveDocsReqBodyT) {
        return instance({
            method: 'DELETE',
            url: "/users/users.removeDocs",
            data: body
        }).then(res => res.data)
    },
    setAvatar(body: FormData | null) {
        return instance.post<BaseResponseType<{ photo: string }>>("/users/users.setAvatar",
            body ? body : {},
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }).then(res => res.data)
    },
    changeAccount(body: ChangeAccountReqBodyT) {
        return instance.post<BaseResponseType<any>>("/users/users.changeAccount", body).then(res => res.data)
    },
    changeSettings(body: ChangeSettingsReqBodyT) {
        return instance.post<BaseResponseType<any>>("/users/users.changeSettings", body).then(res => res.data)
    },
    setSocial(body: SetSocialReqBodyT) {
        return instance.post<BaseResponseType<any>>("/users/users.setSocial", body).then(res => res.data)
    },
    changePassword(body: ChangePasswordReqBodyT) {
        return instance.post<BaseResponseType<any>>("/users/users.changePassword", body).then(res => res.data)
    },
    changeNotify(body: ChangeNotifyReqBodyT) {
        return instance.post<BaseResponseType<[]>>("/users/users.changeNotify", body).then(res => res.data)
    },
    getNotifies() {
        return instance.get<BaseResponseType<Array<NotificationT>>>("/users/users.getNotifies").then(res => res.data)
    },
    clearNotifies() {
        return instance.delete<BaseResponseType<[]>>("/users/users.clearNotifies").then(res => res.data)
    },
    switchFirstMeetingFree(body: { free: boolean }) {
        return instance.post<BaseResponseType<[]>>("/users/users.switchFirstMeetingFree", body).then(res => res.data)
    },
    sendSupportMessage(body: SendSupportMessageReqBodyT) {
        return instance.post<BaseResponseType<{ user_id?: string }>>("/users/users.sendMessageClient", body).then(res => res.data)
    },
    getSupportMessages(body: GetSupportMessagesReqBodyT) {
        return instance.get<BaseResponseType<GetSupportMessagesResDataT>>("/users/users.getSupportChat", {params: body}).then(res => res.data)
    },
    refill(body: RefillReqBodyT) {
        return instance.post<BaseResponseType<{ url: string }>>("/users/users.deposit", body).then(res => res.data)
    },
    withdraw(body: WithdrawReqBodyT) {
        return instance.post<BaseResponseType<{ url: string }>>("/users/users.withdraw", body).then(res => res.data)
    },
};

// response/requests data types
export type SendSupportMessageReqBodyT = {
    user_id: null | string
    message: string
}
export type RefillReqBodyT = {
    amount: number
}
export type WithdrawReqBodyT = {
    amount: number
    card: string
}
export type GetSupportMessagesReqBodyT = {
    user_id: null | string
    offset?: number
    limit?: number
}
export type GetSupportMessagesResDataT = {
    messages?: Array<SupportMessageT>
    user_id?: string
    _id?: ObjectId
}
export type SupportMessageT = {
    text: string
    from: "client" | "admin"
    date: number
}
export type GetUsersReqBodyT = {
    user_ids?: string
    fields?: string
    offset?: number
    limit?: string
}
export type SetSocialReqBodyT = {
    vk?: string
    instagram?: string
    fb?: string
}

export type GetUsersResDataT = {
    first_name?: string
    last_name?: string
    email?: string
    country?: string
    phone?: string
    account_type?: UserTypeT
    avatar?: string
    user_id?: number
    _id?: ObjectId
    files?: Record<string, DocT>
    verified?: boolean
    verify_telegram?: string
    times_price?: Array<PriceT>
    balance?: number
    work_info?: string
    tags?: Array<TagT | string>
    lang?: Array<string>
    notify?: NotifyT
    free_first?: boolean
    stat?: StatsT
    rating?: RatingT
    reviews_count?: number
    verified_specialist?: boolean
    reviews?: Array<ReviewT>
    social?: SetSocialReqBodyT,
    services?: Array<string>,
    uses_free?: Array<string>
}

export type StatT = {
    meetings_passed: number
    earned: number
}

export type StatsT = {
    day: StatT
    week: StatT
    month: StatT
}

interface ArrStr {
    [key: string]: string | number; // Must accommodate all members

    [index: number]: string; // Can be a subset of string indexer

    // Just an example member
    length: number;
}


export type ReviewT = {
    first_name: string
    last_name: string
    rating_set: number
    text: string
    user_id: number
    avatar: string
    date?: string
    moderated: boolean
}
export type RatingT = {
    total: number
    events: number
    sum: number
}

export type NotifyT = {
    telegram: boolean
    email: boolean
}

export type GetDocsResDataT = Record<string, DocT>
export type DocT = {
    moderation: boolean
    path: string
    notLoaded?: boolean
}
export type GetDocsReqBodyT = {
    user_id?: string
    limit?: number
}
export type ChangeAccountReqBodyT = {
    first_name?: string
    last_name?: string
    country?: string
    city?: string
    work_info?: string
    tags?: Array<string>
    lang?: Array<string>
    services?: Array<string>
}
export type ChangeSettingsReqBodyT = {
    email?: string
    phone?: string
}
export type ChangeNotifyReqBodyT = {
    email?: boolean
    telegram?: boolean
    verified?: boolean
}
export type ChangePasswordReqBodyT = {
    old_password: string
    new_password: string
}

export type RemoveDocsReqBodyT = {
    id?: string
    ids?: Array<string>
}