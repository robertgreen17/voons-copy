import "../node_modules/noty/lib/noty.css";
import "../node_modules/noty/lib/themes/bootstrap-v4.css";
import React, {useEffect} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {Main} from "./pages/Main/Main";
import {useDispatch, useSelector} from "react-redux";
import {initialize} from "./redux/app-reducer/app-reducer";
import {Profi} from "./pages/Dashboard/Profi/Profi";
import {Client} from "./pages/Dashboard/Client/Client";
import {Settings} from "./pages/Settings/Settings";
import {RootStateT} from "./redux/store";
import MyLoader from "./components/common/MyLoader/MyLoader";
import {Search} from "./pages/Search/Search";
import {Searching} from "./pages/Searching/Searching";
import {ProfiProfile} from "./pages/ProfiProfile/ProfiProfile";
import {Meeting} from "./pages/Meeting/Meeting";
import {Review} from "./pages/Review/Review";
import {InviteRedirect} from "./pages/InviteRedirect/InviteRedirect";
import {AuthRedirect} from "./pages/AuthRedirect/AuthRedirect";
import {AuthErrorRedirect} from "./pages/AuthErrorRedirect/AuthErrorRedirect";
import {DataPolicy} from "./pages/StaticText/DataPolicy";
import {Statement} from "./pages/StaticText/Statement";
import {TermsOfUse} from "./pages/StaticText/TermsOfUse";
import {LegalInfo} from "./pages/StaticText/LegalInfo";
import {isMobile} from "react-device-detect";
import {MobileError} from "./pages/MobileError/MobileError";
import {HowWorks} from "./pages/StaticText/HowWorks";
import {QuestionsPage} from "./pages/StaticText/QuestionsPage";
import {AboutUs} from "./pages/StaticText/AboutUs";
import {Career} from "./pages/StaticText/Career";
import {Offer} from "./pages/StaticText/Offer";
import CookieConsent from "react-cookie-consent";
import {Page} from "./components/common/Page/Page";
import './cookie.css';

export const App = () => {
    const dispatch = useDispatch()
    const isLoader = useSelector((state: RootStateT) => state.app.isLoader)
    const isInit = useSelector((state: RootStateT) => state.app.isInit)

    useEffect(() => {
        if (!isInit) dispatch(initialize())
    }, [dispatch, isInit])

    if (!isInit) return <MyLoader/>

    /*if (isMobile) return <MobileError/>*/

    return (
        <>
            {isLoader && <MyLoader/>}
            <Switch>
                <Route path={"/main"} component={Main}/>
                <Route path={"/dashboard/profi"} component={Profi}/>
                <Route path={"/dashboard/client"} component={Client}/>
                <Route path={"/settings"} component={Settings}/>
                <Route path={"/search"} component={Search}/>
                <Route path={"/profi/:id"} component={ProfiProfile}/>
                <Route path={"/searching"} component={Searching}/>
                <Route path={"/meeting/:id"} component={Meeting}/>
                <Route path={"/review/:id"} component={Review}/>
                <Route path={"/i/:id"} component={InviteRedirect}/>
                <Route path={"/oauth/login"} component={AuthRedirect}/>
                <Route path={"/oauth/error"} component={AuthErrorRedirect}/>
                <Route path={"/data_policy"} component={DataPolicy}/>
                <Route path={"/statement"} component={Statement}/>
                <Route path={"/terms_of_use"} component={TermsOfUse}/>
                <Route path={"/legal_info"} component={LegalInfo}/>
                <Route path={"/how_works"} component={HowWorks}/>
                <Route path={"/questions"} component={QuestionsPage}/>
                <Route path={"/about"} component={AboutUs}/>
                <Route path={"/career"} component={Career}/>
                <Route path={"/offer"} component={Offer}/>
                <Route path={"/"} component={() => <Redirect to={"/main"}/>}/>
            </Switch>
            <CookieConsent
                location="bottom"
                buttonText="ОК"
                cookieName="VoonsCookie"
                containerClasses = {"cookieWrapper"}
                contentClasses = {"cookieContent"}
                buttonWrapperClasses = {"cookieButton"}
                style={{background: "#ffffff", color: "#161835"}}
                buttonStyle={{
                    color: "#FFFFFF",
                    fontSize: "13px",
                    padding: "1rem",
                    borderRadius: "10px",
                    background: "#5465e7"
                }}
            >
                Регистрируясь на сайте, вы соглашаетесь на обработку персональных данных, политикой конфиденциальности и
                в том числе сбор cookies данных. В случае, если вы отказываетесь предоставлять данные, покиньте
                сайт.
            </CookieConsent>
        </>

    );
}
