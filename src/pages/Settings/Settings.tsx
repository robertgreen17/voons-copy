import React, {FC, useEffect, MouseEvent} from "react"
import {Page} from "../../components/common/Page/Page";
import {Wrapper} from "../../components/Settings/Wrapper/Wrapper";
import {Footer} from "../../components/common/Footer/Footer";
import {Route} from "react-router-dom";
import {EditProfile} from "../../components/Settings/EditProfile/EditProfile";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../redux/store";
import {useRedirect} from "../../hooks/useRedirect";
import {EditSocial} from "../../components/Settings/EditSocial/EditSocial";
import {EditAccount} from "../../components/Settings/EditAccount/EditAccount";
import {EditPassword} from "../../components/Settings/EditPassword/EditPassword";
import {EditNotify} from "../../components/Settings/EditNotify/EditNotify";
import {EditWork} from "../../components/Settings/EditWork/EditWork";
import {getUserData} from "../../redux/user-reducer/user-reducer";

export const Settings: FC = () => {

   const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
   const dispatch = useDispatch()

   useRedirect(!isAuth, "/main")

   useEffect(() => {
      dispatch(getUserData({}))
   }, [])

   return (
      <Page isNavbar={true} navbarBg={"#FFF"}>
         <Wrapper>
            <Route exact path={"/settings"} component={EditProfile}/>
            <Route path={"/settings/social"} component={EditSocial}/>
            <Route path={"/settings/account"} component={EditAccount}/>
            <Route path={"/settings/password"} component={EditPassword}/>
            <Route path={"/settings/notify"} component={EditNotify}/>
            <Route path={"/settings/work"} component={EditWork}/>
         </Wrapper>
         <Footer/>
      </Page>
   )
}