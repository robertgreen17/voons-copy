import React, {FC} from 'react';
import styles from "./styles.module.scss"

type PropsT = {}

export const MobileError: FC<PropsT> = ({}) => {
   return (
      <div className={styles.wrapper}>
         <img src={"/icons/logo2.png"} alt="" className={styles.logo}/>
         <div className={styles.text}>
            Извините, но на данный момент сервис работает только через версию для компьютера
         </div>
      </div>
   )
}