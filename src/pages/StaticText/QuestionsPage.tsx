import React, {FC} from 'react';
import styles from "./styles.module.scss"

type PropsT = {}

export const QuestionsPage: FC<PropsT> = ({}) => {
    return (
        <div className={styles.wrapper}>
            <h1>Часто задаваемые вопросы</h1>
            <h2>Что такое сервис Voons. и как он работает?</h2>
            <p>
                Voons. - это платформа, на которой пользователи могут заказать видеозвонок один на один с лучшими мировыми экспертами, влиятельными лицами и специалистами конкретных
                областей.
                За определенную плату пользователи могут напрямую связываться с людьми: задать вопрос, получить экспертное мнение или совет.
                Специалисты получают оплату за свое время и экспертные знания или опыт в определенных областях.
            </p>

            <h2>Как зарегистрироваться в качестве специалиста в сервисе Voons.?</h2>
            <ul>
                <li>Нажмите кнопку «Зарегистрироваться» на Voons. и заполните короткую форму.</li>
                <li>Заполните информацию о своих достижениях, добавьте хештеги по которым можно вас найти, а также время, в которое вы хотели бы быть доступным для звонков.</li>
                <li>Заполните информацию об оплате ваших временных слотов</li>
                <li> Загрузите ваши сертификаты и фото</li>
                <li>Вы готовы получать заказы!</li>
            </ul>
            <p>
                Поделитесь своей уникальной ссылкой для бронирования ваших консультаций со своими подписчиками. После этого ваши клиенты смогут записываться к вам на
                онлайн-консультации.
                Выплаты осуществляются в течении 3 дней после отправки заявки на вывод денежных средств.
                Мы заботимся о вашем времени и платежах, а также защищаем вашу конфиденциальность, так что вам не нужно ни о чем беспокоиться. Вы можете просто заполнить свой профиль и
                начать общаться со своими клиентами.
                Мы создали сервис, чтобы экспертные знания стали доступнее.
                Мы хотим, чтобы каждый мог учиться, получать совет от разных экспертов из различных отраслей.
                Присоединяйтесь к нам!
            </p>

            <h2>Кто может стать специалистом?</h2>
            <p>
                Чтобы стать специалистом, вам необходимо:
                Быть не моложе 18 лет.
                Иметь банковскую карту.
            </p>

            <h2>Когда пользователь может оспорить консультацию?</h2>
            <p>
                Если пользователь считает, что звонок нарушает наши Условия обслуживания , и хочет вернуть деньги, он может инициировать спор в течение 24 часов после звонка. Мы
                просмотрим запись разговора и примем решение в индивидуальном порядке, получив обратную связь от обоих участников видеовызова.
                Обратите внимание: если вызов не оспаривается, запись автоматически удаляется через 24 часа и не может быть оспорена. Если Условия обслуживания были нарушены или
                консультация не была корректной, мы вернем пользователю плату за звонок.
                Мы Проинформируем вас о решении всеми доступными способами (e-mail, звонок на мобильный номер, указанный в вашем личном кабинете)
            </p>

            <h2>Записываются ли звонки Voons.?</h2>
            <p>
                Да. Все звонки Voons автоматически записываются в случае возникновения споров и в соответствии с нашей Политикой конфиденциальности. Если вызов не оспаривался в течение
                24 часов, запись автоматически удаляется и консультация не может быть оспорена.
            </p>

            <h2>Что нужно делать специалисту Voons., чтобы получать запросы на звонок?</h2>
            <p>
                Чтобы получать запросы на звонки, специалисты должны заполнить свой профиль и поделиться своей уникальной ссылкой для бронирования своих временных слотов со своей
                аудиторией на своих страницах социальных сетей.
                Прежде чем публиковать ссылку на свой профиль, убедитесь, что вы обновили доступные временные интервалы, и включили уведомления, чтобы не пропустить запросы на
                консультации.
            </p>

            <h2>Как и когда происходит оплата специалистам?</h2>
            <p>
                Мы используем UnitPay для обработки всех платежей. Первая выплата занимает около 7 рабочих дней. Впоследствии комиссия будет переведена на ваш банковский счет/банковскую
                карту в течение 2-3 рабочих дней после вашего запроса на вывод денежных средств в личном кабинете специалиста.
            </p>

            <h2>Сколько получают специалисты ?</h2>
            <p>
                В сервисе Voons нет скрытых комиссий и оплат. Более того, мы не взимаем оплату за использование сервиса Voons. Если в вашем профиле цена вашего звонка за временной слот
                составляет 100 рублей, вы можете быть уверены, что получите выплату в размере 100 рублей через 2-3 рабочих дня после запроса этой суммы на вывод денежных средств на свой
                банковский счет или карту. Для того чтобы мы могли обеспечивать разработку и функционирование платформы, к общей стоимости звонка мы добавляем дополнительные комиссии,
                которые составляют не более10% от стоимости временного слота специалиста.
            </p>

            <h2>Если звонок не состоялся?</h2>
            <p>
                Если вы не можете подключиться к вызову, вы можете либо перенести его, либо отменить. Подробные инструкции по отмене или переносе звонка вы можете найти в первом письме
                на ваш e-mail при регистрации на сервисе.
                Мы уважаем время каждого и берем полную плату за неявку клиента без его предупреждения о невозможности быть на консультации не менее чем за 6 часов до начала.
                Чрезвычайные ситуации, которые возникли менее чем за 6 часов до начала консультации, рассматриваются в индивидуальном порядке. Для того, чтобы мы рассмотрели ситуацию,
                необходимо отправить описание возникшей проблемы на e-mail: help@voons.ru
            </p>
        </div>
    )
}