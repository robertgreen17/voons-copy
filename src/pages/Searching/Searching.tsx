import React, {FC} from "react"
import {SearchList} from "../../components/Search/SearchList/SearchList";
import {SearchWrapper} from "../../components/Search/SeacrhWrapper/SearchWrapper";

export const Searching: FC = () => {

   return (
      <SearchWrapper>
         <SearchList/>
      </SearchWrapper>
   )
}
