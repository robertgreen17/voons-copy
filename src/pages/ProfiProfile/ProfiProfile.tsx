import React, {FC, useEffect} from "react";
import {Page} from "../../components/common/Page/Page";
import styles from "./styles.module.scss"
import {Button} from "../../components/common/Button/Button";
import {ProfileCard} from "../../components/ProfiProfile/ProfileCard/ProfleCard";
import {Docs} from "../../components/ProfiProfile/Docs/Docs";
import {Reviews} from "../../components/ProfiProfile/Reviews/Reviews";
import {Footer} from "../../components/common/Footer/Footer";
import {NavLink, Route, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {UserTypeT} from "../../redux/user-reducer/user-reducer";
import {GetUsersReqBodyT} from "../../api/user-api";
import {RootStateT} from "../../redux/store";
import MyLoader from "../../components/common/MyLoader/MyLoader";
import {getCurrentUserData} from "../../redux/search-reducer/search-reducer";
import {Appointment} from "../Appointment/Appointment";

export const ProfiProfile: FC = () => {
   // @ts-ignore
   const {id} = useParams()
   const dispatch = useDispatch()
   const userData = useSelector((state: RootStateT) => state.search.currentUserData)
   const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
   const userType = useSelector((state: RootStateT) => state.user.type)

   useEffect(() => {
      if (userData?.user_id !== +id) {
         const body: GetUsersReqBodyT = {
            fields: "last_name,first_name,avatar,files,user_id,id_,reviews_count,times_price,tags,work_info,verified_specialist,rating,reviews,lang,social,services,uses_free",
            user_ids: id,
         }
         dispatch(getCurrentUserData(body))
      }
   }, [id, userData])

   if (userData?.user_id !== +id) return <MyLoader/>

   const {social} = userData

   return (
      <Page navbarBg={"#FFF"} isNavbar={true} bg={"#F5F6FA"}>
         <Route path={"/profi/:id/appointment/:step"} component={Appointment}/>
         <div className={styles.wrapper}>
            <div className={styles.btnWrap}>
               <NavLink to={"/searching"}>
                  <button className={styles.backBtn}>
                     Все специалисты
                     <img src="/icons/navigation/arrowBack.svg" alt="" />
                  </button>
               </NavLink>

            </div>
            <div className={styles.row}>
               <div className={styles.left}>
                  <img src={userData?.avatar ? userData?.avatar : "/icons/userAvatar.png"} alt="" className={styles.avatar}/>
                  <NavLink to={ isAuth ? `/profi/${id}/appointment/1` : `/main/sign_in/auth`}>
                     <Button size={"large"} mod={"primary"} width={"full-width"} disabled={userType === UserTypeT.profi}>
                        { isAuth ? `Заказать консультацию` : `Войти, чтобы продолжить`}
                     </Button>
                  </NavLink>
                  <div className={styles.social}>
                     {social && social.vk && <a href={social.vk} target="_blank">
			               {/*<img src="/icons/socLinks/vk.svg" alt=""/>*/}
			               <span className={styles.vk}>
                           <i className={"fab fa-vk"}/>
                        </span>
		               </a>}
                     {social && social.instagram && <a href={social.instagram} target="_blank">
			               <img src="/icons/socLinks/inst_color.svg" alt=""/>
		               </a>}
                     {social && social.fb && <a href={social.fb} target="_blank">
			               {/*<img src="/icons/socLinks/fb.svg" alt=""/>*/}
	                     <span className={styles.fb}>
                           <i className={"fab fa-facebook"}/>
                        </span>
		               </a>}
                  </div>
               </div>
               <div className={styles.right}>
                  <ProfileCard/>
                  <Docs/>
                  <Reviews/>
               </div>
            </div>
         </div>
         <Footer/>
      </Page>
   )
}