import React, {FC, useEffect, useState} from "react";
import {SideBar} from "../../components/common/SideBar/SideBar";
import {Calendar} from "../../components/common/Calendar/Calendar";
import {useDispatch} from "react-redux";
import {getWorkTimes} from "../../redux/calendar-reducer/calendar-reducer";
import {getMeetings} from "../../redux/events-reducer/events-reducer";
import { useHistory, useLocation} from "react-router-dom";

export const CalendarPage: FC = () => {
   let location = useLocation();
   const [editMod, setEditMod] = useState(false)
   const dispatch = useDispatch()
   const push = useHistory().push

   useEffect(() => {
      dispatch(getWorkTimes({}))
   }, [])

   useEffect(() => {
      dispatch(getMeetings({limit: 15}))
   }, [])

   return (
      <SideBar title={"Календарь"}
               isArrow={false}
               onClickArrow={() => setEditMod(false)}
               onCloseCallBack={() => push("/dashboard/profi")}
               size={"medium"}>
         <Calendar editMod={editMod} setEditMod={setEditMod} targetData = {location.state}/>
      </SideBar>
   )
}