export function startConference(
    setIsAudioMuted: (flag: boolean) => void,
    setIsVideoMuted: (flag: boolean) => void,
    roomName: string,
    displayName: string,
    email?: string
) {

    const interfaceConfig = {
        APP_NAME: "Voons",
        NATIVE_APP_NAME: 'Voons',
        PROVIDER_NAME: "Voons",
        DEFAULT_BACKGROUND: '#1E276F',
        HIDE_DEEP_LINKING_LOGO: true,
        TOOLBAR_BUTTONS: ["settings", "sharedvideo"],
        SETTINGS_SECTIONS: [],
        SHOW_JITSI_WATERMARK: false,
        SHOW_WATERMARK_FOR_GUESTS: false,
        SHOW_CHROME_EXTENSION_BANNER: true,
        DISPLAY_WELCOME_PAGE_CONTENT: false,
        HIDE_INVITE_MORE_HEADER: true,
        JITSI_WATERMARK_LINK: 'https://voons.ru',
        VIDEO_QUALITY_LABEL_DISABLED: true,
        CONNECTION_INDICATOR_AUTO_HIDE_ENABLED: true,
        DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
        DISABLE_TRANSCRIPTION_SUBTITLES: true,
        ENABLE_DIAL_OUT: false,
        VERTICAL_FILMSTRIP: false,
        SHOW_POWERED_BY: false,
        SHOW_PROMOTIONAL_CLOSE_PAGE: false,
        DISPLAY_WELCOME_FOOTER: false,
        filmStripOnly: false,
        GENERATE_ROOMNAMES_ON_WELCOME_PAGE: false,
        DISABLE_FOCUS_INDICATOR: true,
        MOBILE_APP_PROMO: false,
        OPTIMAL_BROWSERS: ['chrome', 'chromium', 'firefox', 'nwjs', 'electron', 'safari', 'opera'],
    }

    try {
        const domain = process.env.REACT_APP_MEETING_URL;
        const options = {
            roomName: roomName,
            parentNode: document.querySelector('#meet'),
            userInfo: {
                "email": email ? email : "",
                "displayName": displayName
            },
            interfaceConfigOverwrite: interfaceConfig,
            configOverwrite: {
                constraints: {
                    video: {
                        height: {
                            ideal: 1280,
                            max: 1280,
                            min: 480
                        }
                    }
                },
                startWithVideoMuted: false,
                disableSimulcast: true,
                fileRecordingsEnabled: true,
            },
        };

        //@ts-ignore
        const api = new JitsiMeetExternalAPI(domain, options);

        // setTimeout(() => {
        //    api.executeCommand('startRecording', {
        //       mode: "file" //recording mode, either `file` or `stream`.
        //    })
        // }, 5000)

        const externalApi: JitsiApiT = {
            hangup: () => api.executeCommand('hangup'),
            toggleAudio: () => api.executeCommand('toggleAudio'),
            toggleVideo: () => api.executeCommand('toggleVideo'),
            toggleShareScreen: () => api.executeCommand('toggleShareScreen'),
            destroy: () => api.dispose(),
            startRecording: () => api.executeCommand('startRecording', {
                mode: "file" //recording mode, either `file` or `stream`.
            }),
            stopRecording: () => api.executeCommand('stopRecording', {
                mode: "file"
            })
        }

        api.addEventListener('audioMuteStatusChanged', function (e: {
            muted: boolean
        }) {
            setIsAudioMuted(e.muted)
        });
        api.addEventListener('videoMuteStatusChanged', function (e: {
            muted: boolean
        }) {
            setIsVideoMuted(e.muted)
        });
        return externalApi

    } catch (error) {
        console.error('Failed to load Jitsi API', error)
    }

}

export type JitsiApiT = {
    toggleAudio: () => void,
    toggleVideo: () => void,
    hangup: () => void,
    destroy: () => void,
    startRecording: () => void
    stopRecording: () => void,
    toggleShareScreen: () => void
} & any