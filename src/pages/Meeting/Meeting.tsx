import React, {FC, useEffect, useRef, useState} from "react"
import styles from "./styles.module.scss"
import {JitsiApiT, startConference} from "./jitsy";
import {VideoButton} from "../../components/common/VideoButton/VideoButton";
import {Chat} from "../../components/common/Chat/Chat";
import Countdown from 'react-countdown';
import {useHistory, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import meetingRedux, {getMeeting} from "../../redux/meeting-reducer/meeting-reducer";
import {RootStateT} from "../../redux/store";
import {getRoomName} from "../../utils/meeting";
import {UserTypeT} from "../../redux/user-reducer/user-reducer";
import {THREE_HOURS_MS} from "../../utils/calendar";
import {useMessageListener} from "../../hooks/listeners/useMessageListener";
import {useRedirect} from "../../hooks/useRedirect";
import {Review} from "../Review/Review";

export const Meeting: FC = () => {

    const [isAudioMuted, setIsAudioMuted] = useState(false)
    const [isVideoMuted, setIsVideoMuted] = useState(false)
    const [isScreenShare, setIsScreenShare] = useState(true)
    const [isCompleted, setIsCompleted] = useState(false)
    const [isChat, setIsChat] = useState(false)
    const [animationClass, setAnimationClass] = useState("")
    const [btnGroupAnimationClass, setBtnGroupAnimationClass] = useState("animate__fadeInUp")
    const api = useRef<JitsiApiT>(null) // ссылка на кастомный апи Jitsi, который мне возвращает startConference()

    const meeting = useSelector((state: RootStateT) => state.meeting.meeting)
    const isNewMessage = useSelector((state: RootStateT) => state.meeting.isNewMessage)
    const userType = useSelector((state: RootStateT) => state.user.type)
    const email = useSelector((state: RootStateT) => state.user.userData?.email)
    const userMongoId = useSelector((state: RootStateT) => state.user.userData?._id)
    const userId = useSelector((state: RootStateT) => state.user.userData?.user_id)

    // @ts-ignore
    const {id} = useParams()
    const dispatch = useDispatch()
    const push = useHistory().push

    useRedirect(meeting ? !meeting.started : false, "/main")

    useMessageListener(dispatch, id) // подписываемся на событие прихода сообщения

    useEffect(() => {
        dispatch(getMeeting(id))
    }, [])

    useEffect(() => {
        document.body.classList.add('animate__animated', 'animate__backInRight');

        if (meeting && api.current === null) {
            const firstName = meeting.specialist.first_name
            const lastName = meeting.specialist.last_name
            const myLastName = userType === 0 ? meeting.client.last_name : meeting.specialist.last_name
            const myFirstName = userType === 0 ? meeting.client.first_name : meeting.specialist.first_name
            console.log("Time end: " + timestamps.time_end);
            console.log("Time now: " + (Date.now() / 1000 + (3600 * 3)));
            //@ts-ignore
            if (window.JitsiMeetExternalAPI) {
                api.current = startConference(
                    setIsAudioMuted,
                    setIsVideoMuted,
                    getRoomName(firstName, lastName, meeting._id.$oid),
                    myFirstName + myLastName,
                    email)
            } else console.error('Meet API script not loaded')
        }
    }, [meeting])

    if (!meeting) return <></>

    const onToggleAudio = () => {
        api.current.toggleAudio()
    }

    const onToggleVideo = () => {
        api.current.toggleVideo()
    }

    const onToggleShare = () => {

        if (!isScreenShare) {
            api.current.toggleVideo()
        }

        api.current.toggleShareScreen()
        setIsScreenShare(!isScreenShare);

    }


    const onHangup = () => {
        api.current.stopRecording()
        api.current.hangup()
        api.current.destroy()


        if (isCompleted) {
            localStorage.setItem("messages", "")
            push(userType === UserTypeT.client ? `/review/${meeting.specialist._id.$oid}` : "/dashboard/profi")
        } else {
            push("/dashboard/" + (userType === UserTypeT.client ? "client" : "profi"))
        }
    }

    const {timestamps} = meeting

    return (
        <>
            <div id={"meet"} className={`${styles.wrapper} ${styles[animationClass]}`}>
                <div className={styles.header}>
                    <div className={styles.left}>
                        {/*<div className={styles.label}>*/}
                        {/*   Консультация с Юристом, Евой Семеновой*/}
                        {/*</div>*/}
                        {/*<div className={styles.from}>*/}
                        {/*   <Moment format="HH:mm:ss">{new Date(timeLeft)}</Moment>*/}
                        {/*   /!*{timeLeft}*!/*/}
                        {/*</div>*/}
                    </div>
                    <div className={styles.right}>
                        <Countdown
                            date={timestamps.time_end * 1000 - THREE_HOURS_MS}
                            onComplete={onHangup}
                            renderer={({minutes, seconds, completed}) => {
                               if( !completed ) {
                                  setIsCompleted(false)
                                  return (
                                      <div className={`${styles.time} ${minutes < 5 ? styles.danger : styles.default}`}>
                                         {minutes > 0 && `Осталось ${minutes} минут`}
                                         {minutes === 0 && seconds > 0 && `Осталось ${seconds} секунд`}
                                         {minutes === 0 && seconds === 0 && "Время истекло"}
                                      </div>)
                               } else {
                                  setIsCompleted(true)
                                   setTimeout(() => {
                                       localStorage.setItem("messages", "")
                                       push(userType === UserTypeT.client ? `/review/${meeting.specialist._id.$oid}` : "/dashboard/profi")
                                   }, 5000);
                                  return <Review/>
                               }
                            }}/>
                        {!isChat && <button className={styles.btn} onClick={() => {
                            dispatch(meetingRedux.actions.setIsNewMessage(false))
                            setIsChat(true)
                            setAnimationClass("small")
                        }}>
                            {isNewMessage && <span className={styles.bell}><i className={"fas fa-bell"}/></span>}
                            <img src="/icons/stream/openChat.svg" alt=""/>
                        </button>}
                    </div>
                </div>
                <div className={`${styles.toolBar} `}
                     onMouseEnter={() => setBtnGroupAnimationClass("animate__fadeInUp")}
                     onMouseLeave={() => {
                         setBtnGroupAnimationClass("animate__fadeOutDown animate__delay-1s")
                     }}
                >
                    <div className={`${styles.btnGroup}
                animate__animated 
                animate__faster 
                ${btnGroupAnimationClass}`}>
                        <VideoButton onClick={onToggleShare}>
                            <div className={styles.img}>
                                <i className={"fas fa-tv"}/>
                                {isScreenShare && <svg version="1.1" width="512" height="512" x="0" y="0"
                                                       viewBox="0 0 511.998 511.998" className={styles.line}>
                                    <g>
                                        <rect x="-86.01" y="235.971"
                                              transform="matrix(0.7071 -0.7071 0.7071 0.7071 -106.0382 255.9989)"
                                              width="684.019" height="50.056" fill="#15174d" data-original="#000000"/>
                                    </g>
                                </svg>}
                            </div>
                        </VideoButton>
                        <VideoButton onClick={onToggleAudio}>
                            <div className={styles.img}>
                                <img src="/icons/stream/micOffIcon.svg" alt=""/>
                                {isAudioMuted && <svg version="1.1" width="512" height="512" x="0" y="0"
                                                      viewBox="0 0 511.998 511.998" className={styles.line}>
                                    <g>
                                        <rect x="-86.01" y="235.971"
                                              transform="matrix(0.7071 -0.7071 0.7071 0.7071 -106.0382 255.9989)"
                                              width="684.019" height="50.056" fill="#15174d" data-original="#000000"/>
                                    </g>
                                </svg>}
                            </div>
                        </VideoButton>
                        <VideoButton onClick={onToggleVideo}>
                            <div className={styles.img}>
                                <img src="/icons/stream/vidOffIcon.svg" alt=""/>
                                {isVideoMuted && <svg version="1.1" width="512" height="512" x="0" y="0"
                                                      viewBox="0 0 511.998 511.998" className={styles.line}>
                                    <g>
                                        <rect x="-86.01" y="235.971"
                                              transform="matrix(0.7071 -0.7071 0.7071 0.7071 -106.0382 255.9989)"
                                              width="684.019" height="50.056" fill="#15174d" data-original="#000000"/>
                                    </g>
                                </svg>}
                            </div>
                        </VideoButton>
                        <VideoButton mod={"danger"} onClick={onHangup}>
                            <img src="/icons/stream/callOffIcon.svg" alt=""/>
                        </VideoButton>
                    </div>
                </div>
            </div>
            {isChat && <Chat
                userId={userId ? userId : 0}
                userMongoId={userMongoId?.$oid ? userMongoId.$oid : ""}
                eventId={id}
                onCloseCallBack={() => {
                    setAnimationClass("big")
                    setIsChat(false)
                }}/>}
        </>
    )
}





