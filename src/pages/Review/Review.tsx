import React, {FC, useEffect, useState} from "react";
import {Page} from "../../components/common/Page/Page";
import {NavLink, useHistory, useParams} from "react-router-dom";
import styles from "./styles.module.scss"
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../redux/store";
import {getMeeting, rate} from "../../redux/meeting-reducer/meeting-reducer";
import {Rating} from "../../components/common/Rating/Rating";
import {TextArea} from "../../components/common/Inputs/Input";
import {Button} from "../../components/common/Button/Button";
import {useForm} from "react-hook-form";
import {RateReqBodyT} from "../../api/meeting-api";

type FormData = {
    text: string
}
type PropsT = {
    specialistId: string
}

export const Review: FC = () => {

    const meeting = useSelector((state: RootStateT) => state.meeting.meeting)
    // @ts-ignore
    const {id} = useParams()
    const dispatch = useDispatch()
    const push = useHistory().push
    const {register, handleSubmit} = useForm<FormData>({
        defaultValues: {
            text: "",
        }
    });
    const [rating, setRating] = useState(0)

    useEffect(() => {
        if (!meeting) {
            dispatch(getMeeting(id))
        }
    }, [])

    const onSubmit = handleSubmit((data: FormData) => {
        const body: RateReqBodyT = {
            rating: rating,
            specialist_id: meeting!.specialist._id.$oid,
            event_id: id
        }
        if (data.text) body.review = data.text
        dispatch(rate(body, () => push("/dashboard/client")))
    });

    if (!meeting) return <></>

    const {specialist, duration} = meeting

    return (
        <Page bg={"#F5F6FA"}>
            <div className={styles.header}>
                <div>
                    <div className={styles.name}>
                        {specialist.first_name + specialist.last_name}
                    </div>
                    <div className={styles.duration}>
                        {duration}:00
                    </div>
                </div>
                <div className={styles.time}>
                    Время закончилось
                </div>
            </div>
            <div className={styles.container}>
                <div className={styles.box}>
                    <div className={styles.title}>
                        Вам понравилась консультация?
                    </div>
                    <div className={styles.label}>
                        Пожалуйста, оставьте отзыв о специалисте, чтобы помочь другим найти лучшего специалиста для
                        своей задачи!
                    </div>
                    <div className={styles.label}>
                        Если встреча не состоялась, обратитесь в тех.поддержку - help@voons.ru
                    </div>
                    <form onSubmit={onSubmit}>
                        <Rating rating={rating}
                                setRating={setRating}/>
                        <TextArea m={"30px 0 50px"}
                                  name={"text"}
                                  inputRef={register}
                                  bg={"#FFF"}
                                  placeholder={"Ваш комментарий"}/>
                        <div className={styles.btnGroup}>
                            <Button mod={"primary"}
                                    type={"submit"}
                                    iconPos={"icon-right"}
                                    size={"large"} m={"0 20px 0 0"}>
                                Оставить отзыв
                                <img src="/icons/arrowIcon.svg" alt=""/>
                            </Button>
                            <NavLink to={"/dashboard/client"}>
                                <Button size={"large"} type={"button"}>
                                    Не хочу
                                </Button>
                            </NavLink>
                        </div>
                    </form>
                </div>
            </div>
        </Page>
    )
}