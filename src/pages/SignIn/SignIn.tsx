import React, {FC} from "react"
import styles from './styles.module.scss'
import {Route, useHistory} from "react-router-dom"
import {RegForm} from "../../components/SignIn/RegForm/RegForm";
import {AuthForm} from "../../components/SignIn/AuthForm/AuthForm";
import {MobileSidebar} from "../../components/SignIn/MobileSidebar/MobileSidebar";
import {ResetPassword} from "../../components/SignIn/Reset/ResetPassword/ResetPassword";
import {VerifyCode} from "../../components/SignIn/Reset/VerifyCode/VerifyCode";
import {DoPassword} from "../../components/SignIn/Reset/DoPassword/DoPassword";
import {SideBar} from "../../components/common/SideBar/SideBar";

export const SignIn: FC = () => {
   const push = useHistory().push
   return (
      <div className={`${styles.wrapper}`}>
         <Route path={"/main/sign_in/reg/"}>
            <SideBar onCloseCallBack={() => push("/main")} title={"Регистрация"}>
               <RegForm/>
            </SideBar>
         </Route>
         <Route path={"/main/sign_in/auth"}>
            <SideBar onCloseCallBack={() => push("/main")} title={"Авторизация"}>
               <AuthForm/>
            </SideBar>
         </Route>
         <Route path={"/main/sign_in/mobile"}>
            <SideBar onCloseCallBack={() => push("/main")} size={"extrasmall"} contentAlign={"start"}>
               <MobileSidebar/>
            </SideBar>
         </Route>
         <Route path={"/main/sign_in/reset/1"}>
            <SideBar onCloseCallBack={() => push("/main")} title={"Сброс пароля"}>
               <ResetPassword/>
            </SideBar>
         </Route>
         <Route path={"/main/sign_in/reset/2"}>
            <SideBar onCloseCallBack={() => push("/main")} title={"Сброс пароля"}>
               <VerifyCode/>
            </SideBar>
         </Route>
         <Route path={"/main/sign_in/reset/3"}>
            <SideBar onCloseCallBack={() => push("/main")} title={"Сброс пароля"}>
               <DoPassword/>
            </SideBar>
         </Route>
      </div>
   )
}