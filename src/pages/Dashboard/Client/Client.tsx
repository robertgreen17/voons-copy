import React, {FC, useEffect, useState} from "react";
import {Page} from "../../../components/common/Page/Page";
import styles from "./styles.module.scss"
import {Wrapper} from "../../../components/Dashboard/Wrapper/Wrapper";
import {Container} from "../../../components/Dashboard/Container/Container";
import {SegmentControl} from "../../../components/common/Inputs/Input";
import {MiniCategories} from "../../../components/Search/MiniCategories/MiniCategories";
import {Card} from "../../../components/Dashboard/Profi/Meetings/Card/Card";
import {Footer} from "../../../components/common/Footer/Footer";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {useRedirect} from "../../../hooks/useRedirect";
import {getUserData, UserTypeT} from "../../../redux/user-reducer/user-reducer";
import {getMeetings} from "../../../redux/events-reducer/events-reducer";
import {
    convertMeetingsArrToObj,
    filterMeetingsByIsFinished,
    getDayMeetings,
    getMeetingsPrice
} from "../../../utils/calendar";
import {SearchGroup} from "../../../components/common/SearchGroup/SearchGroup";
import {Invite} from "../../../components/Dashboard/Invite/Invite";
import {NavLink} from "react-router-dom";
import {formatMoney} from "../../../utils/string";
import {Swiper, SwiperSlide} from "swiper/react";
import getNodeDimensions from 'get-node-dimensions'


type PropsT = {}

export const Client: FC<PropsT> = () => {
    const [active, setActive] = useState(0 as 0 | 1)
    const dispatch = useDispatch()
    const avatar = useSelector((state: RootStateT) => state.user.userData?.avatar);
    const name = useSelector((state: RootStateT) => state.user.userData?.first_name)
    const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
    const userType = useSelector((state: RootStateT) => state.user.type)
    const meetings = useSelector((state: RootStateT) => state.events.meetings) // планируемые встречи
    const [futureMeetings, setFutureMeetings] = useState(filterMeetingsByIsFinished(meetings, false))
    const [pastMeetings, setPastMeetings] = useState(filterMeetingsByIsFinished(meetings, true))
    const [meetingsObj, setMeetingsObj] = useState(convertMeetingsArrToObj(active === 0 ? futureMeetings : pastMeetings))
    const [dayMeetingsCount, setDayMeetingsCount] = useState(getDayMeetings(new Date(), meetings).length)
    const [dayMeetingsPrice, setDayMeetingsPrice] = useState(getMeetingsPrice(getDayMeetings(new Date(), meetings)));
    const [meetingsCardsHeight, setMeetingsCardsHeight] = useState("0px");
    useEffect(() => {
        setDayMeetingsCount(getDayMeetings(new Date(), meetings).length)
        setDayMeetingsPrice(getMeetingsPrice(getDayMeetings(new Date(), meetings)))
    }, [meetings])

    useEffect(() => {
        dispatch(getMeetings({}))
    }, [])

    // useMeetingsRefresher(meetings, dispatch)

    useEffect(() => {
        let interval = setInterval(() => {
            dispatch(getMeetings({}, false))
        }, 5000)
        return () => clearInterval(interval)
    }, [])

    useEffect(() => {
        let interval2 = setInterval(() => {
            dispatch(getUserData({}))
        }, 5000)
        return () => clearInterval(interval2)
    }, [])

    useEffect(() => {

        if (active === 0) {
            setFutureMeetings(filterMeetingsByIsFinished(meetings, false))
        } else {
            setPastMeetings(filterMeetingsByIsFinished(meetings, true))
        }
    }, [meetings, active])

    useEffect(() => {
        setMeetingsObj(convertMeetingsArrToObj(active === 0 ? futureMeetings : pastMeetings))
    }, [meetings, futureMeetings, pastMeetings])

    useEffect(() => {
        if (Object.keys(meetingsObj).length > 0) {
            let meetingsCards = document.querySelector('#meetings-cards') as HTMLElement;
            let meetingsWrapper = document.querySelector('#meetings-wrapper') as HTMLElement;
            let height;
            if (meetingsWrapper.offsetHeight > meetingsCards.scrollHeight) {
                height = meetingsWrapper.offsetHeight;
            } else {
                height = getNodeDimensions(meetingsWrapper).height * 0.7;
                if (getNodeDimensions(meetingsWrapper).width >= 1366) {
                    height = getNodeDimensions(meetingsWrapper).height;
                }
            }
            console.log(height);
            setMeetingsCardsHeight(`${height}px`);
        }
    },)

    useRedirect(!isAuth, "/main")
    useRedirect(userType === UserTypeT.profi, "/dashboard/profi")


    return (
        <Page isNavbar={true} isHelp={true} isId={'dashboard'}>
            <Wrapper type={'onBoardCleint'}>
                <Container>
                    <div className={styles.greetings_wrapper}>
                        <div className={styles.container}>
                            <img
                                src={avatar ? avatar : "/icons/userAvatar.png"}
                                alt=""
                                className={styles.avatar}
                            />
                            <div>
                                <div className={styles.title}>
                                    Добрый день, {name}
                                </div>
                                <NavLink to={"/settings"}>
                                    <div className={styles.settingsButton}>Редактировать профиль</div>
                                </NavLink>

                                <div className={styles.text}>
                                    {dayMeetingsCount > 0
                                        ? `На сегодня у вас запланировано ${dayMeetingsCount} встречи`
                                        : "На сегодня встреч не запланировано"}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.title}>
                        Нужна онлайн-консультация со специалистом?
                    </div>
                    <div className={styles.search + ' ' + styles.blockOffset}>
                        <SearchGroup/>
                    </div>
                    <div className={styles.blockOffset}>
                        <MiniCategories limit={7} isLabel={true} isSmall={false} countElems={7}/>
                    </div>
                    <div className={styles.invite}>
                        <Invite/>
                    </div>
                </Container>
                <Container>
                    <div className={styles.block}>
                        <div className={styles.card} id={"meetings-wrapper"}>
                            <SegmentControl labels={["Запланированные", "Архив"]}
                                            setActive={setActive}
                                            active={active}
                            />
                            {Object.keys(meetingsObj).length > 0 ?
                                <div className={styles.meetings} id={"meetings-cards"}
                                     style={{maxHeight: meetingsCardsHeight}}>
                                    {Object.keys(meetingsObj).map((key, idx) => (
                                        <div className={styles.dayMeetings} key={idx}>
                                            <div className={styles.date}>
                                                {key}
                                            </div>
                                            {meetingsObj[key].map((item, idx) => (
                                                <Card key={idx}
                                                      isFinished={item.finished}
                                                      isClient={true}
                                                      description={item.description}
                                                      userId={item.specialist.user_id}
                                                      eventId={item._id.$oid}
                                                      isOnline={item.started}
                                                      isReviewed={item.review}
                                                      amount={item.price.format}
                                                      duration={item.duration}
                                                      firstName={item.specialist.first_name}
                                                      lastName={item.specialist.last_name}
                                                      timeStart={item.time_start}
                                                      timeEnd={item.time_end}
                                                      isConfirmed={item.confirmed}
                                                />
                                            ))}
                                        </div>
                                    ))}
                                </div> :
                                <div className={styles.message}>
                                    <img src="/icons/calendar/clock.svg" alt=""/>
                                    {active === 0
                                        ? "Нет запланированных встреч"
                                        : "Нет проведенных встреч"
                                    }
                                </div>}
                        </div>
                    </div>
                </Container>
            </Wrapper>
            <Footer/>
        </Page>
    )
}

const meetings = [
    {
        duration: "30 минут",
        client: "Павел Кальницкий",
        amount: "150.00 ₽",
        isOnline: true,
        time: "19:00—19:30",
        role: "UX дизайнер",
        date: "Сегодня"
    },
    {
        duration: "30 минут",
        client: "Анастасия Семенова",
        amount: "1500.00 ₽",
        isOnline: false,
        time: "19:30—20:00",
        role: "Копирайтер",
        date: "Сентябрь, 11, пятница"
    },
]
