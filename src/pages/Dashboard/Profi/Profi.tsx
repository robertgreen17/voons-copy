import React, {FC, useEffect, useState} from "react";
import {Page} from "../../../components/common/Page/Page";
import styles from "./styles.module.scss"
import Moment from "react-moment";
import {Wrapper} from "../../../components/Dashboard/Wrapper/Wrapper";
import {Container} from "../../../components/Dashboard/Container/Container";
import {Stats} from "../../../components/Dashboard/Profi/Stats/Stats";
import moment from "moment";
import {localeRu} from "../../../locales/momentLocales";
import {Meetings} from "../../../components/Dashboard/Profi/Meetings/Meetings";
import {useRedirect} from "../../../hooks/useRedirect";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {Footer} from "../../../components/common/Footer/Footer";
import {getUserData, UserTypeT} from "../../../redux/user-reducer/user-reducer";
import {NavLink, Route} from "react-router-dom";
import {CalendarPage} from "../../CalendarPage/CalendarPage";
import {getMeetings} from "../../../redux/events-reducer/events-reducer";
import {getDayMeetings, getMeetingsPrice} from "../../../utils/calendar";
import {formatMoney} from "../../../utils/string";
import {FirstInfoEntry} from "../../../components/Dashboard/Profi/FirstInfoEntry/FirstInfoEntry";

type PropsT = {}

export const Profi: FC<PropsT> = () => {

    moment.locale('ru', localeRu);

    const dispatch = useDispatch()

    const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
    const userType = useSelector((state: RootStateT) => state.user.type)
    const name = useSelector((state: RootStateT) => state.user.userData?.first_name)
    const first_enter = useSelector((state: RootStateT) => state.user.userData?.first_enter) || false;
    const avatar = useSelector((state: RootStateT) => state.user.userData?.avatar);
    const meetings = useSelector((state: RootStateT) => state.events.meetings) // планируемые встречи
    const [dayMeetingsCount, setDayMeetingsCount] = useState(getDayMeetings(new Date(), meetings).length)
    const [dayMeetingsPrice, setDayMeetingsPrice] = useState(getMeetingsPrice(getDayMeetings(new Date(), meetings)))
    const [settings, setSettings] = useState(<div></div>);
    useEffect(() => {
        dispatch(getMeetings({limit: 15}))
    }, [])

    useEffect(() => {
        let interval2 = setInterval(() => {
            dispatch(getUserData({}))
        }, 5000)
        return () => clearInterval(interval2)
    }, [])

    useEffect(() => {
        setDayMeetingsCount(getDayMeetings(new Date(), meetings).length)
        setDayMeetingsPrice(getMeetingsPrice(getDayMeetings(new Date(), meetings)))
    }, [meetings])

    useRedirect(!isAuth, "/main")
    useRedirect(userType === UserTypeT.client, "/dashboard/client")

    useEffect(() => {
        dispatch(getUserData({}))
    }, [])
    //
    // useEffect(() => {
    //     setTimeout(() => {
    //         let settings = <Wrapper type={"onBoard"}>
    //             <EditProfileOnBoard/>
    //             <EditSocialOnBoard/>
    //             <EditAccountOnBoard/>
    //             <EditPasswordOnBoard/>
    //             <EditNotifyOnBoard/>
    //             <EditWorkOnBoard/>
    //         </Wrapper>;
    //         setSettings(settings);
    //     }, 1000);
    //
    // },)

    return (
        <Page isNavbar={true} isHelp={true} isId={'profi'}>
            <Route path={"/dashboard/profi/calendar"} component={CalendarPage}/>
            <Wrapper>
                <Container>
                    <div className={styles.time}>
                        <Moment format="HH:mm" interval={1000}/> · <Moment format="MMMM, DD, dddd" locale={"ru"}/>
                    </div>
                    <div className={styles.greetings_wrapper}>
                        <div className={styles.container}>
                            <img
                                src={avatar ? avatar : "/icons/userAvatar.png"}
                                alt=""
                                className={styles.avatar}
                            />
                            <div>
                                <div className={styles.title}>
                                    Добрый день, {name}
                                </div>
                                <NavLink to={"/settings"}>
                                    <div className={styles.settingsButton}>Редактировать профиль</div>
                                </NavLink>
                                <div className={styles.text}>
                                    {dayMeetingsCount > 0
                                        ? `На сегодня у вас запланировано ${dayMeetingsCount} встречи, на которых вы можете заработать ${formatMoney(dayMeetingsPrice - (dayMeetingsPrice / 100 * 10))} ₽`
                                        : "На сегодня встреч не запланировано"}
                                </div>
                            </div>
                        </div>
                    </div>
                    <Stats/>
                </Container>
                <Container>
                    <Meetings meetings={meetings}/>
                </Container>
                <FirstInfoEntry isOpen={!first_enter}/>
            </Wrapper>
            {settings}
            <Footer/>
        </Page>
    )
}