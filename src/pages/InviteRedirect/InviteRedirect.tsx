import React, {FC, useEffect} from 'react';
import { useParams, Redirect } from 'react-router-dom';

type PropsT = {}

export const InviteRedirect: FC<PropsT> = ({}) => {

   // @ts-ignore
   const {id} = useParams()

   useEffect(() => {
      localStorage.setItem("ref", id)
   }, [])

   return <Redirect to={"/main/sign_in/reg"}/>
}