import React, {FC} from 'react';
import {useParams} from 'react-router-dom';
import {getFileMimeTypeByUrl} from "../../utils/files";
import styles from "./styles.module.scss"

type PropsT = {}

export const Document: FC<PropsT> = ({}) => {
   // @ts-ignore
   const {id, file} = useParams()

   return (
      <div className={styles.wrapper}>
         <embed src={`https://api.visterio.com/vcoons/files/${id}/${file}`} type={getFileMimeTypeByUrl(file)}/>
      </div>
   )
}