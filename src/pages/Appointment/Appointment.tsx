import React, {FC, useEffect, useState} from "react";
import {SideBar} from "../../components/common/SideBar/SideBar";
import {useHistory, useParams} from "react-router-dom";
import {Time} from "../../components/Appointment/Time/Time";
import {Success} from "../../components/Appointment/Success/Success";
import {Task} from "../../components/Appointment/Task/Task";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../redux/store";
import {getWeekInterval} from "../../utils/time";
import {getCurrentUserData} from "../../redux/search-reducer/search-reducer";
import {createEvent, getTimeTable} from "../../redux/events-reducer/events-reducer";
import {CreateEventReqBodyT} from "../../api/events-api";
import {showNotification} from "../../utils/notifications";
import {getAllowedTimeTable, THREE_HOURS_MS} from "../../utils/calendar";
import {PaymentsModal} from "../../components/common/Navbar/PaymentsModal/PaymentsModal";

export const Appointment: FC = () => {

   // @ts-ignore
   const {step, id} = useParams()

   const dispatch = useDispatch()
   const push = useHistory().push
   let userData = useSelector((state: RootStateT) => state.search.currentUserData)
   const isFreeMeeting = useSelector((state: RootStateT) => state.events.isFreeMeeting)
   const missingMoney = useSelector((state: RootStateT) => state.events.missingMoney)
   let freeTimes = useSelector((state: RootStateT) => state.events.timeTable) // расписание специалиста
   const [timeTable, setTimeTable] = useState(freeTimes)
   const [duration, setDuration] = useState(null as number | null) // продолжительность встречи
   const [activeTime, setActiveTime] = useState(null as null | number) // время планируемой встречи в секундах (timestamp php)
   const [timeInterval, setTimeInterval] = useState(getWeekInterval()) // текущая неделя
   const [isPayment, setIsPayment] = useState(false)

   useEffect(() => {
      setTimeTable(getAllowedTimeTable(freeTimes, duration))
   }, [duration, freeTimes])

   useEffect(() => { // получение данных о специалисте
      if (+id !== userData?.user_id) {
         dispatch(getCurrentUserData({user_ids: "" + id}))
      }
   }, [id, userData])

   useEffect(() => { // получение расписание на выбранную неделю
      if (userData?._id) {
         dispatch(getTimeTable(timeInterval[0], timeInterval[1], userData?._id)) // получаем расписание для выбранной недели
      }
   }, [timeInterval, userData])

   const onCreateEvent = (desc: string) => { // запись на консультацию
      if (activeTime && userData?._id && duration) {
         if (activeTime * 1000 - THREE_HOURS_MS < new Date().getTime()) {
            showNotification("Запись на время, которое уже прошло, недоступна", "error")
            return
         }
         const body: CreateEventReqBodyT = {
            time: duration,
            specialist_id: userData?._id,
            date: activeTime,
            description: desc,
            free: isFreeMeeting,
         }
         dispatch(createEvent(body,
            () => push(`/profi/${id}/appointment/3`),
            () => setIsPayment(true)
         ))
      }
   }

   return (
      <SideBar title={+step !== 3 ? "Запись на консультацию" : ""}
               onCloseCallBack={() => push(`/profi/${id}`)}
               size={"big"}
      >
         {+step === 1 && <Time
				isFreeMeeting={isFreeMeeting}
				userId={+id}
				duration={duration}
				setTimeInterval={setTimeInterval}
				setActiveTime={setActiveTime}
				setDuration={setDuration}
				activeTime={activeTime}
				timeInterval={timeInterval}
				timeTable={timeTable}
				times_price={userData?.times_price}
			/>}
         {+step === 2 && <Task
				onCreateEvent={onCreateEvent}
				userId={+id}
				duration={duration}
				times_price={userData?.times_price}
				time={activeTime}
				isFreeMeeting={isFreeMeeting}
			/>}
         {+step === 3 && <Success userId={+id}/>}
         {isPayment && <PaymentsModal isOpen={isPayment}
			                             amountValue={missingMoney}
			                             closeModal={() => setIsPayment(false)}/>}
      </SideBar>
   )
}