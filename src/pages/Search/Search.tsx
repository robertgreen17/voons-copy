import React, {FC, useEffect} from "react"
import {Categories} from "../../components/Search/Categories/Categories";
import {MiniCategories} from "../../components/Search/MiniCategories/MiniCategories";
import {SearchWrapper} from "../../components/Search/SeacrhWrapper/SearchWrapper";
import {useDispatch, useSelector} from "react-redux";
import search from "../../redux/search-reducer/search-reducer";
import {getCategoriesWithLimit} from "../../redux/selectors";
import {CategoriesDesctop} from "../../components/Search/CategoriesDesctop/CategoriesDesctop";
import {Page} from "../../components/common/Page/Page";
import {SearchBar} from "../../components/Search/SearchBar/SearchBar";
import styles from "../../components/Search/SeacrhWrapper/styles.module.scss";
import mainStyles from "../../pages/Main/styles.module.scss";
import {Footer} from "../../components/common/Footer/Footer";
import {SearchGroup} from "../../components/common/SearchGroup/SearchGroup";
import {ScrollCards} from "../../components/common/ScrollCards/ScrollCards";
import {SwiperSlide} from "swiper/react";
import {CategoryCard} from "../../components/common/CategoryCard/CategoryCard";

export const Search: FC = () => {

    const dispatch = useDispatch()
    const categories = useSelector(getCategoriesWithLimit(20))

    useEffect(() => {
        dispatch(search.actions.setCurrentSearchData({}))
        dispatch(search.actions.setSearchingData([]))
    }, [])

    return (
        <Page isNavbar={true} bg={"#F5F6FA"} navbarBg={"#FFF"} isId={'searching'}>
            <SearchBar/>
            <div className={styles.container}>
                <MiniCategories isLabel={true}/>
                {/*<Categories/>*/}
            </div>
            <div className={styles.hideDesctop}>
                {categories.length > 0 && <ScrollCards title={"Тысячи специалистов помогут вам"}
                                                       control={<SearchGroup/>}>
                    {categories.filter((item) => item.big != "").map((item, idx) => (
                        <SwiperSlide key={idx} className={mainStyles.specialistHelperCard}>
                            <CategoryCard title={item.title}
                                          label={item.name}
                                          desc={item.text}
                                          profCount={item.users_count}
                                          imgLink={item.big}
                                          key={idx}
                                          tags={item.tags}
                                          bg={"#F5F6FA"}
                            />
                        </SwiperSlide>
                    ))}
                </ScrollCards>}
            </div>
            <Footer/>
        </Page>
    )
}
