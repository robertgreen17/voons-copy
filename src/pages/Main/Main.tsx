import React, {FC, useEffect, useState} from "react"
import {Page} from "../../components/common/Page/Page";
import {ImageBlockHeader} from "../../components/Main/ImageBlockHeader/ImageBlockHeader";
import {ImageBlockFooter} from "../../components/Main/ImageBlockFooter/ImageBlockFooter";
import {ScrollCards} from "../../components/common/ScrollCards/ScrollCards";
import {UserMap} from "../../components/Main/UserMap/UserMap";
import {SpecialistCard} from "../../components/Main/Specialists/SpecialistCard/SpecialistCard";
import {ClientCard} from "../../components/common/ClientCard/ClientCard";
import {Questions} from "../../components/Main/Questions/Questions";
import {Footer} from "../../components/Main/Footer/Footer";
import {Route, useHistory} from "react-router-dom";
import {SignIn} from "../SignIn/SignIn";
import {SwiperSlide} from 'swiper/react';
import avatar from "../../media/icons/avatar.svg"
import {MiniCategories} from "../../components/Search/MiniCategories/MiniCategories";
import styles from "./styles.module.scss"
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../redux/store";
import {UserTypeT} from "../../redux/user-reducer/user-reducer";
import {SearchGroup} from "../../components/common/SearchGroup/SearchGroup";
import {CategoryCard} from "../../components/common/CategoryCard/CategoryCard";
import {getCategoriesWithLimit} from "../../redux/selectors";
import search, {find} from "../../redux/search-reducer/search-reducer";
import {USERS_LIMIT} from "../../redux/constants";
import {Specialists} from "../../components/Main/Specialists/Specialists";


export const Main: FC = () => {
    const userType = useSelector((state: RootStateT) => state.user.type)
    const categories = useSelector(getCategoriesWithLimit(20))
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(search.actions.setCurrentSearchData({}))
        dispatch(search.actions.setSearchingData([]))
    }, [])

    return (
        <Page isNavbar={true} isHelp={true} isId={'main'}>
            <Route path={"/main/sign_in"} component={SignIn}/>

            <ImageBlockHeader
                isUserPhoto={true}
                title={"Видео-консультации со специалистами"}
                text={"Получи консультацию специалиста не выходя из дома, в удобное тебе время или зарабатывай, консультируя клиентов со всего мира."}
                mainBtnText={"Забронировать встречу"}
                secondaryBtnText={"Я специалист"}
                mainBtnLink={"/searching"}
                secondaryBtnLink={userType === UserTypeT.profi ? "/dashboard/profi" : "/main/sign_in/reg/profi"}
                isNav={true}
            />

            <div className={styles.wrapper}>
                <div className={styles.header}>
                    <div className={styles.title}>
                        Категории
                    </div>
                    <SearchGroup/>
                </div>

                <MiniCategories isBlueCard={true}/>
            </div>
            <Specialists/>
            <UserMap/>

            {categories.length > 0 && <ScrollCards title={"Популярные кейсы для Voons встреч"}
                                                   control={<SearchGroup/>}>
                {categories.filter((item) => item.big != "").map((item, idx) => (
                    <SwiperSlide key={idx} className={styles.specialistHelperCard}>
                        <CategoryCard title={item.title}
                                      label={item.name}
                                      desc={item.text}
                                      profCount={item.users_count}
                                      imgLink={item.big}
                                      key={idx}
                                      width={item.width}
                                      height={item.height}
                                      tags={item.tags}
                                      bg={"#F5F6FA"}
                        />
                    </SwiperSlide>
                ))}
            </ScrollCards>}

            <Questions/>

            <ImageBlockFooter title={"Остались вопросы? Напишите нам!"}
                              text={""}
                              mainBtnText={"Написать на Email"} secondaryBtnText={"Позвонить"}
                              primaryBtnMod={"danger"}
                              mainBtnLink={"mailto:info@voons.ru"}
                              secondaryBtnLink={"mailto:info@voons.ru"}
            />
            <Footer/>
        </Page>
    )
}

const clientCards = [
    {
        name: "Ирина Орлова",
        desc: "Занятие по фитнесу прошло отлично. Буду рекомендовать знакомым. Договорились, что Александр напишет программу питания для снижения веса и продолжим онлайн занятия.",
        imgLink: "/images/reviews/IrinaOrlova.jpg",
        rating: 0
    },
    {
        name: "Владимир Усольцев",
        desc: "Петр качественно провел консультацию карьерного коучинга. Определил мои компетенции, и помог построить стратегию для новых карьерных вершин",
        imgLink: "/images/reviews/VladimirYsolcov.jpg",
        rating: 0
    },
    {
        name: "Ольга Алексеева",
        desc: "Ксения - прекрасный специалист. Она рассказала об основных техниках нанесения макияжа, подобрала тон для лица и оттенок помады",
        imgLink: "/images/reviews/OlgaAlekseevna.jpg",
        rating: 0
    },
    {
        name: "Наталья Ларина",
        desc: "Сергей помог написать маркетинговую стратегию для нашего бренда и грамотно определил целевую аудиторию проекта. Договорились на еженедельные разовые консультации.",
        imgLink: "/images/reviews/Elena.jpg",
        rating: 0
    },
]

const mapData = [
    {
        name: "Дед Мороз",
        desc: "Самые настоящие Дед Мороз и Снегурочка готовы поздравить каждого, кто хорошо(и не очень, потому что такой выдался год) себя вел...",
        imgLink: "https://api.voons.ru/photos/5fdb7c80cc2e8212756af704.jpg",
        rating: 5,
        reviewsCount: 0,
        id: 14,
        tags: ["Поздравления"]
    },
    {
        name: "Татьяна Миронова",
        desc: "Копирайтер. Консультирую, оказываю помощь в создании текстового контента.",
        imgLink: "https://api.voons.ru/photos/5fe0bddbe0777409857303fa.jpg",
        rating: 4.7,
        reviewsCount: 0,
        id: 36,
        tags: ["Маркетинг"]
    },
    {
        name: "Петр Липов",
        desc: "Помогаю строить осознанную карьеру Сертифицированный карьерный консультант, коуч ECF (Европейская федерация коучинга). 16 лет помогаю людям находить любимую работу...",
        imgLink: "https://api.voons.ru/photos/5fe1ec3f784948391b7313d6.jpg",
        rating: 4.9,
        reviewsCount: 0,
        id: 43,
        tags: ["Бизнес", "Образование"]
    },
    {
        name: "Александр Гутов",
        desc: "Сертифицированный тренер и диетолог. 6 лет опыта персональных тренировок. Составляю программы тренировок и питания. Онлайн ведение от 1 месяца...",
        imgLink: "https://api.voons.ru/photos/5fe1e585784948391b7313d1.jpg",
        rating: 4.8,
        reviewsCount: 0,
        id: 42,
        tags: ["Здоровье", "Красота"]
    },
]