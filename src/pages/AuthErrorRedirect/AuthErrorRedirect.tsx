import React, {FC, useEffect} from "react";
import {Redirect, useHistory} from "react-router-dom";
import {showNotification} from "../../utils/notifications";

type PropsT = {}

export const AuthErrorRedirect: FC<PropsT> = ({}) => {

   const location = useHistory().location
   const params = new URLSearchParams(location.search);

   useEffect(() => {
      const error = params.get("text")
      if (error) showNotification(error, "error")
   }, [params])

   return <Redirect to={"/main"}/>
}