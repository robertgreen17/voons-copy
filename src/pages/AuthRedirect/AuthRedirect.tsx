import React, {FC, useEffect, useState} from 'react';
import {Redirect, useHistory} from "react-router-dom";
import {setAccessToken} from "../../api/api";
import {useDispatch} from "react-redux";
import {refresh} from "../../redux/auth-reducer/auth-reducer";

type PropsT = {}

export const AuthRedirect: FC<PropsT> = ({}) => {

   const [isLoader, setIsLoader] = useState(true)
   const location = useHistory().location
   const params = new URLSearchParams(location.search);
   const dispatch = useDispatch()

   useEffect(() => {
      const access_token = params.get("access_token")
      const refresh_token = params.get("refresh_token")
      if (access_token && refresh_token) {
         setAccessToken(access_token)
         localStorage.setItem("refresh", refresh_token)
         dispatch(refresh(() => setIsLoader(false)))
      }
   }, [])

   if (isLoader) {
      return <></>
   } else {
      return <Redirect to={"/dashboard/client"}/>
   }
}