import React, {FC, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {ChangeSettingsReqBodyT} from "../../../../api/user-api";
import {changeSettings, getUserData} from "../../../../redux/user-reducer/user-reducer";
import {SettingsCard} from "../../SettingsCard/SettingsCard";
import {EditBlock} from "../../EditBlock/EditBlock";
import styles from "../../styles.module.scss";
import {Input} from "../../../common/Inputs/Input";
import {Button} from "../../../common/Button/Button";

type PropsT = {}

export type EditAccountValuesT = {
    email: string
    phone: string
}

export const EditAccountOnBoard: FC<PropsT> = ({children}) => {


    const dispatch = useDispatch()
    const userData = useSelector((state: RootStateT) => state.user.userData)

    const onSubmit = (values: EditAccountValuesT, {resetForm}: FormikValues) => {
        const payload: ChangeSettingsReqBodyT = {
            email: values.email,
            phone: values.phone,
        }
        dispatch(changeSettings(payload))
    }

    const initialValues: EditAccountValuesT = {
        email: userData?.email ? userData?.email : "",
        phone: userData?.phone ? userData?.phone : "",
    }

    useEffect(() => {
        dispatch(getUserData({}))
    }, [])

    return (
        <SettingsCard label={"Настройки аккаунта"} hideLabel wrapperType={"onBoard"} m={"20px"}>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
            >
                {({setFieldValue, values}) => (
                    <Form>
                        <EditBlock title={"Данные аккаунта"}
                                   label={"Для СМС информирования и доступа к аккаунту"}
                                   type = {"board"}>
                            <div className={styles.column_on_board}>
                                <Field name={"email"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            m={"0 0 20px"}
                                            type={"email"}
                                            label={"Email"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                                <Field name={"phone"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            type={"tel"}
                                            label={"Номер телефона"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                            </div>
                        </EditBlock>
                        <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
                    </Form>
                )}
            </Formik>
        </SettingsCard>
    )
}