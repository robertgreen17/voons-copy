import React, {FC} from "react";
import {SettingsCard} from "../SettingsCard/SettingsCard";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {Button} from "../../common/Button/Button";
import {EditBlock} from "../EditBlock/EditBlock";
import {Input, InputWithMask} from "../../common/Inputs/Input";
import styles from "../styles.module.scss"
import {ChangeSettingsReqBodyT} from "../../../api/user-api";
import {changeSettings} from "../../../redux/user-reducer/user-reducer";

type PropsT = {}

export type EditAccountValuesT = {
   email: string
   phone: string
}

export const EditAccount: FC<PropsT> = ({children}) => {


   const dispatch = useDispatch()
   const userData = useSelector((state: RootStateT) => state.user.userData)

   const onSubmit = (values: EditAccountValuesT, {resetForm}: FormikValues) => {
      const payload: ChangeSettingsReqBodyT = {
         email: values.email,
         phone: values.phone,
      }
      dispatch(changeSettings(payload))
   }

   const initialValues: EditAccountValuesT = {
      email: userData?.email ? userData?.email : "",
      phone: userData?.phone ? userData?.phone : "",
   }

   return (
      <SettingsCard label={"Настройки аккаунта"}>
         <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
         >
            {({setFieldValue, values}) => (
               <Form>
                  <EditBlock title={"Данные аккаунта"}
                             label={"Для СМС информирования и доступа к аккаунту"}>
                     <div className={styles.column}>
                        <Field name={"email"}>
                           {({
                                field,
                             }: FieldProps) => (
                              <Input
                                 m={"0 0 20px"}
                                 type={"email"}
                                 label={"Email"}
                                 {...field}
                              />
                           )}
                        </Field>
                        <Field name={"phone"}>
                           {({
                                field,
                             }: FieldProps) => (
                              <Input
                                 type={"tel"}
                                 label={"Номер телефона"}
                                 {...field}
                              />
                           )}
                        </Field>
                     </div>
                  </EditBlock>
                  <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
               </Form>
            )}
         </Formik>
      </SettingsCard>
   )
}