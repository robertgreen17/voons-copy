import React, {FC, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import user, {getDocs, switchFirstMeetingFree} from "../../../../redux/user-reducer/user-reducer";
import {SetTimesPriceReqBodyT} from "../../../../api/events-api";
import {setTimesPrice} from "../../../../redux/events-reducer/events-reducer";
import {SettingsCard} from "../../SettingsCard/SettingsCard";
import {EditBlock} from "../../EditBlock/EditBlock";
import styles from "../../styles.module.scss";
import {Toggler} from "../../../common/Toggler/Toggler";
import {Input} from "../../../common/Inputs/Input";
import {EditDocs} from "../../EditDocs/EditDocs";
import {Button} from "../../../common/Button/Button";
import {EditProfileValuesT} from "../../EditProfile/EditProfile";

type PropsT = {
    setFieldValue: (field: string, value: any) => void,
    isFreeMeeting: boolean
}

export type EditAccountValuesT = {
    15: string
    30: string
    60: string
    isFreeMeeting: boolean
}

export const EditWorkMini: FC<PropsT> = ({setFieldValue, isFreeMeeting}) => {

    const prices = useSelector((state: RootStateT) => state.user.userData?.times_price)
    const isFreeFirst = useSelector((state: RootStateT) => state.user.userData?.free_first)
    const [initialValues, setInitialValues] = useState(
        {
            15: prices ? prices[15] : 0,
            30: prices ? prices[30] : 0,
            60: prices ? prices[60] : 0,
            isFreeMeeting: isFreeFirst ? isFreeFirst : false,
        } as EditAccountValuesT
    )
    useEffect(() => {
        setInitialValues({
            15: prices ? prices[15] : "",
            30: prices ? prices[30] : "",
            60: prices ? prices[60] : "",
            isFreeMeeting: isFreeFirst ? isFreeFirst : false,
        })
    }, [prices])

    const validateForm = (value: string) => {
        let errorMessage;
        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            errorMessage = 'Invalid email address';
        }
        return errorMessage;
    };

    return (
        <Form>
            <div className={styles.title}>
                Стоимость консультации
            </div>
            <div className={styles.wrapper}>
                <div className={styles.price_container}>
                    <Field name={"15"}>
                        {({
                              field,
                          }: FieldProps) => (
                            <Input
                                required={false}
                                m={"0 0 20px"}
                                type={"text"}
                                label={"15 минут"}
                                {...field}
                            />
                        )}
                    </Field>
                </div>
                <div className={styles.price_container}>
                    <Field name={"30"}>
                        {({
                              field,
                          }: FieldProps) => (
                            <Input
                                required={false}
                                m={"0 0 20px"}
                                type={"text"}
                                label={"30 минут"}
                                {...field}
                            />
                        )}
                    </Field>
                </div>

                <div className={styles.price_container}>
                    <Field name={"60"}>
                        {({
                              field,
                          }: FieldProps) => (
                            <Input
                                required={false}
                                m={"0 0 20px"}
                                type={"text"}
                                label={"60 минут"}
                                {...field}
                            />
                        )}
                    </Field>
                </div>
            </div>
            <div className={styles.toogler_wrapper}>
                <div className={styles.container}>
                    <div className={styles.label_container}>
                        <div className={styles.label}>
                            Первая встреча бесплатно
                        </div>
                    </div>
                    <div className={styles.toogler_container}>
                        <Toggler onClick={() => setFieldValue("isFreeMeeting", !isFreeMeeting)}
                                 id={"free-3"}
                                 checked={isFreeMeeting}
                        />

                    </div>
                </div>
            </div>
        </Form>
    )
}