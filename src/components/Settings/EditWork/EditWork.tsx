import React, {FC, useEffect, useState} from "react";
import {SettingsCard} from "../SettingsCard/SettingsCard";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {Button} from "../../common/Button/Button";
import {EditBlock} from "../EditBlock/EditBlock";
import styles from "../styles.module.scss"
import {Input} from "../../common/Inputs/Input";
import "../../common/Button/styles.scss"
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import user, {getDocs, removeDocs, setDocs, switchFirstMeetingFree} from "../../../redux/user-reducer/user-reducer";
import {EditDocs} from "../EditDocs/EditDocs";
import {SetTimesPriceReqBodyT} from "../../../api/events-api";
import {setTimesPrice} from "../../../redux/events-reducer/events-reducer";
import {Toggler} from "../../common/Toggler/Toggler";
import {verifyCode} from "../../../redux/auth-reducer/auth-reducer";

type PropsT = {}

export type EditAccountValuesT = {
   15: string
   30: string
   60: string
   uploadDocs: Array<File> | null
   removeDocs: Array<string> | null
   isFreeMeeting: boolean
}

export const EditWork: FC<PropsT> = () => {

   const docs = useSelector((state: RootStateT) => state.user.docs)
   const prices = useSelector((state: RootStateT) => state.user.userData?.times_price)
   const isFreeFirst = useSelector((state: RootStateT) => state.user.userData?.free_first)
   const [initialValues, setInitialValues] = useState(
      {
         15: prices ? prices[15] : 0,
         30: prices ? prices[30] : 0,
         60: prices ? prices[60] : 0,
         uploadDocs: null,
         removeDocs: null,
         isFreeMeeting: isFreeFirst ? isFreeFirst : false,
      } as EditAccountValuesT
   )
   const dispatch = useDispatch()

   const onSubmit = (values: EditAccountValuesT, {resetForm}: FormikValues) => {
      if (values.isFreeMeeting !== initialValues.isFreeMeeting) {
         dispatch(switchFirstMeetingFree({free: values.isFreeMeeting}))
      }
      // if (values.uploadDocs) {
      //    dispatch(setDocs(values.uploadDocs))
      //    resetForm()
      // }
      // if (values.removeDocs) {
      //    dispatch(removeDocs(values.removeDocs))
      //    resetForm()
      // }
      const timesPrice: SetTimesPriceReqBodyT = {
         time: [15, 30, 60],
         price: [
             initialValues["15"] !== values["15"] ? values[15] : initialValues["15"],
             initialValues["30"] !== values["30"] ? values[30] : initialValues["30"],
             initialValues["60"] !== values["60"] ? values[60] : initialValues["60"],
         ],
      }
      dispatch(setTimesPrice(timesPrice))
   }

   useEffect(() => {
      dispatch(getDocs({}))
      return () => {
         dispatch(user.actions.setDocs(null))
      }
   }, [])

   useEffect(() => {
      setInitialValues({
         15: prices ? prices[15] : "",
         30: prices ? prices[30] : "",
         60: prices ? prices[60] : "",
         uploadDocs: null,
         removeDocs: null,
         isFreeMeeting: isFreeFirst ? isFreeFirst : false,
      })
   }, [prices])


   return (
      <SettingsCard label={"Рабочий профиль"}>
         <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
         >
            {({setFieldValue, values, resetForm}) => (
               <Form>
                  <EditBlock title={"Подарок для клиента"}
                             label={""}>
                     <div className={`${styles.row} ${styles.top}`}>
                        <Toggler onClick={() => setFieldValue("isFreeMeeting", !values.isFreeMeeting)}
                                 id={"free-1"}
                                 checked={values.isFreeMeeting}
                        />
                        <div className={styles.label}>
                           Первая встреча бесплатно
                        </div>
                     </div>
                  </EditBlock>
                  <EditBlock title={"Цены"}
                             label={"Цены на видео — консультацию в рублях"}>
                     <div className={styles.column}>
                        <Field name={"15"}>
                           {({
                                field,
                             }: FieldProps) => (
                              <Input
                                 required={false}
                                 m={"0 0 20px"}
                                 type={"text"}
                                 label={"15 минут"}
                                 {...field}
                              />
                           )}
                        </Field>
                        <Field name={"30"}>
                           {({
                                field,
                             }: FieldProps) => (
                              <Input
                                 required={false}
                                 m={"0 0 20px"}
                                 type={"text"}
                                 label={"30 минут"}
                                 {...field}
                              />
                           )}
                        </Field>
                        <Field name={"60"}>
                           {({
                                field,
                             }: FieldProps) => (
                              <Input
                                 required={false}
                                 m={"0 0 20px"}
                                 type={"text"}
                                 label={"60 минут"}
                                 {...field}
                              />
                           )}
                        </Field>
                     </div>
                  </EditBlock>
                  <EditDocs
                     resetForm={resetForm}
                     addUploadDoc={(doc) => {
                        // функция добавления документа в массив документов, которые хотим добавить
                        if (values.uploadDocs) {
                           setFieldValue("uploadDocs", [...values.uploadDocs, doc])
                        } else {
                           setFieldValue("uploadDocs", [doc])
                        }
                     }}
                     addRemoveDoc={(id) => {
                        // функция добавления документа в массив документов, которые хотим удалить
                        if (values.removeDocs) {
                           setFieldValue("removeDocs", [...values.removeDocs, id])
                        } else {
                           setFieldValue("removeDocs", [id])
                        }
                     }}
                     docs={docs}/>
                  <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
               </Form>
            )}
         </Formik>
      </SettingsCard>
   )
}