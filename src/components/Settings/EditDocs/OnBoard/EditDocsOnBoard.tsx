import {GetDocsResDataT} from "../../../../api/user-api";
import React, {FC, useRef} from "react";
import {useDispatch} from "react-redux";
import {showNotification} from "../../../../utils/notifications";
import {getDocs, removeDocs, setDocs} from "../../../../redux/user-reducer/user-reducer";
import {EditBlock} from "../../EditBlock/EditBlock";
import styles from "../../styles.module.scss";
import {Button} from "../../../common/Button/Button";

export type PropsT = {
    docs: GetDocsResDataT | null
    addUploadDoc: (doc: File) => void
    addRemoveDoc: (id: string) => void
    resetForm: () => void
}

export const EditDocsOnBoard: FC<PropsT> = ({
                                                docs,
                                                addUploadDoc,
                                                addRemoveDoc,
                                                resetForm,
                                            }) => {

    const dispatch = useDispatch()

    const inputFile = useRef<HTMLInputElement>(null)

    const onAddDoc = (event: React.ChangeEvent<HTMLInputElement>) => {
        // добавление документа
        const files = event.target.files
        if (files && files[0]) {
            // если есть добавленные файлы
            const filesize = +((files[0].size / 1024) / 1024).toFixed(4); // MB
            if (filesize > 5) {
                showNotification(
                    "Превышен максимальный размер загружаемого файла - 5Mb",
                    "error")
                return
            }

            dispatch(setDocs([files[0]],
                () => {
                    if (inputFile.current) {
                        inputFile.current.value = ""
                    }
                    resetForm()
                    dispatch(getDocs({}))

                }))

            // const newDoc: DocT = {
            //    // создаем новый документ
            //    moderation: false,
            //    path: URL.createObjectURL(files[0]),
            //    notLoaded: true, // помечаем, что он еще не загружен
            // }
            // const key = randomStringGenerator() // фейковый id
            // dispatch(user.actions.setDocs({...docs, [key]: newDoc}))
            // addUploadDoc(files[0]) // добавляем в массив файлов, которые хотим добавить
        }
    }

    const onRemoveDoc = (id: string) => {
        dispatch(removeDocs([id],
            () => {
                if (inputFile.current) {
                    inputFile.current.value = ""
                }
                resetForm()
                dispatch(getDocs({}))
            }))
        // if (docs && id in docs) {
        //    addRemoveDoc(id)
        //    dispatch(user.actions.setDocs({...deleteItemByKey(docs, id)}))
        // }
    }

    return (
        <EditBlock title={"Сертификаты"}
                   label={"Загрузите сертификаты удостоверяющие ваш профессиональный уровень"} type = {"board"}>
            <div className={styles.column}>
                {docs && <div className={styles.docs}>
                    {Object.keys(docs).map((key, idx) => (
                        <div className={styles.doc} key={idx}>
                            <div className={styles.dark}>
                                <div className={styles.label}>
                                    {/*{docs[key].path.slice(-10)}*/}
                                    {docs[key].moderation && !docs[key].notLoaded &&
                                    <img src="/icons/settings/clock.svg" alt="" className={styles.icon}/>}
                                    {!docs[key].moderation && !docs[key].notLoaded && "Проверяется"}
                                    {docs[key].moderation && !docs[key].notLoaded && "Одобрено"}
                                    {docs[key].notLoaded && "Не загружено"}
                                    {<a href={docs[key].path} target="_blank" rel="noopener noreferrer"
                                        className={styles.link}>
                                        Открыть
                                    </a>}
                                </div>
                                {!docs[key].notLoaded && <svg className={styles.cross}
                                                              onClick={() => onRemoveDoc(key)}
                                                              width="10" height="10"
                                                              viewBox="0 0 20 20" fill="none"
                                                              xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M19.3332 2.54666L17.4532 0.666656L9.99984 8.11999L2.5465 0.666656L0.666504 2.54666L8.11984 9.99999L0.666504 17.4533L2.5465 19.3333L9.99984 11.88L17.4532 19.3333L19.3332 17.4533L11.8798 9.99999L19.3332 2.54666Z"
                                        fill="#FFFD"/>
                                </svg>}
                            </div>
                            {/*<embed className={styles.objectFit} type={getFileMimeTypeByUrl(docs[key].path)}*/}
                            {/*       src={docs[key].path}/>*/}
                            <img alt={"File"} src={docs[key].path} className={styles.objectFit}/>
                        </div>
                    ))}
                </div>}
                <div className={styles.text}>
                    Загрузите файл с вашего устройства. Вам понадобится квадратное изображение размером не менее
                    180 на 140 пикселей.
                </div>
                <div className={styles.uploader}>
                    <Button mod={"secondary"}
                            size={"medium"}
                            iconPos={"icon-right"}
                            m={"25px 0 0"}>
                        <label htmlFor="input-file" className={styles.label}>
                            <input
                                ref={inputFile}
                                onChange={onAddDoc}
                                id="input-file"
                                accept={"image/*"}
                                className={styles.input}
                                type={"file"}
                            />
                        </label>
                        Загрузить скан
                        <img src="/icons/settings/upload.svg" alt=""/>
                    </Button>
                </div>
            </div>
        </EditBlock>

    )
}