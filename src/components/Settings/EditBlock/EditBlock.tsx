import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"

type PropsT = {
    type?: "mini" | "full" | "board"
    title: string
    label: string
}

export const EditBlock: FC<PropsT> = ({
                                          children,
                                          label,
                                          title,
                                          type = "full",
                                      }) => {
    if (type === "full") {
        return (
            <div className={styles.wrapper}>
                <div className={styles.container}>
                    <div className={styles.desc}>
                        <div className={styles.title}>
                            {title}
                        </div>
                        <div className={styles.label}>
                            {label}
                        </div>
                    </div>
                    {children}
                </div>
            </div>
        )
    }
    if (type === "mini") {
        return (
            <div className={styles.wrapper}>
                <div className={styles.container_mini}>
                    <div className={styles.desc_mini}>
                        <div className={styles.title}>
                            {title}
                        </div>
                    </div>
                    {children}
                </div>
            </div>
        )
    }
    if (type === "board") {
        return (
            <div className={styles.wrapper}>
                <div className={styles.container_mini}>
                    <div className={styles.desc_on_board}>
                        <div className={styles.title}>
                            {title}
                        </div>
                        <div className={styles.label}>
                            {label}
                        </div>
                    </div>
                    {children}
                </div>
            </div>
        )
    }
    return (<p>ok</p>);

}