import React, {FC, useEffect, useState} from "react";
import {SettingsCard} from "../SettingsCard/SettingsCard";
import {EditAvatar} from "../EditAvatar/EditAvatar";
import {EditMainInfo} from "../EditMainInfo/EditMainInfo";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {changeAccount, setAvatar, UserTypeT} from "../../../redux/user-reducer/user-reducer";
import {EditWorkInfo} from "../EditWorkInfo/EditWorkInfo";
import {Form, Formik, FormikHelpers} from "formik";
import {Separator} from "../../common/Separator/Separator";
import {Button} from "../../common/Button/Button";
import {ChangeAccountReqBodyT} from "../../../api/user-api";
import {isEqualObjProps} from "../../../utils/objects";

type PropsT = {}

export type EditProfileValuesT = {
    image: string
    name: string
    surname: string
    country: string
    // city: string
    tags: Array<string>
    langs: Array<string>
    services: Array<string>
    about: string
    newTag: string
    newLang: string
    newService: string
    // uploadImage: null | File ,
}

export const EditProfile: FC<PropsT> = () => {

    const userType = useSelector((state: RootStateT) => state.user.type)
    const userData = useSelector((state: RootStateT) => state.user.userData)
    const dispatch = useDispatch()

    const onSubmit = (values: EditProfileValuesT, {resetForm}: FormikHelpers<EditProfileValuesT>) => {
        // if (values.uploadImage === null && initialValues.image !== "") {
        //    dispatch(setAvatar(values.uploadImage))
        // }
        // if (values.uploadImage && values.uploadImage !== initialValues.uploadImage) {
        //    debugger
        //    // если есть изображение и оно не равно старому, то делаем dispatch
        //    dispatch(setAvatar(values.uploadImage))
        // }

        if (!isEqualObjProps(values, initialValues, ["country", "name", "surname", "langs", "about", "tags"])) {
            const payload: ChangeAccountReqBodyT = {
                country: values.country,
                first_name: values.name,
                last_name: values.surname,
                lang: values.langs,
                tags: values.tags,
                services: values.services
            }
            if (values.about.length > 0) payload["work_info"] = values.about
            dispatch(changeAccount(payload))
        }
    }

    let initialValues: EditProfileValuesT = {
        image: userData?.avatar ? userData?.avatar : "/icons/userAvatar.png",
        // uploadImage: null,
        country: userData?.country ? userData?.country : "",
        name: userData?.first_name ? userData?.first_name : "",
        surname: userData?.last_name ? userData?.last_name : "",
        tags: userData?.tags ? userData?.tags.map(t => typeof t === "string" || !t ? t : t.name_ru) : [],
        langs: userData?.lang ? userData?.lang : [],
        services: userData?.services ? userData?.services : [],
        about: userData?.work_info ? userData?.work_info : "",
        newTag: "",
        newLang: "",
        newService: "",
    }

    useEffect(() => {
        initialValues = {
            image: userData?.avatar ? userData?.avatar : "/icons/userAvatar.png",
            // uploadImage: null,
            country: userData?.country ? userData?.country : "",
            name: userData?.first_name ? userData?.first_name : "",
            surname: userData?.last_name ? userData?.last_name : "",
            tags: userData?.tags ? userData?.tags.map(t => typeof t === "string" || !t ? t : t.name_ru) : [],
            langs: userData?.lang ? userData?.lang : [],
            services: userData?.services ? userData?.services : [],
            about: userData?.work_info ? userData?.work_info : "",
            newTag: "",
            newLang: "",
            newService: "",
        }
    }, [userData])

    return (
        <SettingsCard label={"Редактировать профиль"}>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
            >
                {({setFieldValue, values}) => (
                    <Form>
                        <EditAvatar image={values.image}
                                    setImage={(image: string) => setFieldValue("image", image)}
                                    setUploadImage={(image: File | null) => setFieldValue("uploadImage", image)}
                        />
                        <Separator m={"0 0 39px"}/>
                        <EditMainInfo/>
                        <Separator m={"0 0 39px"}/>
                        {userType === UserTypeT.profi &&
                        <EditWorkInfo
                            setFieldValue={setFieldValue}
                            tags={values.tags}
                            langs={values.langs}
                            services={values.services}
                            newTag={values.newTag}
                            newLang={values.newLang}
                            newService={values.newService}
                        />}
                        <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
                    </Form>
                )}
            </Formik>
        </SettingsCard>
    )
}