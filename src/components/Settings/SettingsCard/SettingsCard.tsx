import React, {FC} from "react";
import styles from "./styles.module.scss"
import {Button} from "../../common/Button/Button";
import {isMobile} from "react-device-detect";

type PropsT = {
    label: string,
    wrapperType?: string
    hideLabel?: true
    m?: string
}

export const SettingsCard: FC<PropsT> = ({
                                             children,
                                             label,
                                             wrapperType,
                                             hideLabel,
                                             m
                                         }) => {

    if (isMobile) {
        const elem = document.getElementById('settingsCard')
        if (elem) elem.scrollIntoView({behavior: "smooth"})
    }

    if (wrapperType === undefined) {
        return (
            <div className={styles.wrapper} style={{margin: m}} id="settingsCard">
                {hideLabel === undefined && <div className={styles.label}>
                    {label}
                </div>}
                {children}
            </div>
        )
    }

    if (wrapperType === "onBoard") {
        return (
            <div className={styles.wrapperOnBoard} style={{margin: m}} id="settingsCard">
                {hideLabel === undefined && <div className={styles.label}>
                    {label}
                </div>}
                {children}
            </div>
        )
    }

    return (<p>ok</p>)
}