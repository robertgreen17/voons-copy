import React, {FC} from "react";
import styles from "./styles.module.scss"
import {Nav} from "../Nav/Nav";

type PropsT = {}

export const Wrapper: FC<PropsT> = ({children}) => {

   return (
      <div className={styles.wrapper}>
         <div className={styles.container}>
            <div className={styles.title}>
               Настройки
            </div>
            <Nav/>
         </div>
         {children}
      </div>
   )
}