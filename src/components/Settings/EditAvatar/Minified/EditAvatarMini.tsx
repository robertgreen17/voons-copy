import {EditBlock} from "../../EditBlock/EditBlock";
import styles from "../styles.module.scss";
import {Button} from "../../../common/Button/Button";
import {setAvatar} from "../../../../redux/user-reducer/user-reducer";
import React, {FC} from "react";
import {showNotification} from "../../../../utils/notifications";
import {useDispatch} from "react-redux";

type PropsT = {
    image?: string
    setImage: (image: string) => void
    setUploadImage: (image: File | null) => void
}

export const EditAvatarMini: FC<PropsT> = ({image, setImage, setUploadImage}) => {

    const onChangeImage = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files && event.target.files[0]) {
            // setUploadImage(event.target.files[0])
            const filesize = +((event.target.files[0].size / 1024) / 1024).toFixed(4); // MB
            if (filesize > 5) {
                showNotification(
                    "Превышен максимальный размер загружаемого файла - 5Mb",
                    "error")
            } else {
                setImage(URL.createObjectURL(event.target.files[0]))
                dispatch(setAvatar(event.target.files[0]))
            }
        }
    }
    const dispatch = useDispatch()

    return (
        <EditBlock label={""}
                   title={"Фотография"}
                   type={"mini"}>
            <div className={styles.wrapper_mini}>
                <img
                    src={image ? image : "/icons/userAvatar.png"}
                    alt="" className={styles.avatar}/>
                <div className={styles.action}>
                    <div className={styles.label}>
                        Загрузите файл с вашего устройства. Вам потребуется квадратное изображение размером не менее
                        184
                        на
                        184 пикселя.
                    </div>
                    <div className={styles.row}>
                        <div className={styles.uploader}>
                            <Button mod={"secondary"}
                                    m={"0 10px 0 0"}
                                    p={"0.6rem 1rem 0.6rem"}
                                    size={"large"}
                                    fWeight={"normal"}
                            >
                                <label htmlFor="input-file" className={styles.label}>
                                    <input
                                        onChange={onChangeImage}
                                        accept=".png, .jpg,"
                                        id="input-file"
                                        className={styles.input}
                                        type={"file"}
                                    />
                                </label>
                                Обновить фотографию
                            </Button>
                        </div>
                        {/*<Button mod={"secondary"} m={"0 10px 0 0"} size={"medium"}>*/}
                        {/*   Обновить фотографию*/}
                        {/*</Button>*/}
                        <Button mod={"danger-light"} size={"large"} fWeight={"normal"}
                                p={"0.6rem 1rem 0.6rem"}
                                onClick={() => {
                                    dispatch(setAvatar(null, () => setImage("")))
                                }}>
                            Удалить
                        </Button>
                    </div>
                </div>
            </div>
        </EditBlock>
    )
}