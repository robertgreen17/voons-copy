import React, {FC} from "react";
import styles from "./styles.module.scss"
import {Button} from "../../common/Button/Button";
import {EditBlock} from "../EditBlock/EditBlock";
import {useDispatch} from "react-redux";
import {setAvatar} from "../../../redux/user-reducer/user-reducer";
import {showNotification} from "../../../utils/notifications";

type PropsT = {
    image?: string
    setImage: (image: string) => void
    setUploadImage: (image: File | null) => void
}

export const EditAvatar: FC<PropsT> = ({image, setImage, setUploadImage}) => {

    const onChangeImage = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files && event.target.files[0]) {
            // setUploadImage(event.target.files[0])
            const filesize = +((event.target.files[0].size / 1024) / 1024).toFixed(4); // MB
            if (filesize > 5) {
                showNotification(
                    "Превышен максимальный размер загружаемого файла - 5Mb",
                    "error")
            } else {
                setImage(URL.createObjectURL(event.target.files[0]))
                dispatch(setAvatar(event.target.files[0]))
            }
        }
    }
    const dispatch = useDispatch()

   return (
      <EditBlock label={""}
                 title={"Фотография"}
      >
         <div className={styles.actionWrapper}>
            <img
               src={image ? image : "/icons/userAvatar.png"}
                 alt="" className={styles.avatar}/>
            <div className={styles.action}>
               <div className={styles.label}>
                  Загрузите файл с вашего устройства. Вам потребуется квадратное изображение размером не менее 184 на
                  184 пикселя. Максимальный размер фотографии: <span className={styles.colorRed}>5мб</span>
               </div>
               <div className={styles.row}>
                  <div className={styles.uploader}>
                     <Button mod={"secondary"}
                             m={"0 10px 0 0"}
                             size={"medium"}
                     >
                        <label htmlFor="input-file" className={styles.label}>
                           <input
                              onChange={onChangeImage}
                              accept=".png, .jpg,"
                              id="input-file"
                              className={styles.input}
                              type={"file"}
                           />
                        </label>
                        Обновить фотографию
                     </Button>
                  </div>
                  {/*<Button mod={"secondary"} m={"0 10px 0 0"} size={"medium"}>*/}
                  {/*   Обновить фотографию*/}
                  {/*</Button>*/}
                  <Button mod={"danger"} size={"medium"} onClick={() => {
                     dispatch(setAvatar(null, () => setImage("")))
                  }}>
                     Удалить
                  </Button>
               </div>
            </div>
         </div>

      </EditBlock>

   )
}