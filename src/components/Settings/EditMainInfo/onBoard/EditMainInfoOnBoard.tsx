import React, {FC} from "react";
import styles from "../styles.module.scss";
import {EditBlock} from "../../EditBlock/EditBlock";
import {Field, FieldProps} from "formik";
import {Input} from "../../../common/Inputs/Input";

type PropsT = {}

export const EditMainInfoOnBoard: FC<PropsT> = ({children}) => {

    return (
        <div className={styles.wrapperOnBoard}>
            <EditBlock label={""}
                       title={"Основная информация"}
                       type = {"board"}
            >
                <div className={styles.grid_on_board}>
                    <Field
                        name={"name"}
                    >
                        {({
                              field,
                              form: {touched, errors}
                          }: FieldProps) => (
                            <Input
                                type={"text"}
                                label={"Ваше имя"}
                                className={styles.input_on_board}
                                mod={errors.name && touched.name ? "error" : undefined}
                                {...field}
                            />
                        )}
                    </Field>
                    <Field name={"surname"}>
                        {({
                              field,
                              form: {touched, errors}
                          }: FieldProps) => (
                            <Input
                                type={"text"}
                                label={"Фамилия"}
                                className={styles.input_on_board}
                                mod={errors.surname && touched.surname ? "error" : undefined}
                                {...field}
                            />
                        )}
                    </Field>
                    <Field name={"country"}>
                        {({
                              field,
                              form: {touched, errors}
                          }: FieldProps) => (
                            <Input
                                type={"text"}
                                label={"Страна"}
                                className={styles.input_on_board}
                                mod={errors.country && touched.country ? "error" : undefined}
                                {...field}
                            />
                        )}
                    </Field>
                    {/*<Field name={"city"}>*/}
                    {/*   {({*/}
                    {/*        field,*/}
                    {/*        form: {touched, errors}*/}
                    {/*     }: FieldProps) => (*/}
                    {/*      <Input*/}
                    {/*         type={"text"}*/}
                    {/*         label={"Город"}*/}
                    {/*         mod={errors.city && touched.city ? "error" : undefined}*/}
                    {/*         {...field}*/}
                    {/*      />*/}
                    {/*   )}*/}
                    {/*</Field>*/}
                </div>
            </EditBlock>
        </div>
    )
}