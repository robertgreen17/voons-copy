
import React, {FC} from "react";
import styles from "./styles.module.scss"
import {useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {UserTypeT} from "../../../redux/user-reducer/user-reducer";
import {NavLink, useHistory} from "react-router-dom";

type PropsT = {}

export const Nav: FC<PropsT> = ({children}) => {

   const userType = useSelector((state: RootStateT) => state.user.type)
   const name = useSelector((state: RootStateT) => state.user.userData?.first_name)
   const surname = useSelector((state: RootStateT) => state.user.userData?.last_name)
   const avatar = useSelector((state: RootStateT) => state.user.userData?.avatar)
   const pathname = useHistory().location.pathname

   return (
      <nav className={styles.nav}>
         <div className={styles.user}>
            <img
               src={avatar ? avatar : "/icons/userAvatar.png"}
               alt=""/>
            <div>
               <div className={styles.name}>
                  {name} {surname}
               </div>
               <NavLink to={"/settings"}>
                  <div className={styles.edit}>
                     Редактировать профиль
                  </div>
               </NavLink>
            </div>
         </div>
         <div className={styles.navItem}>
            {navLinks.map((item, idx) => (
               <NavLink to={item.link} className={`${styles.navLink} ${pathname === item.link && styles.active}`} key={idx}>
                  <img src={item.imgLink} alt=""/>
                  <div className={styles.label}>
                     {item.label}
                  </div>
               </NavLink>
            ))}
         </div>
         {userType === UserTypeT.profi &&
         <NavLink
            to={"/settings/work"}
            className={`${styles.navItem} ${styles.navLink} ${pathname === "/settings/work" && styles.active}`}>
            <img src="/icons/settings/work.svg" alt=""/>
            <div className={styles.label}>
               Рабочий профиль
            </div>
         </NavLink>}
         <NavLink to={"/settings/notify"}
                  className={`${styles.navItem} ${styles.navLink} ${pathname === "/settings/notify" && styles.active}`}>
            <img src="/icons/settings/notify.svg" alt=""/>
            <div className={styles.label}>
               Настройки уведомлений
            </div>
         </NavLink>
      </nav>
   )
}



const navLinks = [
   {
      label: "Социальные сети",
      imgLink: "/icons/settings/social.svg",
      link: "/settings/social",
   },
   {
      label: "Настройки аккаунта",
      imgLink: "/icons/settings/account.svg",
      link: "/settings/account",
   },
   {
      label: "Сменить пароль",
      imgLink: "/icons/settings/password.svg",
      link: "/settings/password",
   },
]
const navLinksProfi = [
   {
      label: "Социальные сети",
      imgLink: "/icons/settings/social.svg",
      link: "/settings/social",
   },
   {
      label: "Настройки аккаунта",
      imgLink: "/icons/settings/account.svg",
      link: "/settings/account",
   },
   {
      label: "Сменить пароль",
      imgLink: "/icons/settings/password.svg",
      link: "/settings/password",
   },
   {
      label: "Рабочий профиль",
      imgLink: "/icons/settings/work.svg",
      link: "/settings/work",
   },
   {
      label: "Настройки уведомлений",
      imgLink: "/icons/settings/notify.svg",
      link: "/settings/notify",
   },
]