import {EditProfileValuesT} from "../../EditProfile/EditProfile";
import React, {FC, useState} from "react";
import {EditBlock} from "../../EditBlock/EditBlock";
import styles from "../styles.module.scss";
import {Field, FieldProps} from "formik";
import {TextArea} from "../../../common/Inputs/Input";
import {ToolTip} from "../../../common/ToolTip/ToolTip";
import {InputTag, PlusBtn, Tag} from "../../../common/Tag/Tag";
import {filterBySubstr, toProperName} from "../../../../utils/string";
import ReactTooltip from "react-tooltip";

type PropsT = {
    tags: Array<string>
    langs: Array<string>
    newTag: string
    setFieldValue: (field: keyof EditProfileValuesT, value: any) => void
    newLang: string
}

const tagsData = ["Красота", "Образование", "Здоровье", "Юриспруденция", "Финансы", "IT",
    "Маркетинг", "Бизнес", "Поздравления", "Астрология", "Психология", "Телемедицина"]
const langsData = ["Английский", "Испанский", "Итальянский", "Китайский", "Немецкий", "Русский", "Французский", "Японский"]

export const EditWorkInfoMini: FC<PropsT> = ({
                                             langs,
                                             tags,
                                             newTag,
                                             setFieldValue,
                                             newLang,
                                         }) => {

    const [addTag, setAddTag] = useState(false)
    const [addLang, setAddLang] = useState(false)

    return (
        <EditBlock label={""}
                   title={"Рабочая информация"}
                   type = {"mini"}
        >
            <div className={styles.form}>
                <Field name={"about"}>
                    {({
                          field,
                      }: FieldProps) => (
                        <TextArea
                            rows = {1}
                            type={"text"}
                            placeholder={"Обо мне"}
                            {...field}
                        />
                    )}
                </Field>
                <div className={styles.container}>
                    <div className={styles.label}>
                        Теги и категории
                        <i data-tip="Введите свой хештег или выберите из предложеннных" className={"fas fa-info-circle"}/>
                        <ReactTooltip place="right" type="dark" effect="solid"/>
                    </div>


                    <div className={styles.row}>
                        {tags.map((item, idx) => (
                            <Tag icon={"closable"}
                                 mod={"secondary"}
                                 size={"medium"}
                                 m={"0 10px 15px 0"}
                                 onClose={() => setFieldValue("tags", tags.filter(tag => tag !== item))}
                                 key={idx}>
                                {item}
                            </Tag>
                        ))}
                        {!addTag && <PlusBtn onClick={() => setAddTag(true)} m={"0 0 15px"}/>}
                        {addTag &&
                        <Field name={"newTag"}>
                            {({
                                  field,
                              }: FieldProps) => (
                                <InputTag m={"0 0 15px"}
                                          options={filterBySubstr(tagsData, field.value)}
                                          placeholder={"Выберите тег"}
                                          autoFocus={true}
                                          added={tags}
                                          disabled={false}
                                          setValue={(value: string) => setFieldValue("newTag", value)}
                                          onPlusClick={(value?: string) => {
                                              if (value || newTag.length > 0) {
                                                  if (tags.indexOf(value ? value : newTag) == -1)
                                                      setFieldValue("tags", [...tags, toProperName(value ? value : newTag)])
                                              }
                                              setFieldValue("newTag", "")
                                              setAddTag(false)
                                          }}
                                          {...field}
                                />
                            )}
                        </Field>
                        }
                    </div>
                </div>
                <div className={styles.container}>
                    <div className={styles.label}>
                        Я говорю на
                    </div>
                    <div className={styles.row}>
                        {langs.map((item, idx) => (
                            <Tag icon={"closable"}
                                 mod={"secondary"}
                                 size={"medium"}
                                 m={"0 10px 15px 0"}
                                 onClose={() => setFieldValue("langs", langs.filter(language => language !== item))}
                                 key={idx}>
                                {item}
                            </Tag>
                        ))}

                        {!addLang && <PlusBtn onClick={() => setAddLang(true)} m={"0 0 15px"}/>}
                        {addLang &&
                        <Field name={"newLang"}>
                            {({
                                  field,
                              }: FieldProps) => (
                                <InputTag m={"0 0 15px"}
                                          placeholder={"Выберите язык"}
                                          autoFocus={true}
                                          added={langs}
                                          disabled={true}
                                          options={filterBySubstr(langsData, field.value)}
                                          setValue={(value: string) => setFieldValue("newLang", value)}
                                          onPlusClick={(value?: string) => {
                                              if (value || newLang.length > 0) {
                                                  if (langs.indexOf(value ? value.toLowerCase() : newLang.toLowerCase()) == -1)
                                                      setFieldValue("langs", [...langs, toProperName(value ? value : newLang)])
                                              }
                                              setFieldValue("newLang", "")
                                              setAddLang(false)
                                          }}
                                          {...field}
                                />
                            )}
                        </Field>
                        }
                    </div>
                </div>
            </div>
        </EditBlock>

    )
}