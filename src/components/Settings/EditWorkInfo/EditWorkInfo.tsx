import React, {FC, useState} from "react";
import styles from "./styles.module.scss"
import {EditBlock} from "../EditBlock/EditBlock";
import {Field, FieldProps} from "formik";
import {TextArea} from "../../common/Inputs/Input";
import {InputTag, PlusBtn, Tag} from "../../common/Tag/Tag";
import {EditProfileValuesT} from "../EditProfile/EditProfile";
import {filterBySubstr, toProperName} from "../../../utils/string";
import {ToolTip} from "../../common/ToolTip/ToolTip";
import {ToolTipSettings} from "../../common/ToolTip/Settings/ToolTipSettings";
import ReactTooltip from 'react-tooltip';

type PropsT = {
    tags: Array<string>
    langs: Array<string>
    services: Array<string>
    newTag: string
    newLang: string
    newService: string
    setFieldValue: (field: keyof EditProfileValuesT, value: any) => void
}

const tagsData = ["Красота", "Образование", "Здоровье", "Юриспруденция", "Финансы", "IT",
    "Маркетинг", "Бизнес", "Поздравления", "Астрология", "Психология", "Телемедицина"]
const langsData = ["Английский", "Испанский", "Итальянский", "Китайский", "Немецкий", "Русский", "Французский", "Японский"]
const servicesData = [""];

export const EditWorkInfo: FC<PropsT> = ({
                                             langs,
                                             tags,
                                             services,
                                             newTag,
                                             newLang,
                                             newService,
                                             setFieldValue,
                                         }) => {

    const [addTag, setAddTag] = useState(false)
    const [addLang, setAddLang] = useState(false)
    const [addService, setAddService] = useState(false)

    return (
        <EditBlock label={"Профессиональная информация обо мне"}
                   title={"Рабочая информация"}
        >
            <div className={styles.form}>
                <Field name={"about"}>
                    {({
                          field,
                      }: FieldProps) => (
                        <TextArea
                            type={"text"}
                            placeholder={"Обо мне"}
                            {...field}
                        />
                    )}
                </Field>
                <div className={styles.container}>
                    <div className={styles.label}>
                        Теги и категории
                        <i data-tip="Введите свой хештег или выберите из предложеннных" data-for={"tags"} className={"fas fa-info-circle"}/>
                        <ReactTooltip place="top" type="dark" id={"tags"} effect="float"/>
                    </div>


                    <div className={styles.row}>
                        {tags.map((item, idx) => (
                            <Tag icon={"closable"}
                                 mod={"secondary"}
                                 size={"medium"}
                                 m={"0 10px 15px 0"}
                                 onClose={() => setFieldValue("tags", tags.filter(tag => tag !== item))}
                                 key={idx}>
                                {item}
                            </Tag>
                        ))}
                        {!addTag && <PlusBtn onClick={() => setAddTag(true)} m={"0 0 15px"}/>}
                        {addTag &&
                        <Field name={"newTag"}>
                            {({
                                  field,
                              }: FieldProps) => (
                                <InputTag m={"0 0 15px"}
                                          options={filterBySubstr(tagsData, field.value)}
                                          added={tags}
                                          placeholder={"Выберите тег"}
                                          autoFocus={true}
                                          disabled={false}
                                          setValue={(value: string) => setFieldValue("newTag", value)}
                                          onPlusClick={(value?: string) => {
                                              if (value || newTag.length > 0) {
                                                  if (tags.indexOf(value ? value : newTag) == -1)
                                                      setFieldValue("tags", [...tags, toProperName(value ? value : newTag)])
                                              }
                                              setFieldValue("newTag", "")
                                              setAddTag(false)
                                          }}
                                          {...field}

                                />
                            )}
                        </Field>
                        }
                    </div>
                </div>


                <div className={styles.container}>
                    <div className={styles.label}>
                        Услуги
                        <i data-tip="Добавьте услуги, которые вы предоставляете" data-for={"services"} className={"fas fa-info-circle"}/>
                        <ReactTooltip place="top" type="dark" id={"services"} effect="float"/>
                    </div>

                    <div className={styles.row}>
                        {services.map((item, idx) => (
                            <Tag icon={"closable"}
                                 mod={"secondary"}
                                 size={"medium"}
                                 m={"0 10px 15px 0"}
                                 onClose={() => setFieldValue("services", services.filter(services => services !== item))}
                                 key={idx}>
                                {item}
                            </Tag>
                        ))}

                        {!addService && <PlusBtn onClick={() => setAddService(true)} m={"0 0 15px"}/>}
                        {addService &&
                        <Field name={"newService"}>
                            {({
                                  field,
                              }: FieldProps) => (
                                <InputTag m={"0 0 15px"}
                                          placeholder={"Напишите услугу"}
                                          autoFocus={true}
                                          disabled={false}
                                          options={filterBySubstr(servicesData, field.value)}
                                          setValue={(value: string) => setFieldValue("newService", value)}
                                          onPlusClick={(value?: string) => {
                                              if (value || newService.length > 0) {
                                                  if (services.indexOf(value ? value : newService) == -1)
                                                      setFieldValue("services", [...services, toProperName(value ? value : newService)])
                                              }
                                              setFieldValue("newService", "")
                                              setAddService(false)
                                          }}
                                          {...field}
                                />
                            )}
                        </Field>
                        }
                    </div>
                </div>


                <div className={styles.container}>
                    <div className={styles.label}>
                        Я говорю на
                    </div>
                    <div className={styles.row}>
                        {langs.map((item, idx) => (
                            <Tag icon={"closable"}
                                 mod={"secondary"}
                                 size={"medium"}
                                 m={"0 10px 15px 0"}
                                 onClose={() => setFieldValue("langs", langs.filter(language => language !== item))}
                                 key={idx}>
                                {item}
                            </Tag>
                        ))}

                        {!addLang && <PlusBtn onClick={() => setAddLang(true)} m={"0 0 15px"}/>}
                        {addLang &&
                        <Field name={"newLang"}>
                            {({
                                  field,
                              }: FieldProps) => (
                                <InputTag m={"0 0 15px"}
                                          placeholder={"Выберите язык"}
                                          autoFocus={true}
                                          disabled={true}
                                          added={langs}
                                          options={filterBySubstr(langsData, field.value)}
                                          setValue={(value: string) => setFieldValue("newLang", value)}
                                          onPlusClick={(value?: string) => {
                                              if (value || newLang.length > 0) {
                                                  if (langs.indexOf(value ? value : newLang) == -1)
                                                      setFieldValue("langs", [...langs, toProperName(value ? value : newLang)])
                                              }
                                              setFieldValue("newLang", "")
                                              setAddLang(false)
                                          }}
                                          {...field}
                                />
                            )}
                        </Field>
                        }
                    </div>
                </div>
            </div>
        </EditBlock>

    )
}