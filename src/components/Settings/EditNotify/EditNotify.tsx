import React, {FC} from "react";
import {SettingsCard} from "../SettingsCard/SettingsCard";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {Form, Formik, FormikValues} from "formik";
import {Button} from "../../common/Button/Button";
import {EditBlock} from "../EditBlock/EditBlock";
import styles from "../styles.module.scss"
import {Toggler} from "../../common/Toggler/Toggler";
import {ChangeNotifyReqBodyT} from "../../../api/user-api";
import {changeNotify} from "../../../redux/user-reducer/user-reducer";

type PropsT = {}

export type EditAccountValuesT = {
   isTelegram: boolean
   isEmail: boolean
}

export const EditNotify: FC<PropsT> = ({children}) => {

   const dispatch = useDispatch()
   const data = useSelector((state: RootStateT) => state.user.userData?.notify)

   const onSubmit = ({isTelegram, isEmail}: EditAccountValuesT, {resetForm}: FormikValues) => {
      const body: ChangeNotifyReqBodyT = {
         email: isEmail,
         telegram: isTelegram
      }
      dispatch(changeNotify(body))
   }

   const code = useSelector((state: RootStateT) => state.user.userData?.verify_telegram)
   const isVerified = useSelector((state: RootStateT) => state.user.userData?.verified)

   const initialValues: EditAccountValuesT = {
      isEmail: data?.email !== undefined ? data.email : false,
      isTelegram: isVerified !== undefined ? isVerified : false,
   }

   return (
      <SettingsCard label={"Настройки уведомлений"}>
         <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
         >
            {({setFieldValue, values}) => (
               <Form>
                  <EditBlock title={"Уведомления"}
                             label={"Получайте уведомления, как вам удобно"}>
                     <div className={styles.column}>
                        <div className={styles.row}>
                           <Toggler onClick={() => setFieldValue("isEmail", !values.isEmail)}
                                    id={"notify-1"}
                                    checked={values.isEmail}
                           />
                           <div className={styles.label}>
                              Уведомления по Email
                           </div>
                        </div>
                        <div className={styles.row}>
                           <Toggler onClick={() => setFieldValue("isTelegram", !values.isTelegram)}
                                    id={"notify-2"}
                                    checked={values.isTelegram}
                           />
                           <div className={styles.label}>
                              Уведомления в Telegram боте
                           </div>
                        </div>
                        {code && (""+code).search(":") !== -1 && <div className={styles.column}>
                           <div className={styles.desc}>
                              {isVerified ? "Вы успешно привязали свой телеграмм-аккаунт. Теперь уведомления будут приходить туда."
                                 : `Откройте чат-бота в Telegram и отправьте ему код #${code} для подключения к своей учетной записи в нашем сервисе.`}
                           </div>
                           <a href="https://t.me/voons_bot" target="_blank" rel="noopener noreferrer">
                              <Button mod={"secondary"}
                                      size={"medium"}
                                      m={"15px 0 0 70px"}>
                                 Открыть в Telegram
                              </Button>
                           </a>
                        </div>}
                     </div>
                  </EditBlock>
                  <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
               </Form>
            )}
         </Formik>
      </SettingsCard>
   )
}