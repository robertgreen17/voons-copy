import React, {FC} from "react";
import {SettingsCard} from "../SettingsCard/SettingsCard";
import {useDispatch} from "react-redux";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {Button} from "../../common/Button/Button";
import {EditBlock} from "../EditBlock/EditBlock";
import {Input} from "../../common/Inputs/Input";
import styles from "../styles.module.scss"
import {changePassword} from "../../../redux/user-reducer/user-reducer";
import {ChangePasswordReqBodyT} from "../../../api/user-api";

type PropsT = {}

export type EditPasswordValuesT = {
   old: string
   new: string
}

export const EditPassword: FC<PropsT> = ({children}) => {

   const dispatch = useDispatch()

   const onSubmit = (values: EditPasswordValuesT, {resetForm}: FormikValues) => {
      const payload: ChangePasswordReqBodyT = {
         new_password: values.new,
         old_password: values.old
      }
      dispatch(changePassword(payload, resetForm))
   }

   const initialValues: EditPasswordValuesT = {
      old: "",
      new: "",
   }

   return (
      <SettingsCard label={"Сменить пароль"}>
         <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
         >
            {({setFieldValue, values}) => (
               <Form>
                  <EditBlock title={"Пароль"}
                             label={"Смените пароль от аккаунта"}>
                     <div className={styles.column}>
                        <Field name={"old"}>
                           {({
                                field,
                             }: FieldProps) => (
                              <Input
                                 m={"0 0 20px"}
                                 type={"password"}
                                 label={"Текущий пароль"}
                                 {...field}
                              />
                           )}
                        </Field>
                        <Field name={"new"}>
                           {({
                                field,
                             }: FieldProps) => (
                              <Input
                                 type={"password"}
                                 label={"Новый пароль"}
                                 {...field}
                              />
                           )}
                        </Field>
                     </div>
                  </EditBlock>
                  <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
               </Form>
            )}
         </Formik>
      </SettingsCard>
   )
}