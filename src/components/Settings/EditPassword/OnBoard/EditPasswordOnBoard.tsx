import React, {FC, useEffect} from "react";
import {useDispatch} from "react-redux";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {ChangePasswordReqBodyT} from "../../../../api/user-api";
import {changePassword, getUserData} from "../../../../redux/user-reducer/user-reducer";
import {SettingsCard} from "../../SettingsCard/SettingsCard";
import {EditBlock} from "../../EditBlock/EditBlock";
import styles from "../../styles.module.scss";
import {Input} from "../../../common/Inputs/Input";
import {Button} from "../../../common/Button/Button";

type PropsT = {}

export type EditPasswordValuesT = {
    old: string
    new: string
}

export const EditPasswordOnBoard: FC<PropsT> = ({children}) => {

    const dispatch = useDispatch()

    const onSubmit = (values: EditPasswordValuesT, {resetForm}: FormikValues) => {
        const payload: ChangePasswordReqBodyT = {
            new_password: values.new,
            old_password: values.old
        }
        dispatch(changePassword(payload, resetForm))
    }

    const initialValues: EditPasswordValuesT = {
        old: "",
        new: "",
    }

    useEffect(() => {
        dispatch(getUserData({}))
    }, [])

    return (
        <SettingsCard label={"Сменить пароль"} hideLabel wrapperType={"onBoard"} m={"20px"}>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
            >
                {({setFieldValue, values}) => (
                    <Form>
                        <EditBlock title={"Пароль"}
                                   label={"Смените пароль от аккаунта"}
                                   type = {"board"}>
                            <div className={styles.column_on_board}>
                                <Field name={"old"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            m={"0 0 20px"}
                                            type={"password"}
                                            label={"Текущий пароль"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                                <Field name={"new"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            type={"password"}
                                            label={"Новый пароль"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                            </div>
                        </EditBlock>
                        <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
                    </Form>
                )}
            </Formik>
        </SettingsCard>
    )
}