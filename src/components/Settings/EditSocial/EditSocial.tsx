import React, {FC, useEffect} from "react";
import {SettingsCard} from "../SettingsCard/SettingsCard";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {Button} from "../../common/Button/Button";
import {EditBlock} from "../EditBlock/EditBlock";
import {Input} from "../../common/Inputs/Input";
import styles from "./styles.module.scss"
import {SetSocialReqBodyT} from "../../../api/user-api";
import {setSocial} from "../../../redux/user-reducer/user-reducer";


type PropsT = {}

export type EditSocialValuesT = {
    inst: string
    vk: string
    fb: string
}

export const EditSocial: FC<PropsT> = ({children}) => {

    const dispatch = useDispatch()
    const userData = useSelector((state: RootStateT) => state.user.userData)
    const checkSocial = (social: string) => {
        if (social.includes('https://')) return social
        else {
            if (social.includes('www.')) return social.replace('www.', 'https://');
            if (social.includes('http://')) return social.replace('http://', 'https://');
            return 'https://' + social;
        }
    }
    const onSubmit = (values: EditSocialValuesT, {resetForm}: FormikValues) => {
        if (values.fb !== initialValues.fb || values.inst !== initialValues.inst || values.vk !== initialValues.vk) {
            const body: SetSocialReqBodyT = {}
            if (values.vk) body.vk = checkSocial(values.vk);
            if (values.inst) body.instagram = checkSocial(values.inst);
            if (values.fb) body.fb = checkSocial(values.fb);
            dispatch(setSocial(body))
        }
    }

    let initialValues: EditSocialValuesT = {
        inst: userData?.social?.instagram ? userData?.social.instagram : "",
        fb: userData?.social?.fb ? userData?.social.fb : "",
        vk: userData?.social?.vk ? userData?.social.vk : "",
    }

    useEffect(() => {
        initialValues = {
            inst: userData?.social?.instagram ? userData?.social.instagram : "",
            fb: userData?.social?.fb ? userData?.social.fb : "",
            vk: userData?.social?.vk ? userData?.social.vk : "",
        }
    }, [userData])

    return (
        <SettingsCard label={"Социальные сети"} hideLabel>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
            >
                {({setFieldValue, values}) => (
                    <Form>
                        <EditBlock title={"Социальные сети"}
                                   label={"Добавьте ссылки на свои аккаунты в социальных сетях"}>
                            <div className={styles.column}>
                                <Field name={"inst"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            m={"0 0 20px"}
                                            type={"text"}
                                            label={"Instagram"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                                <Field name={"vk"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            m={"0 0 20px"}
                                            type={"text"}
                                            label={"Вконтакте"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                                <Field name={"fb"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            type={"text"}
                                            label={"Facebook"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                            </div>
                        </EditBlock>
                        <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
                    </Form>
                )}
            </Formik>
        </SettingsCard>
    )
}