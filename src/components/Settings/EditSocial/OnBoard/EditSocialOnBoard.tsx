import React, {FC, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {SetSocialReqBodyT} from "../../../../api/user-api";
import {getUserData, setSocial} from "../../../../redux/user-reducer/user-reducer";
import {SettingsCard} from "../../SettingsCard/SettingsCard";
import {EditBlock} from "../../EditBlock/EditBlock";
import styles from "../styles.module.scss";
import {Input} from "../../../common/Inputs/Input";
import {Button} from "../../../common/Button/Button";

type PropsT = {}

export type EditSocialValuesT = {
    inst: string
    vk: string
    fb: string
}

export const EditSocialOnBoard: FC<PropsT> = ({children}) => {

    const dispatch = useDispatch()
    const userData = useSelector((state: RootStateT) => state.user.userData)

    const onSubmit = (values: EditSocialValuesT, {resetForm}: FormikValues) => {
        if (values.fb !== initialValues.fb || values.inst !== initialValues.inst || values.vk !== initialValues.vk) {
            const body: SetSocialReqBodyT = {}
            if (values.vk) body.vk = values.vk
            if (values.inst) body.instagram = values.inst
            if (values.fb) body.fb = values.fb
            dispatch(setSocial(body))
        }
    }

    let initialValues: EditSocialValuesT = {
        inst: userData?.social?.instagram ? userData?.social.instagram : "",
        fb: userData?.social?.fb ? userData?.social.fb : "",
        vk: userData?.social?.vk ? userData?.social.vk : "",
    }

    useEffect(() => {
        initialValues = {
            inst: userData?.social?.instagram ? userData?.social.instagram : "",
            fb: userData?.social?.fb ? userData?.social.fb : "",
            vk: userData?.social?.vk ? userData?.social.vk : "",
        }
    }, [userData])

    useEffect(() => {
        dispatch(getUserData({}))
    }, [])

    return (
        <SettingsCard label={"Социальные сети"} hideLabel wrapperType={"onBoard"} m = {"20px"}>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
            >
                {({setFieldValue, values}) => (
                    <Form>
                        <EditBlock title={"Социальные сети"}
                                   label={"Добавьте ссылки на свои аккаунты в социальных сетях"}
                                   type = {"board"}>
                            <div className={styles.column_on_board}>
                                <Field name={"inst"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            m={"0 0 20px"}
                                            type={"text"}
                                            label={"Instagram"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                                <Field name={"vk"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            m={"0 0 20px"}
                                            type={"text"}
                                            label={"Вконтакте"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                                <Field name={"fb"}>
                                    {({
                                          field,
                                      }: FieldProps) => (
                                        <Input
                                            type={"text"}
                                            label={"Facebook"}
                                            {...field}
                                        />
                                    )}
                                </Field>
                            </div>
                        </EditBlock>
                        <Button mod={"primary"} size={"large"} type={"submit"}>Сохранить</Button>
                    </Form>
                )}
            </Formik>
        </SettingsCard>
    )
}