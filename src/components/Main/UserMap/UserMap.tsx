import React, {FC} from "react"
import styles from "./styles.module.scss"
import {MapCard} from "./MapCard/MapCard";
import map1 from "../../../media/icons/userMap/01.svg"
import map2 from "../../../media/icons/userMap/02.svg"
import map3 from "../../../media/icons/userMap/03.svg"
import map4 from "../../../media/icons/userMap/04.svg"

export const UserMap: FC = () => {

   const mapData = [
      {
         step: "01",
         title: "Зарегистрируйся",
         subTitle: "Регистрация займет не более 5 минут",
         imgLink: map1
      },
      {
         step: "02",
         title: "Заполни",
         subTitle: "Профиль и укажи в календаре доступное время",
         imgLink: map2
      },
      {
         step: "03",
         title: "Поделись",
         subTitle: "Ссылкой на свой профиль в соц. сетях",
         imgLink: map3
      },
      {
         step: "04",
         title: "Принимай",
         subTitle: "Запросы на встречи, помогай другим развиваться и монетизируй свое время! ",
         imgLink: map4
      },
   ]

   return (
      <div className={styles.wrapper} id={"how-does-it-works"}>
         <div className={styles.title}>
            Как это работает
         </div>
         <div className={styles.container}>
            {mapData.map((item, idx) => (
               <MapCard key={idx}
                        title={item.title}
                        imgLink={item.imgLink}
                        step={item.step}
                        subTitle={item.subTitle}/>
            ))}
         </div>
      </div>
   )
}