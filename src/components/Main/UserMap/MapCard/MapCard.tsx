import React, {FC} from "react"
import styles from "./styles.module.scss"

type PropsT = {
   step: string
   title: string
   subTitle: string
   imgLink: string
}

{console.log(styles)}

export const MapCard: FC<PropsT> = ({imgLink, title, subTitle, step}) => {
   return (
      <div className={styles.wrapper}>
         <img src={imgLink} alt="" className={styles.image}/>
         <div className={styles.box}>
            <div className={styles.label}>
               Шаг {step}
            </div>
            <div className={styles.title}>
               {title}
            </div>
            <div className={styles.label}>
               {subTitle}
            </div>
         </div>
      </div>
   )
}