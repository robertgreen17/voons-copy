import React, {FC, MouseEvent} from "react"
import styles from "./styles.module.scss"
import {NavLink} from "react-router-dom";
import {Answer} from "./Answer/Answer";
import CookieBanner from 'react-cookie-banner';
import CookieConsent from "react-cookie-consent";
import {Page} from "../../common/Page/Page";
import Payments from "../../../media/cards.jpg"

type PropsT = {}

export const Footer: FC<PropsT> = ({}) => {

    const scrollTo = (id: string) => {
        const elem = document.getElementById(id)
        if (elem) {
            elem.scrollIntoView({behavior: "smooth"})
        }
    }

    const columns = [
        {
            title: "Главное",
            items: [
                {
                    name: "Как это работает",
                    id: "how-does-it-works"
                },
                {
                    name: "Найти Voons Leaders",
                    id: "specialists",
                    isNotNewPage: true,
                    link: "/search"
                },
                {
                    name: "Часто задаваемые вопросы",
                    id: "questions",
                    link: ""
                },
            ]
        },
        {
            title: "Персональные данные",
            items: [
                {
                    name: "Политика защиты персональных данных",
                    id: "",
                    link: "/data_policy"
                },
                {
                    name: "Заявление-согласие на обработку предоставленой информации",
                    id: "",
                    link: "/statement"
                },
            ]
        },
        {
            title: "Информация",
            items: [
                {
                    name: "Условия использования",
                    id: "",
                    link: "/terms_of_use",
                },
                {
                    name: "Правовая информация",
                    id: "",
                    link: "/legal_info"
                },
                {
                    name: "Договор - оферта",
                    id: "",
                    link: "/offer"
                },
            ]
        },
    ]

    const accordion = [
        {
            question: "Клиенту",
            answer:
                <div className={styles.answerItemsWrapper}>
                    <div className={styles.item} onClick={() => scrollTo('how-does-it-works')}>
                        Как это работает
                    </div>
                    <NavLink className={styles.item} to={"/search"}>
                        Найти специалиста
                    </NavLink>
                    <div className={styles.item} onClick={() => scrollTo('reviews')}>
                        Отзывы
                    </div>
                    <div className={styles.item} onClick={() => scrollTo('questions')}>
                        Часто задаваемые вопросы
                    </div>
                </div>
        },
        {
            question: "Специалисту",
            answer:
                <div className={styles.answerItemsWrapper}>
                    <div className={styles.item} onClick={() => scrollTo('how-does-it-works')}>
                        Как это работает
                    </div>
                    <NavLink to="/main/sign_in/reg/profi" className={styles.item}>
                        Стать специалистом
                    </NavLink>
                    <div className={styles.item} onClick={() => scrollTo('questions')}>
                        Часто задаваемые вопросы
                    </div>
                </div>
        },
        {
            question: "Информация",
            answer:
                <div className={styles.answerItemsWrapper}>
                    <div className={styles.item}>
                        <a href="/data_policy" target="_blank" rel="noopener noreferrer" className={styles.item}>
                            Политика защиты персональных данных
                        </a>
                    </div>
                    <div className={styles.item}>
                        <a href="/statement" target="_blank" rel="noopener noreferrer" className={styles.item}>
                            Заявление-согласие на обработку предоставленой информации
                        </a>
                    </div>
                    <div className={styles.item}>
                        <a href="/terms_of_use" target="_blank" rel="noopener noreferrer" className={styles.item}>
                            Условия использования
                        </a>
                    </div>
                    <div className={styles.item}>
                        <a href="/legal_info" target="_blank" rel="noopener noreferrer" className={styles.item}>
                            Правовая информация
                        </a>
                    </div>
                    <div className={styles.item}>
                        <a href="/offer" target="_blank" rel="noopener noreferrer" className={styles.item}>
                            Договор - оферта
                        </a>
                    </div>
                </div>
        }
    ]

    return (
        <div className={styles.wrapper}>
            <div className={styles.socLinks}>
                {/*<a href="https://vk.com/" className={styles.link}>*/}
                {/*   <img src="/icons/socLinks/vk.svg" alt=""/>*/}
                {/*</a>*/}
                <a href="https://t.me/voons_ru" className={styles.link} target={"_blank"}>
                    {/*<img src="/icons/socLinks/tg.svg" alt=""/>*/}
                    <span>
                  <i className={"fab fa-telegram-plane"}/>
               </span>
                </a>
                <a href="https://www.facebook.com/Voonsru-103177178353182" target={"_blank"} className={styles.link}>
                    <img src="/icons/socLinks/fb.svg" alt=""/>
                </a>
                <a href="https://instagram.com/voons.ru?igshid=1vksc5ytzmd4f" className={styles.link} target={"_blank"}>
                    <img src="/icons/socLinks/inst.svg" alt=""/>
                </a>
                {/*<a href="https://twitter.com/" className={styles.link}>*/}
                {/*   <img src="/icons/socLinks/twit.svg" alt=""/>*/}
                {/*</a>*/}
            </div>
            <div className={styles.row}>
                {columns.map((column, idx) => (
                    <div className={styles.column} key={idx}>
                        <div className={styles.title}>
                            {column.title}
                        </div>
                        {column.items.map((item, idx) => {
                            if (!item.link) return <div className={styles.item} key={idx}
                                                        onClick={() => scrollTo(item.id)}>
                                {item.name}
                            </div>
                            else if (item.isNotNewPage) return <NavLink to={item.link} className={styles.item}>
                                {item.name}
                            </NavLink>
                            else
                                return <a href={item.link} target={"_blank"} className={styles.item}>
                                    {item.name}
                                </a>
                        })}
                    </div>
                ))}
            </div>
            <div className={styles.accordion}>
                {accordion.map((item, idx) => (
                    <Answer key={idx} answer={item.answer} question={item.question}/>
                ))}
            </div>
            <div className={styles.about}>
                <div className={styles.text}>
                    {/*Information service, services are provided by service partners. It is not clear how the final disclaimer,*/}
                    {/*so I write that to fill in space for two or three lines. Then we'll see how it goes.*/}
                    <div className={styles.rights}>
                        Voons. Copyright 2021. Все права защищены
                    </div>
                    <div className={styles.item}>
                        <br/>
                        <img src={Payments} alt={""} width={'350px'} height={'50px'}/>
                    </div>
                </div>
            </div>
        </div>
    )
}