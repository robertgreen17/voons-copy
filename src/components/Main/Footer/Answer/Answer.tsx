import React, {FC, ReactNode, useState} from "react"
import styles from "./styles.module.scss"

type PropsT = {
   question: string
   answer: ReactNode
}

export const Answer: FC<PropsT> = ({answer, question}) => {

   const [isAnswer, setIsAnswer] = useState(false)


   return (
      <div className={styles.wrapper}>
         <div className={styles.row} onClick={() => setIsAnswer(!isAnswer)}>
            <div className={styles.question}>
               {question}
            </div>
            <img src="/icons/expandIcon.svg"
                 alt=""
                 className={styles.icon}
                 />
         </div>
         {isAnswer && <div className={styles.answer}>
            {answer}
         </div>}
      </div>
   )
}