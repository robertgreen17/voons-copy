import React, {FC, useEffect, useState} from "react"
import styles from "./styles.module.scss"
import {Button} from "../../common/Button/Button";
import {NavLink, useHistory} from "react-router-dom";
import landingHeaderBg from "../../../media/icons/main/landingHeaderBg.svg"
import landingUserPhoto from "../../../media/icons/main/landingUserPhoto1.png"
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {USERS_LIMIT} from "../../../redux/constants";
import {useDispatch} from "react-redux";
// @ts-ignore
import TypeIt from "typeit";

type PropsT = {
    title: string
    text: string
    mainBtnText: string
    secondaryBtnText: string
    primaryBtnMod?: "primary" | "danger"
    mainBtnLink: string
    secondaryBtnLink: string
    isNav?: boolean
    isUserPhoto?: boolean
}

export const ImageBlockHeader: FC<PropsT> = ({
                                                 title,
                                                 text,
                                                 primaryBtnMod = "primary",
                                                 mainBtnText,
                                                 secondaryBtnText,
                                                 mainBtnLink,
                                                 secondaryBtnLink,
                                                 isUserPhoto,
                                                 isNav
                                             }) => {
    useEffect(() => {
        const imageList = [landingHeaderBg, landingUserPhoto]
        imageList.forEach((im) => {
            new Image().src = im
        })
        new TypeIt(".dynamic-text", {
            speed: 100,
            startDelay: 100,
            loop: true
        })
            .type('Советуй!', {delay: 2000})
            .delete(null, {speed: 100})
            .type('Учи!', {delay: 2000})
            .delete(null, {speed: 100})
            .type('Знакомься!', {delay: 2000})
            .go()
    }, [])
    const dispatch = useDispatch()
    const [isReset, setIsReset] = useState(false)
    const push = useHistory().push

    const showAll = () => {
        dispatch(search.actions.setIsResetSearch(true))
        setIsReset(true)
        dispatch(search.actions.setCurrentSearchData(null))
        dispatch(search.actions.setInputValue(""))
        dispatch(find({limit: USERS_LIMIT, offset: 0}, () => push("/searching")))
    }

    return (
        <div className={styles.wrapper}>
            {/*{!isUserPhoto && <img src={"/images/landingHeaderBg.svg"} alt="" className={styles.background}/>}*/}
            {/*{isUserPhoto && <img src={"/images/landingUserPhoto.svg"} alt="" className={styles.userPhoto}/>}*/}
            {!isUserPhoto && <img src={landingHeaderBg} alt="" className={styles.background}/>}
            {isUserPhoto && <img src={landingUserPhoto} alt="" className={styles.userPhoto}/>}

            <div className={styles.container}>
                <div className={styles.title}>
                    Voons – платформа для видео встреч 2.0
                </div>
                <div className={styles.text} id={"text"}>
                    <span className={"dynamic-text"}/>
                    <p>Общайся 1:1 с помощью видео! Монетизируй свои встречи!
                        Время – деньги, тебе решать сколько стоит твое время!</p>
                    {/*<Typewriter*/}
                    {/*    options={{*/}
                    {/*        loop: true,*/}
                    {/*        wrapperClassName: "dynamic-text"*/}
                    {/*    }}*/}
                    {/*    onInit={(typewriter) => {*/}
                    {/*        typewriter*/}
                    {/*            .changeDelay(50)*/}
                    {/*            .callFunction(state => console.log(state))*/}
                    {/*            .typeString('Знакомься!')*/}
                    {/*            .pauseFor(2500)*/}
                    {/*            .deleteAll(20)*/}
                    {/*            .typeString('Общайся!')*/}
                    {/*            .pauseFor(2500)*/}
                    {/*            .deleteAll(20)*/}
                    {/*            .typeString('Советуй!')*/}
                    {/*            .pauseFor(2500)*/}
                    {/*            .deleteAll(20)*/}
                    {/*            .typeString('Учи!')*/}
                    {/*            .pauseFor(2500)*/}
                    {/*            .start();*/}
                    {/*    }}*/}
                    {/*/>Монетизируй свои встречи! Время – деньги, тебе решать сколько стоит твое время!*/}
                </div>
                <div className={styles.btnGroup}>
                    {isNav && <span onClick={showAll}>
                        <Button size={"large"} mod={primaryBtnMod}>
                            {mainBtnText}
                        </Button>
                    </span>
                    }
                </div>
            </div>
        </div>
    )
}