import React, {FC} from "react"
import styles from "./styles.module.scss"
import {Button} from "../../common/Button/Button";

type PropsT = {}

export const MobileApp: FC<PropsT> = ({}) => {
   return (
      <div className={styles.wrapper}>

         <img src="/images/mobileAppImg.svg" alt="" className={styles.background}/>


         <div className={styles.container}>
            <div className={styles.title}>
               Загрузите наше мобильное приложение
            </div>
            <div className={styles.text}>
               Мобильное приложение обеспечит вам свободу перелвижения. Загружайте с Google Play абсолютно бесплатно.
               Консультируйте и получайте консультации еще проще
            </div>
            <a href="https://play.google.com/">
               <Button mod={"googlePlay"}/>
            </a>
         </div>
      </div>
   )
}