import React, {FC, useEffect, useState} from "react"
import styles from "./styles.module.scss"
import {Button} from "../../common/Button/Button";
import {NavLink, useHistory} from "react-router-dom";
import landingHeaderBg from "../../../media/icons/main/landingHeaderBg.svg"
import landingUserPhoto from "../../../media/icons/main/landingUserPhoto.svg"
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {USERS_LIMIT} from "../../../redux/constants";
import {useDispatch} from "react-redux";

type PropsT = {
    title: string
    text: string
    mainBtnText: string
    secondaryBtnText: string
    primaryBtnMod?: "primary" | "danger"
    mainBtnLink: string
    secondaryBtnLink: string
    isNav?: boolean
    isUserPhoto?: boolean
}

export const ImageBlock: FC<PropsT> = ({
                                           title,
                                           text,
                                           primaryBtnMod = "primary",
                                           mainBtnText,
                                           secondaryBtnText,
                                           mainBtnLink,
                                           secondaryBtnLink,
                                           isUserPhoto,
                                           isNav
                                       }) => {
    const push = useHistory().push

    const dispatch = useDispatch()
    const [isReset, setIsReset] = useState(false)


    const showAll = () => {
        dispatch(search.actions.setIsResetSearch(true))
        setIsReset(true)
        dispatch(search.actions.setCurrentSearchData(null))
        dispatch(search.actions.setInputValue(""))
        dispatch(find({limit: USERS_LIMIT, offset: 0}, () => push("/searching")))
    }

    useEffect(() => {
      const imageList = [landingHeaderBg, landingUserPhoto]
      imageList.forEach((im) => {
         new Image().src = im
      })
   }, [])



    return (
        <div className={styles.wrapper}>
            {/*{!isUserPhoto && <img src={"/images/landingHeaderBg.svg"} alt="" className={styles.background}/>}*/}
            {/*{isUserPhoto && <img src={"/images/landingUserPhoto.svg"} alt="" className={styles.userPhoto}/>}*/}
            {!isUserPhoto && <img src={landingHeaderBg} alt="" className={styles.background}/>}
            {isUserPhoto && <img src={landingUserPhoto} alt="" className={styles.userPhoto}/>}

            <div className={styles.container}>
                <div className={styles.title}>
                    {title}
                </div>
                <div className={styles.text}>
                    {text}
                </div>
                <div className={styles.btnGroup}>
                    {isNav && <NavLink to={mainBtnLink} onClick={showAll}>
                        <Button size={"large"} mod={primaryBtnMod}>
                            {mainBtnText}
                        </Button>
                    </NavLink>
                    }
                    {isNav && <NavLink to={secondaryBtnLink}>
                        <Button mod={"secondary"} size={"large"}>
                            {secondaryBtnText}
                        </Button>
                    </NavLink>
                    }
                    {!isNav && <NavLink to={mainBtnLink} onClick={showAll}>
                        <Button size={"large"} mod={primaryBtnMod}>
                            {mainBtnText}
                        </Button>
                    </NavLink>
                    }
                </div>
            </div>
        </div>
    )
}