import React, {FC, ReactElement, ReactNode, useRef, useState} from "react"
import styles from "./styles.module.scss"
import stylesScrollCard from '../../common/ScrollCards/styles.module.scss';
import stylesMain from '../../../pages/Main/styles.module.scss';
import {SpecialistCard} from "./SpecialistCard/SpecialistCard";
import avatar from "../../../media/icons/avatar.svg"
import {SearchGroup} from "../../common/SearchGroup/SearchGroup";
import {useMedia} from "use-media";
import {useHistory} from "react-router-dom";
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {USERS_LIMIT} from "../../../redux/constants";
import {useDispatch} from "react-redux";
import {SwiperSlide} from "swiper/react";
import {ScrollCards} from "../../common/ScrollCards/ScrollCards";
import Swiper, {SwiperRefNode} from "react-id-swiper";


type PropsT = {
    id?: string,
    additional?: any
}


export const Specialists: FC<PropsT> = ({
                                            children,
                                            id,
                                            additional
                                        }) => {

    const mapData = [
        {
            name: "Дед Мороз",
            desc: "Самые настоящие Дед Мороз и Снегурочка готовы поздравить каждого, кто хорошо(и не очень, потому что такой выдался год) себя вел...",
            imgLink: "https://api.voons.ru/photos/5fdb7c80cc2e8212756af704.jpg",
            rating: 5,
            reviewsCount: 0,
            id: 14,
            tags: ["Поздравления"]
        },
        {
            name: "Татьяна Миронова",
            desc: "Копирайтер. Консультирую, оказываю помощь в создании текстового контента.",
            imgLink: "https://api.voons.ru/photos/5fe0bddbe0777409857303fa.jpg",
            rating: 4.7,
            reviewsCount: 0,
            id: 36,
            tags: ["Маркетинг"]
        },
        {
            name: "Петр Липов",
            desc: "Помогаю строить осознанную карьеру Сертифицированный карьерный консультант, коуч ECF (Европейская федерация коучинга). 16 лет помогаю людям находить любимую работу...",
            imgLink: "https://api.voons.ru/photos/5fe1ec3f784948391b7313d6.jpg",
            rating: 4.9,
            reviewsCount: 0,
            id: 43,
            tags: ["Бизнес", "Образование"]
        },
        {
            name: "Александр Гутов",
            desc: "Сертифицированный тренер и диетолог. 6 лет опыта персональных тренировок. Составляю программы тренировок и питания. Онлайн ведение от 1 месяца...",
            imgLink: "https://api.voons.ru/photos/5fe1e585784948391b7313d1.jpg",
            rating: 4.8,
            reviewsCount: 0,
            id: 42,
            tags: ["Здоровье", "Красота"]
        },
    ]

    const swiper = useRef<SwiperRefNode>(null);

    const onNextSlide = () => {
        if (swiper.current && swiper.current.swiper) {
            swiper.current.swiper.slideNext()
        }
    }
    const onPrevSlide = () => {
        if (swiper.current && swiper.current.swiper) {
            swiper.current.swiper.slidePrev()
        }
    }

    const params = additional ? additional : {
        spaceBetween: 20,
        containerClass: 'swiper-container ' + styles.slider,
        freeMode: true,
        breakpoints: {
            768: {
                spaceBetween: 30
            },
        }
    }

    const dispatch = useDispatch()

    const push = useHistory().push

    const [isReset, setIsReset] = useState(false)


    const showAll = () => {
        dispatch(search.actions.setIsResetSearch(true))
        setIsReset(true)
        dispatch(search.actions.setCurrentSearchData(null))
        dispatch(search.actions.setInputValue(""))
        dispatch(find({limit: USERS_LIMIT, offset: 0}, () => push("/searching")))
    }

    return (
        <div className={stylesScrollCard.wrapper} id={"specialists"}>

            <div className={stylesScrollCard.header + ' ' + styles.hideAdaptive}>
                <div className={stylesScrollCard.text}>
                    <div className={stylesScrollCard.title}>
                        Voons Leaders
                    </div>
                </div>
                <div className={stylesScrollCard.wrapper}>
                    <SearchGroup/>
                </div>
                <div className={styles.showAllWrapper}>
                    <div className={styles.showAll} onClick={showAll}>
                        <div className={styles.snowAllTitle}>Показать всех</div>
                    </div>
                </div>
            </div>


            <div className={styles.header + ' ' + styles.showOriginal}>
                <div className={styles.categoryWrapper}
                >
                    <div className={styles.text}>
                        <div className={styles.title}>
                            Voons Leaders
                        </div>
                    </div>
                    <div className={stylesScrollCard.control}>
                        <SearchGroup/>
                    </div>
                </div>
                <div className={styles.showAllWrapper}>
                    <div className={styles.showAll} onClick={showAll}>
                        <div className={styles.snowAllTitle}>Показать всех</div>
                    </div>
                </div>
            </div>


            <div className={stylesScrollCard.container}>
            <span onClick={onPrevSlide} className={`${stylesScrollCard.arrow} ${stylesScrollCard.left}`}>
               <i className={"fas fa-arrow-left"}/>
            </span>
                <Swiper slidesPerView={"auto"}
                        {...params}
                        ref={swiper}>
                    {mapData.map((item, idx) => (
                        <SwiperSlide key={idx} className={stylesMain.specialistCard}>
                            <SpecialistCard
                                size={"small"}
                                imgLink={item.imgLink}
                                desc={item.desc}
                                name={item.name}
                                userId={item.id}
                                key={idx}
                                reviewsCount={item.reviewsCount}
                                rating={item.rating}
                                tags={item.tags}
                            />
                        </SwiperSlide>
                    ))}
                </Swiper>
                <span onClick={onNextSlide} className={`${stylesScrollCard.arrow} ${stylesScrollCard.right}`}>
               <i className={"fas fa-arrow-right"}/>
            </span>

            </div>
        </div>);
}