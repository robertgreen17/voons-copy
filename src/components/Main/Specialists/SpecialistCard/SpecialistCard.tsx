import React, {FC, LegacyRef} from "react"
import styles from "./styles.module.scss"
import {MiniProfile} from "../../../common/MiniProfile/MiniProfile";
import {Button} from "../../../common/Button/Button";
import {NavLink} from "react-router-dom";
import {TagT} from "../../../../api/search-api";
import {formatMoney} from "../../../../utils/string";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import {UserTypeT} from "../../../../redux/user-reducer/user-reducer";
import ReactTooltip from "react-tooltip";

type PropsT = {
    name: string
    desc: string
    imgLink: string
    size: "big" | "small"
    userId?: number
    tags?: Array<TagT | string>
    minPrice?: string
    blockRef?: LegacyRef<HTMLDivElement>
    rating?: number
    reviewsCount?: number,
    services?: Array<string>,
    uses_free?: Array<string>
}

export const SpecialistCard: FC<PropsT> = ({
                                               imgLink,
                                               desc,
                                               name,
                                               size,
                                               userId,
                                               tags = [],
                                               minPrice,
                                               blockRef,
                                               rating,
                                               reviewsCount,
                                               services,
                                               uses_free,
                                           }) => {
    const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
    const userType = useSelector((state: RootStateT) => state.user.type)
    const me = useSelector((state: RootStateT) => state.user.userData)

    const isFree = () => {
        return uses_free !== undefined && uses_free.indexOf(me !== undefined && me !== null && me._id !== undefined ? me?._id.$oid : "Not Auth") == -1;
    }

    return (
        <div className={`${styles.wrapper} ${styles[size]}` + " specialistCard"} ref={blockRef}>
            <div className={styles.header}>
                <div className={styles.left}>
                    <MiniProfile
                        userId={userId}
                        size={size}
                        isChecked={true}
                        imgLink={imgLink}
                        name={name}
                        services={services}
                        rating={rating}
                        reviews={reviewsCount}
                        tags={tags}
                        imageWidth={"90px"}
                        isFree={isFree()}
                    />
                </div>
                {size === "small" && <div className={styles.desc + " specialistDesc"}>
                    {desc}
                </div>}
                <div className={styles.right}>
                    {userType === UserTypeT.client || !isAuth ? (
                        <NavLink to={isAuth ? `/profi/${userId}/appointment/1` : `/profi/${userId}`}>
                            <Button data-tip="Будет доступно с обновлением 2.0" data-for={"appo"} mod={"primary"} width={"full-width"} size={"medium"} className={
                                userType === UserTypeT.client ? styles.buttons1 + " specialistBtn" : styles.buttons1Disabled + " specialistBtn"
                            }>
                                {
                                    isAuth ?
                                        userType === UserTypeT.client ? (isFree() ? `Заказать бесплатную консультацию` : `Заказать консультацию`) :
                                            "Недоступно для специлистов"
                                        : `Подробнее`
                                }
                            </Button>
                        </NavLink>
                    ) : (
                        <Button data-tip="Будет доступно с обновлением 2.0" data-for={"appo"} mod={"primary"} width={"full-width"} size={"medium"} className={
                             styles.buttons1Disabled + " specialistBtn"
                        }>
                            Недоступно для специлистов
                        </Button>
                    )}
                    {isAuth && userType === UserTypeT.profi ? <ReactTooltip place="top" type="dark" id={"appo"} effect="float"/> : ""}
                    {!isFree() ? minPrice !== undefined && <div className={styles.addition}>
                        Консультация от {formatMoney(minPrice ? +minPrice : undefined)} ₽
                    </div> : (
                        minPrice !== undefined && <div className={`${styles.addition} ${styles.free}`}>
                            Консультация от <span>{formatMoney(minPrice ? +minPrice : undefined)} ₽</span>
                            <span>
                                0.00 ₽
                            </span>
                        </div>
                    )}

                </div>
            </div>
            {size === "big" && <div className={styles.desc}>
                {desc}
            </div>}
        </div>
    )
}