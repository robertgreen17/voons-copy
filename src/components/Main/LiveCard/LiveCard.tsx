import React, {FC} from "react"
import styles from "./styles.module.scss"
import {Tag} from "../../common/Tag/Tag";

type PropsT = {
   title: string
   desc: string
   imgLink: string
   views: number
}

export const LiveCard: FC<PropsT> = ({imgLink, title, desc, views}) => {
   return (
      <div className={styles.wrapper}>
         <div className={styles.image}>
            <div className={styles.live}>
               <Tag size={"small"} mod={"danger"}>• Live</Tag>
            </div>
            <img src={imgLink} alt=""/>
            <div className={styles.views}>
               <Tag size={"small"} mod={"dark-opacity"} icon={"icon-left"}>{views}</Tag>
            </div>
            <div className={styles.play}>
               <img src="/icons/playArrow.svg" alt=""/>
            </div>

         </div>
         <Tag size={"small"} mod={"danger"}/>
         <div className={styles.title}>
            {title}
         </div>
         <div className={styles.desc}>
            {desc}
         </div>
      </div>
   )
}