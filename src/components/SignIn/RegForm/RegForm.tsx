import React, {FC, useCallback, useEffect, useRef, useState} from "react"
import {useDispatch, useSelector} from "react-redux";
import {createAcc} from "../../../redux/auth-reducer/auth-reducer";
import {AuthCreateReqBodyT} from "../../../api/auth-api";
import styles from "./styles.module.scss"
import {CheckBox, Input, InputWithMask, SegmentControl} from "../../common/Inputs/Input";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {Button} from "../../common/Button/Button";
import {NavLink, useHistory} from "react-router-dom";
import {useRedirect} from "../../../hooks/useRedirect";
import {RootStateT} from "../../../redux/store";
import {UserTypeT} from "../../../redux/user-reducer/user-reducer";
import {CountryT, PredictionT, searchApi} from "../../../api/search-api";
import {debounce} from "lodash";
import {useStateCallback} from "../../../hooks/useStateCallback";
import {SocialAuth} from "../SocialAuth/SocialAuth";
// import ym from "react-yandex-metrika";

type ValuesT = {
   account_type: 0 | 1,
   name: string
   surname: string
   email: string
   country: string
   // city: string
   phone: string,
   checked: boolean
   password: string
}

export const RegForm: FC = () => {

   const dispatch = useDispatch()
   const history = useHistory()
   const [options, setOptions] = useState([] as Array<CountryT>)
   const [value, setValue] = useStateCallback("")
   const input = useRef<HTMLInputElement>(null)
   const box = useRef<HTMLDivElement>(null)

   const onKeyUp = (value: string) => {
      if (value.length > 0) {
         setValue(value)
      }
   }

   const fetchPrediction = async () => {
      // запрос за подсказками
      if (value.length > 0) {
         const body = {
            name: value
         }
         const resData = await searchApi.predictCountry(body)
         setOptions(resData.data)
      }
   }

   // debounced запрос за подсказками
   const delayedFetch = useCallback(debounce(fetchPrediction, 500), [value])

   useEffect(() => {
      // запрашиваем подсказки при изменении значения в input
      delayedFetch();
      // cancel the debounce on useEffect cleanup.
      return () => delayedFetch.cancel();
   }, [delayedFetch]);

   const onSubmit = (values: ValuesT, {resetForm}: FormikValues) => {
      if (values.checked) {

         const ref = localStorage.getItem("ref") // реферальный id
         const payload: AuthCreateReqBodyT = {
            email: values.email,
            password: values.password,
            phone: values.phone,
            last_name: values.surname,
            first_name: values.name,
            country: values.country,
            // city: values.city,
            account_type: values.account_type,
         }
         if (ref) payload.invited_by = ref
         dispatch(createAcc(payload, history.push))
      }
   }

   return (
      <Formik
         initialValues={{
            account_type: history.location.pathname === "/main/sign_in/reg/client" ? 0 : 1,
            name: "",
            surname: "",
            email: "",
            country: "",
            phone: "",
            password: "",
            checked: false,
         } as ValuesT}
         onSubmit={onSubmit}
         className={styles.wrapper}>
         {({setFieldValue, values}) => (
            <Form>
               <SegmentControl active={values.account_type}
                               labels={["Клиент", "Специалист"]}
                               m={values.account_type === 0 ? "0 0 22px" : "0 0 22px"}
                               setActive={(number) => setFieldValue("account_type", number)}/>
               <SocialAuth accountType={values.account_type}/>
               <Field name={"name"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        required
                        m={"22px 0 0"}
                        type={"text"}
                        label={"Имя"}
                        mod={errors.name && touched.name ? "error" : undefined}
                        errorMessage={errors.name}
                        {...field}
                     />
                  )}
               </Field>
               <Field name={"surname"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        required
                        m={"22px 0 0"}
                        type={"text"}
                        label={"Фамилия"}
                        mod={errors.surname && touched.surname ? "error" : undefined}
                        errorMessage={errors.surname}
                        {...field}
                     />
                  )}
               </Field>
               <Field name={"email"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        required
                        m={"22px 0 0"}
                        type={"email"}
                        label={"Email (логин)"}
                        mod={errors.email && touched.email ? "error" : undefined}
                        errorMessage={errors.email}
                        {...field}
                     />
                  )}
               </Field>
               <Field name={"password"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        required
                        m={"22px 0 0"}
                        type={"password"}
                        label={"Пароль"}
                        mod={errors.password && touched.password ? "error" : undefined}
                        errorMessage={errors.password}
                        {...field}
                     />
                  )}
               </Field>
               <Field name={"country"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <div style={{position: "relative"}}>
                        <Input
                           required
                           m={"22px 0 0"}
                           type={"text"}
                           label={"Страна"}
                           inputRef={input}
                           autoComplete={"off"}
                           onKeyUp={() => onKeyUp(values.country)}
                           mod={errors.country && touched.country ? "error" : undefined}
                           errorMessage={errors.country}
                           {...field}
                        />
                        {options.length > 0 && <div className={styles.box} ref={box}>
                           {options.map((item, key) => (
                              <div className={styles.item} key={key} onClick={() => {
                                 setFieldValue("country", item.name)
                                 setOptions([])
                              }}>
                                 {item.name}
                              </div>
                           ))}
								</div>}
                     </div>
                  )}
               </Field>

               {/*<Field name={"city"}>*/}
               {/*   {({*/}
               {/*        field,*/}
               {/*        form: {touched, errors}*/}
               {/*     }: FieldProps) => (*/}
               {/*      <Input*/}
               {/*         m={"22px 0 0"}*/}
               {/*         type={"text"}*/}
               {/*         label={"Город"}*/}
               {/*         mod={errors.city && touched.city ? "error" : undefined}*/}
               {/*         errorMessage={errors.city}*/}
               {/*         {...field}*/}
               {/*      />*/}
               {/*   )}*/}
               {/*</Field>*/}
               {/*<Field name={"phone"}>*/}
               {/*   {({*/}
               {/*        field,*/}
               {/*        form: {touched, errors}*/}
               {/*     }: FieldProps) => (*/}
               {/*      <InputWithMask*/}
               {/*         guide={true}*/}
               {/*         mask={['+', /[1-9]/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]}*/}
               {/*         m={"22px 0 0"}*/}
               {/*         type={"tel"}*/}
               {/*         label={"Номер телефона"}*/}
               {/*         mod={errors.phone && touched.phone ? "error" : undefined}*/}
               {/*         errorMessage={errors.phone}*/}
               {/*         {...field}*/}
               {/*      />*/}
               {/*   )}*/}
               {/*</Field>*/}
               <Field name={"phone"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        m={"22px 0 0"}
                        type={"tel"}
                        label={"Номер телефона"}
                        mod={errors.phone && touched.phone ? "error" : undefined}
                        errorMessage={errors.phone}
                        {...field}
                     />
                  )}
               </Field>
               <Field name={"checked"}>
                  {({
                       field,
                    }: FieldProps) => (
                     <div className={styles.checkBox}>
                        <CheckBox {...field}/>
                        <div className={styles.checkBox__text}>
                           Я подтверждаю, что ознакомился и согласен с заявлением-согласием на обработку предоставленной
                           информации
                        </div>
                     </div>
                  )}
               </Field>

               <Button
                  type={"submit"}
                  m={"40px 0 0"}
                  mod={"primary"}
                  size={"large"}
                  width={"full-width"}>
                  Зарегистрироваться
               </Button>
               <div className={styles.label}>
                  У меня уже есть аккаунт
               </div>
               <NavLink to={"/main/sign_in/auth"}>
                  <Button
                     type={"button"}
                     width={"full-width"}
                     mod={"secondary"}
                     m={"16px 0 0"}
                     size={"large"}>Войти в систему</Button>
               </NavLink>
            </Form>
         )}


      </Formik>
   )
}