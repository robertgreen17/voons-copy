import React, {FC} from "react"
import {useDispatch} from "react-redux";
import styles from "./styles.module.scss"
import {Button} from "../../common/Button/Button";
import {Input} from "../../common/Inputs/Input";
import {NavLink, useHistory} from "react-router-dom";
import {SocialAuth} from "../SocialAuth/SocialAuth";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {AuthLoginReqBodyT} from "../../../api/auth-api";
import {login} from "../../../redux/auth-reducer/auth-reducer";

type ValuesT = {
   account: string
   password: string
}

export const AuthForm: FC = () => {

   const dispatch = useDispatch()
   const push =  useHistory().push

   const onSubmit = (values: ValuesT, {resetForm}: FormikValues) => {
      const payload: AuthLoginReqBodyT = {
         account: values.account,
         password: values.password
      }
      dispatch(login(payload, push))
   }

   return (
      <div className={styles.wrapper}>
         <SocialAuth/>
         <Formik
            initialValues={{
               account: "",
               password: "",
            } as ValuesT}
            onSubmit={onSubmit}
            className={styles.wrapper}>
            {({setFieldValue, values}) => (
               <Form className={styles.inputGroup}>
                  <Field name={"account"}>
                     {({
                          field,
                          form: {touched, errors}
                       }: FieldProps) => (
                        <Input
                           required
                           m={"0 0 20px"}
                           type={"text"}
                           label={"Логин"}
                           mod={errors.account && touched.account ? "error" : undefined}
                           errorMessage={errors.account}
                           {...field}
                        />
                     )}
                  </Field>
                  <Field name={"password"}>
                     {({
                          field,
                          form: {touched, errors}
                       }: FieldProps) => (
                        <Input
                           required
                           m={"0 0 25px"}
                           type={"password"}
                           label={"Пароль"}
                           mod={errors.password && touched.password ? "error" : undefined}
                           errorMessage={errors.password}
                           {...field}
                        />
                     )}
                  </Field>
                  <Button
                     type={"submit"}
                     mod={"primary"}
                     size={"large"}
                     width={"full-width"}
                     m={"0 0 20px"}>
                     Войти в систему
                  </Button>
                  <NavLink to={"/main/sign_in/reset/1"} className={styles.trouble}>
                     Забыл пароль
                  </NavLink>
               </Form>
            )}
         </Formik>
         <NavLink to={"/main/sign_in/reg/client"}>
            <Button mod={"secondary"} size={"large"} width={"full-width"}>
               Регистрация
            </Button>
         </NavLink>
      </div>
   )
}