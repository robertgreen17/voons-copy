import React, {FC, MouseEvent, useState, useEffect} from "react"
import styles from "./styles.module.scss"
import {RootStateT} from "../../../redux/store";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "../../common/Button/Button";
import {exit} from "../../../redux/app-reducer/app-reducer";
import {NavLink} from "react-router-dom";
import {Separator} from "../../common/Separator/Separator";
import {UserTypeT} from "../../../redux/user-reducer/user-reducer";

import {DropDownList} from "../../common/Navbar/DropDownList/DropDownList";
import {Notifications} from "../../common/Navbar/Notifications/Notifications";
import {useNotificationsListener} from "../../../hooks/listeners/useNotificationsListener";
import {formatMoney} from "../../../utils/string";
import {PaymentsModal} from "../../common/Navbar//PaymentsModal/PaymentsModal";
import {getNotifies} from "../../../redux/app-reducer/app-reducer";

type PropsT = {}

export const MobileSidebar: FC = () => {

   const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
   const avatar = useSelector((state: RootStateT) => state.user.userData?.avatar)
   const userId = useSelector((state: RootStateT) => state.user.userData?._id)
   const balance = useSelector((state: RootStateT) => state.user.userData?.balance)
   const notifications = useSelector((state: RootStateT) => state.app.notifications)
   const [isDropDownList, setIsDropDownList] = useState(false)
   const [isNotifications, setIsNotifications] = useState(false)
   const [isPayment, setIsPayment] = useState(false)
   const dispatch = useDispatch()

   const userType = useSelector((state: RootStateT) => state.user.type)

   const scrollTo = (id: string) => {
      const elem = document.getElementById(id)
      if (elem) {
         document.getElementById('closeSidebar')!.click()
         elem.scrollIntoView({behavior: "smooth"})
      }
   }

   useEffect(() => {
      dispatch(getNotifies())
   }, [])

   const closeWithdraw = () => {
      setIsPayment(false)
   }
   const toggleNotifications = () => {
      setIsDropDownList(false)
      if (isNotifications) {
         setIsNotifications(false)
      } else {
         setIsNotifications(true)
      }
   }

   useNotificationsListener(dispatch, userId?.$oid)

   return (
      <div className={styles.wrapper}>
         {isAuth && <>
            <div className={styles.login}>
               <div className={styles.notification}>
                  <img src="/icons/notifications.svg"
                       onClick={toggleNotifications}
                       alt="" className={styles.icon}/>
                  {notifications.length > 0 && <div className={styles.circle}>{notifications.length}</div>}
               </div>
               <div className={styles.balance} onClick={() => {
                  setIsPayment(true)
               }}>
                  <div>
                     {formatMoney(balance)} ₽
                  </div>
               </div>
               <img
                  src={avatar ? avatar : "/icons/userAvatar.png"}
                  alt=""
                  className={styles.avatar}
               />
               {isNotifications && <Notifications/>}
               {isPayment && <PaymentsModal isOpen={isPayment} closeModal={closeWithdraw}/>}
            </div>
         </>}
         <Separator/>
         <div className={styles.links}>
            <div className={styles.item} onClick={() => scrollTo('how-does-it-works')}>
               Как это работает
            </div>
            <NavLink className={styles.item} to={"/search"}>
               Найти специалиста
            </NavLink>
            <div className={styles.item} onClick={() => scrollTo('questions')}>
               Часто задаваемые вопросы
            </div>
         </div>
         <Separator/>
         <div className={styles.buttons}>

            {!isAuth && <> 
               <NavLink to={"/main/sign_in/auth"}>
                  <Button mod={"primary"} size={"large"} width={"full-width"}>
                     Войти
                  </Button>
               </NavLink>
               <NavLink to={"/main/sign_in/reg/client"}>
                  <Button mod={"secondary"} size={"large"} width={"full-width"}>
                     Регистрация
                  </Button>
               </NavLink> 
            </>}

            {isAuth && <> 
               <NavLink to={userType === UserTypeT.client ? "/dashboard/client" : "/dashboard/profi"}>
                  <Button mod={"primary"} size={"large"} width={"full-width"}>
                     Мой профиль
                  </Button>
               </NavLink>
               <NavLink to={"/settings"}>
                  <Button mod={"primary"} size={"large"} width={"full-width"}>
                     Настройки
                  </Button>
               </NavLink>
               <NavLink to={"/main/sign_in/reg/client"} className={styles.exit} onClick={() => dispatch(exit())}>
                  <Button mod={"secondary"} size={"large"} width={"full-width"}>
                     Выйти
                  </Button>
               </NavLink> 
            </>}
            {/**<NavLink to={"mailto:info@voons.ru"} onClick={() => document.getElementById('closeSidebar')!.click()}>
              <Button mod={"danger"} size={"large"} width={"full-width"}>Написать на Email</Button>
            </NavLink>*/}
            <a href="mailto:info@voons.ru" onClick={() => document.getElementById('closeSidebar')!.click()}>
               <Button mod={"danger"} size={"large"} width={"full-width"}>
                  Написать на Email
               </Button>
            </a>
         </div>
         <div className={styles.socLinks}>
            <a href="https://www.facebook.com/Voonsru-103177178353182" target={"_blank"} className={styles.link}>
               <span>
                  <i className={"fab fa-facebook-f"}/>
               </span>
            </a>
            <a href="https://instagram.com/voons.ru?igshid=1vksc5ytzmd4f" className={styles.link} target={"_blank"}>
               <span>
                  <i className={"fab fa-instagram"}/>
               </span>
            </a>
            <a href="https://t.me/voons_ru" className={styles.link} target={"_blank"}>
               <span>
                  <i className={"fab fa-telegram-plane"}/>
               </span>
            </a>
         </div>
      </div>
   )
}