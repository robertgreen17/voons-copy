import React, {FC} from "react"
import {useDispatch, useSelector} from "react-redux";
import styles from "../styles.module.scss"
import {useHistory} from "react-router-dom";
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {VerifyCodeReqBodyT} from "../../../../api/auth-api";
import {verifyCode} from "../../../../redux/auth-reducer/auth-reducer";
import {RootStateT} from "../../../../redux/store";
import {Input} from "../../../common/Inputs/Input";
import {Button} from "../../../common/Button/Button";

type ValuesT = {
   code: string
}

export const VerifyCode: FC = () => {

   const dispatch = useDispatch()
   const history = useHistory()
   const account = useSelector((state: RootStateT) => state.auth.account)

   const onSubmit = (values: ValuesT, {resetForm}: FormikValues) => {
      if (account) {
         const payload: VerifyCodeReqBodyT = {
            code: values.code,
            account: account
         }
         dispatch(verifyCode(payload, history.push, resetForm))
      }
   }

   return (
      <Formik
         initialValues={{
            code: "",
         } as ValuesT}
         onSubmit={onSubmit}
         className={styles.wrapper}>
         {({setFieldValue, values}) => (
            <Form className={styles.inputGroup}>
               <Field name={"code"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        m={"0 0 20px"}
                        type={"password"}
                        label={"Секретный код"}
                        mod={errors.account && touched.account ? "error" : undefined}
                        {...field}
                     />
                  )}
               </Field>
               <div className={styles.text}>
                  Введите код из письма или СМС
               </div>
               <Button
                  type={"submit"}
                  mod={"primary"}
                  size={"large"}
                  width={"full-width"}
                  m={"0 0 20px"}>
                  Сбросить пароль
               </Button>
            </Form>
         )}
      </Formik>
   )
}