import React, {FC} from "react"
import {useDispatch} from "react-redux";
import styles from "../styles.module.scss"
import {Field, FieldProps, Form, Formik} from "formik";
import {resetPassword} from "../../../../redux/auth-reducer/auth-reducer";
import {Input} from "../../../common/Inputs/Input";
import {Button} from "../../../common/Button/Button";
import {useHistory} from "react-router-dom";

type ValuesT = {
   account: string
}

export const ResetPassword: FC = () => {

   const dispatch = useDispatch()
   const history = useHistory()

   const onSubmit = (values: ValuesT) => {
      const payload: { account: string } = {
         account: values.account,
      }
      dispatch(resetPassword(payload, history.push))
   }

   return (
      <Formik
         initialValues={{
            account: "",
         } as ValuesT}
         onSubmit={onSubmit}
         className={styles.wrapper}>
         {({setFieldValue, values}) => (
            <Form className={styles.inputGroup}>
               <Field name={"account"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        m={"0 0 20px"}
                        type={"text"}
                        label={"Email или телефон"}
                        mod={errors.account && touched.account ? "error" : undefined}
                        errorMessage={errors.account}
                        {...field}
                     />
                  )}
               </Field>
               <div className={styles.text}>
                  Введите Email или номер телефона, чтобы получить секретный код для сброса пароля
               </div>
               <Button
                  type={"submit"}
                  mod={"primary"}
                  size={"large"}
                  width={"full-width"}
                  m={"0 0 20px"}>
                  Получить секретный код
               </Button>
            </Form>
         )}
      </Formik>
   )
}