import React, {FC} from "react"
import {useDispatch, useSelector} from "react-redux";
import styles from "../styles.module.scss"
import {Field, FieldProps, Form, Formik, FormikValues} from "formik";
import {doPassword} from "../../../../redux/auth-reducer/auth-reducer";
import {Input} from "../../../common/Inputs/Input";
import {Button} from "../../../common/Button/Button";
import {DoPasswordReqBodyT} from "../../../../api/auth-api";
import {RootStateT} from "../../../../redux/store";
import { useHistory } from "react-router-dom";

type ValuesT = {
   password: string
}

export const DoPassword: FC = () => {

   const dispatch = useDispatch()
   const push = useHistory().push
   const tempToken = useSelector((state: RootStateT) => state.auth.tempToken)

   const onSubmit = (values: ValuesT, {resetForm}: FormikValues) => {
      if (tempToken) {
         const payload: DoPasswordReqBodyT = {
            destory_sessions: false,
            password: values.password,
            temp_token: tempToken,
         }
         dispatch(doPassword(payload, push))
      }
   }

   return (
      <Formik
         initialValues={{
            password: "",
         } as ValuesT}
         onSubmit={onSubmit}
         className={styles.wrapper}>
         {({setFieldValue, values}) => (
            <Form className={styles.inputGroup}>
               <Field name={"password"}>
                  {({
                       field,
                       form: {touched, errors}
                    }: FieldProps) => (
                     <Input
                        m={"0 0 20px"}
                        type={"password"}
                        label={"Новый пароль"}
                        mod={errors.account && touched.account ? "error" : undefined}
                        {...field}
                     />
                  )}
               </Field>
               <div className={styles.text}>
                  Введите новый пароль
               </div>
               <Button
                  type={"submit"}
                  mod={"primary"}
                  size={"large"}
                  width={"full-width"}
                  m={"0 0 20px"}>
                  Изменить пароль
               </Button>
            </Form>
         )}
      </Formik>
   )
}