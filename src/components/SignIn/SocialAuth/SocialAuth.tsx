import React, {FC} from "react"
import {useDispatch} from "react-redux";
import styles from "./styles.module.scss"
import {doSocial} from "../../../redux/auth-reducer/auth-reducer";
import google from "../../../media/icons/socLinks/google.svg"
import vkDark from "../../../media/icons/socLinks/vk_dark.svg"
import fbAuth from "../../../media/icons/socLinks/fbAuth.svg"


export type SocialAuthT = {
    accountType?: number
}

export const SocialAuth: FC<SocialAuthT> = ({accountType}) => {

   const dispatch = useDispatch()

   const onLogin = (type: string) => {
      const payload = {
         type: type
      }
      dispatch(doSocial(payload))
   }

   return (
      <div className={styles.btnGroup}>
         <div className={`${styles.btn} ${styles.large}`}>
            <a href={accountType === 0 ? "https://api.voons.ru/oauth/redirect/google" : "https://api.voons.ru/oauth/redirect/google/specialist"} className={styles.btnItem} onClick={() => onLogin("google")}>
               <img src={google} alt="" className={styles.google}/>
               <div>Войти через Google</div>
            </a>
         </div>
         <div className={styles.btn}>
            <a href={accountType === 0 ? "https://api.voons.ru/oauth/redirect/google" : "https://api.voons.ru/oauth/redirect/vk/specialist"} className={`${styles.btnItem}`} onClick={() => onLogin("vk")}>
               <img src={vkDark} alt="" className={styles.vk}/>
            </a>
         </div>
         <div className={styles.btn}>
            <a href={accountType === 0 ? "https://api.voons.ru/oauth/redirect/facebook" : "https://api.voons.ru/oauth/redirect/facebook/specialist"} className={styles.btnItem} onClick={() => onLogin("facebook")}>
               <img src={fbAuth} alt=""/>
            </a>
         </div>
      </div>
   )
}
