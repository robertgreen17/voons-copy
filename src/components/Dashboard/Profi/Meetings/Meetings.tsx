import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"
import {LinearCalendar} from "./LinearCalendar/LinearCalendar";
import {Button} from "../../../common/Button/Button";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {getWorkTimes} from "../../../../redux/calendar-reducer/calendar-reducer";
import {Card} from "./Card/Card";
import {getDayMeetings} from "../../../../utils/calendar";
import {MeetingT} from "../../../../api/events-api";
import {getMeetings} from "../../../../redux/events-reducer/events-reducer";
import {formatMoney} from "../../../../utils/string";

type PropsT = {
    meetings: Array<MeetingT>
}

export const Meetings: FC<PropsT> = ({
                                         meetings
                                     }) => {

    const dispatch = useDispatch()
    const [date, setDate] = useState(new Date()) // текущая дата
    const [selectedDates, setSelectedDates] = useState([date] as Array<Date>) // выбранные дни
    const [currentMeetings, setCurrentMeetings] = useState(getDayMeetings(selectedDates[0], meetings))

    useEffect(() => {
        setCurrentMeetings(getDayMeetings(selectedDates[0], meetings))
    }, [selectedDates, meetings])

    // useMeetingsRefresher(meetings, dispatch, currentMeetings)

    useEffect(() => {
        let interval = setInterval(() => {
            dispatch(getMeetings({}, false))
        }, 5000)
        return () => clearInterval(interval)
    }, [])

    useEffect(() => {
        dispatch(getWorkTimes({}))
    }, [])

    return (
        <div className={styles.wrapper}>
            <div className={styles.row}>
                <div className={styles.title}>
                    Настройте свой рабочий график
                </div>
                <NavLink to={{pathname: "/dashboard/profi/calendar", state: selectedDates}}>
                    <Button mod={"secondary"} size={"medium"}>
                        Открыть календарь
                    </Button>
                </NavLink>
            </div>
            <LinearCalendar meetings={meetings}
                            date={date}
                            setDate={setDate}
                            setSelectedDates={setSelectedDates}
                            selectedDates={selectedDates}/>
            {currentMeetings.length > 0 ?
                <div className={styles.meetings}>
                    {currentMeetings.map((item, idx) => (
                        <Card key={idx}
                              isClient={false}
                              description={item.description}
                              isOnline={item.started}
                              eventId={item._id.$oid}
                              isReviewed={item.review}
                              amount={formatMoney(item.price.value * 0.9)}
                              duration={item.duration}
                              firstName={item.client.first_name}
                              lastName={item.client.last_name}
                              timeStart={item.time_start}
                              timeEnd={item.time_end}
                              isConfirmed={item.confirmed}
                        />
                    ))}
                </div> :
                <div className={styles.message}>
                    <img src="/icons/calendar/clock.svg" alt=""/>
                    На этот день встреч не запланировано
                </div>
            }
        </div>
    )
}