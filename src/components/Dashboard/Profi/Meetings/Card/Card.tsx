import React, {FC, useState} from "react";
import styles from "./styles.module.scss"
import {Tag} from "../../../../common/Tag/Tag";
import {NavLink} from "react-router-dom";
import {cancelMeeting, confirmMeeting} from "../../../../../redux/meeting-reducer/meeting-reducer";
import {NotificationT} from "../../../../../hooks/listeners/useNotificationsListener";
import app from "../../../../../redux/app-reducer/app-reducer";
import {getMeetings} from "../../../../../redux/events-reducer/events-reducer";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../../redux/store";
import {UserTypeT} from "../../../../../redux/user-reducer/user-reducer";
import {Modal} from "../../../../common/Modal/Modal";
import Popup from "reactjs-popup";

type PropsT = {
    duration: string
    firstName: string
    lastName: string
    amount: string
    isOnline?: boolean
    role?: string
    date?: string
    timeStart?: string
    timeEnd?: string
    eventId: string
    userId?: number
    isConfirmed?: boolean
    description?: string
    isReviewed?: boolean
    isFinished?: boolean
    isClient: boolean
}

export const Card: FC<PropsT> = ({
                                     role,
                                     amount,
                                     firstName,
                                     lastName,
                                     duration,
                                     isOnline,
                                     timeEnd,
                                     timeStart,
                                     date,
                                     eventId,
                                     userId,
                                     isConfirmed,
                                     description,
                                     isReviewed,
                                     isFinished,
                                     isClient
                                 }) => {

    const dispatch = useDispatch()
    const notifications = useSelector((state: RootStateT) => state.app.notifications)
    const userType = useSelector((state: RootStateT) => state.user.type)
    const [isModal, setIsModal] = useState(false)

    const onCancelMeeting = (eventId: string) => {
        dispatch(cancelMeeting(eventId))
        const data: Array<NotificationT> = notifications.map(m => {
            if (m.payload.event_id === eventId) {
                return {...m, type: "canceled"}
            }
            return m
        })
        dispatch(app.actions.setNotifications(data))
    }

    const onConfirmMeeting = (eventId: string) => {
        dispatch(confirmMeeting(eventId))
        const data: Array<NotificationT> = notifications.map(m => {
            if (m.payload.event_id === eventId) {
                return {...m, type: "confirmed"}
            }
            return m
        })
        dispatch(app.actions.setNotifications(data))
        dispatch(getMeetings({}))
    }
    const reviewCode = <a href={`/review/${eventId}`}>
        <Tag mod={"warning"}>Нужен отзыв</Tag>
    </a>;
    return (
        <>
            {isModal &&
            <Popup open={isModal}
                   overlayStyle={{background: "rgba(22, 24, 53, 0.4)"}}
                   closeOnDocumentClick
                   onClose={() => setIsModal(false)}>
                <Modal size={"small"}>
                    <div className={styles.modal}>
                        <div className={styles.modal__label}>
                            Тема встречи:
                        </div>
                        <div className={styles.modal__title}>
                            {description}
                        </div>
                    </div>
                </Modal>
            </Popup>
            }
            {date && <div className={styles.date}>
                {date}
            </div>}
            <div
                className={`${styles.wrapper} animate__animated animate__fadeIn ${
                    isOnline ? styles.red : isConfirmed && !isReviewed && isFinished && isClient ? styles.orange : ""
                }`}
                onClick={() => setIsModal(true)}>
                <div className={styles.time}>
                    <div className={styles.label}>
                        {isOnline && eventId ?
                            <Tag mod={"danger"}>·
                                Онлайн</Tag> : !isReviewed && isConfirmed && isFinished && isClient ?
                                reviewCode : timeStart + " — " + timeEnd
                        }
                    </div>
                    <div className={styles.duration}>
                        {duration} минут
                    </div>
                </div>
                {userId
                    ? <NavLink to={`/profi/${userId}`} className={styles.client}>
                        {firstName} {lastName}
                        {role && <div className={styles.role}>
                            {role}
                        </div>}
                    </NavLink>
                    : <div className={styles.client}>
                        {firstName} {lastName}
                        {role && <div className={styles.role}>
                            {role}
                        </div>}
                    </div>}
                <div className={styles.amount}>
                    {amount}
                </div>
                <div className={styles.btn}>
                    <CardButtons isOnline={isOnline}
                                 isClient={isClient}
                                 isConfirmed={isConfirmed}
                                 isFinished={isFinished}
                                 isReviewed={isReviewed}
                                 eventId={eventId}
                                 userType={userType}
                                 onConfirmMeeting={onConfirmMeeting}
                                 onCancelMeeting={onCancelMeeting}/>
                </div>
            </div>
        </>
    )
}


type CardButtonsPropsT = {
    isOnline?: boolean
    isConfirmed?: boolean
    isReviewed?: boolean
    isFinished?: boolean
    isClient: boolean
    eventId: string
    userType?: UserTypeT | null
    onConfirmMeeting: (eventId: string) => void
    onCancelMeeting: (eventId: string) => void
}

export const CardButtons: FC<CardButtonsPropsT> = ({
                                                       userType,
                                                       isConfirmed,
                                                       eventId,
                                                       isOnline,
                                                       onConfirmMeeting,
                                                       onCancelMeeting,
                                                       isReviewed,
                                                       isFinished
                                                   }) => {


    if (isOnline && eventId) {
        return (
            // eslint-disable-next-line react/jsx-no-target-blank
            <a href={`/meeting/${eventId}`} target={"_blank"} onClick={() => {
            }} className={styles.img}>
                <img src={"/icons/dashboard/videoBtn.svg"} alt="" className={styles.img}/>
            </a>
            // <NavLink to={`/meeting/${eventId}`} className={styles.img}>
            //    <img src="/icons/dashboard/videoBtn.svg" alt="" className={styles.img}/>
            // </NavLink>
        )
    } else {
        if (isConfirmed && !isFinished) {
            return (
                <span className={styles.confirmed}>
               <i className="fas fa-check"/>
               <div className={styles.tooltip}>
                  Встреча подтверждена
               </div>
            </span>
            )
        } else if (!isConfirmed) {
            if (userType === UserTypeT.profi) {
                return (
                    <>
                  <span onClick={() => onConfirmMeeting(eventId)}>
                                 <i className="fas fa-plus"/>
                              </span>
                        <span onClick={() => onCancelMeeting(eventId)}>
                                 <i className="fas fa-minus"/>
                              </span>
                    </>
                )
            } else {
                return <span className={styles.confirmed}>
               <i className="fas fa-user-clock"/>
               <div className={styles.tooltip}>
                  Ожидает подтверждения
               </div>
            </span>
            }
        } else {
            if (!isReviewed) {
                return (
                    <a href={`/review/${eventId}`} className={styles.img}>
                        <i className={`fas fa-star ${styles.reviewImg}`}/>
                    </a>
                )
            } else {
                return (
                    <a className={styles.img}>
                    </a>
                )

            }
        }

        // if (isConfirmed && !isFinished) {
        //     return (
        //         <span className={styles.confirmed}>
        //        <i className="fas fa-check"/>
        //        <div className={styles.tooltip}>
        //           Встреча подтверждена
        //        </div>
        //     </span>
        //     )
        // } else {
        //     if (!isReviewed && isConfirmed) {
        //         return (
        //             <a href={`/review/${eventId}`} className={styles.img}>
        //                 <i className={`fas fa-star ${styles.reviewImg}`}/>
        //
        //                 {/*<img src={"/icons/dashboard/videoBtn.svg"} alt="" className={styles.img}/>*/}
        //             </a>
        //         )
        //     } else {
        //         if (userType === UserTypeT.profi) {
        //             return (
        //                 <>
        //           <span onClick={() => onConfirmMeeting(eventId)}>
        //                          <i className="fas fa-plus"/>
        //                       </span>
        //                     <span onClick={() => onCancelMeeting(eventId)}>
        //                          <i className="fas fa-minus"/>
        //                       </span>
        //                 </>
        //             )
        //         } else {
        //             return <span className={styles.confirmed}>
        //        <i className="fas fa-user-clock"/>
        //        <div className={styles.tooltip}>
        //           Ожидает подтверждения
        //        </div>
        //     </span>
        //         }
        //     }
    }

}

// return (
//    <>
//       {
//          isOnline && eventId ?
//             <>
//                <NavLink to={`/meeting/${eventId}`} className={styles.img}>
//                   <img src="/icons/dashboard/videoBtn.svg" alt="" className={styles.img}/>
//                </NavLink>
//             </> :
//             // <img src="/icons/dashboard/videoBtnGrey.svg" alt="" className={styles.img}/>
//             isConfirmed
//                ? <span className={styles.confirmed}>
//                            <i className="fas fa-check"/>
//                         </span>
//                : userType === UserTypeT.profi ? <>
//                            <span onClick={() => onConfirmMeeting(eventId)}>
//                               <i className="fas fa-plus"/>
//                            </span>
//                   <span onClick={() => onCancelMeeting(eventId)}>
//                               <i className="fas fa-minus"/>
//                            </span>
//                </>
//                : <img src="/icons/dashboard/videoBtnGrey.svg" alt="" className={styles.img}/>
//       }
//    </>
// )
