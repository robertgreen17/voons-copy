import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"
import "../../../../common/Calendar/styles.scss"
import {
   getCurrentWeek,
   getMonthData,
   getWeeksCount,
   markMeetingDays,
   markSelectedDays,
   markWorkDays,
   WEEKDAYS_RU
} from "../../../../../utils/calendar";
import {Week} from "../../../../common/Calendar/Week";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../../../redux/store";
import {Separator} from "../../../../common/Separator/Separator";
import {MeetingT} from "../../../../../api/events-api";

type PropsT = {
   meetings: Array<MeetingT>
   selectedDates: Array<Date>
   setSelectedDates: (dates: Array<Date>) => void
   date: Date
   setDate: (date: Date) => void
}

export const LinearCalendar: FC<PropsT> = ({
                                              meetings,
                                              selectedDates,
                                              setSelectedDates,
                                              date,
                                              setDate
                                           }) => {


   const workTimes = useSelector((state: RootStateT) => state.calendar.workTimes) // раписание специалиста

   const [monthData, setMonthData] = useState( // данные о всем месяце с правильной подсветкой дней
      markMeetingDays(markSelectedDays(getMonthData(date.getFullYear(), date.getMonth()), selectedDates), meetings)
   )
   const [currentWeek, setCurrentWeek] = useState(getCurrentWeek() - 1)

   const onSelectDay = (date: Date) => {
      if (selectedDates.indexOf(date) !== -1) {
         // убираем пометку с дня, если он уже был выбран
         setSelectedDates(selectedDates.filter(day => day !== date))
      } else {
         setSelectedDates([date])
      }
   }
   useEffect(() => { // помечаем выбранные дни при изменении selectedDays
      setMonthData(markSelectedDays(monthData, selectedDates))
   }, [selectedDates, date])

   useEffect(() => { // помечаем рабочие дни при изменении расписания или выбранных дней
      setMonthData(markWorkDays(monthData, workTimes))
   }, [workTimes, selectedDates, date])

   useEffect(() => { // помечаем дни встреч при измении массива встреч или выбранных дней
      setMonthData(markMeetingDays(monthData, meetings))
   }, [meetings, selectedDates, date])


   const onNextWeek = () => {
      if (monthData[currentWeek + 1] !== undefined) {
         setCurrentWeek(currentWeek + 1)
      } else {
         const newDate = new Date(date.getFullYear(), date.getMonth() + 1)
         setDate(newDate)
         setMonthData(getMonthData(newDate.getFullYear(), newDate.getMonth()))
         setCurrentWeek(0)
      }
   }

   const onPrevWeek = () => {
      if (monthData[currentWeek - 1]) {
         setCurrentWeek(currentWeek - 1)
      } else {
         const newDate = new Date(date.getFullYear(), date.getMonth() - 1)
         setDate(newDate)
         setMonthData(getMonthData(newDate.getFullYear(), newDate.getMonth()))
         setCurrentWeek(getWeeksCount(newDate.getFullYear(), newDate.getMonth()) - 1)
      }
   }

   return (
      <div className={styles.wrapper}>
         <div className={styles.labels}>
            {WEEKDAYS_RU.map((day, idx) => (
               <span key={idx}>{day}</span>
            ))}
         </div>
         <div className={styles.row}>
            <button className={`${styles.nav} ${styles.left}`}
                    onClick={onPrevWeek}
            >
               <img src="/icons/calendar/arrow.svg" alt=""/>
            </button>
            <div className={styles.week}>
               <Week week={monthData[currentWeek]} onDayClick={onSelectDay}/>
            </div>

            <button onClick={onNextWeek} className={styles.nav}>
               <img src="/icons/calendar/arrow.svg" alt=""/>
            </button>
         </div>
         <Separator m={"24px 0 20px"}/>
      </div>
   )
}