import React, {FC, useEffect, useState} from "react";
import Popup from "reactjs-popup";
import {Modal} from "../../../../common/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../../redux/store";
import {EditProfileValuesT} from "../../../../Settings/EditProfile/EditProfile";
import {Form, Formik, FormikHelpers} from "formik";
import {isEqualObjProps} from "../../../../../utils/objects";
import {ChangeAccountReqBodyT} from "../../../../../api/user-api";
import {changeAccount, getUserData, switchFirstMeetingFree} from "../../../../../redux/user-reducer/user-reducer";
import styles from "../styles.module.scss";
import {EditAvatarMini} from "../../../../Settings/EditAvatar/Minified/EditAvatarMini";
import {Separator} from "../../../../common/Separator/Separator";
import {EditMainInfoMini} from "../../../../Settings/EditMainInfo/Minified/EditMainInfoMini";
import {Button} from "../../../../common/Button/Button";
import {EditWorkInfoMini} from "../../../../Settings/EditWorkInfo/Minified/EditWorkInfoMini";
import {EditWorkMini} from "../../../../Settings/EditWork/Minified/EditWorkMini";
import {WEEKDAYS_RU} from "../../../../../utils/calendar";
import {CalendarMini} from "../Minified/CalendarMini";
import {calendarApi} from "../../../../../api/calendar-api";
import {CircularProgress} from "@material-ui/core";
import {SetTimesPriceReqBodyT} from "../../../../../api/events-api";
import {setTimesPrice, setTimesPriceWithoutMessage} from "../../../../../redux/events-reducer/events-reducer";

type PropsT = {
    closeModal: any
}

export type EditProfileValuesMiniT = {
    image: string
    name: string
    surname: string
    country: string
    // city: string
    tags: Array<string>
    langs: Array<string>
    about: string
    newTag: string
    newLang: string
    isFreeMeeting: boolean
    15: string,
    30: string,
    60: string,
    // times_price: any
    // uploadImage: null | File ,
}

export const    Body: FC<PropsT> = ({closeModal}) => {
    const [time, setTime] = useState(
        [
            [],
            [],
            [],
            []
        ]
    )

    const [load, setLoad] = useState({
        title: <span>Сохранить</span>,
        disabled: true
    })

    const setDisable = (disabled: boolean) => {
        setLoad({
            title: <span>Cохранить</span>,
            disabled: disabled
        })
    }


    const [modalState, setModalState] = useState('mainInfo');
    const userType = useSelector((state: RootStateT) => state.user.type)
    const userData = useSelector((state: RootStateT) => state.user.userData)
    const prices = useSelector((state: RootStateT) => state.user.userData?.times_price)
    const dispatch = useDispatch()

    const onSubmit = (values: EditProfileValuesMiniT, {resetForm}: FormikHelpers<EditProfileValuesMiniT>) => {
        // if (values.uploadImage === null && initialValues.image !== "") {
        //    dispatch(setAvatar(values.uploadImage))
        // }
        // if (values.uploadImage && values.uploadImage !== initialValues.uploadImage) {
        //    debugger
        //    // если есть изображение и оно не равно старому, то делаем dispatch
        //    dispatch(setAvatar(values.uploadImage))
        // }
        const timesPrice: SetTimesPriceReqBodyT = {
            time: [15, 30, 60],
            price: [values['15'], values['30'], values['60']],
        }
        dispatch(setTimesPriceWithoutMessage(timesPrice))


        if (!isEqualObjProps(values, initialValues, ["country", "name", "surname", "langs", "about", "tags"])) {
            const payload: ChangeAccountReqBodyT = {
                country: values.country,
                first_name: values.name,
                last_name: values.surname,
                lang: values.langs,
                tags: values.tags,
            }
            if (values.about.length > 0) payload["work_info"] = values.about
            dispatch(changeAccount(payload))
            if (values.isFreeMeeting !== initialValues.isFreeMeeting) {
                dispatch(switchFirstMeetingFree({free: values.isFreeMeeting}))
            }
            if (load.disabled) return;
            else {
                let data = calendarApi.setTime(time);
                let load = <CircularProgress size={"18px"}/>;
                setLoad({
                    title: load,
                    disabled: true
                })
                setTimeout(() => {
                    closeModal()
                }, 1000)
            }
        }
    }


    let initialValues: EditProfileValuesMiniT = {
        image: userData?.avatar ? userData?.avatar : "/icons/userAvatar.png",
        // uploadImage: null,
        country: userData?.country ? userData?.country : "",
        name: userData?.first_name ? userData?.first_name : "",
        surname: userData?.last_name ? userData?.last_name : "",
        tags: userData?.tags ? userData?.tags.map(t => typeof t === "string" || !t ? t : t.name_ru) : [],
        langs: userData?.lang ? userData?.lang : [],
        about: userData?.work_info ? userData?.work_info : "",
        newTag: "",
        newLang: "",
        isFreeMeeting: userData?.free_first ? userData?.free_first : false,
        15: prices ? prices[15] : 0,
        30: prices ? prices[30] : 0,
        60: prices ? prices[60] : 0,
        // times_price: userData?.times_price ? userData?.times_price: ['0', '0', '0']
    } as EditProfileValuesMiniT

    useEffect(() => {
        initialValues = {
            image: userData?.avatar ? userData?.avatar : "/icons/userAvatar.png",
            // uploadImage: null,
            country: userData?.country ? userData?.country : "",
            name: userData?.first_name ? userData?.first_name : "",
            surname: userData?.last_name ? userData?.last_name : "",
            tags: userData?.tags ? userData?.tags.map(t => typeof t === "string" || !t ? t : t.name_ru) : [],
            langs: userData?.lang ? userData?.lang : [],
            about: userData?.work_info ? userData?.work_info : "",
            isFreeMeeting: userData?.free_first ? userData?.free_first : false,
            15: prices ? prices[15] : 0,
            30: prices ? prices[30] : 0,
            60: prices ? prices[60] : 0,
            newTag: "",
            newLang: "",
        } as EditProfileValuesMiniT
    }, [userData])

    if (modalState === "mainInfo") {
        return (
            <Modal size={"small"} full>
                <div className={styles.container}>
                    <div className={styles.title}>Добро пожаловать в Voons.!</div>
                    <div className={styles.subtitle}>Для того, чтобы начать работу, заполните свой профиль.</div>
                </div>
                <div className={styles.body_wrapper}>
                    <Formik
                        initialValues={initialValues}
                        onSubmit={onSubmit}
                    >
                        {({setFieldValue, values}) => (
                            <Form>
                                <EditAvatarMini image={values.image}
                                                setImage={(image: string) => setFieldValue("image", image)}
                                                setUploadImage={(image: File | null) => setFieldValue("uploadImage", image)}
                                />
                                <Separator m={"0 0 39px"}/>
                                <EditMainInfoMini/>
                            </Form>
                        )}
                    </Formik>
                </div>
                <Separator m={"0 0 0"}/>
                <div className={styles.btns_wrapper}>
                    <div className={styles.btns_container_first}>
                        <Button className={styles.actionButton + " button primary large"} mod={"primary"} size={"large"}
                                type={"button"} p={"0.8rem 3.2rem 0.8rem"}
                                onClick={() => {
                                    setModalState("workInfo")
                                }}>Далее</Button>
                    </div>
                </div>

            </Modal>
        )
    }

    if (modalState === "workInfo") {
        // @ts-ignore
        return (
            <Modal size={"small"} full>
                <div className={styles.container}>
                    <div className={styles.title}>Настройки рабочего профиля</div>
                </div>
                <div className={styles.body_wrapper}>
                    <Formik
                        initialValues={initialValues}
                        onSubmit={onSubmit}
                    >
                        {({setFieldValue, values}) => (
                            <Form>
                                <EditWorkInfoMini
                                    setFieldValue={setFieldValue}
                                    tags={values.tags}
                                    langs={values.langs}
                                    newTag={values.newTag}
                                    newLang={values.newLang}
                                />
                                <Separator m={"0 0 20px"}/>
                                <EditWorkMini isFreeMeeting={values.isFreeMeeting}
                                              setFieldValue={setFieldValue}
                                />
                            </Form>
                        )}
                    </Formik>
                </div>
                <Separator m={"0 0 0"}/>
                <div className={styles.btns_wrapper}>
                    <div className={styles.btns_container}>
                        <Button className={styles.actionButton + " button danger-light large"} mod={"danger-light"}
                                size={"large"} type={"button"} p={"0.8rem 3.2rem 0.8rem"}
                                onClick={() => {
                                    setModalState("mainInfo")
                                }}>Назад</Button>
                        <Button className={styles.actionButton + " button primary large"} mod={"primary"} size={"large"}
                                type={"button"} p={"0.8rem 3.2rem 0.8rem"}
                                onClick={() => {
                                    setModalState("timeInfo")
                                }}>Далее</Button>
                    </div>
                </div>
            </Modal>
        )
    }

    if (modalState === "timeInfo") {
        return (
            <Modal size={"small"} full>
                <div className={styles.container}>
                    <div className={styles.title}>Настройки рабочего профиля</div>
                </div>
                <div className={styles.body_wrapper}>
                    <Formik
                        initialValues={initialValues}
                        onSubmit={onSubmit}
                    >
                        {({setFieldValue, values}) => (
                            <Form>
                                <CalendarMini setDisable={setDisable} time={time} setTime={setTime}/>
                                <Separator m={"0 0 0"}/>
                                <div className={styles.btns_wrapper}>
                                    <div className={styles.btns_container}>
                                        <Button className={styles.actionButton + " button danger-light large"}
                                                mod={"danger-light"} size={"large"} type={"button"}
                                                p={"0.8rem 3.2rem 0.8rem"} onClick={() => {
                                            setModalState("workInfo")
                                        }}>Назад</Button>

                                        <Button className={styles.actionButton + " button primary large"}
                                                mod={"primary"} disabled={load.disabled} size={"large"} type={"submit"}
                                                p={"0.8rem 3.2rem 0.8rem"}
                                        >{load.title}</Button>
                                    </div>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </Modal>
        )
    }

    return (
        <p>ok</p>
    )

}