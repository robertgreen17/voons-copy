import React, {FC, useEffect, useState} from 'react';
import Popup from "reactjs-popup";
import {Body} from "./Body/Body";
import {getUserData} from "../../../../redux/user-reducer/user-reducer";
import {useDispatch} from "react-redux";

type PropsT = {
    isOpen: boolean | undefined
}

export const FirstInfoEntry: FC<PropsT> = ({isOpen}) => {
    const [opened, setOpened] = useState(true);
    const [body, setBody] = useState(<p></p>);
    setTimeout(() => {
        setBody(<Body closeModal={() => setOpened(false)}/>)
    }, 1000);
    return (
        <Popup open={isOpen && opened}
               overlayStyle={{background: "rgba(22, 24, 53, 0.4)"}}
               closeOnDocumentClick={false}
               disabled={false}
        >
            {body}
        </Popup>
    )
}