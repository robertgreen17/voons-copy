import React, {FC, useState} from "react";
import styles from "../styles.module.scss";
import {Item} from "./Item";


type PropsT = {
    time: any,
    setTime: any
    setDisable: any
};

export const CalendarMini: FC<PropsT> = ({time, setTime, setDisable}) => {
    // const [time, setTime] = useState(
    //     [
    //         [],
    //         [],
    //         [],
    //         []
    //     ]
    // )

    const setItemInTime = (row: number, column: number, time: any) => {
        let newArr = time;
        newArr[row].push(column);
        setTime(newArr);
    }

    const removeItemFromTime = (row: number, column: number, time: any) => {
        let newArr = time;
        let index = newArr[row].indexOf(column)
        newArr[row].splice(index, 1);
        setTime(newArr);
    }


    let calendar_items = [];
    let week_items = [];
    let initial_data = [
        {
            name: 'Утро',
            time: '06.00 - 12.00'
        },
        {
            name: 'День',
            time: '12.00 - 18.00'
        },
        {
            name: 'Вечер',
            time: '18.00 - 21.00'
        },
        {
            name: 'Ночь',
            time: '21.00 - 00.00'
        }
    ]
    for (let i = 0; i < 4; i++) {
        for (let j = -1; j < 7; j++) {
            if (j === -1) {
                calendar_items.push(
                    <div className={styles.column_begin}>
                        <div className={styles.wrapper}>
                            <div className={styles.name}>{initial_data[i].name}</div>
                            <div className={styles.time}>{initial_data[i].time}</div>
                        </div>
                    </div>
                )
            } else {
                calendar_items.push(<Item setDisable={setDisable} row={i} column={j} setItemInTime={setItemInTime}
                                          time={time}
                                          removeItemFromTime={removeItemFromTime}/>)
            }
        }
    }
    let week = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];
    week_items.push(<div></div>);
    for (let item of week) {
        week_items.push(<div className={styles.week_item}>{item}</div>);
    }

    return (
        // <p>loneliness</p>
        <div className={styles.calendar_wrapper}>
            <div className={styles.title}>
                Выберите удобное рабочее время
                <img src="/icons/calendar_mini_info.svg"
                     alt={""}
                />
            </div>
            <div className={styles.calendar}>
                {week_items}
                {calendar_items}
            </div>
            <div className={styles.subtitle}>
                <img src="/icons/calendar_mini_info.svg"
                     alt={""}
                />
                Вы можете изменить данную информацию в настройках личного кабинета
            </div>
        </div>
    );
}