import React, {FC, useEffect, useState} from "react";
import styles from "../styles.module.scss";

type PropsT = {
    row: number,
    column: number,
    setItemInTime: (row: number, column: number, time: any) => void,
    removeItemFromTime: (row: number, column: number, time: any) => void,
    time: any
    setDisable: any
};

interface State {
    value: any,
    isActive: boolean,
    style: string
}

export const Item: FC<PropsT> = ({row, column, setItemInTime, removeItemFromTime, time, setDisable}) => {
    let initial_state: State = {
        value: <span>+</span>,
        isActive: false,
        style: styles.item
    };
    const [body, setBody] = useState(initial_state);

    useEffect(() => {
        if (!body.isActive) {
            setStyle(styles.item, <span>+</span>);
        } else {
            setStyle(styles.item_active, <img  className={styles.check} src="/icons/check-solid.svg"
                                              alt={""}/>);
        }
    }, [body.isActive])

    const setStyle = (style: string, value: any) => {
        setBody({...body, style: style, value: value});
    }
    const setState = () => {
        if (!body.isActive) {
            setItemInTime(row, column, time);
        } else {
            removeItemFromTime(row, column, time);
        }
        setBody({...body, isActive: !body.isActive});
        if (time[0].length > 0 || time[1].length > 0 || time[2].length > 0 || time[3].length > 0) {
            setDisable(false);
        } else {
            setDisable(true);
        }
        console.log(time);
    }
    return (
        <div className={body.style} onClick={setState}>{body.value}</div>);

}