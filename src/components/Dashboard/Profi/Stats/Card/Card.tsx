import React, {FC} from "react";
import styles from "./styles.module.scss"

type PropsT = {
   imgLink: string
   value: string | number
   label: string
   onClick?: () => void
}

export const Card: FC<PropsT> = ({
                                    imgLink,
                                    value,
                                    label,
                                    onClick
                                 }) => {

   return (
      <div className={`${styles.wrapper} ${onClick && styles.clickable}`} onClick={onClick}>
         <div className={styles.row}>
            <img src={imgLink} alt="" className={styles.icon}/>
            <div className={styles.column}>
               <div className={styles.value}>
                  {value}
               </div>
               <div className={styles.label}>
                  {label}
               </div>
            </div>
         </div>

      </div>
   )
}