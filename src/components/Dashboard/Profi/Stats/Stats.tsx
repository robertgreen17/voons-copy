import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"
import {Card} from "./Card/Card";
import {History} from "../History/History";
import {PaymentsModal} from "../../../common/Navbar/PaymentsModal/PaymentsModal";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import {formatMoney} from "../../../../utils/string";
import {setIn} from "formik";
import {StatT} from "../../../../api/user-api";

type PropsT = {}

export const Stats: FC<PropsT> = ({}) => {
    const [isHistory, setIsHistory] = useState(false)
    const [isWithdraw, setIsWithdraw] = useState(false)
    let currentStat = useSelector((state: RootStateT) => state.user.userData?.stat!)
    const [statIndex, setStatIndex] = useState(3);
    const [stat, setStat] = useState({
        meetings_passed: currentStat === undefined ? 0 : currentStat.day.meetings_passed,
        earned: currentStat === undefined ? 0 : currentStat.day.earned
    });

    useEffect(() => {
        currentStat = {
            day: {
                meetings_passed: 0,
                earned: 0
            },
            week: {
                meetings_passed: 0,
                earned: 0
            },
            month: {
                meetings_passed: 0,
                earned: 0
            }
        }
    }, [])

    useEffect(() => {

        if (currentStat.day !== undefined) {
            if (statIndex == 1) {
                setStat(currentStat.day)
            }
            if (statIndex == 7) {
                setStat(currentStat.week)
            }
            if (statIndex == 30) {
                setStat(currentStat.month)
            }
        }
    }, [statIndex])
    const closeHistory = () => {
        setIsHistory(false)
    }
    const closeWithdraw = () => {
        setIsWithdraw(false)
    }

    return (
        <div className={styles.stats}>
            <History onClose={closeHistory} isOpen={isHistory}/>
            <PaymentsModal isOpen={isWithdraw} closeModal={closeWithdraw}/>
            <div className={styles.row}>
                <div className={styles.label}>
                    Статистика
                </div>
                <select onChange={(event) => {
                    setStatIndex(Number(event.target.value))
                }} className={styles.selector}>
                    <option value={1}>за 1 дней</option>
                    <option value={7}>за 7 дней</option>
                    <option value={30}>за месяц</option>
                </select>
            </div>
            <div className={styles.row}>
                <Card imgLink={"/icons/dashboard/wallet.svg"}
                      label={"Вы заработали"}
                      onClick={() => setIsWithdraw(true)}
                      value={formatMoney(stat ? stat.earned * 0.9 : undefined) + " ₽"}/>
                <Card onClick={() => setIsHistory(true)}
                      imgLink={"/icons/dashboard/stream.svg"}
                      label={"Вы провели консультаций"}
                      value={stat?.meetings_passed ? stat.meetings_passed : 0}/>
            </div>
        </div>
    )
}