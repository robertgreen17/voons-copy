import React, {FC, useCallback, useEffect, useRef, useState} from 'react';
import styles from "./styles.module.scss";
import Popup from "reactjs-popup";
import {Modal} from "../../../common/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import {getMeetings} from "../../../../redux/events-reducer/events-reducer";
import {MEETINGS_LIMIT, USERS_LIMIT} from "../../../../redux/constants";
import {GetMeetingsReqBody} from "../../../../api/events-api";
import {formatMoney} from "../../../../utils/string";

type PropsT = {
   isOpen: boolean
   onClose: () => void
}

export const History: FC<PropsT> = ({onClose, isOpen}) => {

   const meetings = useSelector((state:RootStateT) => state.events.meetings.filter(m => m.finished))
   const [isLoading, setIsLoading] = useState(false)
   const [hasMore, setHasMore] = useState(true)
   const [offset, setOffset] = useState(0)
   const dispatch = useDispatch()

   useEffect(() => {
      dispatch(getMeetings({}))
   }, [])

   // const observer = useRef<any>()
   // const lastMeeting = useCallback((node: HTMLTableRowElement) => {
   //    if (isLoading) return
   //    if (observer && observer.current) observer.current.disconnect()
   //    observer.current = new IntersectionObserver(async entries => {
   //       if (entries[0].isIntersecting && hasMore)  {
   //          await fetchMeetings(offset + MEETINGS_LIMIT)
   //          setOffset(prev => prev + MEETINGS_LIMIT)
   //       }
   //    })
   //    if (node) observer.current.observe(node)
   // }, [isLoading, offset, observer])
   //
   //
   // const fetchMeetings = async (offset: number) => {
   //    setIsLoading(true)
   //    const body: GetMeetingsReqBody = {
   //       limit: USERS_LIMIT,
   //       offset: offset,
   //    }
   //    await dispatch(getMeetings(body, false, true, setHasMore))
   //    setIsLoading(false)
   // }

   return (
      <Popup open={isOpen}
             overlayStyle={{background: "rgba(22, 24, 53, 0.4)"}}
             closeOnDocumentClick
             onClose={onClose}>
         <Modal size={"big"}>
            <table className={styles.table}>
               <thead>
               <tr>
                  <th>Клиент</th>
                  <th>Дата</th>
                  <th>Время</th>
                  <th>Цена</th>
               </tr>
               </thead>
               <tbody>
               {meetings.filter(h => h.finished).map((item, idx) => (
                  <tr key={idx}>
                     <td>{item.client.first_name + " " + item.client.last_name}</td>
                     <td>{item.date}</td>
                     <td>{item.time_start}</td>
                     <td>{formatMoney(item.price.value * 0.9)}</td>
                  </tr>
               ))}
               </tbody>
            </table>
         </Modal>
      </Popup>
   )
}