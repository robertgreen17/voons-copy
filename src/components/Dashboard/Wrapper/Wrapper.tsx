import React, {FC} from "react";
import styles from "./styles.module.scss"

type PropsT = {
    type?: string
}

export const Wrapper: FC<PropsT> = ({children, type}) => {

    if (type === undefined) {
        return (
            <div className={styles.wrapper}>
                {/*<img src="/images/dashboardBg.svg" alt="" className={styles.background}/>*/}
                {children}
            </div>
        )
    }

    if (type === "onBoard") {
        return (
            <div className={styles.wrapperOnBoard}>
                {/*<img src="/images/dashboardBg.svg" alt="" className={styles.background}/>*/}
                {children}
            </div>
        )
    }

    if (type === 'onBoardCleint') {
        return (
            <div className={styles.wrapperOnBoardClient}>
                {/*<img src="/images/dashboardBg.svg" alt="" className={styles.background}/>*/}
                {children}
            </div>
        )
    }


    return (
        <p>Ok</p>
    );

}