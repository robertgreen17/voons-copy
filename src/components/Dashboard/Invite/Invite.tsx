import React, {FC, useEffect, useState} from 'react';
import styles from "./styles.module.scss"
import {useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {Button} from '../../common/Button/Button';
import {ToolTip} from "../../common/ToolTip/ToolTip";

type PropsT = {}

export const Invite: FC<PropsT> = ({}) => {
   const id = useSelector((state: RootStateT) => state.user.userData?._id?.$oid)
   const [isCopied, setIsCopied] = useState(false)
   const [link, setLink] = useState(document.location.origin + "/i/" + id)

   useEffect(() => {
      setLink(document.location.origin + "/i/" + id)
   }, [id])

   if (!id) return <></>

   const onCopy = async () => {
      await navigator.clipboard.writeText(link)
      setIsCopied(true)
      setTimeout(() => {
         setIsCopied(false)
      }, 3000)
   }

   return (
      <div className={styles.wrapper}>
         <div className={styles.label}>
            Ваша реферальная ссылка
         </div>
         <div className={styles.container}>
            <span className={styles.text}>Приведи друга и получи 250 рублей на счет Voons!</span>
            <ToolTip w={"200px"}>
               Сумма будет начислена за каждого нового клиента, который зарегистрируется по вашей ссылке и оплатит консультацию у любого специалиста
            </ToolTip>
            <div className={styles.row}>
               <input value={link} readOnly={true}/>
               <Button mod={"primary"} disabled={isCopied} onClick={onCopy} size={"small"}>
                  <i className={"fas fa-clone"}/>
               </Button>
            </div>
         </div>

      </div>
   )
}