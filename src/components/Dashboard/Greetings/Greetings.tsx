import React, {FC} from "react";
import styles from "./styles.module.scss"

type PropsT = {}

export const Greetings: FC<PropsT> = ({children}) => {

    return (
        <div className={styles.container}>
            {children}
        </div>
    )
}