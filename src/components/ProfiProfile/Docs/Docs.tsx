import React, {FC} from "react";
import styles from "./styles.module.scss"
import {getFileMimeTypeByUrl, getPathArrayFromDocs} from "../../../utils/files";
import 'swiper/swiper.scss';
import {useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {isEmptyObj} from "../../../utils/objects";


export const Docs: FC = () => {

   const files = useSelector((state: RootStateT) => state.search.currentUserData?.files)

   if (!files || isEmptyObj(files)) return <></>

   return (
      <div className={styles.wrapper}>
         <div className={styles.title}>
            Документы и сертификаты
         </div>
         <div className={styles.row}>
            {getPathArrayFromDocs(files).map((item, idx) => (
               <div className={styles.doc} key={idx}>
                  <div className={styles.dark}>
                     <div className={styles.label}>
                        {/*<a href={`/${item}`} target="_blank" rel="noopener noreferrer" className={styles.link}>*/}
                        {/*   Открыть*/}
                        {/*</a>*/}
                        <a href={item} target="_blank" rel="noopener noreferrer" className={styles.link}>
                           Открыть
                        </a>
                     </div>
                  </div>
                  {/*<embed src={`https://api.visterio.com/vcoons/${item}`} type={getFileMimeTypeByUrl(item)}/>*/}
                  <embed src={item} type={getFileMimeTypeByUrl(item)} className={styles.preview}/>
               </div>
            ))}
         </div>
      </div>
   )
}


const docs = ["/icons/settings/doc.svg", "/icons/settings/doc.svg", "/icons/settings/doc.svg", "/icons/settings/doc.svg", "/icons/settings/doc.svg",]