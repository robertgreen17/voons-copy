import React, {FC, useState} from "react";
import styles from "./styles.module.scss"
import {Tag} from "../../common/Tag/Tag";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store"
import {sortBy} from 'lodash';
import {declReviews, formatMoney} from "../../../utils/string";
import user from "../../../redux/user-reducer/user-reducer";

export const ProfileCard: FC = () => {

    const userData = useSelector((state: RootStateT) => state.search.currentUserData)
    const me = useSelector((state: RootStateT) => state.user.userData)

    if (!userData) return <></>

    const {last_name, reviews_count, first_name, times_price, work_info, tags, reviews, rating, verified_specialist, lang, social, services, uses_free} = userData


    return (
        <div className={styles.wrapper}>
            {uses_free !== undefined && uses_free.indexOf(me !== undefined && me !== null && me._id !== undefined ? me?._id.$oid : "Not Auth") == -1 ? (
                <div className={styles.freeMeet}>
                    У вас есть бесплатная встреча с этим специалистом!
                </div>
            ) : ("")}
            <div className={styles.header}>
                <div className={styles.name}>
                    {first_name} {last_name}
                    {verified_specialist && <img src="/icons/checkCircle.svg" alt=""/>}
                </div>
                {/*<div className={styles.social}>*/}
                {/*   {social && social.vk && <a href={social.vk} target="_blank">*/}
                {/*      <img src="/icons/socLinks/vk.svg" alt=""/>*/}
                {/*   </a>}*/}
                {/*   {social && social.instagram && <a href={social.instagram} target="_blank">*/}
                {/*      <img src="/icons/socLinks/inst.svg" alt=""/>*/}
                {/*   </a>}*/}
                {/*   {social && social.fb && <a href={social.fb} target="_blank">*/}
                {/*      <img src="/icons/socLinks/fb.svg" alt=""/>*/}
                {/*   </a>}*/}
                {/*</div>*/}
            </div>
            <div className={styles.row}>
                {rating?.total && <div className={styles.statItem}>
                    <img src="/icons/star.svg" alt="" className={styles.icon}/>
                    {rating?.total}
                </div>}
                <div className={styles.statItem}>
                    <img src="/icons/chatBubble.svg" alt="" className={styles.icon}/>
                    {reviews_count ? reviews_count + " " + declReviews(reviews_count) : "Нет отзывов"}
                </div>
            </div>

            <div className={styles.label}>
                Обо мне
            </div>
            {tags && <div className={styles.tags}>
                {tags.map((item, idx) => (
                    <div key={idx}>
                        <Tag size={"small"} mod={"secondary"}>{typeof item === "string" || !item ? item : item.name_ru}</Tag>
                    </div>
                ))}
            </div>}


            {work_info && <div dangerouslySetInnerHTML={{__html: work_info}} className={styles.text}/>}
            {lang && lang.length > 0 && <>
                <div className={styles.label}>
                    Языки
                </div>
                <div className={styles.lang}>
                    {lang.map((l, key) => (
                        <span key={key}>
                     {l + (key === lang?.length - 1 ? "" : ", ")}
                  </span>
                    ))}
                </div>
            </>}

            {services && services.length > 0 && <>
                <div className={styles.label}>
                    Мои услуги
                </div>
                {services && <div className={styles.tags}>
                    {services.map((item, idx) => (
                        <div key={idx}>
                            <Tag size={"small"} mod={"secondary"}>{item}</Tag>
                        </div>
                    ))}
                </div>}
            </>}
            <hr/>

            {times_price && <>
                <div className={styles.label}>
                    Цены
                </div>
                <div className={styles.prices}>
                    {sortBy(times_price, "time").map((item, idx) => (
                        <div className={styles.item} key={item.time}>
                            <div className={styles.price}>
                                {uses_free !== undefined && uses_free.indexOf(me !== undefined && me !== null && me._id !== undefined ? me?._id.$oid : "Not Auth") == -1
                                    ? (
                                        <div className={`${styles.price} ${styles.free}`}>
                                            <span>{formatMoney(+item.price)} ₽</span> <span>0.00 ₽</span>
                                        </div>

                                    ) : (
                                        formatMoney(+item.price)
                                    )
                                }
                            </div>
                            <div className={styles.label}>
                                {item.time} минут
                            </div>
                        </div>
                    ))}
                </div>
            </>}
        </div>
    )
}