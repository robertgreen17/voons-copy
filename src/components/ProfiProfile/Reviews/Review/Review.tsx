import React, {FC, useState} from "react";
import styles from "./styles.module.scss"
import {cutter} from "../../../../utils/string";

type PropsT = {
   name: string
   desc: string
   imgLink: string
   rating: number
   date?: string
}

export const Review: FC<PropsT> = ({
                                      imgLink,
                                      desc,
                                      name,
                                      rating,
                                      date,
                                   }) => {

   const [isShowMore, setIsShowMore] = useState(false)
   const cutDesc = cutter(220, desc)

   return (
      <div className={styles.wrapper}>
         <img src={imgLink ? imgLink : "/icons/userAvatar.png"} alt="" className={styles.avatar}/>
         <div className={styles.column}>
            <div className={styles.name}>
               {name}
            </div>
            <div className={styles.row}>
               <div className={styles.rating}>
                  {Array.from(Array(rating)).map((item, idx) => (
                     <img src="/icons/star.svg" alt="" className={styles.icon} key={idx}/>
                  ))}
                  {/*<span>*/}
                  {/*   {rating}*/}
                  {/*</span>*/}
               </div>
               {date && <div className={styles.date}>
                  {date}
               </div>}
            </div>
            <div className={styles.desc}>
               {!isShowMore && cutDesc}
               {isShowMore && desc}
            </div>
            {cutDesc && <div className={styles.showMore} onClick={() => setIsShowMore(!isShowMore)}>
               {!isShowMore && desc.length !== cutDesc.length && "Развернуть"}
               {isShowMore && desc.length !== cutDesc.length && "Свернуть"}
				</div>}
         </div>
      </div>
   )
}