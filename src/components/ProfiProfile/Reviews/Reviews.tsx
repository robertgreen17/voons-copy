import React, {FC} from "react";
import styles from "./styles.module.scss"
import {Separator} from "../../common/Separator/Separator";
import {Review} from "./Review/Review";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {declReviews} from "../../../utils/string";

export const Reviews: FC = () => {

   const userData = useSelector((state: RootStateT) => state.search.currentUserData)

   if (!userData) return <></>

   const {rating, reviews, reviews_count} = userData

   return (
      <div className={styles.wrapper}>
         <div className={styles.title}>
            Отзывы
         </div>
         <div className={styles.row}>
            {rating?.total && <div className={styles.rating}>
               {rating.total}
               <span>
                  из 5
               </span>
            </div>}
            <div className={styles.label}>
               {reviews_count ? reviews_count + " " + declReviews(reviews_count) : "Нет отзывов"}
            </div>
         </div>
         <Separator m={"18px 0 0"}/>
         {reviews && <div className={styles.reviews}>
            {reviews.filter(r => r.moderated).map((item, idx) => (
               <div key={idx}>
                  <Review imgLink={item.avatar}
                          name={item.first_name + " " + item.last_name}
                          desc={item.text}
                          date={item.date}
                          rating={item.rating_set}
                  />
                  {idx !== reviews.length - 1 && <Separator m={"33px 0 29px"}/>}
               </div>
            ))}
			</div>}

      </div>
   )
}