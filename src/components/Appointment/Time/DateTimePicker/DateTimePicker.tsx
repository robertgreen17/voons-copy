import React, {FC, useState} from "react";
import styles from "./styles.module.scss"
import Moment from "react-moment";
import moment from "moment";
import {localeRu} from "../../../../locales/momentLocales";
import {DAY_MILLISECONDS, getTimestampNow, WEEK_MILLISECONDS} from "../../../../utils/time";
import {TimesT} from "../../../../api/events-api";
import Collapse from '@kunukn/react-collapse';

type PropsT = {
   timeTable: Array<TimesT>
   setActiveTime: (time: number | null) => void
   activeTime: number | null
   timeInterval: Array<number>
   setTimeInterval: (interval: Array<number>) => void
}

export const DateTimePicker: FC<PropsT> = ({
                                              timeTable,
                                              setActiveTime,
                                              activeTime,
                                              timeInterval,
                                              setTimeInterval,
                                           }) => {

   const [openedIndex, setOpenedIndex] = useState(0)

   moment.locale('ru', localeRu);

   return (
      <div className={styles.wrapper}>
         <div className={styles.title}>
            Выберите дату и время
         </div>
         <div className={styles.header}>
            <button className={`${styles.btn}`}
               // отключаем кнопку "назад", если начало интервала - сегодняшний день
                    disabled={timeInterval[0] - getTimestampNow() < DAY_MILLISECONDS}
                    onClick={() => setTimeInterval(
                       timeInterval.map(item => item - WEEK_MILLISECONDS) // прыгаем на неделю назад
                    )}
            >
               <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"
                    className={styles.prev}>
                  <path
                     d="M10 3.33334L8.825 4.50834L13.475 9.16667H3.33333V10.8333H13.475L8.825 15.4917L10 16.6667L16.6667 10L10 3.33334Z"
                     fill="#5465E7"/>
               </svg>
               Назад
            </button>
            <div>
               <Moment format="MMMM, DD" locale={"ru"}>
                  {timeInterval[0]}
               </Moment>
               {" — "}
               <Moment format="MMMM, DD" locale={"ru"}>
                  {timeInterval[1]}
               </Moment>
            </div>
            <button className={styles.btn}
                    onClick={() => setTimeInterval(
                       timeInterval.map(item => item + WEEK_MILLISECONDS) // на неделю вперед
                    )}
            >
               Вперед
               <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"
                    className={styles.next}>
                  <path
                     d="M10 3.33334L8.825 4.50834L13.475 9.16667H3.33333V10.8333H13.475L8.825 15.4917L10 16.6667L16.6667 10L10 3.33334Z"
                     fill="#5465E7"/>
               </svg>
            </button>
         </div>
         {timeTable.length === 0 ?
            <div className={styles.message}>На этой неделе специалист занят...</div>
            : <div className={styles.container}>
            {timeTable.map((day, idx) => {
               if (day.times.length > 0) return (
               <div className={styles.time} key={idx}>
                  <div className={`${styles.label} ${idx === openedIndex && styles.active}`} onClick={() => setOpenedIndex(idx)}>
                     {day.date} <span><i className={"fas fa-arrow-down"}/></span>
                  </div>
                  <Collapse isOpen={idx === openedIndex}  transition="height 300ms cubic-bezier(.4, 0, .2, 1)">
                     <div className={styles.row}>
                        {day.times.map((time, idx) => (
                           <div className={`${styles.item}
                         ${time.timestamp === activeTime ? styles.active : ""}`}
                                key={idx}
                                onClick={() => setActiveTime(time.timestamp)}
                           >
                              {time.human}
                           </div>
                        ))}
                     </div>
                  </Collapse>
                  {/*<div className={styles.row}>*/}
                  {/*   {day.times.map((time, idx) => (*/}
                  {/*      <div className={`${styles.item}*/}
                  {/*       ${time.timestamp === activeTime ? styles.active : ""}`}*/}
                  {/*           key={idx}*/}
                  {/*           onClick={() => setActiveTime(time.timestamp)}*/}
                  {/*      >*/}
                  {/*         {time.human}*/}
                  {/*      </div>*/}
                  {/*   ))}*/}
                  {/*</div>*/}
               </div>)
            })}
         </div>}

      </div>
   )
}