import React, {FC} from "react";
import styles from "./styles.module.scss"
import Moment from "react-moment";
import {Button} from "../../../common/Button/Button";
import {Separator} from "../../../common/Separator/Separator";
import {PriceT} from "../../../../api/events-api";
import {useHistory} from "react-router-dom";
import {THREE_HOURS_MS} from "../../../../utils/calendar";

type PropsT = {
   isEdit?: boolean
   btnText: string
   duration: number | null
   time: number | null
   times_price: Array<PriceT>
   userId: number
   isAllowedSubmitting: boolean
   onSubmit?: () => void
   isFreeMeeting: boolean
}

export const Submit: FC<PropsT> = ({
                                      time,
                                      duration,
                                      times_price,
                                      userId,
                                      btnText,
                                      isEdit,
                                      isAllowedSubmitting,
                                      onSubmit,
                                      isFreeMeeting,
                                   }) => {

   const push = useHistory().push

   return (
      <div className={styles.wrapper}>
         <Separator m={"18px 0"}/>
         <div className={styles.row}>
            {duration && <div className={styles.title}>
               Консультация, {duration} минут
            </div>}
            {isEdit && <div className={styles.edit} onClick={() => push(`/profi/${userId}/appointment/1`)}>
					<img src="/icons/editPencil.svg" alt=""/>
					Изменить
				</div>}
         </div>

         {time && duration && <div className={styles.row}>
				<Moment format="MMMM, DD, dddd" locale={"ru"}>
               {time * 1000 - THREE_HOURS_MS}
				</Moment>
				<div className={styles.amount}>
					Cтоимость {isFreeMeeting ? "0.00" : times_price.filter(item => item.time === duration)[0].price} ₽
				</div>
			</div>}
         <Button width={"full-width"}
                 type={"submit"}
                 mod={"primary"}
                 size={"large"} disabled={!isAllowedSubmitting} onClick={onSubmit}>
            {btnText}
         </Button>
      </div>
   )
}