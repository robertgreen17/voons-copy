import React, {FC, useState} from "react";
import styles from "./styles.module.scss"
import {DurationPicker} from "./DurationPicker/DurationPicker";
import {DateTimePicker} from "./DateTimePicker/DateTimePicker";
import {PriceT, TimesT, TimeT} from "../../../api/events-api";
import {Submit} from "./Submit/Submit";
import {useHistory} from "react-router-dom";
import {PaymentsModal} from "../../common/Navbar/PaymentsModal/PaymentsModal";

type PropsT = {
   duration: number | null
   times_price?: Array<PriceT>
   setDuration: (duration: number) => void
   timeTable: Array<TimesT> | null
   setActiveTime: (time: number | null) => void
   activeTime: number | null
   timeInterval: Array<number>
   setTimeInterval: (interval: Array<number>) => void
   userId: number
   isFreeMeeting: boolean
}
export type TimeTableT = Array<Array<TimeT>>

export const Time: FC<PropsT> = ({
                                    duration,
                                    activeTime,
                                    times_price,
                                    timeTable,
                                    timeInterval,
                                    setTimeInterval,
                                    setActiveTime,
                                    setDuration,
                                    userId,
                                    isFreeMeeting,
                                 }) => {

   const push = useHistory().push



   if (!times_price || !timeTable) return <div>
      Запись к специалисту пока недоступна...
   </div>

   return (
      <div className={styles.wrapper}>
         <div>
            {isFreeMeeting && <div className={styles.free}>
	            У вас есть бесплатная встреча с этим специалистом!
            </div>}
            <DurationPicker isFreeMeeting={isFreeMeeting}
                            activeDuration={duration}
                            setDuration={setDuration} timesPrice={times_price}/>
            <DateTimePicker timeTable={timeTable}
                            setActiveTime={setActiveTime}
                            activeTime={activeTime}
                            timeInterval={timeInterval}
                            setTimeInterval={setTimeInterval}
            />
         </div>
         <Submit
            isFreeMeeting={isFreeMeeting}
            onSubmit={() => {
               if (activeTime) push(`/profi/${userId}/appointment/2`)
            }}
            isAllowedSubmitting={!!activeTime && !!duration}
            btnText={"Продолжить"}
            userId={userId}
            time={activeTime}
            times_price={times_price}
            duration={duration}/>

      </div>
   )
}