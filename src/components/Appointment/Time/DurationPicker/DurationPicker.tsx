import React, {FC} from "react";
import styles from "./styles.module.scss"
import {PriceT} from "../../../../api/events-api";
import {sortBy} from 'lodash';

type PropsT = {
   timesPrice: Array<PriceT>
   setDuration: (duration: number) => void
   activeDuration: number | null
   isFreeMeeting?: boolean
}

export const DurationPicker: FC<PropsT> = ({
                                              timesPrice,
                                              setDuration,
                                              activeDuration,
                                              isFreeMeeting,
                                           }) => {

   return (
      <div className={styles.wrapper}>
         <div className={styles.title}>
            Длительность консультации
         </div>
         <div className={styles.row}>
            {sortBy(timesPrice, "time").map((item, idx) => (
               <div key={idx}
                    className={`${styles.priceTime} ${activeDuration === item.time ? styles.active : ""}`}
                    onClick={() => setDuration(item.time)}
               >
                  <div className={styles.time}>
                     {item.time} минут
                  </div>
                  <div className={`${styles.price} ${isFreeMeeting && styles.free}`}>
                     <span>{item.price} ₽</span> {isFreeMeeting && <span>0.00 ₽</span>}
                  </div>
               </div>
            ))}
         </div>
      </div>
   )
}