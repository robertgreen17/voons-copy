import React, {FC} from "react";
import styles from "./styles.module.scss"
import {Button} from "../../common/Button/Button";
import {NavLink} from "react-router-dom";

type PropsT = {
   userId: number
}

export const Success: FC<PropsT> = ({userId}) => {

   return (
      <div className={styles.wrapper}>
         <div className={styles.container}>
            <img src="/icons/successCircle.svg" alt="" className={styles.circle}/>
            <div className={styles.title}>
               Вы записались на консультацию
            </div>
            <div className={styles.label}>
               Ожидайте подтверждения заявки специалистом.
               Вы получите уведомление, как только ваша заявка будет рассмотрена.
            </div>
            <NavLink to={"/dashboard/client"}>
               <Button size={"large"} mod={"primary"} iconPos={"icon-right"} m={"44px auto 25px"}>
                  Вернуться на главную
                  <img src="/icons/arrowIcon.svg" alt=""/>
               </Button>
            </NavLink>
            <NavLink to={`/profi/${userId}`}>
               <div className={styles.close}>
                  Закрыть
               </div>
            </NavLink>

         </div>
      </div>
   )
}