import React, {FC} from "react";
import styles from "./styles.module.scss"
import {TextArea} from "../../common/Inputs/Input";
import {Submit} from "../Time/Submit/Submit";
import {PriceT} from "../../../api/events-api";
import {useForm} from "react-hook-form";

type PropsT = {
   duration: number | null
   time: number | null
   times_price?: Array<PriceT>
   userId: number
   onCreateEvent: (desc: string) => void
   isFreeMeeting: boolean
}

type FormData = {
   desc: string
}


export const Task: FC<PropsT> = ({
                                    duration,
                                    userId,
                                    times_price,
                                    time,
                                    onCreateEvent,
                                    isFreeMeeting,
                                 }) => {

   const {register, handleSubmit, getValues, watch} = useForm<FormData>();

   watch("desc") // включили наблюдение за полем desc

   const onSubmit = handleSubmit((data: FormData) => {
      onCreateEvent(data.desc)
   });

   if (!times_price) return <div>
      Запись к специалисту пока недоступна...
   </div>

   return (
      <form onSubmit={onSubmit} className={styles.wrapper}>
         <div>
            <div className={styles.title}>
               Опишите задачу
            </div>
            <TextArea placeholder={"Задача"}
                      name={"desc"}
                      rows={10}
                      maxLength={500}
                      inputRef={register}/>
            <div className={styles.label}>
               Описание не больше 500 символов
            </div>
         </div>
         <Submit
            isAllowedSubmitting={!!getValues().desc}
            isEdit={true}
            btnText={"Записаться на консультацию"}
            userId={userId}
            times_price={times_price}
            duration={duration}
            time={time}
            isFreeMeeting={isFreeMeeting}
         />
      </form>
   )
}