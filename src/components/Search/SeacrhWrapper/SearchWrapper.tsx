import React, {FC} from "react";
import {Page} from "../../common/Page/Page";
import {SearchBar} from "../SearchBar/SearchBar";
import {Footer} from "../../common/Footer/Footer";
import styles from "./styles.module.scss"

export const SearchWrapper: FC = ({children}) => {

	return (
   	<Page isNavbar={true} bg={"#F5F6FA"} navbarBg={"#FFF"} isId={'searching'}>
      	<SearchBar/>
         <div className={styles.container}>
            {children}
         </div>
      	<Footer/>
   	</Page>
	)
}