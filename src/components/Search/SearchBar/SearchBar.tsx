import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"
import {SearchGroup} from "../../common/SearchGroup/SearchGroup";
import {useDispatch} from "react-redux";
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {useHistory} from "react-router-dom";
import {USERS_LIMIT} from "../../../redux/constants";

type PropsT = {}

export const SearchBar: FC<PropsT> = () => {

   const dispatch = useDispatch()
   const push = useHistory().push

   const [isReset, setIsReset] = useState(false)

   const showAll = () => {
      dispatch(search.actions.setIsResetSearch(true))
      setIsReset(true)
      dispatch(search.actions.setCurrentSearchData(null))
      dispatch(search.actions.setInputValue(""))
      dispatch(find({limit: USERS_LIMIT, offset: 0}, () => push("/searching")))
   }

   return (
      <div className={styles.wrapper}>
         <div className={styles.title}>
            Найти специалиста
         </div>
         <div className={styles.row}>
            <SearchGroup isReset={isReset}/>
            <div className={styles.showAll} onClick={showAll}>
               Показать всех
            </div>
         </div>
      </div>
   )
}