import React, {FC} from "react";
import {ScrollCards} from "../../common/ScrollCards/ScrollCards";
import {SearchGroup} from "../../common/SearchGroup/SearchGroup";
import {SwiperSlide} from "swiper/react";
import styles from "./styles.module.scss";
import customStyles from './styles.module.scss';
import {CategoryCard} from "../../common/CategoryCard/CategoryCard";
import {Page} from "../../common/Page/Page";
import {useSelector} from "react-redux";
import {getCategoriesWithLimit} from "../../../redux/selectors";

type PropsT = {}

export const CategoriesDesctop: FC<PropsT> = ({}) => {

    const categories = useSelector(getCategoriesWithLimit(20))

    return (
            <ScrollCards title={"Тысячи специалистов помогут вам"}
                             control={<SearchGroup/>}
            id = {"search"}>
            {categories.filter((item) => item.big != "").map((item, idx) => (
                <SwiperSlide key={idx} className={styles.specialistHelperCard}>
                    <CategoryCard title={item.title}
                                  label={item.name}
                                  desc={item.text}
                                  profCount={item.users_count}
                                  imgLink={item.big}
                                  key={idx}
                                  tags={item.tags}
                                  bg={"#F5F6FA"}
                    />
                </SwiperSlide>
            ))}
        </ScrollCards>
    );
}

const cardsData = [
    {
        title: "Помощь с юридическими вопросами",
        label: "Закон и право",
        desc: "Подготовка документов, защита в суде, составление исков",
        profCount: 152,
        link: "/",
        imgLink: "/images/lawCardBg.svg",
    },
    {
        title: "Помощь с подбором стиля",
        label: "Красота и мода",
        desc: "Подбор стиля, консультирование по вопросам с модой",
        profCount: 91,
        link: "/",
        imgLink: "/images/styleCardBg.svg",
    },
    {
        title: "Помощь с юридическими вопросами",
        label: "Закон и право",
        desc: "Подготовка документов, защита в суде, составление исков",
        profCount: 152,
        link: "/",
        imgLink: "/images/lawCardBg.svg",
    },
    {
        title: "Помощь с юридическими вопросами",
        label: "Закон и право",
        desc: "Подготовка документов, защита в суде, составление исков",
        profCount: 152,
        link: "/",
        imgLink: "/images/lawCardBg.svg",
    },
]