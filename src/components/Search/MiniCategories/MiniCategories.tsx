import React, {FC, useEffect, useRef} from "react";
import styles from "./styles.module.scss"
import {MiniCategory} from "../../common/MiniCategories/MiniCategory/MiniCategory";
import {useDispatch, useSelector} from "react-redux";
import {getCategories} from "../../../redux/search-reducer/search-reducer";
import {getCategoriesWithLimit} from "../../../redux/selectors";
import {SwiperSlide} from 'swiper/react';
import Swiper, {SwiperRefNode} from "react-id-swiper";

type PropsT = {
   limit?: number
   isLabel?: boolean
   isBlueCard?: boolean
   isSmall?: boolean,
   countElems?: number
}

export const MiniCategories: FC<PropsT> = ({limit, isSmall, isLabel, isBlueCard, countElems}) => {

   const swiper = useRef<SwiperRefNode>(null);

   const onNextSlide = () => {
      if (swiper.current && swiper.current.swiper) {
         swiper.current.swiper.slideNext()
      }
   }
   const onPrevSlide = () => {
      if (swiper.current && swiper.current.swiper) {
         swiper.current.swiper.slidePrev()
      }
   }

   const params = {
      spaceBetween: 20,
      containerClass: 'swiper-container ' + styles.slider,
      freeMode: true,
      breakpoints: {
         768: {
            spaceBetween: 30
         },
      }
   }

   const dispatch = useDispatch()
   const categories = useSelector(getCategoriesWithLimit(limit))

   useEffect(() => {
      dispatch(getCategories())
   }, [])

   return (
      <div className={styles.wrapper + " categoriesContainer"}>
         {isLabel && <div className={styles.label}>
            Популярные категории
         </div>}
         <div className={styles.container}>
            {!isSmall && <span onClick={onPrevSlide} className={`${styles.arrow} ${styles.left}`}>
               <i className={"fas fa-arrow-left"}/>
            </span>}
            {categories.length > 0 && <Swiper slidesPerView={"auto"} {...params} ref={swiper}>
               {categories.map((item, idx) => (
                  <SwiperSlide key={idx} className={styles.categorySlide}>
                     <MiniCategory imgLink={item.icon}
                                   tags={item.tags}
                                   title={item.name}
                                   isBlueCard={isBlueCard}
                     />
                  </SwiperSlide>
               ))}
            </Swiper>}
            {!isSmall && <span onClick={onNextSlide} className={`${styles.arrow} ${styles.right}`}>
               <i className={"fas fa-arrow-right"}/>
            </span>}
         </div>
      </div>
   )
}

const data = [
   {
      title: "Красота и мода",
      imgLink: "/icons/miniCategories/beauty.svg"
   },
   {
      title: "Наука и образование",
      imgLink: "/icons/miniCategories/science.svg"
   },
   {
      title: "Фитнес и спорт",
      imgLink: "/icons/miniCategories/sport.svg"
   },
   {
      title: "Закон и право",
      imgLink: "/icons/miniCategories/law.svg"
   },
   {
      title: "Красота и мода",
      imgLink: "/icons/miniCategories/beauty.svg"
   },
   {
      title: "Наука и образование",
      imgLink: "/icons/miniCategories/science.svg"
   },
   {
      title: "Фитнес и спорт",
      imgLink: "/icons/miniCategories/sport.svg"
   },
   {
      title: "Закон и право",
      imgLink: "/icons/miniCategories/law.svg"
   },
]