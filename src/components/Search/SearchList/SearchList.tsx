import React, {FC, useCallback, useEffect, useRef, useState} from "react";
import styles from "./styles.module.scss"
import {SpecialistCard} from "../../Main/Specialists/SpecialistCard/SpecialistCard";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {FindReqBodyT, TagT} from "../../../api/search-api";
import search, {find, getCategories, infinityFind} from "../../../redux/search-reducer/search-reducer";
import {getBestItemByComparison} from "../../../utils/arrays";
import {PriceT} from "../../../api/events-api";
import {MyMiniLoader} from "../../common/MyLoader/MyMiniLoader";
import {USERS_LIMIT} from "../../../redux/constants";
import {MiniCategories} from "../MiniCategories/MiniCategories";

type PropsT = {}

export const SearchList: FC<PropsT> = () => {

   const users = useSelector((state: RootStateT) => state.search.searchingData)
   const isReset = useSelector((state: RootStateT) => state.search.isResetSearch)
   const offset = useSelector((state: RootStateT) => state.search.offset)
   const categories = useSelector((state: RootStateT) => state.search.categories)
   const currentSearchData = useSelector((state: RootStateT) => state.search.currentSearchData)
   const [isLoading, setIsLoading] = useState(false)
   const [hasMore, setHasMore] = useState(true)
   const dispatch = useDispatch()

   useEffect(() => {
      if (isReset) {
         setHasMore(true)
         dispatch(search.actions.setOffset(0))
         dispatch(search.actions.setIsResetSearch(false))
      }
   }, [isReset])

   useEffect(() => {
      window.scrollTo({top: 0})
   }, [])

   const observer = useRef<any>()
   const lastUser = useCallback((node: HTMLDivElement) => {
      if (isLoading) return
      if (observer && observer.current) observer.current.disconnect()
      observer.current = new IntersectionObserver(async entries => {
         if (entries[0].isIntersecting && hasMore)  {
            await fetchUsers(offset + USERS_LIMIT, currentSearchData, true)
            dispatch(search.actions.setOffset(offset + USERS_LIMIT))
         }
      })
      if (node) observer.current.observe(node)
   }, [currentSearchData, isLoading, offset, observer])


   useEffect(() => {
      if (users.length === 0 && !currentSearchData) {
         dispatch(find({limit: USERS_LIMIT}))

      }
   }, [])

   useEffect(() => {
      if (categories.length === 0) {
         dispatch(getCategories())
      }
   }, [])

   const onCategoryClick = (tags: Array<TagT>) => {
      const body: FindReqBodyT = {
         category: tags ? tags.map(c => c._id.$oid) : [],
         limit: USERS_LIMIT,
      }
      dispatch(find(body))
   }

   const fetchUsers = async (offset: number, currentSearchData: FindReqBodyT | null, isLoader: boolean) => {
      if (isLoader) setIsLoading(true)
      const body: FindReqBodyT = {
         category: currentSearchData?.category,
         name: currentSearchData?.name,
         limit: USERS_LIMIT,
         offset: offset,
      }
      await dispatch(infinityFind(body, setIsLoading, setHasMore))
   }

   const isActiveCategory = (tags: Array<TagT>) => {
      if (currentSearchData?.category) {
         for (let i = 0; i < tags.length; ++i) {
            if (tags[i]._id.$oid !== currentSearchData.category[i]) {
               return false
            }
         }
         return true
      } else {
         return false
      }
   }

   return (
      <div className={styles.wrapper}>
         <div className={styles.navWrap}>

            <nav className={styles.nav}>
               {categories.map((item, idx) => (
                  <div className={styles.item} key={idx} onClick={() => onCategoryClick(item.tags)}>
                     <img src={item.icon} alt=""/>
                     <div className={`${styles.label} 
                     ${isActiveCategory(item.tags) && styles.active}`}>
                        {item.name}
                     </div>
                  </div>
               ))}
            </nav>

            <div className={styles.navScrollable}>
               <div className={styles.header}>
                  <div className={styles.title}>
                     Популярные категории
                  </div>
               </div>

               <MiniCategories isBlueCard={false}/>
            </div>

         </div>
         <div className={styles.container}>
            {users.length > 0
               ? users.map((item, idx) => (
                  <SpecialistCard
                     minPrice={getBestItemByComparison<PriceT>(
                        item.times_price,
                        (a, b) => a.price < b.price ? a : b)?.price}
                     tags={item.tags}
                     userId={item.user_id}
                     size={"big"}
                     imgLink={item.avatar}
                     services={item.services}
                     desc={item.work_info}
                     name={item.first_name + " " + item.last_name}
                     key={idx}
                     rating={item.rating}
                     reviewsCount={item.reviews_count}
                     blockRef={idx === users.length - 1 ? lastUser : undefined}
                     uses_free={item.uses_free}
                  />
               ))
               : <div className={styles.message}>
                  К сожалению, по Вашему запросу ничего не найдено
               </div>
            }
            {isLoading && <MyMiniLoader m={"35px auto 20px"}/>}
         </div>
      </div>
   )
}