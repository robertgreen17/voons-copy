import React, {FC} from "react";
import styles from "./styles.module.scss"
import {CategoryCard} from "../../common/CategoryCard/CategoryCard";
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";
import {getCategoriesWithLimit} from "../../../redux/selectors";
import lawCardBg from "../../../media/icons/main/lawCardBg.svg";

type PropsT = {}

export const Categories: FC<PropsT> = ({}) => {

   const categories = useSelector(getCategoriesWithLimit(4))

   return (
      <div className={styles.wrapper}>
         <div className={styles.label}>
            Популярные проблемы
         </div>
         <div className={styles.grid}>
            {categories.map((item, idx) => (
               <CategoryCard title={item.title}
                             label={item.name}
                             desc={item.text}
                             profCount={item.users_count}
                             imgLink={item.big}
                             key={idx}
                             tags={item.tags}
                             bg={"#FFF"}
               />
            ))}
         </div>
      </div>
   )
}

const cardsData = [
   {
      title: "Помощь с юридическими вопросами",
      label: "Закон и право",
      desc: "Подготовка документов, защита в суде, составление исков",
      profCount: 152,
      link: "/",
      imgLink: "/images/lawCardBg.svg",
   },
   {
      title: "Помощь с подбором стиля",
      label: "Красота и мода",
      desc: "Подбор стиля, консультирование по вопросам с модой",
      profCount: 91,
      link: "/",
      imgLink: "/images/styleCardBg.svg",
   },
   {
      title: "Помощь с юридическими вопросами",
      label: "Закон и право",
      desc: "Подготовка документов, защита в суде, составление исков",
      profCount: 152,
      link: "/",
      imgLink: "/images/lawCardBg.svg",
   },
   {
      title: "Помощь с юридическими вопросами",
      label: "Закон и право",
      desc: "Подготовка документов, защита в суде, составление исков",
      profCount: 152,
      link: "/",
      imgLink: "/images/lawCardBg.svg",
   },
]
