import React, {FC} from "react";
import styles from "./styles.module.scss";
import ReactTooltip from "react-tooltip";

type PropsT = {
    id: string
}

export const ToolTipSettings: FC<PropsT> = ({
                                        children,
                                        id,
                                    }) => {
    return (
        <ReactTooltip
            id = {id}
            place={"top"}
            multiline={true}
            class = {styles.tooltip}>
        </ReactTooltip>
    )
}