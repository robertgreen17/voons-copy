import React, {FC} from 'react';
import styles from "./styles.module.scss";

type PropsT = {
    m?: string
    w?: string
}

export const ToolTip: FC<PropsT> = ({
                                        children,
                                        m,
                                        w
                                    }) => {
    return (
        <span className={styles.tooltip} style={{margin: m,}}>
                    <div className={styles.body} style={{width: w, right: `calc(-${w} / 2)`}}>
                        {children}
                    </div>
                     <i className={"fas fa-info-circle"}/>
                  </span>
    )
}