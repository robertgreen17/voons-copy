import React, {FC, KeyboardEvent, useCallback, useEffect, useRef, useState} from 'react';
import {SearchInput} from "../Inputs/Input";
import styles from "./styles.module.scss";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {FindReqBodyT, PredictionT, searchApi} from "../../../api/search-api";
import {debounce} from "lodash";
import {find} from "../../../redux/search-reducer/search-reducer";
import {useStateCallback} from "../../../hooks/useStateCallback";
import {USERS_LIMIT} from "../../../redux/constants";

type PropsT = {
   isReset?: boolean
}

export const SearchGroup: FC<PropsT> = ({isReset}) => {

   const history = useHistory()
   const currentSearchData = useSelector((state: RootStateT) => state.search.currentSearchData)
   const inputValue = useSelector((state: RootStateT) => state.search.inputValue)
   const [value, setValue] = useStateCallback(history.location.pathname === "/searching" ? inputValue : "")
   const [options, setOptions] = useState([] as Array<string | PredictionT>)
   const input = useRef<HTMLInputElement>(null)
   const box = useRef<HTMLDivElement>(null)
   const dispatch = useDispatch()

   useEffect(() => {
      if (isReset) {
         setValue("")
         setOptions([])
      }
   }, [isReset])

   const fetchPrediction = async () => {
      // запрос за подсказками
      if (value.length > 0) {
         const body = {
            name: value
         }
         const resData = await searchApi.predict(body)
         setOptions(resData.data)
      }
   }

   // debounced запрос за подсказками
   const delayedFetch = useCallback(debounce(fetchPrediction, 500), [value])

   useEffect(() => {
      // запрашиваем подсказки при изменении значения в input
      delayedFetch();
      // cancel the debounce on useEffect cleanup.
      return () => delayedFetch.cancel();
   }, [value, delayedFetch]);


   const onChange = (e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      setValue(e.currentTarget.value)
   }

   useEffect(() => {
      // обработчик клика на все окно браузера
      window.addEventListener("click", handleMouseClick)
      return () => window.removeEventListener("click", handleMouseClick)
   }, [])


   const handleMouseClick = (e: MouseEvent) => {
      // если клик был не по инпуту и не по коробке с подсказками, то очищаем коробку, чтобы скрыть её
      if (e.target !== input.current && e.target !== box.current) {
         setOptions([])
      }
   }

   const handleSubmit = (name?: string) => {
      if (value.trim().length > 0) {
         const body: FindReqBodyT = {
            category: currentSearchData?.category,
            name: name? name : value,
            limit: USERS_LIMIT
         }
         dispatch(find(body, () => {
            history.push("/searching")
            setOptions([])
         }))
      }
   }

   const handleKeyPress = (event: KeyboardEvent<HTMLDivElement>) => {
      if (event.key === 'Enter') {
         handleSubmit()
      }
   }

   return (
      <div className={styles.wrapper}>
         <SearchInput onKeyPress={handleKeyPress}
                      placeholder={"Поиск..."}
                      value={value}
                      inputRef={input}
                      onChange={onChange}
         />
         {options.length > 0 && <div className={styles.box} ref={box}>
            {options.map((item, key) => (
               <div className={styles.item} key={key} onClick={() => {
                  setValue(typeof item === "string" ? item : item.ru)
                  handleSubmit(typeof item === "string" ? item : item.ru)
               }}>
                  {typeof item === "string" ? item : item.ru}
               </div>
            ))}
			</div>}
      </div>
   )
}