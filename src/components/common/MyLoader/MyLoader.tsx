import React, {FC} from "react";
import styles from "./styles.module.scss"

type PropsT = {

}

const MyLoader: FC<PropsT> = ({}) => {

		return (
			<div className={styles.wrapper}>
				<div className={styles.container}>
					<div className={styles.circle}/>
				</div>
			</div>
		)

};

export default MyLoader;