import React, {FC} from 'react';
import styles from "./styles.module.scss"

type PropsT = {
   m?: string
}

export const MyMiniLoader: FC<PropsT> = ({m}) => {
   return (
      <div className={styles.mini} style={{margin: m}}>
         <div className={styles.circle}/>
      </div>
   )
}