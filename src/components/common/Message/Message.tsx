import React, {FC} from "react";
import "./styles.scss"


type PropsT = {
   mod?: "not-from-me" | "from-me"
   message: string
   time: string
   linkMsg?: string
   linkUrl?: string
}

export const Message: FC<PropsT> = ({
                                       mod = "from-me",
                                       message,
                                       time,
                                       linkMsg,
                                       linkUrl,
                                    }) => {
   return (
      <div className={`message ${mod}`}>
         <div className="bubble">{message}</div>

         {linkMsg && <a href={linkUrl} className="link">
				<div className="header">
                    <span className="name">
                       {linkMsg}
                    </span>
					<img src="./icons/inNewTab.svg" alt=""/>
				</div>
				<span className="source">www.nalog.ru</span>
			</a>}

         <time>{time}</time>
      </div>
   )
}