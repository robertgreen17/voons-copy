import React, {FC, useEffect} from "react";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import styles from "./styles.module.scss";

export const DarkBg: FC = () => {
   // dark background for modals and some other components (RegForm, SocialAuth, ...)

   useEffect(() => {
      return () => enablePageScroll() // when component will unmounted
   }, [])

   return <div className={styles.dark}/>
}