import React, {FC} from "react"
import styles from "./styles.module.scss"
import {MiniProfile} from "../MiniProfile/MiniProfile";

type PropsT = {
    name: string
    desc: string
    imgLink: string
    isShowMore?: boolean,
    rating?: number
}

export const ClientCard: FC<PropsT> = ({imgLink, desc, name, isShowMore, rating}) => {
    return (
        <div className={styles.wrapper}>
            <MiniProfile
                size={"small"}
                imgLink={imgLink}
                name={name}
                rating={rating}
                imageWidth={"90px"}
            />
            <div className={styles.desc}>
                {desc}
            </div>
        </div>
    )
}