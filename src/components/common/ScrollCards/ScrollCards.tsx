import React, {FC, ReactElement, ReactNode, useEffect, useRef, useState} from "react";
import styles from "./styles.module.scss"
import stylesSpecialists from "../../Main/Specialists/styles.module.scss"
import Swiper, {SwiperRefNode} from 'react-id-swiper';
import 'swiper/swiper.scss';
import {ReactIdSwiperProps} from "react-id-swiper/lib/types";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {USERS_LIMIT} from "../../../redux/constants";
import stylesScrollCard from "./styles.module.scss";
import {SearchGroup} from "../SearchGroup/SearchGroup";

type PropsT = {
    control?: ReactNode
    title: string
    subTitle?: string
    isShowMore?: boolean
    id?: string,
    additional?: any
}

export const ScrollCards: FC<PropsT> = ({
                                            children,
                                            title,
                                            control,
                                            subTitle,
                                            id,
                                            additional
                                        }) => {

    const swiper = useRef<SwiperRefNode>(null);

    const onNextSlide = () => {
        if (swiper.current && swiper.current.swiper) {
            swiper.current.swiper.slideNext()
        }
    }
    const onPrevSlide = () => {
        if (swiper.current && swiper.current.swiper) {
            swiper.current.swiper.slidePrev()
        }
    }

    const params = additional ? additional : {
        spaceBetween: 20,
        containerClass: 'swiper-container ' + styles.slider,
        freeMode: true,
        breakpoints: {
            768: {
                spaceBetween: 30
            },
        }
    }

    const dispatch = useDispatch()

    const push = useHistory().push

    const [isReset, setIsReset] = useState(false)


    const showAll = () => {
        dispatch(search.actions.setIsResetSearch(true))
        setIsReset(true)
        dispatch(search.actions.setCurrentSearchData(null))
        dispatch(search.actions.setInputValue(""))
        dispatch(find({limit: USERS_LIMIT, offset: 0}, () => push("/searching")))
    }


    return (
        <div className={styles.wrapper} id={id}>

            <div className={styles.header + ' ' + stylesSpecialists.hideAdaptive}>
                <div className={styles.text}>
                    <div className={styles.title}>
                        {title}
                    </div>
                    <div className={styles.subTitle}>
                        {subTitle}
                    </div>
                </div>
                <div className={stylesSpecialists.wrapper}>
                    {control}
                </div>
            </div>


            <div className={stylesSpecialists.header + ' ' + stylesSpecialists.showOriginal}>
                <div className={stylesSpecialists.categoryWrapper}
                >
                    <div className={stylesSpecialists.text}>
                        <div className={stylesSpecialists.title}>
                            {title}
                        </div>
                        <div className={styles.subTitle}>
                            {subTitle}
                        </div>
                    </div>
                    <div className={styles.control}>
                        {control}
                    </div>
                </div>
            </div>


            {/*<div className={styles.header}>*/}
            {/*    <div className={styles.text}>*/}
            {/*        <div className={styles.title}>*/}
            {/*            {title}*/}
            {/*        </div>*/}
            {/*        <div className={styles.subTitle}>*/}
            {/*            {subTitle}*/}
            {/*        </div>*/}
            {/*    </div>*/}
            {/*    <div className={styles.control}>*/}
            {/*        {control}*/}
            {/*    </div>*/}
            {/*</div>*/}
            <div className={styles.container}>
            <span onClick={onPrevSlide} className={`${styles.arrow} ${styles.left}`}>
               <i className={"fas fa-arrow-left"}/>
            </span>
                <Swiper
                    slidesPerView={"auto"}
                    {...params}
                    ref={swiper}>
                    {children as ReactElement}
                </Swiper>
                <span onClick={onNextSlide} className={`${styles.arrow} ${styles.right}`}>
               <i className={"fas fa-arrow-right"}/>
            </span>
            </div>
            {/*<div className={styles.showMore}>Show more</div>*/}
            {/*{isShowMore && <div className={styles.showMore}>Show more</div>}*/}
        </div>
    )
}