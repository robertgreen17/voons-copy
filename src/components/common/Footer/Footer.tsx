import React, {FC} from "react";
import styles from "./styles.module.scss";
import CookieConsent from "react-cookie-consent";

export const Footer: FC = () => {
   return (
      <div className={styles.footer}>
          <span className={styles.item}>
            2021 © Все права защищены
         </span>
         <a href={"/terms_of_use"} className={styles.item} target={"_blank"} rel="noopener noreferrer">
            Условия использования
         </a>
         <a href={"/data_policy"} className={styles.item} target={"_blank"} rel="noopener noreferrer">
            Политика защиты персональных данных
         </a>
      </div>
   )
}