import React, {FC, useEffect, useRef, useState} from "react";
import styles from "./styles.module.scss"
import {useForm} from "react-hook-form";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import 'animate.css'
import {Message} from "../../Chat/Message/Message";
import TextareaAutosize from "react-textarea-autosize";
import {GetSupportMessagesReqBodyT, SendSupportMessageReqBodyT} from "../../../../api/user-api";
import support, {
   getSupportMessagesInfinity,
   MESSAGES_LIMIT,
   sendSupportMessage
} from "../../../../redux/support-reducer/support-reducer";
import {useSupportMessageListener} from "../../../../hooks/listeners/useSupportMessageListener";
import {useIntersectionObserver} from "../../../../hooks/useIntersectionObserver";
import InfiniteScroll from 'react-infinite-scroll-component';

type PropsT = {
   onClose: () => void
}
type FormData = {
   title: string
   text: string
   email: string
}

export const SupportChat: FC<PropsT> = ({onClose}) => {

   const {register, handleSubmit, reset, setValue, getValues} = useForm<FormData>();
   const messages = useSelector((state: RootStateT) => state.support.supportMessages)
   const user_id = useSelector((state: RootStateT) => state.support.user_id)
   const [isLoading, setIsLoading] = useState(false)
   const [hasMore, setHasMore] = useState(true)
   const [offset, setOffset] = useState(0)
   const scrollBar = useRef<HTMLDivElement>(null)
   const dispatch = useDispatch()

   useSupportMessageListener(dispatch, user_id)

   const fetchMessages = (offset: number) => {
      const body: GetSupportMessagesReqBodyT = {
         limit: MESSAGES_LIMIT,
         offset: offset,
         user_id: user_id ? user_id : null
      }
      dispatch(getSupportMessagesInfinity(body, setIsLoading, setHasMore))
   }

   const lastMessage = useIntersectionObserver(isLoading, hasMore, fetchMessages, offset, setOffset, MESSAGES_LIMIT)

   useEffect(() => {
      dispatch(support.actions.setSupportMessages([]))
      setOffset(0)
      fetchMessages(0)
   }, [])

   const onSubmit = handleSubmit(({text}: FormData) => {
      const body: SendSupportMessageReqBodyT = {
         user_id: user_id ? user_id : null,
         message: text,
      }
      dispatch(sendSupportMessage(body, () => reset()))
   });

   useEffect(() => {
      if (scrollBar.current && messages.length > 0) {
         scrollBar.current.scrollTop = scrollBar.current.scrollHeight
      }
   }, [scrollBar, messages])

   const keyDownListener = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
      if (e.key === "Enter") {
         e.preventDefault()
         onSubmit()
      }
      // if (e.key === "Enter" && e.ctrlKey) {
      //    const value = getValues("text")
      //    // @ts-ignore
      //    const newValue = value.substring(0, e.target.selectionStart) + "\n" + value.substring(e.target.selectionStart, value.length)
      //    setValue("text", newValue)
      // }
   }

   //

   return (
      <div className={`${styles.wrapper} animate__animated animate__slideInRight`}>
         <div className={styles.row}>
            <div className={styles.title}>
               Чат с тех. поддержкой
            </div>
            <img src="/icons/navigation/cross.svg" alt="" className={styles.cross} onClick={onClose}/>
         </div>
         {messages && messages.length > 0 && <div className={styles.messages} ref={scrollBar}>
            {messages.map((item, idx) => (
               <Message text={item.text}
                        time={item.date}
                        key={idx}
                        // innerRef={idx === messages.length - 1 ? messagesEndRef : undefined}
                        // innerRef={idx === 0 ? lastMessage : undefined}
                        mod={item.from === "client" ? "active" : "default"}/>
            ))}
         </div>}
         {/*{messages && messages.length > 0 &&*/}
         {/*<InfiniteScroll*/}
         {/*   className={styles.messages}*/}
         {/*   dataLength={MESSAGES_LIMIT}*/}
         {/*   hasMore={hasMore}*/}
         {/*   loader={<h4>Loading...</h4>}*/}
         {/*   next={() => fetchMessages(offset)}*/}
         {/*   inverse={true}*/}
         {/*>*/}
         {/*   {messages.map((item, idx) => (*/}
         {/*      <Message text={item.text}*/}
         {/*               time={item.date}*/}
         {/*               key={idx}*/}
         {/*         // innerRef={idx === messages.length - 1 ? messagesEndRef : undefined}*/}
         {/*         // innerRef={idx === 0 ? lastMessage : undefined}*/}
         {/*               mod={item.from === "client" ? "active" : "default"}/>*/}
         {/*   ))}*/}
	      {/*</InfiniteScroll>}*/}
         <form className={styles.area} onSubmit={onSubmit}>
            <div className={styles.row}>
               <TextareaAutosize
                  onKeyDown={keyDownListener}
                  name={"text"}
                  ref={register}
                  autoFocus={true}
                  placeholder={"Ваше сообщение"}
                  className={styles.textarea}
                  minRows={1}
                  maxRows={5}/>
               <button className={styles.submit} type={"submit"} >
                  <img src="/icons/arrowIcon.svg" alt=""/>
               </button>
            </div>
         </form>
      </div>
   )
}