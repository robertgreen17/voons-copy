import React, {FC} from "react";
import styles from "./styles.module.scss"
import {useForm} from "react-hook-form";
import {Input, TextArea} from "../../Inputs/Input";
import {Button} from "../../Button/Button";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import 'animate.css'

type PropsT = {
   onClose: () => void
}
type FormData = {
   title: string
   desc: string
   email: string
}

export const SupportForm: FC<PropsT> = ({onClose}) => {

   const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
   const {register, handleSubmit} = useForm<FormData>();

   const onSubmit = handleSubmit((data: FormData) => {
      console.log(data)
   });

   return (
      <div className={`${styles.wrapper} animate__animated animate__slideInRight`}>
         <div className={styles.row}>
            <div className={styles.title}>
               Опишите свою проблему
            </div>
            <img src="/icons/navigation/cross.svg" alt="" className={styles.cross} onClick={onClose}/>
         </div>
         <form onSubmit={onSubmit}>
            <Input name={"title"}
                   label={"Тема"}
                   m={"0 0 15px"}
                   inputRef={register}
            />
            {!isAuth && <Input name={"email"}
                               label={"Email"}
                               m={"0 0 15px"}
                               type={"email"}
                               inputRef={register}
            />}
            <TextArea name={"desc"}
                      inputRef={register}
                      placeholder={"Сообщение"}
                      rows={7} m={"0 0 25px"}/>
            <Button type={"submit"} mod={"primary"} size={"medium"} width={"full-width"}>
               Отправить сообщение
            </Button>
         </form>

      </div>

   )
}