import React, {FC, useState} from "react";
import styles from "./styles.module.scss"
import 'animate.css'
import {SupportChat} from "../SupportChat/SupportChat";

export const Cloud: FC = () => {

   const [isOpen, setIsOpen] = useState(false)

   return (
      <div>
         {!isOpen && <img src="/icons/supportCloud.svg"
                          onClick={() => setIsOpen(true)}
			                 alt="" /* animate__delay-3s - если нужна будет задержка */
			                 className={`${styles.icon} animate__animated animate__bounceInRight`}/>}
         {/*{isOpen && <SupportForm onClose={() => setIsOpen(false)}/>}*/}
         {isOpen && <SupportChat onClose={() => setIsOpen(false)}/>}
      </div>

   )
}