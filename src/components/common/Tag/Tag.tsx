import React, {ButtonHTMLAttributes, DetailedHTMLProps, FC, HTMLProps, useEffect, useRef} from "react";
import "./styles.scss"
import {PredictionT} from "../../../api/search-api";


type PropsT = {
    size?: "small" | "medium"
    icon?: "icon-left" | "closable"
    mod?: "secondary" | "dark-opacity" | "danger" | "warning"
    m?: string
    onClose?: () => void
}

export const Tag: FC<PropsT> = ({
                                    size = "small",
                                    m,
                                    icon,
                                    mod,
                                    onClose,
                                    children
                                }) => {
    return (
        <span className={`tag ${size} ${icon} ${mod}`} style={{margin: m}}>
         {icon === "icon-left" && <img src="/icons/peopleIcons.svg" alt=""/>}
            {children}
            {icon === "closable" && <img src="/icons/closeIcon.svg" alt="" onClick={onClose}/>}
      </span>
    )
}

type InputTagPropsT = {
    m?: string
    onPlusClick: (value?: string) => void
    options?: Array<PredictionT | string>
    setValue?: (value: string) => void,
    added?: Array<String>
}

export const InputTag: FC<InputTagPropsT & HTMLProps<HTMLInputElement>> = ({
                                                                               m,
                                                                               children,
                                                                               onPlusClick,
                                                                               options,
                                                                               setValue,
                                                                               added,
                                                                               ...rest
                                                                           }) => {
    const input = useRef<HTMLInputElement>(null)
    const box = useRef<HTMLDivElement>(null)

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && onPlusClick) {
            onPlusClick()
        }
    }

    useEffect(() => {
        // обработчик клика на все окно браузера
        window.addEventListener("click", handleMouseClick)
        return () => window.removeEventListener("click", handleMouseClick)
    }, [])

    const handleMouseClick = (e: MouseEvent) => {
        if (e.target !== input.current && e.target !== box.current && onPlusClick) {
            onPlusClick()
        }
    }

    return (
        <div className={"inputWrap"} style={{margin: m}}>
            <input className={`inputTag`} onKeyPress={handleKeyPress} ref={input} {...rest} autoComplete={'false'}/>
            <img src="/icons/settings/plus.svg" alt="" className={"icon"} onClick={() => onPlusClick()}/>
            {options && options.length > 0 &&
            <div className={"box"} ref={box}>
                {options?.filter((item) => {
                    return added?.indexOf(typeof item === "string" ? item : item.ru) == -1;
                }).map((item, idx) => (
                    <div key={idx} className={"box-item"} onClick={() => {
                        if (setValue) {
                            setValue(typeof item === "string" ? item : item.ru)
                            if (onPlusClick) onPlusClick(typeof item === "string" ? item : item.ru)
                        }
                    }}>
                        {typeof item === "string" ? item : item.ru}
                    </div>
                ))}
            </div>

            }
        </div>

    )
}

type PlusBtnPropsT = {
    m?: string
}

export const PlusBtn: FC<PlusBtnPropsT &
    DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>> = ({
                                                                                          m,
                                                                                          children,
                                                                                          ...rest
                                                                                      }) => {
    return (
        <button className={`plusBtn`} style={{margin: m}} {...rest} type={"button"}>
            <img src="/icons/settings/plus.svg" alt=""/>
        </button>
    )
}