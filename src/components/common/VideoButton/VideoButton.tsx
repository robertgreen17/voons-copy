import React, {ButtonHTMLAttributes, DetailedHTMLProps, FC, Ref} from "react";
import "./styles.scss"


type PropsT = {
   mod?: "white" | "danger"
   buttonRef?: Ref<HTMLButtonElement>
}

export const VideoButton: FC<PropsT &
   DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>> = ({
                                                                                        mod = "white",
                                                                                        children,
                                                                                        buttonRef,
                                                                                        ...rest
                                                                                     }) => {
   return (
      <button className={`video-button ${mod}`} ref={buttonRef} {...rest}>
         {children}
      </button>
   )
}