import React, {FC} from "react";
import styles from "./styles.module.scss"

type PropsT = {
   m?: string
}

export const Separator: FC<PropsT> = ({m}) => (
   <div className={styles.line} style={{margin: m}}/>
)