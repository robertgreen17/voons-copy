import React, {ButtonHTMLAttributes, DetailedHTMLProps, FC, HTMLProps} from "react";
import "./styles.scss"


type PropsT = {
    mod?: "primary" | "secondary" | "danger" | "danger-light" | "googlePlay"
    size?: "large" | "xlarge" | "medium" | "small"
    iconPos?: "icon-left" | "icon-right"
    width?: "full-width"
    isArrow?: boolean
    m?: string
    p?: string
    fWeight?: "inherit" | "-moz-initial" | "initial" | "revert" | "unset" | "bold" | "normal" | (number & {}) | "bolder" | "lighter" | undefined
}

export const Button: FC<PropsT &
    DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>> = ({
                                                                                          mod,
                                                                                          iconPos,
                                                                                          size = "large",
                                                                                          width,
                                                                                          isArrow,
                                                                                          m,
                                                                                          children,
                                                                                          p,
                                                                                          fWeight,
                                                                                          ...rest
                                                                                      }) => {

    return (
        <button className={`button ${mod} ${iconPos} ${size} ${width}`}
                style={{
                    margin: m,
                    padding: p,
                    fontWeight: fWeight
                }}
                type={rest.type ? rest.type : "button"}
                {...rest}>
            {children}
            {mod === "googlePlay" && <img src="/icons/googlePlay.svg" alt=""/>}
            {isArrow && <img src="/icons/arrowIcon.svg"/>}
        </button>
    )
}