import React, {FC, useEffect, useState} from 'react';
import styles from "./styles.module.scss";
import {Input} from "../../Inputs/Input";
import {Button} from "../../Button/Button";
import Popup from "reactjs-popup";
import {Modal} from "../../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../../redux/store";
import {refill, UserTypeT, withdraw} from "../../../../redux/user-reducer/user-reducer";
import {useForm} from "react-hook-form";
import {RefillReqBodyT, WithdrawReqBodyT} from "../../../../api/user-api";
import InputMask from "react-input-mask";
import {CircularProgress} from "@material-ui/core";

type PropsT = {
    isOpen: boolean
    closeModal: () => void
    amountValue?: number
}

type PaymentsFormValuesT = {
    amount: number
    card?: string
}

export const PaymentsModal: FC<PropsT> = ({isOpen, closeModal, amountValue}) => {

    const userType = useSelector((state: RootStateT) => state.user.type)
    const {register, handleSubmit, setValue} = useForm<PaymentsFormValuesT>()

    const [load, setLoad] = useState({
        title: <span>Вывести</span>,
        disabled: false
    })

    useEffect(() => {
        if (amountValue) setValue("amount", amountValue)
    }, [amountValue])

    const dispatch = useDispatch()

    const onSubmit = handleSubmit(({amount, card}) => {
        if (amount) {
            if (userType === UserTypeT.client) {
                const body: RefillReqBodyT = {
                    amount: amount
                }
                dispatch(refill(body))
            } else if (card) {
                const body: WithdrawReqBodyT = {
                    amount: amount,
                    card: card.split(' ').join(''),
                }
                dispatch(withdraw(body))
                let load = <CircularProgress size={"18px"}/>;
                setLoad({
                    title: load,
                    disabled: true
                })
                setTimeout(() => {
                    closeModal()
                    setLoad({
                        title: <span>Вывести</span>,
                        disabled: false
                    })
                }, 1000)
            }
        }
    })

    return (
        <Popup open={isOpen}
               overlayStyle={{background: "rgba(22, 24, 53, 0.4)"}}
               closeOnDocumentClick
               onClose={closeModal}>
            <Modal>
                <form onSubmit={onSubmit}>
                    <div className={styles.label}>
                        {userType === UserTypeT.profi
                            ? "Введите сумму, которую хотите вывести"
                            : "Введите сумму, на которую хотите пополнить счет voons."
                        }
                    </div>

                    {userType === UserTypeT.profi &&
                    <InputMask mask="9999 9999 9999 9999">
                        <Input label={"Номер карты"}
                               m={"0 auto 15px"}
                               inputRef={register}
                               name={"card"}
                               required/>
                    </InputMask>
                    }
                    <Input label={"Сумма в рублях"}
                           m={"0 auto 30px"}
                           inputRef={register}
                        // min={userType === UserTypeT.profi ? 1000 : 1}
                           name={"amount"}
                           type={"number"} required/>
                    <Button mod={"primary"} disabled={load.disabled} size={"large"} m={"0 auto"} width={"full-width"} type={"submit"}>
                        {userType === UserTypeT.profi
                            ? load.title
                            : "Пополнить"
                        }
                    </Button>
                </form>
            </Modal>
        </Popup>
    )
}