import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"
import {Button} from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {NavLink, useHistory} from "react-router-dom";
import {NavLinks} from "./NavLinks";
import {DropDownList} from "./DropDownList/DropDownList";
import {Notifications} from "./Notifications/Notifications";
import {useNotificationsListener} from "../../../hooks/listeners/useNotificationsListener";
import {formatMoney} from "../../../utils/string";
import {PaymentsModal} from "./PaymentsModal/PaymentsModal";
import {getNotifies} from "../../../redux/app-reducer/app-reducer";
import {UserTypeT} from "../../../redux/user-reducer/user-reducer";
import ReactTooltip from "react-tooltip";
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {USERS_LIMIT} from "../../../redux/constants";

type PropsT = {
    bg?: string
}

export const Navbar: FC<PropsT> = ({bg}) => {

    const isAuth = useSelector((state: RootStateT) => state.auth.isAuth)
    const avatar = useSelector((state: RootStateT) => state.user.userData?.avatar)
    const userId = useSelector((state: RootStateT) => state.user.userData?._id)
    const balance = useSelector((state: RootStateT) => state.user.userData?.balance)
    const av_sum = useSelector((state: RootStateT) => state.user.userData?.av_sum)
    const notifications = useSelector((state: RootStateT) => state.app.notifications)
    const [isDropDownList, setIsDropDownList] = useState(false)
    const [isNotifications, setIsNotifications] = useState(false)
    const [isPayment, setIsPayment] = useState(false)
    const dispatch = useDispatch()
    const userType = useSelector((state: RootStateT) => state.user.type)


    useEffect(() => {
        dispatch(getNotifies())
    }, [])

    const closeWithdraw = () => {
        setIsPayment(false)
    }
    const toggleNotifications = () => {
        setIsDropDownList(false)
        if (isNotifications) {
            setIsNotifications(false)
        } else {
            setIsNotifications(true)
        }
    }
    const toggleDropDownList = () => {
        if (isNotifications) {
            setIsNotifications(false)
        }
        setIsDropDownList(!isDropDownList)
    }

    useNotificationsListener(dispatch, userId?.$oid)

    const path = useHistory().location.pathname

    const push = useHistory().push

    const [isReset, setIsReset] = useState(false)
    const scrollTo = (id: string) => {
        const elem = document.getElementById(id)
        if (elem) {
            elem.scrollIntoView({behavior: "smooth"})
        }
    }

    const showAll = () => {
        dispatch(search.actions.setIsResetSearch(true))
        setIsReset(true)
        dispatch(search.actions.setCurrentSearchData(null))
        dispatch(search.actions.setInputValue(""))
        dispatch(find({limit: USERS_LIMIT, offset: 0}, () => push("/searching")))
    }
    return (
        <div className={styles.wrapper} style={{background: bg}}>
            <NavLink to={"/main"} className={styles.logo}>
                <img src={"/icons/logo3.png"} alt=""/>
            </NavLink>
            <NavLinks/>

            <div className={styles.login}>
                <div className={styles.menu}>
                    {/*<NavLink to={"/main"}*/}
                    {/*         className={`${styles.item}  ${path === "/main" ? styles.active : ""}`}>*/}
                    {/*   Главная*/}
                    {/*</NavLink>*/}
                    {path.slice(0, 5) === "/main" ? <span onClick={() => scrollTo("how-does-it-works")}
                                                          className={`${styles.item}`}>
            Как это работает
         </span> : <NavLink to={'/main'} style={{marginRight: "27px"}}><span onClick={() => {
             setTimeout(() => {
                 scrollTo("how-does-it-works")
             }, 1000)
                    }}
                         className={`${styles.item}`}>
            Как это работает
                    </span></NavLink>}

                    <span onClick={showAll}
                          className={`${styles.item}`}>
            Voons Leaders
         </span>
                </div>
                {!isAuth && <>
                    <NavLink to={"/main/sign_in/reg/client"}>
                        <div className={styles.item}>Регистрация</div>
                    </NavLink>
                    <NavLink to={"/main/sign_in/auth"}>
                        <Button mod={"primary"} size={"large"}>Войти</Button>
                    </NavLink>
                </>}

                {isAuth &&
                <>
                    <div className={styles.balance} onClick={() => {
                        setIsPayment(true)
                    }}>
                        <div>
                            <span data-tip={"Доступный баланс"} data-for={"balance"}>{formatMoney(balance)} ₽</span>
                            <ReactTooltip place="bottom" type="dark" id={"balance"} effect="solid"/>

                            {userType === UserTypeT.profi ? (
                                <span> |
                                   <span data-tip={"Сумма ожидает выплаты"} data-for={'av_sum'} className={styles.paymentsWait}> {formatMoney(av_sum)} ₽</span>
                                   <ReactTooltip place="bottom" type="dark" id={"av_sum"} effect="solid"/>
                               </span>
                            ) : (<></>)}
                        </div>
                    </div>
                    <div className={styles.notification}>
                        <img src="/icons/notifications.svg"
                             onClick={toggleNotifications}
                             alt="" className={styles.icon}/>
                        {notifications.length > 0 && <div className={styles.circle}>{notifications.length}</div>}
                    </div>
                    <img
                        src={avatar ? avatar : "/icons/userAvatar.png"}
                        alt=""
                        className={styles.avatar}
                        onClick={toggleDropDownList} onMouseEnter={toggleDropDownList}
                    />
                    {isDropDownList && <DropDownList setIsDropDownList={setIsDropDownList} isDropDownList={isDropDownList}/>}
                    {isNotifications && <Notifications/>}
                    {isPayment && <PaymentsModal isOpen={isPayment} closeModal={closeWithdraw}/>}
                </>}

            </div>
            <>
                <NavLink to={"/main/sign_in/mobile"} className={styles.menuBtn}>
                    <Button mod={"secondary"} size={"large"}><span></span></Button>
                </NavLink>
            </>
        </div>
    )
}

