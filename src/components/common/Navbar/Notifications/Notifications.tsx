import {useDispatch, useSelector} from "react-redux";
import styles from "./styles.module.scss";
import React, {useEffect} from "react";
import {Button} from "../../Button/Button";
import {RootStateT} from "../../../../redux/store";
import {cancelMeeting, confirmMeeting} from "../../../../redux/meeting-reducer/meeting-reducer";
import app, {clearNotifies, getNotifies} from "../../../../redux/app-reducer/app-reducer";
import {NotificationT} from "../../../../hooks/listeners/useNotificationsListener";
import {getMeetings} from "../../../../redux/events-reducer/events-reducer";

export const Notifications = () => {

   const dispatch = useDispatch()
   const notifications = useSelector((state: RootStateT) => state.app.notifications)

   useEffect(() => {
      dispatch(getNotifies())
   }, [])

   const onCancelMeeting = (eventId: string) => {
      dispatch(cancelMeeting(eventId))
      const data: Array<NotificationT> = notifications.map(m => {
         if (m.payload.event_id === eventId) {
            return {...m, type: "canceled"}
         }
         return m
      })
      dispatch(app.actions.setNotifications(data))
   }

   const onConfirmMeeting = (eventId: string) => {
      dispatch(confirmMeeting(eventId))
      const data: Array<NotificationT> = notifications.map(m => {
         if (m.payload.event_id === eventId) {
            return {...m, type: "confirmed"}
         }
         return m
      })
      dispatch(app.actions.setNotifications(data))
      dispatch(getMeetings({}))
   }

   return (
      <div className={styles.container}>
         {notifications.length > 0
            ? <div className={styles.scroll}>
               {notifications.slice().reverse().map((item, idx) => (
                  <div className={styles.item} key={idx}>
                     <div className={`${styles.icon}`}>
                     <span className={`${styles[item.payload.icon.substr(7)]}`}>
                        <i className={`fa-lg ${item.payload.icon}`}/>
                     </span>
                     </div>
                     <div>
                        <div className={styles.message}>
                           <div className={styles.header}>
                              <div className={styles.title}>
                                 {item.title}
                              </div>
                              <div className={styles.date}>
                                 {item.payload.date}
                              </div>
                           </div>
                           <div className={styles.text}>
                              {item.text}
                           </div>
                           {item.type === "confirm" && <div className={styles.btnGroup}>
										<Button mod={"primary"} size={"medium"} width={"full-width"}
                                      onClick={() => onConfirmMeeting(item.payload.event_id)}>
											Подтвердить
										</Button>
										<Button mod={"danger-light"} size={"medium"} width={"full-width"}
										        onClick={() => onCancelMeeting(item.payload.event_id)}>
											Отклонить
										</Button>
									</div>}
                           {item.type === "confirmed" && <div className={`${styles.btnGroup} ${styles.single}`}>
										<Button mod={"primary"} disabled={true} size={"medium"} width={"full-width"}>
											Подтверждено
										</Button>
									</div>}
                           {item.type === "canceled" && <div className={`${styles.btnGroup} ${styles.single}`}>
										<Button mod={"primary"} disabled={true} size={"medium"} width={"full-width"}>
											Отменено
										</Button>
									</div>}
                        </div>
                     </div>

                  </div>
               ))}
            </div>
            : <div className={styles.empty}>
               У вас пока нет уведомлений
            </div>
         }
         {notifications.length > 0 && <div className={styles.clear} onClick={() => dispatch(clearNotifies())}>
	         Удалить все уведомления
         </div>}
      </div>

   )
}

const data = [
   {
      title: "Заголовок",
      text: "Уважаемый специалист! Вами успешно была проведена консультация для Ивана Иванова. Денежные средства в размере 50$ поступили на Ваш счет.",
      isConfirm: false,
      mod: "check",
      date: "1 сентября"
   },
   {
      title: "Заголовок",
      text:
         <div>
            <p>Уважаемый специалист, Иван Иванов хочет с вами запланировать консультацию.</p>
            <p>Дата и время консультации: 27.11.2020 / 17:30 </p>
            <p>Стоимость и временной слот: 1000$ / 15 мин</p>
            <p>Вопросы для обсуждения: Как реализовать маркетинговую стратегию для компании Бизнес Дон?</p>
         </div>,
      isConfirm: true,
      mod: "question",
      date: "1 сентября",
   },
   {
      title: "Заголовок",
      date: "1 сентября",
      text: "Уважаемый специалист! Запрос на вывод денежных средств в размере 300$  успешно обработан! Ожидайте зачисления денежных средств на указанный Вами счет!",
      isConfirm: false,
      mod: "coins",
   },
]
