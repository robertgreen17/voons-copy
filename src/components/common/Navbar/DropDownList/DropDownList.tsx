import styles from "./styles.module.scss";
import React, {FC, useState} from "react";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {exit} from "../../../../redux/app-reducer/app-reducer";
import {RootStateT} from "../../../../redux/store";
import {UserTypeT} from "../../../../redux/user-reducer/user-reducer";

type PropT = {
    setIsDropDownList: (value: boolean) => void,
    isDropDownList: boolean
};

export const DropDownList: FC<PropT> = ({setIsDropDownList, isDropDownList}) => {

    const dispatch = useDispatch()
    const userType = useSelector((state: RootStateT) => state.user.type)

    return (
        <div className={styles.wrapper} onMouseLeave={() => {
            setIsDropDownList(false)
        }}>
            <div className={styles.item}>
                <NavLink to={userType === UserTypeT.client ? "/dashboard/client" : "/dashboard/profi"}>
                    Мой профиль
                </NavLink>
            </div>
            <div className={styles.item}>
                <NavLink to={"/settings"}>
                    Настройки
                </NavLink>
            </div>
            <div className={`${styles.item} ${styles.red}`}>
                <div onClick={() => dispatch(exit())}>
                    Выйти
                </div>
            </div>
        </div>
    )
}
