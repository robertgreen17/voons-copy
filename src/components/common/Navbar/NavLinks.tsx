import React, {useState} from "react";
import styles from "./styles.module.scss"
import {useDispatch, useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";
import {NavLink, useHistory} from "react-router-dom";
import {UserTypeT} from "../../../redux/user-reducer/user-reducer";
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {USERS_LIMIT} from "../../../redux/constants";

export const NavLinks = () => {

   const userType = useSelector((state: RootStateT) => state.user.type)
   const path = useHistory().location.pathname

   const scrollTo = (id: string) => {
      const elem = document.getElementById(id)
      if (elem) {
         elem.scrollIntoView({behavior: "smooth"})
      }
   }

   const dispatch = useDispatch()

   const push = useHistory().push

   const [isReset, setIsReset] = useState(false)


   const showAll = () => {
      dispatch(search.actions.setIsResetSearch(true))
      setIsReset(true)
      dispatch(search.actions.setCurrentSearchData(null))
      dispatch(search.actions.setInputValue(""))
      dispatch(find({limit: USERS_LIMIT, offset: 0}, () => push("/searching")))
   }

   if (path.slice(0, 5) === "/main") {
      return (
         <></>
      )
   } else {
      if (userType === UserTypeT.client) {
         return (
            <div className={styles.menu}>
               {/*<NavLink to={"/main"}*/}
               {/*         className={`${styles.item}  ${path === "/dashboard/client" ? styles.active : ""}`}>*/}
               {/*   Главная*/}
               {/*</NavLink>*/}
               <NavLink to={"/search"}
                        className={`${styles.item}  ${path === "/search" || path === "/searching" ? styles.active : ""}`}>
                  Поиск Voons Leaders
               </NavLink>
            </div>
         )
      } else {
         return (
            <div className={styles.menu}>
               {/*<NavLink to={"/main"}*/}
               {/*         className={`${styles.item}  ${path === "/dashboard/profi" ? styles.active : ""}`}>*/}
               {/*   Главная*/}
               {/*</NavLink>*/}
            </div>
         )
      }
   }
}

