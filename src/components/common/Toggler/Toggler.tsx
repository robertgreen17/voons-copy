import React, {FC, HTMLProps} from "react";
import styles from "./styles.module.scss"

type PropsT = {
   id: string
}

export const Toggler: FC<PropsT & HTMLProps<HTMLInputElement>> = ({ id,  ...rest}) => {
   return (
      <div className={styles.switchWrap}>
         <input
            className={styles.checkbox}
            type="checkbox"
            id={id}
            {...rest}
         />
         <label
            className={styles.label}
            htmlFor={id}
         >
            <span className={styles.switchBtn}/>
         </label>
      </div>
   )
}