import React, {FC} from "react"
import styles from "./styles.module.scss"
import {Button} from "../Button/Button";
import {NavLink} from "react-router-dom";
import {FindReqBodyT, TagT} from "../../../api/search-api";
import search, {find} from "../../../redux/search-reducer/search-reducer";
import {useDispatch} from "react-redux";
import {USERS_LIMIT} from "../../../redux/constants";
import {declWord} from "../../../utils/string";

type PropsT = {
    label: string
    title: string
    desc: string
    profCount: number
    imgLink: string
    bg?: string
    height?: string
    tags?: Array<TagT>
    width?: string
}

export const CategoryCard: FC<PropsT> = ({
                                             title,
                                             label,
                                             desc,
                                             imgLink,
                                             profCount,
                                             bg,
                                             height,
                                             tags,
                                             width,
                                         }) => {
    const dispatch = useDispatch()

    const onClick = () => {
        const body: FindReqBodyT = {
            category: tags ? tags.map(c => c._id.$oid) : [],
            limit: USERS_LIMIT,
        }
        dispatch(search.actions.setCurrentSearchData(body))
        dispatch(find(body))
    }

    return (
        <div className={styles.wrapper} style={{background: bg}}>
            <img src={imgLink} alt="" className={styles.background} style={{width: width, height: height}}/>
            <div className={styles.container}>
                <div>
                    <div className={styles.label}>
                        {label}
                    </div>
                    <div className={styles.title}>
                        {title}
                    </div>
                    <div className={styles.label}>
                        {desc}
                    </div>
                </div>
                <div>
                    <NavLink to={"/searching"}>
                        <Button mod={"primary"}
                                size={"large"}
                                iconPos={"icon-right"}
                                isArrow={true}
                                onClick={onClick}
                        >
                            {declWord(profCount, "специалист")}
                        </Button>
                    </NavLink>
                </div>
            </div>
        </div>
    )
}