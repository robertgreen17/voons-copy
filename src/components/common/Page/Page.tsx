import React, {FC} from "react"
import styles from "./styles.module.scss"
import {Navbar} from "../Navbar/Navbar";
import {Cloud} from "../Support/Cloud/Cloud";

type PropsT = {
   isNavbar?: boolean
   bg?: string
   navbarBg?: string
   isHelp?: boolean,
   isId?: string,
}

export const Page: FC<PropsT> = ({
                                    isNavbar,
                                    bg,
                                    navbarBg,
                                    children,
                                    isHelp= true,
                                    isId,
                                 }) => {
   return (
      <div className={styles.wrapper} style={{background: bg}} id={isId ? isId : ""}>
         {isNavbar && <Navbar bg={navbarBg}/>}
         {children}
         {isHelp && <Cloud/>}
      </div>
   )
}