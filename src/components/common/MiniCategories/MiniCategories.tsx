import React, {FC} from "react";
import styles from "./styles.module.scss";
import {MiniCategory} from "./MiniCategory/MiniCategory";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";

export const MiniCategories: FC = () => {

   const tags = useSelector((state:RootStateT) => state.search.categories.map(c  => c.tags))

   return (
      <div className={styles.wrapper}>
         <div className={styles.label}>
            Популярные категории
         </div>
         <div className={styles.container}>
            {data.map((item, idx) => (
               <MiniCategory imgLink={item.imgLink}
                             title={item.title}
                             key={idx}
               />
            ))}
         </div>
      </div>
   )
}

const data = [
   {
      title: "Красота и мода",
      imgLink: "/icons/miniCategories/beauty.svg"
   },
   {
      title: "Наука и образование",
      imgLink: "/icons/miniCategories/science.svg"
   },
   {
      title: "Фитнес и спорт",
      imgLink: "/icons/miniCategories/sport.svg"
   },
   {
      title: "Закон и право",
      imgLink: "/icons/miniCategories/law.svg"
   },
]