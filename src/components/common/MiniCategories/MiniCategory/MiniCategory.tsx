import React, {FC} from "react"
import styles from "./styles.module.scss"
import {NavLink} from "react-router-dom"
import {useDispatch} from "react-redux";
import {find} from "../../../../redux/search-reducer/search-reducer";
import {FindReqBodyT, TagT} from "../../../../api/search-api";

type PropsT = {
   title: string
   imgLink: string
   tags?: Array<TagT>
   isBlueCard?: boolean
}

export const MiniCategory: FC<PropsT> = ({
                                            title,
                                            imgLink,
                                            tags,
                                            isBlueCard,
                                         }) => {
   const dispatch = useDispatch()

   const onClick = () => {
      const body: FindReqBodyT = {
         category: tags ? tags.map(c => c._id.$oid) : []
      }
      dispatch(find(body))
   }

   return (
      <NavLink to={"/searching"} className={`${styles.wrapper} ${isBlueCard && styles.blue}`} onClick={onClick}>
         <img src={imgLink} alt="" className={styles.icon}/>
         <div className={styles.title}>
            {title}
         </div>
      </NavLink>
   )
}