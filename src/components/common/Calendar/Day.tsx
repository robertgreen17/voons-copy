import React, {FC} from "react"
import "./styles.scss"
import ReactTooltip from "react-tooltip";

type PropsT = {
   dayMod?: "disabled" | "active" | "default"
   statusMod?: "active" | "disabled"
   isNone?: boolean
   onDayClick: (date: Date) => void
   date?: Date
}
export type DayT = {
   mod: "disabled" | "active" | "default"
   statusMod: "active" | "disabled"
   date: Date
}

export const Day: FC<PropsT> = ({
                                   onDayClick,
                                   dayMod,
                                   statusMod,
                                   date,
                                   isNone,
                                }) => {

   return (
      <div className={`calendar-day ${isNone && "none"}`}>
         <div className={`calendar-item calendar-item-${dayMod}`}
              onClick={date ? () => onDayClick(date) : undefined}>
            <div className={`calendar-item-status calendar-item-status-${statusMod}`}/>
            {date?.getDate()}
         </div>
      </div>
   )
}