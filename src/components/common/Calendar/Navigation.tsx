import React, {FC} from "react"
import "./styles.scss"
import {getMonthData} from "../../../utils/calendar";
import {DayT} from "./Day";
import Moment from "react-moment";

type PropsT = {
   date: Date
   setDate: (date: Date) => void
   setMonthData: (data: Array<Array<DayT | null>>) => void
   setSelectedDays: (days: Array<Date>) => void
}

export const Navigation: FC<PropsT> = ({
                                          setMonthData,
                                          date,
                                          setDate,
                                          setSelectedDays,
                                       }) => {

   const onNextMonth = () => {
      const newDate = new Date(date.getFullYear(), date.getMonth() + 1)
      setSelectedDays([])
      setDate(newDate)
      setMonthData(getMonthData(newDate.getFullYear(), newDate.getMonth()))
   }

   const onPrevMonth = () => {
      const newDate = new Date(date.getFullYear(), date.getMonth() - 1)
      setSelectedDays([])
      setDate(newDate)
      setMonthData(getMonthData(newDate.getFullYear(), newDate.getMonth()))
   }


   return (
      <div className="calendar-header">
         <button className="left nav"
                 onClick={onPrevMonth}
                 disabled={date <= new Date()}
         >
            <img src="/icons/calendar/arrow.svg" alt=""/>
         </button>
         <span className="name">
               <Moment format="MMMM, YYYY г" locale={"ru"}>
                  {date}
               </Moment>
               </span>
         <button className="nav" onClick={onNextMonth}>
            <img src="/icons/calendar/arrow.svg" alt=""/>
         </button>
      </div>
   )
}