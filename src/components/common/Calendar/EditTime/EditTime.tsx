import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"
import {
    getAllWeeksWithSelectedDays, getSelectedDatesTimesFrom, getSelectedDatesTimesTo, getSelectedDatesWorkIdsByTimeFrom,
    getSelectedDatesWorkTimes,
    MONTHS_RU,
    transformDatesToString
} from "../../../../utils/calendar";
import {Button} from "../../Button/Button";
import {Separator} from "../../Separator/Separator";
import {GetWorkTimesResDataT} from "../../../../api/calendar-api";
import {useDispatch} from "react-redux";
import {createCalendar, deleteCalendar} from "../../../../redux/calendar-reducer/calendar-reducer";
import {MonthDataT} from "../Calendar";

type PropsT = {
    selectedDates: Array<Date>
    workTimes: GetWorkTimesResDataT
    setEditMod: (mod: boolean) => void
    setSelectedDates: (dates: Array<Date>) => void
    monthData: MonthDataT
}

export const EditTime: FC<PropsT> = ({
                                         selectedDates,
                                         workTimes,
                                         setEditMod,
                                         setSelectedDates,
                                         monthData,
                                     }) => {

    const [isAddTime, setIsAddTime] = useState(false)
    const [daysWorkTime, setDaysWorkTime] = useState(getSelectedDatesWorkTimes(selectedDates, workTimes))
    const [timeFrom, setTimeFrom] = useState([] as Array<string>)
    const [timeTo, setTimeTo] = useState([] as Array<string>)
    const [deletedTimes, setDeletedTimes] = useState([] as Array<string>)
    const dispatch = useDispatch()

    useEffect(() => {
        if (isAddTime) {
            setTimeFrom([...timeFrom, ...getSelectedDatesTimesFrom(daysWorkTime, selectedDates)])
            setTimeTo([...timeTo, ...getSelectedDatesTimesTo(daysWorkTime, selectedDates)])
        }

    }, [daysWorkTime, selectedDates, isAddTime])

    const setTime = (newTimeFrom: string) => {
        const idx = timeOptions.indexOf(newTimeFrom)
        let newTimeTo = timeOptions[idx + 1]
        if (idx + 1 === timeOptions.length) {
            newTimeTo = timeOptions[0]
        }
        if (timeFrom.indexOf(newTimeFrom) === -1) {
            const workIds = getSelectedDatesWorkIdsByTimeFrom(daysWorkTime, newTimeFrom)
            setDeletedTimes(deletedTimes.filter(t => workIds.indexOf(t) === -1))
            setTimeFrom([...timeFrom, newTimeFrom])
            setTimeTo([...timeTo, newTimeTo])
        } else {
            const workIds = getSelectedDatesWorkIdsByTimeFrom(daysWorkTime, newTimeFrom)
            setDeletedTimes([...deletedTimes, ...workIds])
            setTimeFrom(timeFrom.filter(t => t !== newTimeFrom))
            setTimeTo(timeTo.filter(t => t !== newTimeTo))
        }
    }

    const selectWeek = () => {
        setSelectedDates(getAllWeeksWithSelectedDays(selectedDates, monthData))
    }

    const isActive = (time: string) => {
        return timeFrom.indexOf(time) !== -1
    }

    useEffect(() => {
        setDaysWorkTime(getSelectedDatesWorkTimes(selectedDates, workTimes))
    }, [selectedDates, workTimes])

    const onCreateCalendar = (goBack: boolean) => {
        if (selectedDates.length > 0 && isAddTime && timeFrom && timeTo) {
            dispatch(createCalendar(selectedDates, timeFrom, timeTo))
            if (deletedTimes.length > 0) {
                dispatch(deleteCalendar(deletedTimes))
            }
            setTimeFrom([])
            setTimeTo([])
            setDeletedTimes([])
            setIsAddTime(false)
            setSelectedDates([
                selectedDates.length == 1 ? new Date(selectedDates[0]) : new Date(selectedDates[selectedDates.length - 1])
            ])
        }
        if (goBack) setEditMod(false)
    }

    const onDeleteCalendar = async (work_id: string) => {
        await dispatch(deleteCalendar([work_id]))
    }

    return (
        <div className={styles.wrapper}>
            <Separator m={"32px 0 25px"}/>
            <div className={styles.title}>
                Рабочее время
            </div>
            {!isAddTime && daysWorkTime.length > 0 && // если задано рабочее время
            <>
                {daysWorkTime.map((item, idx) => (
                    <div key={idx}>
                        <div className={styles.label}>
                            для {transformDatesToString([new Date(item.day)]) + " " + MONTHS_RU[new Date(item.day).getMonth() as keyof typeof MONTHS_RU]}
                        </div>
                        <div className={styles.container}>
                            <div className={styles.times}>
                                {item.times.map((time, idx) => (
                                    <div className={styles.workTime} key={idx}>
                                        <div className={styles.item}>
                                            Рабочее время {time.time_from + "—" + time.time_to}
                                            <img src="/icons/calendar/delete.svg" alt=""
                                                 onClick={() => onDeleteCalendar(time.work_id)}
                                            />
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>))}

            </>}
            {!isAddTime && daysWorkTime.length === 0 && <div className={styles.message}>
                <img src="/icons/calendar/clock.svg" alt=""/>
                Рабочее время не задано
            </div>}
            {isAddTime && selectedDates.length > 0 && <div className={`${styles.label} ${styles.noMarginBottom}`}>
                для {transformDatesToString(selectedDates) + " " + MONTHS_RU[selectedDates[0].getMonth() as keyof typeof MONTHS_RU]}
            </div>}
            {/*{isAddTime && <div className={styles.addTime}>*/}
            {/*	<div className={styles.selectors}>*/}
            {/*		<select className={styles.select} onChange={onChangeTimeFrom}>*/}
            {/*         {timeOptions.map((item, idx) => (*/}
            {/*            <option value={item} key={idx}>{item}</option>*/}
            {/*         ))}*/}
            {/*		</select>*/}
            {/*		<div className={styles.separator}>—</div>*/}
            {/*		<select className={styles.select} onChange={onChangeTimeTo}>*/}
            {/*         {timeOptions.map((item, idx) => (*/}
            {/*            <option value={item} key={idx}>{item}</option>*/}
            {/*         ))}*/}
            {/*		</select>*/}
            {/*	</div>*/}
            {/*	<div className={styles.save} onClick={() => onCreateCalendar(false)}>Сохранить</div>*/}
            {/*</div>}*/}
            {isAddTime &&
            <div className={styles.container}>
                <div className={styles.grid}>
                    {timeOptions.map((item, idx) => (
                        <div className={`${styles.item}
                   ${isActive(item) ? styles.active : ""}`}
                             key={idx}
                             onClick={() => setTime(item)}>
                            {item}
                        </div>
                    ))}
                </div>
            </div>
            }
            {!isAddTime &&
            <Button size={"large"} mod={"secondary"} width={"full-width"} m={"20px 0 0"}
                    onClick={() => {
                        setEditMod(true)
                        setIsAddTime(true)
                    }}
            >
                {daysWorkTime.length > 0 ? "Редактировать время" : "Добавить время"}
            </Button>}
            {isAddTime && <div>
                <Button mod={"secondary"} width={"full-width"} onClick={() => selectWeek()}>Применить ко всей
                    неделе</Button>
                <div className={styles.row}>
                    <Button mod={"primary"} size={"large"} width={"full-width"} onClick={() => {

                        onCreateCalendar(true)
                    }}>
                        Сохранить
                    </Button>
                    <Button mod={"danger-light"} size={"large"} width={"full-width"} onClick={() => {
                        setSelectedDates([
                            selectedDates.length == 1 ? new Date(selectedDates[0]) : new Date(selectedDates[selectedDates.length - 1])
                        ])
                        setIsAddTime(false)
                        setEditMod(false)
                    }}>
                        Отменить
                    </Button>
                </div>
            </div>}
        </div>
    )
}

const timeOptions = [
    "00:00",
    "01:00",
    "02:00",
    "03:00",
    "04:00",
    "05:00",
    "06:00",
    "07:00",
    "08:00",
    "09:00",
    "10:00",
    "11:00",
    "12:00",
    "13:00",
    "14:00",
    "15:00",
    "16:00",
    "17:00",
    "18:00",
    "19:00",
    "20:00",
    "21:00",
    "22:00",
    "23:00",
]