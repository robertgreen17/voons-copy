import React, {FC, useEffect, useState} from "react"
import "./styles.scss"
import {
    getMonthData,
    isEqualDays,
    markMeetingDays,
    markSelectedDays,
    markWorkDays,
    WEEKDAYS_RU
} from "../../../utils/calendar";
import {Week} from "./Week";
import {Navigation} from "./Navigation";
import {DayT} from "./Day";
import {EditTime} from "./EditTime/EditTime";
import {useSelector} from "react-redux";
import {RootStateT} from "../../../redux/store";

type PropsT = {
    editMod: boolean
    setEditMod: (mod: boolean) => void
    targetData: any
}
export type MonthDataT = Array<Array<DayT | null>>

export const Calendar: FC<PropsT> = ({editMod, setEditMod, targetData}) => {

    const workTimes = useSelector((state: RootStateT) => state.calendar.workTimes) // раписание специалиста
    const meetings = useSelector((state: RootStateT) => state.events.meetings) // планируемые встречи
    const [date, setDate] = useState(new Date()) // текущая дата
    const [selectedDates, setSelectedDates] = useState([new Date()] as Array<Date>) // выбранные дни
    // const [selectedDates, setSelectedDates] = useState(targetData as Array<Date>) // выбранные дни
    const [monthData, setMonthData] = useState( // данные о всем месяце
        getMonthData(date.getFullYear(), date.getMonth())
    )


   useEffect(() => {
      if (!editMod) {
         // console.log(selectedDates);
         // setSelectedDates([selectedDates[selectedDates.length - 1]])
      }
   }, [editMod])
    const onSelectDay = (date: Date) => {
        if (selectedDates.filter(day => isEqualDays(day, date)).length > 0) {
            // если этот день уже выбран и это не единственный выбранный день
            if (editMod && selectedDates.length > 1) {
                // убираем пометку с дня, если он уже был выбран
                setSelectedDates(selectedDates.filter(day => !isEqualDays(day, date)))
            }
        } else {
            // setSelectedDates([date])
            if (editMod) {
                setSelectedDates([...selectedDates, date])
            } else {
                setSelectedDates([date])
            }
        }
    }

    useEffect(() => { // помечаем выбранные дни при изменении selectedDays
        setMonthData(markSelectedDays(monthData, selectedDates))
    }, [selectedDates])

    useEffect(() => { // помечаем рабочие дни при изменении расписания или выбранных дней
        setMonthData(markWorkDays(monthData, workTimes))
    }, [workTimes, selectedDates])

    useEffect(() => { // помечаем дни встреч при измении массива встреч или выбранных дней
        setMonthData(markMeetingDays(monthData, meetings))
    }, [meetings, date])


    return (
        <div>
            <div className="calendar">
                <Navigation date={date} setDate={setDate} setMonthData={setMonthData}
                            setSelectedDays={setSelectedDates}/>
                <div className="calendar-labels">
                    {WEEKDAYS_RU.map((day, idx) => (
                        <span key={idx}>{day}</span>
                    ))}
                </div>
                {monthData.map((week, idx) => (
                    <Week week={week} key={idx} onDayClick={onSelectDay}/>
                ))}
            </div>
            {/*{editMod ?*/}
            {/*   <EditTime*/}
            {/*      setSelectedDates={setSelectedDates}*/}
            {/*      setEditMod={setEditMod}*/}
            {/*      workTimes={workTimes ? workTimes : []}*/}
            {/*      selectedDates={selectedDates}/> :*/}
            {/*   <EventsTable*/}
            {/*      meetings={meetings}*/}
            {/*      workTimes={workTimes}*/}
            {/*      setEditMod={setEditMod}*/}
            {/*      selectedDate={selectedDates[0]}/>*/}
            {/*}*/}
            <EditTime
                monthData={monthData}
                setSelectedDates={setSelectedDates}
                setEditMod={setEditMod}
                workTimes={workTimes ? workTimes : []}
                selectedDates={selectedDates}/>
        </div>
    )
}