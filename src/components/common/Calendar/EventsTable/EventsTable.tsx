import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss"
import Moment from "react-moment";
import {Separator} from "../../Separator/Separator";
import {Button} from "../../Button/Button";
import {WorkTimeT} from "../../../../api/calendar-api";
import {getDayMeetings, getDayWorkTime} from "../../../../utils/calendar";
import {MeetingT} from "../../../../api/events-api";

type PropsT = {
   selectedDate: Date
   meetings: Array<MeetingT>
   setEditMod: (mod: boolean) => void
   workTimes: Array<WorkTimeT>
}

export const EventsTable: FC<PropsT> = ({
                                           selectedDate,
                                           setEditMod,
                                           workTimes,
                                           meetings
                                        }) => {

   const [selectedDateMeetings, setSelectedDateMeetings] = useState(getDayMeetings(selectedDate, meetings))

   useEffect(() => {
      setSelectedDateMeetings(getDayMeetings(selectedDate, meetings))
   }, [selectedDate, meetings])


   return (
      <div className={styles.wrapper}>
         <Separator m={"32px 0 25px"}/>
         <div className={styles.row}>
            {selectedDate && <Moment format="MMMM, DD, dddd" locale={"ru"} className={styles.title}>
               {selectedDate}
            </Moment>}
            {/*<div className={styles.label}>*/}
            {/*   {dayWorkTime.length > 0 ? `${dayWorkTime[0].time_from} — ${dayWorkTime[0].time_to}` : "Время не задано"}*/}
            {/*</div>*/}
         </div>
         {selectedDateMeetings.length > 0
            ? <div>
               {selectedDateMeetings.map((item, idx) => (
                  <div className={styles.meeting} key={idx}>
                     <div>{item.time_start} — {item.time_end}</div>
                     <div>{item.client.first_name} {item.client.last_name}</div>
                     <div>{item.price.format}</div>
                  </div>
               ))}
            </div>
            : <div className={styles.message}>
               <img src="/icons/calendar/clock.svg" alt=""/>
               На этот день встреч не запланировано
            </div>
         }
         <Button mod={"secondary"} size={"large"} m={"25px 0 0"} onClick={() => setEditMod(true)}>
            Редактировать время
         </Button>
      </div>
   )
}

const array: Array<any> = [
   // {
   //    time: "19:00—19:30",
   //    person: "Вероника Иванова",
   //    amount: "70000.00 ₽"
   // },
   // {
   //    time: "19:00—19:30",
   //    person: "Вероника Иванова",
   //    amount: "700.00 ₽"
   // },
]