import React, {FC} from "react"
import "./styles.scss"
import {Day, DayT} from "./Day";

type PropsT = {
   week: Array<DayT | null>
   onDayClick: (date: Date) => void
}

export const Week: FC<PropsT> = ({week, onDayClick}) => {

   return (
      <div className="calendar-week">
         {week.map((day, idx) => (
            <Day dayMod={day?.mod}
                 onDayClick={onDayClick}
                 date={day?.date}
                 statusMod={day?.statusMod} isNone={!day} key={idx}/>
         ))}
      </div>
   )
}