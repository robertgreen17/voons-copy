import React, {FC} from "react"
import styles from "./styles.module.scss"
import {Tag} from "../Tag/Tag";
import {NavLink} from "react-router-dom";
import {TagT} from "../../../api/search-api";
import {declReviews} from "../../../utils/string";

type PropsT = {
    name: string
    imgLink: string
    rating?: number
    reviews?: number
    tags?: Array<TagT | string>
    isChecked?: boolean
    size: "big" | "small"
    userId?: number
    imageWidth?: string,
    services?: Array<string>,
    isFree?: boolean
}

export const MiniProfile: FC<PropsT> = ({
                                            name,
                                            imgLink,
                                            rating,
                                            reviews,
                                            tags,
                                            isChecked,
                                            size,
                                            userId,
                                            imageWidth,
                                            services,
                                            isFree
                                        }) => {

    return (
        <div className={`${styles.wrapper} ${styles[size]}`}>
            <div className={styles.row}>
                <NavLink to={userId ? `/profi/${userId}` : ""}>
                    <img src={imgLink ? imgLink : "/icons/userAvatar.png"}
                         style={{width: imageWidth, height: imageWidth}}
                         alt="" className={styles.image}/>
                </NavLink>
                <div className={styles.info}>
                    <NavLink to={userId ? `/profi/${userId}` : ""}>
                        <div className={styles.name}>
                            {name}
                            {isChecked && <img src="/icons/checkCircle.svg" alt="" className={styles.tick}/>}
                        </div>
                    </NavLink>
                    <div className={styles.rowSecondary}>
                        {rating && +rating > 0 ? <div className={styles.statItem}>
                            <img src="/icons/star.svg" alt="" className={styles.icon}/>
                            {rating}
                        </div> : <></>}
                        {reviews && +reviews > 0 ? <div className={styles.statItem}>
                            <img src="/icons/chatBubble.svg" alt="" className={styles.icon}/>
                            {reviews} {declReviews(reviews)}
                        </div> : <></>}
                    </div>
                </div>
            </div>
            {isFree ? (
                <div className={styles.freeMeet}>
                    У вас есть бесплатная встреча с этим специалистом!
                </div>
            ) : ""}

            {tags && tags.length > 0 && <div className={styles.tags}>
                {tags.map((item, idx) => (
                    <div key={idx}>
                        <Tag size={"small"} mod={"secondary"}>{typeof item === "string" || !item ? item : item.name_ru}</Tag>
                    </div>
                ))}
            </div>}

            {services && services.length > 0 && <div className={styles.tags}>
                {services.map((item, idx) => (
                    <div key={idx}>
                        <Tag size={"small"} mod={"secondary"}>{item}</Tag>
                    </div>
                ))}
            </div>}
        </div>
    )
}