import React, {FC, LegacyRef} from "react"
import styles from "./styles.module.scss";
import {LinkT} from "../../../../redux/meeting-reducer/meeting-reducer";
import Moment from "react-moment";

type PropsT = {
   text: string
   mod: "active" | "default"
   time: number
   link?: LinkT
   innerRef?: LegacyRef<any>
}

export const Message: FC<PropsT> = ({text, mod, time, link, innerRef}) => {

   return (
      <div className={`${styles.wrapper} animate__animated animate__fadeInDown animate__faster ${styles[mod]}`} ref={innerRef}>
         <div className={`${styles.container} `}>
            <div className={styles.text}>
               {text}
            </div>
            {link && <a className={styles.link} href={"http://" + link.url} target={"_blank"} rel="noopener noreferrer">
               <div className={styles.title}>
                  {link.title}
               </div>
               <div className={styles.url}>
                  {link.url}
               </div>
            </a>}
         </div>
         <div className={styles.time}>
           <Moment format="HH:mm">{new Date(time * 1000)}</Moment>
         </div>
      </div>
   )
}