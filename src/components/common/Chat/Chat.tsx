import React, {FC, useEffect, useRef, useState} from "react"
import styles from "./styles.module.scss";
import {Message} from "./Message/Message";
import TextareaAutosize from 'react-textarea-autosize';
import {useDispatch, useSelector} from "react-redux";
import {useForm} from "react-hook-form";
import {SendMessageReqBody} from "../../../api/meeting-api";
import meeting, {sendMessage} from "../../../redux/meeting-reducer/meeting-reducer";
import {RootStateT} from "../../../redux/store";
import {UserTypeT} from "../../../redux/user-reducer/user-reducer";

type PropsT = {
   onCloseCallBack: () => void
   eventId: string
   userMongoId: string
   userId: number
}

type FormData = {
   text: string
}

export const Chat: FC<PropsT> = ({
                                    eventId,
                                    userMongoId,
                                    onCloseCallBack,
                                    userId
                                 }) => {
   const [animationClass, setAnimationClass] = useState("animate__slideInRight ")
   const dispatch = useDispatch()
   const {register, handleSubmit, reset} = useForm<FormData>();
   let messages = useSelector((state: RootStateT) => state.meeting.messages)
   const userType = useSelector((state: RootStateT) => state.user.type)
   const messagesCached = localStorage.getItem("messages")

   if (messages.length === 0 && messagesCached) {
      dispatch(meeting.actions.setAllMessages(JSON.parse(messagesCached)))
   }

   const messagesEndRef = useRef<HTMLDivElement>(null)

   const scrollToBottom = () => {
      if (messagesEndRef.current) {
         messagesEndRef.current.scrollIntoView({behavior: "smooth"})
      }
   }

   useEffect(() => {
      scrollToBottom()
   }, [messages])

   const onClose = () => {
      setAnimationClass("animate__slideOutRight")
      onCloseCallBack()
      dispatch(meeting.actions.setIsNewMessage(false))
   }

   const onSubmit = handleSubmit((data: FormData) => {
      const body: SendMessageReqBody = {
         attachments: [],
         by_id: userMongoId,
         event_id: eventId,
         text: data.text
      }
      dispatch(sendMessage(body, reset))
   });

   const keyDownListener = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
      if (e.key === "Enter") {
         e.preventDefault()
         onSubmit()
      }
   }

   return (
      <div className={styles.wrapper}>
         <div className={`${styles.container} animate__animated animate__faster ${animationClass}`}>
            <div className={styles.column}>
               <div className={styles.header}>
                  <button className={styles.btn} onClick={onClose}>
                     <img src="/icons/stream/openChat.svg" alt=""/>
                  </button>
                  <div className={`${styles.title}`}>
                     Чат {userType ===  UserTypeT.client
                     ? "со специалистом"
                     : "с клиентом"
                  }
                  </div>
               </div>
               <div className={styles.messages}>
                  {messages.map((item, idx) => (
                     <Message text={item.message}
                              link={item.link}
                              time={item.time}
                              key={idx}
                              mod={item.user.user_id === userId ? "active" : "default"}/>
                  ))}
                  <div ref={messagesEndRef}/>
               </div>
               <form className={styles.area} onSubmit={onSubmit}>
                  <div className={styles.row}>
                     <TextareaAutosize
                        onKeyDown={keyDownListener}
                        name={"text"}
                        ref={register}
                        autoFocus={true}
                        placeholder={"Ваше сообщение"}
                        className={styles.textarea}
                        minRows={1}
                        maxRows={5}/>
                  </div>
                  <button className={styles.submit} type={"submit"}>
                     <img src="/icons/arrowIcon.svg" alt=""/>
                  </button>
               </form>
            </div>
         </div>
      </div>
   )
}