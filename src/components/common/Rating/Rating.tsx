import React, {FC, useEffect, useState} from "react";
import styles from "./styles.module.scss";

type PropsT = {
   setRating: (value: number) => void
   rating: number
}

export const Rating: FC<PropsT> = ({setRating, rating}) => {


   // const [stars, setStars] = useState(Array.from({length: 5}, (v, idx) => idx + 1))
   const [hovered, setHovered] = useState(0)

   const stars = [1, 2, 3, 4, 5]
   const selectedIcon = "/icons/star.svg"
   const deselectedIcon = "/icons/starEmpty.svg"

   return (
      <div className={styles.wrapper}>
         {stars.map((star, idx) => (
            <img key={idx}
                 onClick={() => setRating(star)}
                 onMouseEnter={() => setHovered(star)}
                 onMouseLeave={() => setHovered(0)}
                 className={styles.star}
                 src={rating < star ?
                    hovered < star ? deselectedIcon : selectedIcon
                    :
                    selectedIcon
                 }
                 alt=""
            />
         ))}
      </div>
   )
}

