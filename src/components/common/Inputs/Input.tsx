import React, {FC, HTMLProps, Ref} from "react";
import "./styles.scss"
import {FormikErrors} from "formik";
import MaskedInput, {maskArray, MaskedInputProps} from "react-text-mask";
import {FieldError} from "react-hook-form";


type InputGroupPropsT = {
    mod?: "error"
    label?: string
    m?: string
    errorMessage?: string | Array<string> | FormikErrors<any> | Array<FormikErrors<any>> | FieldError
    inputRef?: Ref<any>
}

export const Input: FC<InputGroupPropsT & HTMLProps<HTMLInputElement>> = ({
                                                                              mod,
                                                                              label,
                                                                              m,
                                                                              errorMessage,
                                                                              inputRef,
                                                                              ...rest
                                                                          }) => {
    return (
        <div className={`input-group ${mod}`} style={{margin: m}}>
            <input type="text" {...rest} ref={inputRef}/>
            <span className="highlight"/>
            {label && <label>{label}</label>}
        </div>
    )
}


type InputWithMaskPropsT = {
    mod?: "error"
    label: string
    m?: string
    errorMessage?: string | Array<string> | FormikErrors<any> | Array<FormikErrors<any>>
    mask: maskArray
    isError?: boolean
}

export const InputWithMask: FC<InputWithMaskPropsT & MaskedInputProps> = ({
                                                                              mask,
                                                                              mod,
                                                                              m,
                                                                              label,
                                                                              isError,
                                                                              errorMessage,
                                                                              ...rest
                                                                          }) => {
    return (
        <div className={`input-group ${mod}`} style={{margin: m}}>
            <MaskedInput type="text" required mask={mask} {...rest}/>
            <span className="highlight"/>
            <label>{label}</label>
        </div>
    )
}

export const SearchInput: FC<HTMLProps<HTMLInputElement> & TextAreaPropsT> = ({inputRef, ...rest}) => {
    return (
        <div className="search-group">
            <img src="/icons/search.svg" alt=""/>
            <input type="text" ref={inputRef} {...rest}/>
        </div>
    )
}
type TextAreaPropsT = {
    rows?: number
    m?: string
    inputRef?: Ref<any>
    bg?: string
}

export const TextArea: FC<TextAreaPropsT & HTMLProps<HTMLTextAreaElement>> = ({
                                                                                  m,
                                                                                  inputRef,
                                                                                  bg,
                                                                                  rows = 3,
                                                                                  ...rest
                                                                              }) => {
    return (
        <textarea className="textarea" rows={rows} data-min-rows="3"
                  {...rest}
                  style={{margin: m, background: bg}}
                  ref={inputRef}/>
    )
}
type CheckBoxPropsT = {
    label?: string
    m?: string
}

export const CheckBox: FC<CheckBoxPropsT & HTMLProps<HTMLInputElement>> = ({
                                                                               label,
                                                                               m,
                                                                               ...rest
                                                                           }) => {


    return (
        <label className="checkbox" style={{margin: m}}>
            <input className="checkbox__input" type="checkbox" {...rest}/>
            <svg className="checkbox__check" width="24" height="24">
                <polyline points="20 6 9 17 4 12"/>
            </svg>
            <div className={"checkbox__text"}>
                {label}
            </div>
        </label>
    )
}

type ControlPropsT = {
    active: 0 | 1
    setActive: (number: 0 | 1) => void
    labels: Array<string>
    m?: string
}

export const SegmentControl: FC<ControlPropsT> = ({active, setActive, labels, m, ...rest}) => {

    return (
        <div className="segmentControl" style={{margin: m}}>
            <div className="wrap">
                <button
                    type={"button"}
                    onClick={() => setActive(0)}
                    className={`nav ${active === 0 && "active"}`}>{labels[0]}
                </button>
            </div>
            <div className="wrap">
                <button
                    type={"button"}
                    onClick={() => setActive(1)}
                    className={`nav ${active === 1 && "active"}`}>{labels[1]}
                </button>
            </div>
        </div>
    )
}