import React, {FC} from 'react';
import styles from "./styles.module.scss"

type PropsT = {
    full?: true
    size?: "big" | "small"
}

export const Modal: FC<PropsT> = ({
                                      children,
                                      size = "small",
                                      full
                                  }) => {
    if (full === undefined) {
        return (
            <div className={`${styles.modal} ${styles[size]} animate__animated animate__faster animate__fadeInDown`}>
                <div className={styles.full_container}>
                    {children}
                </div>
            </div>
        )
    }
    else {
        return (
            <div className={`${styles.modal_mini} ${styles[size]} animate__animated animate__faster animate__fadeInDown`}>
                <div className={styles.full_container}>
                    {children}
                </div>
            </div>
        )
    }
}