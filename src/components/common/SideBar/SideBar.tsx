import React, {FC, useState} from "react"
import styles from "./styles.module.scss"
import {DarkBg} from "../DarkBg/DarkBg";

type PropsT = {
   onCloseCallBack: () => void
   title?: string
   size?: "big" | "medium" | "small" | "extrasmall"
   contentAlign?: "stretch" | "start"
   isArrow?: boolean
   onClickArrow?: () => void
   isNoDarkBg?: boolean
}

export const SideBar: FC<PropsT> = ({
                                       onCloseCallBack,
                                       isArrow,
                                       title,
                                       size = "small",
                                       contentAlign = "stretch",
                                       onClickArrow,
                                       isNoDarkBg,
                                       children,
                                    }) => {

   const [animationClass, setAnimationClass] = useState("animate__slideInRight ")

   const onClose = () => {
      setAnimationClass("animate__slideOutRight")
      setTimeout(() => {
         onCloseCallBack()
      }, 250)
   }

   return (
      <div className={`${styles.wrapper} ${styles[contentAlign]} ${isNoDarkBg && styles.noDark}`}>
         {!isNoDarkBg && <DarkBg/>}
         <div
            className={`${styles.container} animate__animated animate__faster ${animationClass} ${styles[size]}`}>
            {!isArrow ? <div className={`${styles.title} ${!title && styles.noContent}`}>
                  {title}
                  <img id="closeSidebar" src="/icons/navigation/cross.svg"
                       onClick={onClose}
                       alt={""}
                  />
               </div> :
               <div className={`${styles.title}`}>
                  <img src="/icons/navigation/arrowBackBig.svg"
                       onClick={onClickArrow}
                       alt={""}
                  />
               </div>}

            {children}
         </div>
      </div>
   )
}