import React from 'react';
import ReactDOM from 'react-dom';
import "./styles/normalize.scss";
import "./styles/common.scss";
import {App} from './App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
// import {YMInitializer} from "react-yandex-metrika";

require('intersection-observer');

ReactDOM.render(
    <React.StrictMode>
        {/*<YMInitializer accounts={[987654321]} options={{webvisor: true, trackLinks: true, accurateTrackBounce: true, clickmap: true}}/>*/}
        <Provider store={store}>
            <BrowserRouter>
                {/*<Switch>*/}
                {/*   <Route path={"/files/:id/:file"} component={Document}/>*/}
                {/*   <App/>*/}
                {/*</Switch>*/}
                <App/>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);