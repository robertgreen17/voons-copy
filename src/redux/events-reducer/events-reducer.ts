import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {BaseThunkT} from "../store";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {
   CreateEventReqBodyT,
   eventsApi,
   GetFreeTimesReqBodyT,
   GetMeetingsReqBody,
   MeetingT,
   ObjectId,
   PriceT,
   SetTimesPriceReqBodyT,
   TimesT
} from "../../api/events-api";
import {checkMessage} from "../../utils/notifications";
import {getUserData} from "../user-reducer/user-reducer";
import {MEETINGS_LIMIT} from "../constants";

const initialState = {
   timesPrice: null as null | Array<PriceT>,
   timeTable: [] as Array<TimesT>,
   meetings: [] as Array<MeetingT>,
   isFreeMeeting: false,
   missingMoney: 0,
}

const events = createSlice({
   name: 'events',
   initialState: initialState,
   reducers: {
      setTimesPrice: (state: EventsStateT, {payload}: PayloadAction<Array<PriceT>>): EventsStateT => ({
         ...state,
         timesPrice: payload
      }),
      setTimeTable: (state: EventsStateT, {payload}: PayloadAction<Array<TimesT>>): EventsStateT => ({
         ...state,
         timeTable: payload
      }),
      setMissingMoney: (state: EventsStateT, {payload}: PayloadAction<number>): EventsStateT => ({
         ...state,
         missingMoney: payload
      }),
      setIsFreeFirst: (state: EventsStateT, {payload}: PayloadAction<boolean>): EventsStateT => ({
         ...state,
         isFreeMeeting: payload
      }),
      setMeetings: (state: EventsStateT, {payload}: PayloadAction<Array<MeetingT>>): EventsStateT => ({
         ...state,
         meetings: payload
      }),
      addMeetings: (state: EventsStateT, {payload}: PayloadAction<Array<MeetingT>>): EventsStateT => ({
         ...state,
         meetings: [...state.meetings, ...payload]
      }),
      exit: (state: EventsStateT): EventsStateT => ({
         ...state,
         timesPrice: null
      }),
   }
})
export default events

export const createEvent = (body: CreateEventReqBodyT,
                            onSuccess?: () => void,
                            onIsNoMoney?: () => void,
                            checkMessageOnlyOnSuccess?: boolean,
                            isDelayedAppointment?: boolean): BaseThunkT => {
   // make appointment
   return async (dispatch, getState) => {
      await commonThunkHandler(async () => {
         const resData = await eventsApi.createEvent(body)
         if (resData.success) {
            if (onSuccess) onSuccess()
            if (checkMessageOnlyOnSuccess) checkMessage(resData)
         } else if (resData.data.isNoMoney) {
            if (onIsNoMoney) onIsNoMoney()
            if (!isDelayedAppointment) {
               const balance = getState().user.userData?.balance
               const price = getState().search.currentUserData?.times_price?.filter(t => t.time == body.time)[0].price
               if (balance !== undefined && price !== undefined) {
                  const missingMoney = +price - balance
                  dispatch(events.actions.setMissingMoney(missingMoney))
               }
               localStorage.setItem("delayed_appointment", JSON.stringify(body))
            }
         }
         await checkForRefresh(resData, dispatch)
         if (!checkMessageOnlyOnSuccess) checkMessage(resData)
      }, dispatch, true)
   }
}
export const setTimesPrice = (body: SetTimesPriceReqBodyT): BaseThunkT => {
   //set price policy
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await eventsApi.setTimesPrice(body)
         if (resData.success) {
            await dispatch(getUserData({}))
         }
         await checkForRefresh(resData, dispatch)
         checkMessage(resData)
      }, dispatch, true)
   }
}

export const setTimesPriceWithoutMessage = (body: SetTimesPriceReqBodyT): BaseThunkT => {
   //set price policy
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await eventsApi.setTimesPrice(body)
         if (resData.success) {
            await dispatch(getUserData({}))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}

export const getTimeTable = (dateFrom: number, dateTo: number, mongoId: ObjectId): BaseThunkT => {
   // get free times for appointment
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const body: GetFreeTimesReqBodyT = {
            date_from: Math.floor(dateFrom / 1000), // делаем секунды вместо милисекунд
            date_to: Math.floor(dateTo / 1000), // делаем секунды вместо милисекунд
            specialist_id: mongoId,
         }
         const resData = await eventsApi.getFreeTimes(body)

         if (resData.success) {
            // массив с расписанием приходит в обратном порядке, поэтому разворачиваю
            dispatch(events.actions.setTimeTable(resData.data.times.reverse()))
            dispatch(events.actions.setIsFreeFirst(resData.data.available_free))
         }
         await checkForRefresh(resData, dispatch)

      }, dispatch, true)
   }
}
export const getMeetings = (body: GetMeetingsReqBody,
                            isLoader?: boolean,
                            isAddMeetings?: boolean,
                            setHasMore?: (flag: boolean) => void): BaseThunkT => {
   // get meetings
   return async (dispatch) => {
      await commonThunkHandler(async () => {

         body.limit = MEETINGS_LIMIT
         const resData = await eventsApi.getMeetings(body)

         if (resData.success) {
            if (isAddMeetings) {
               if (resData.data.length < MEETINGS_LIMIT && setHasMore) {
                  setHasMore(false)
               }
               dispatch(events.actions.addMeetings(resData.data))
            } else {
               dispatch(events.actions.setMeetings(resData.data))
            }
         }
         await checkForRefresh(resData, dispatch)

      }, dispatch, isLoader === undefined ? true : isLoader)
   }
}


// types
export type EventsStateT = typeof initialState