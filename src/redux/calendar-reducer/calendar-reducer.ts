import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {BaseThunkT} from "../store";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {calendarApi, CreateCalendarReqBodyT, GetWorkTimesReqBodyT, GetWorkTimesResDataT} from "../../api/calendar-api";
import {checkMessage} from "../../utils/notifications";
import {getWorkIdByDate} from "../../utils/calendar";

const initialState = {
   workTimes: [] as GetWorkTimesResDataT
}

const calendar = createSlice({
   name: 'calendar',
   initialState: initialState,
   reducers: {
      setWorkTimes: (state: CalendarStateT, {payload}: PayloadAction<GetWorkTimesResDataT>): CalendarStateT => ({
         ...state,
         workTimes: payload
      }),
      exit: (state: CalendarStateT): CalendarStateT => ({
         ...state,
         workTimes: [],
      }),
   }
})
export default calendar

export const getWorkTimes = (body: GetWorkTimesReqBodyT): BaseThunkT => {
   // get specialist's work time schedule
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await calendarApi.getWorkTimes(body)
         if (resData.success) {
            dispatch(calendar.actions.setWorkTimes(resData.data))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}

export const createCalendar = (selectedDays: Array<Date> ,
                               timeFrom: Array<string>,
                               timeTo: Array<string>): BaseThunkT => {
   // create specialist's work time schedule
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         if (timeFrom.length === 0 || timeTo.length === 0 || selectedDays.length === 0) return
         const body: CreateCalendarReqBodyT = {
            time_from: timeFrom,
            time_to: timeTo,
            date: selectedDays.map(date => +date / 1000) // преобразуем в timestamp в секунды
         }

         const resData = await calendarApi.createCalendar(body)

         if (resData.success) {
            await dispatch(getWorkTimes({}))
         }
         await checkForRefresh(resData, dispatch)
         checkMessage({success: true, message: "Настройки календаря сохранены", data: []})
      }, dispatch, true)
   }
}

export const deleteCalendar = (work_id: Array<string>): BaseThunkT => {
   // delete specialist's work time schedule
   return async (dispatch, getState) => {
      await commonThunkHandler(async () => {

         if (work_id.length === 0) return

         const body = {
            work_id: work_id
         }
         const resData = await calendarApi.deleteCalendar(body)
         if (resData.success) {
            await dispatch(getWorkTimes({}))
         }
      }, dispatch, true)
   }
}

// types
export type CalendarStateT = typeof initialState