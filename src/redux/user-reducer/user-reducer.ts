import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {BaseThunkT} from "../store";
import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {checkMessage} from "../../utils/notifications";
import {
    ChangeAccountReqBodyT, ChangeNotifyReqBodyT,
    ChangePasswordReqBodyT,
    ChangeSettingsReqBodyT,
    DocT,
    GetDocsReqBodyT,
    GetDocsResDataT,
    GetUsersReqBodyT, NotifyT, RatingT, RefillReqBodyT,
    RemoveDocsReqBodyT, ReviewT, SendSupportMessageReqBodyT, SetSocialReqBodyT, StatsT, StatT, SupportMessageT,
    userApi, WithdrawReqBodyT
} from "../../api/user-api";
import {transformPricesToObject} from "../../utils/transformators";
import {ObjectId} from "../../api/events-api";
import {FindReqBodyT, searchApi, TagT} from "../../api/search-api";
import {USERS_LIMIT} from "../constants";
import search from "../search-reducer/search-reducer";
import support from "../support-reducer/support-reducer";

const initialState = {
    type: null as null | UserTypeT,
    userData: null as null | UserDataT,
    docs: null as null | GetDocsResDataT,
}

const user = createSlice({
    name: 'user',
    initialState: initialState,
    reducers: {
        setType: (state: UserStateT, {payload}: PayloadAction<UserTypeT>): UserStateT => ({
            ...state,
            type: payload
        }),
        setUserData: (state: UserStateT, {payload}: PayloadAction<UserDataT>): UserStateT => ({
            ...state,
            userData: payload
        }),
        setDocs: (state: UserStateT, {payload}: PayloadAction<GetDocsResDataT | null>): UserStateT => ({
            ...state,
            docs: payload
        }),
        setAvatar: (state: UserStateT, {payload}: PayloadAction<string>): UserStateT => ({
            ...state,
            userData: {
                ...state.userData,
                avatar: payload
            }
        }),
        exit: (state: UserStateT): UserStateT => ({
            ...state,
            type: null,
            userData: null,
        }),
    }
})
export default user

export const getUserData = (body: GetUsersReqBodyT): BaseThunkT => {
    // get user data account
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            if (getState().auth.isAuth) {
                const resData = await userApi.getUsers(body)
                if (resData.success) {
                    let userData: UserDataT = { // приводим данные о пользователе в нужный вид
                        ...resData.data,
                        times_price: transformPricesToObject(resData.data.times_price)
                    }
                    if (resData.data.account_type !== undefined) {
                        dispatch(user.actions.setType(resData.data.account_type))
                    }
                    dispatch(user.actions.setUserData(userData))
                    if (resData.data?._id?.$oid) {
                        dispatch(support.actions.setUserId(resData.data._id.$oid))
                    }
                }
                await checkForRefresh(resData, dispatch)
            }
        }, dispatch)
    }
}
export const getDocs = (body: GetDocsReqBodyT): BaseThunkT => {
    // get user docs
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.getDocs(body)
            if (resData.success) {
                dispatch(user.actions.setDocs(resData.data))
            }
            await checkForRefresh(resData, dispatch)
        }, dispatch, true)
    }
}
export const setDocs = (docs: Array<File>,
                        onSuccess?: () => void): BaseThunkT => {
    // set user's docs
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            let body = new FormData()
            if (docs.length > 1) {
                // если больше одного документа, то формируем массив в FormData
                docs.forEach(doc => {
                    body.append("files[]", doc)
                })
            } else {
                body.append("file", docs[0])
            }
            const resData = await userApi.setDocs(body)
            if (resData.success) {
                dispatch(user.actions.setDocs(resData.data))
                if (onSuccess) onSuccess()
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}
export const removeDocs = (ids: Array<string>,
                           onSuccess?: () => void): BaseThunkT => {
    // remove user's docs
    return async (dispatch) => {
        await commonThunkHandler(async () => {
            let body: RemoveDocsReqBodyT = {}
            if (ids.length > 1) {
                body = {
                    ids: ids
                }
            } else {
                body = {
                    id: ids[0]
                }
            }
            const resData = await userApi.removeDocs(body)
            if (resData.success) {
                await dispatch(getUserData({}))
                if (onSuccess) onSuccess()
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}
export const setAvatar = (image: File | null, onSuccess?: () => void): BaseThunkT => {
    // set user avatar
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            // делаем из File объект FormData для отправки на сервер

            let formData = image ? new FormData() : null;
            if (image && formData) formData.append("avatar", image)

            const resData = await userApi.setAvatar(formData)
            if (resData.success) {
                if (image) {
                    dispatch(user.actions.setAvatar(URL.createObjectURL(image)))
                } else {
                    dispatch(user.actions.setAvatar(""))
                }
                if (onSuccess) onSuccess()
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}
export const changeAccount = (body: ChangeAccountReqBodyT): BaseThunkT => {
    // change user profile data
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.changeAccount(body)
            if (resData.success) {
                await dispatch(getUserData({}))
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}
export const setSocial = (body: SetSocialReqBodyT): BaseThunkT => {
    // set social networks
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.setSocial(body)
            if (resData.success) {
                await dispatch(getUserData({}))
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}

export const changeNotify = (body: ChangeNotifyReqBodyT): BaseThunkT => {
    // change user notifications settings
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.changeNotify(body)
            if (resData.success) {
                await dispatch(getUserData({}))
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}
export const switchFirstMeetingFree = (body: { free: boolean }): BaseThunkT => {
    // change bonus
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.switchFirstMeetingFree(body)
            if (resData.success) {
                await dispatch(getUserData({}))
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}

export const changePassword = (body: ChangePasswordReqBodyT,
                               resetForm: () => void): BaseThunkT => {
    // change user password
    return async (dispatch) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.changePassword(body)
            if (resData.success) {
                await dispatch(getUserData({}))
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
            resetForm()
        }, dispatch, true)
    }
}
export const changeSettings = (body: ChangeSettingsReqBodyT): BaseThunkT => {
    // change user account data
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.changeSettings(body)
            if (resData.success) {
                await dispatch(getUserData({}))
            }
            await checkForRefresh(resData, dispatch)
            checkMessage(resData)
        }, dispatch, true)
    }
}
export const refill = (body: RefillReqBodyT, onSuccess?: () => void): BaseThunkT => {
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.refill(body)
            if (resData.success) {
                if (onSuccess) onSuccess()
                window.location.href = resData.data.url
            }
            await checkForRefresh(resData, dispatch)
        }, dispatch, true)
    }
}
export const withdraw = (body: WithdrawReqBodyT, onSuccess?: () => void): BaseThunkT => {
    return async (dispatch, getState) => {
        await commonThunkHandler(async () => {
            const resData = await userApi.withdraw(body)
            if (resData.success) {
                if (onSuccess) onSuccess()
            }
            checkMessage(resData)
            await checkForRefresh(resData, dispatch)
        }, dispatch, true)
    }
}


export enum UserTypeT {
    client,
    profi,
}

export type UserStateT = typeof initialState
export type DocsT = {
    [key: string]: {
        url: string
        type: string
    }
}

export type UserDataT = {
    first_enter?: boolean
    first_name?: string
    last_name?: string
    email?: string
    country?: string
    phone?: string
    account_type?: UserTypeT
    avatar?: string
    user_id?: number
    _id?: ObjectId
    files?: Record<string, DocT>
    verified?: boolean
    verify_telegram?: string
    times_price?: Record<number, string>
    balance?: number
    work_info?: string
    tags?: Array<TagT | string>
    lang?: Array<string>
    services?: Array<string>
    notify?: NotifyT
    free_first?: boolean
    stat?: StatsT
    rating?: RatingT
    reviews_count?: number
    verified_specialist?: boolean
    reviews?: Array<ReviewT>
    social?: SetSocialReqBodyT,
    av_sum?: number
}