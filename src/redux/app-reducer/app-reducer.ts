import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {BaseThunkT} from "../store";
import auth, {refresh} from "../auth-reducer/auth-reducer";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import user, {getUserData} from "../user-reducer/user-reducer";
import search from "../search-reducer/search-reducer";
import events, {createEvent} from "../events-reducer/events-reducer";
import calendar from "../calendar-reducer/calendar-reducer";
import {NotificationT} from "../../hooks/listeners/useNotificationsListener";
import {userApi} from "../../api/user-api";
import support from "../support-reducer/support-reducer";
import {CreateEventReqBodyT} from "../../api/events-api";

const initialState = {
   isInit: false,
   isLoader: false,
   notifications: [] as Array<NotificationT>
}

const app = createSlice({
   name: 'app',
   initialState: initialState,
   reducers: {
      setIsInit: (state: AppStateT, {payload}: PayloadAction<boolean>): AppStateT => ({
         ...state,
         isInit: payload,
      }),
      setIsLoader: (state: AppStateT, {payload}: PayloadAction<boolean>): AppStateT => ({
         ...state,
         isLoader: payload,
      }),
      addNotification: (state: AppStateT, {payload}: PayloadAction<NotificationT>): AppStateT => ({
         ...state,
         notifications: [payload, ...state.notifications],
      }),
      setNotifications: (state: AppStateT, {payload}: PayloadAction<Array<NotificationT>>): AppStateT => ({
         ...state,
         notifications: payload,
      }),
      exit: (state: AppStateT): AppStateT => ({
         ...state,
         isLoader: false,
         isInit: false,
      }),
   }
})
export default app


export const initialize = (): BaseThunkT => {
   // initializing of application (get necessary data, refresh tokens, etc)
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         await dispatch(refresh())
         await dispatch(getUserData({}))
         const body = localStorage.getItem("delayed_appointment")
         if (body) {
            const bodyObj = JSON.parse(body)
            await dispatch(createEvent(bodyObj as CreateEventReqBodyT,
               () => localStorage.setItem("delayed_appointment", ""),
               () => localStorage.setItem("delayed_appointment", ""),
               true,
               true))
         }
         dispatch(app.actions.setIsInit(true))
      }, dispatch)
   }
}
export const getNotifies = (): BaseThunkT => {
   // get user notifications
   return async (dispatch, getState) => {
      await commonThunkHandler(async () => {
         const resData = await userApi.getNotifies()
         if (resData.success) {
            dispatch(app.actions.setNotifications(resData.data))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch)
   }
}
export const clearNotifies = (): BaseThunkT => {
   // clear user notifications
   return async (dispatch, getState) => {
      await commonThunkHandler(async () => {
         const resData = await userApi.clearNotifies()
         if (resData.success) {
            dispatch(app.actions.setNotifications([]))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch)
   }
}
export const exit = (): BaseThunkT => {
   // exit of application and clear all data
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         dispatch(auth.actions.exit())
         dispatch(app.actions.exit())
         dispatch(user.actions.exit())
         dispatch(search.actions.exit())
         dispatch(events.actions.exit())
         dispatch(calendar.actions.exit())
         dispatch(support.actions.exit())

         // Cookies.remove("wpac_id", {path: "/"})
         localStorage.setItem("refresh", "")
      }, dispatch)
   }
}

// types
export type AppStateT = typeof initialState
