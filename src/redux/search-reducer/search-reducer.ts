import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {BaseThunkT} from "../store";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {GetUsersReqBodyT, userApi, GetUsersResDataT} from "../../api/user-api";
import {CategoryT, FindReqBodyT, searchApi, SearchUserT} from "../../api/search-api";
import {USERS_LIMIT} from "../constants";



const initialState = {
   currentUserData: null as null | GetUsersResDataT, // информация о пользователе, на профиль которого мы перешли
   searchingData: [] as Array<SearchUserT>,
   categories: [] as Array<CategoryT>,
   currentSearchData: null as null | FindReqBodyT,
   inputValue: "",
   isResetSearch: false,
   offset: 0,
}

const search = createSlice({
   name: 'search',
   initialState: initialState,
   reducers: {
      setCurrentUserData: (state: SearchStateT, {payload}: PayloadAction<GetUsersResDataT>): SearchStateT => ({
         ...state,
         currentUserData: payload
      }),
      setSearchingData: (state: SearchStateT, {payload}: PayloadAction<Array<SearchUserT>>): SearchStateT => ({
         ...state,
         searchingData: payload
      }),
      addSearchingData: (state: SearchStateT, {payload}: PayloadAction<Array<SearchUserT>>): SearchStateT => ({
         ...state,
         searchingData: [...state.searchingData, ...payload]
      }),
      setCategories: (state: SearchStateT, {payload}: PayloadAction<Array<CategoryT>>): SearchStateT => ({
         ...state,
         categories: payload
      }),
      setCurrentSearchData: (state: SearchStateT, {payload}: PayloadAction<FindReqBodyT | null>): SearchStateT => ({
         ...state,
         currentSearchData: payload
      }),
      setInputValue: (state: SearchStateT, {payload}: PayloadAction<string>): SearchStateT => ({
         ...state,
         inputValue: payload
      }),
      setIsResetSearch: (state: SearchStateT, {payload}: PayloadAction<boolean>): SearchStateT => ({
         ...state,
         isResetSearch: payload
      }),
      setOffset: (state: SearchStateT, {payload}: PayloadAction<number>): SearchStateT => ({
         ...state,
         offset: payload
      }),
      exit: (state: SearchStateT): SearchStateT => ({
         ...state,
         currentUserData: null,
      }),
   }
})
export default search

export const getCurrentUserData = (body: GetUsersReqBodyT): BaseThunkT => {
   // get current user profile data, when you search specialists and visit their profiles
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await userApi.getUsers(body)
         if (resData.success) {
            dispatch(search.actions.setCurrentUserData(resData.data))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}
export const getCategories = (): BaseThunkT => {
   // get categories of specialists
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await searchApi.getCategories()
         if (resData.success) {
            dispatch(search.actions.setCategories(resData.data))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}
export const find = (body: FindReqBodyT,
                     onSuccess?: () => void): BaseThunkT => {
   // find users
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await searchApi.find(body)
         if (resData.success) {
            dispatch(search.actions.setSearchingData(resData.data))
            dispatch(search.actions.setCurrentSearchData(body))
            if (onSuccess) {
               await dispatch(search.actions.setInputValue(body.name ? body.name : ""))
               onSuccess()
            }
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}
export const infinityFind = (body: FindReqBodyT,
                             setLoader: (flag: boolean) => void,
                             setHasMore: (flag: boolean) => void): BaseThunkT => {
   // find users
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         setLoader(true)
         const resData = await searchApi.find(body)
         if (resData.success) {
            dispatch(search.actions.addSearchingData(resData.data))
            if (resData.data.length < USERS_LIMIT) {
               setHasMore(false)
            }
         }
         await checkForRefresh(resData, dispatch)
         setLoader(false)
      }, dispatch, false)
   }
}
// types
export type SearchStateT = typeof initialState
