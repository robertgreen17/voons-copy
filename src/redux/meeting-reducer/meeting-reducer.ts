import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {BaseThunkT} from "../store";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {eventsApi, MeetingT} from "../../api/events-api";
import {meetingApi, RateReqBodyT, SendMessageReqBody} from "../../api/meeting-api";
import {SocketLinkT, SocketMessageT} from "../../hooks/listeners/useMessageListener";
import {checkMessage} from "../../utils/notifications";
import {getMeetings} from "../events-reducer/events-reducer";

const initialState = {
   meeting: null as null | MeetingT,
   messages: [] as Array<MessageT>,
   isNewMessage: false
}

const meeting = createSlice({
   name: 'meeting',
   initialState: initialState,
   reducers: {
      setMeeting: (state: MeetingStateT, {payload}: PayloadAction<MeetingT | null>): MeetingStateT => ({
         ...state,
         meeting: payload,
      }),
      setIsNewMessage: (state: MeetingStateT, {payload}: PayloadAction<boolean>): MeetingStateT => ({
         ...state,
         isNewMessage: payload,
      }),
      addMessage: (state: MeetingStateT, {payload}: PayloadAction<MessageT>): MeetingStateT => {
         localStorage.setItem("messages", JSON.stringify([...state.messages, payload]))
         return {
            ...state,
            messages: [...state.messages, payload],
      }},
      setAllMessages: (state: MeetingStateT, {payload}: PayloadAction<Array<MessageT>>): MeetingStateT => {
         localStorage.setItem("messages", JSON.stringify(payload))
         return {
            ...state,
            messages: payload,
      }},
      setLink: (state: MeetingStateT, {payload}: PayloadAction<SocketLinkT>): MeetingStateT => {
         const newMessages = state.messages.map(m => {
            if (m.time === payload.time) {
               return {...m, link: payload.link}
            }
            return m
         })
         localStorage.setItem("messages", JSON.stringify(newMessages))
         return {
            ...state,
            messages: newMessages,
         }
      },
   }
})
export default meeting


export const getMeeting = (id: string): BaseThunkT => {
   // get current meeting data
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await eventsApi.getMeetings({event_id: id})
         if (resData.success && resData.data) {
            dispatch(meeting.actions.setMeeting(resData.data[0]))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}
export const sendMessage = (body: SendMessageReqBody, resetForm: () => void): BaseThunkT => {
   // send meting chat message
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await meetingApi.sendMessage(body)
         if (resData.success) {
            resetForm()
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch)
   }
}
export const cancelMeeting = (eventId: string): BaseThunkT => {
   // reject a consultation request
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await meetingApi.cancelMeeting({event_id: eventId})
         checkMessage(resData)
         if (resData.success) {
            await dispatch(getMeetings({}))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}
export const confirmMeeting = (eventId: string): BaseThunkT => {
   // confirm a consultation request
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await meetingApi.confirmMeeting({event_id: eventId})
         checkMessage(resData)
         if (resData.success) {
            await dispatch(getMeetings({}))
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}
export const rate = (body: RateReqBodyT, onSuccess?: () => void): BaseThunkT => {
   // send review
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await meetingApi.rate(body)
         checkMessage(resData)
         if (resData.success) {
            if (onSuccess) onSuccess()
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, true)
   }
}

// types
export type MeetingStateT = typeof initialState

export type MessageT = {
   link?: LinkT
} & SocketMessageT

export type LinkT = {
   title: string
   url: string
}
