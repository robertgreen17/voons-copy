import {combineReducers} from "@reduxjs/toolkit";
import auth from "./auth-reducer/auth-reducer"
import app from "./app-reducer/app-reducer";
import user from "./user-reducer/user-reducer";
import search from "./search-reducer/search-reducer";
import events from "./events-reducer/events-reducer";
import calendar from "./calendar-reducer/calendar-reducer";
import meeting from "./meeting-reducer/meeting-reducer";
import support from "./support-reducer/support-reducer";


const createRootReducer = () => combineReducers({
   auth: auth.reducer,
   app: app.reducer,
   user: user.reducer,
   search: search.reducer,
   events: events.reducer,
   calendar: calendar.reducer,
   meeting: meeting.reducer,
   support: support.reducer
})

export default createRootReducer