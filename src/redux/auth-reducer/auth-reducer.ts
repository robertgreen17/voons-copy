import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {BaseThunkT} from "../store";
import {
   authApi,
   AuthCreateReqBodyT,
   AuthLoginReqBodyT,
   DoPasswordReqBodyT,
   VerifyCodeReqBodyT
} from "../../api/auth-api";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {checkMessage} from "../../utils/notifications";
import app from "../app-reducer/app-reducer";
import {getUserData, UserTypeT} from "../user-reducer/user-reducer";

const initialState = {
   isAuth: false,
   account: null as null | string,
   tempToken: null as null | string,
}

const auth = createSlice({
   name: 'auth',
   initialState: initialState,
   reducers: {
      setIsAuth: (state: AuthStateT, {payload}: PayloadAction<boolean>): AuthStateT => ({
         ...state,
         isAuth: payload
      }),
      setAccount: (state: AuthStateT, {payload}: PayloadAction<string>): AuthStateT => ({
         ...state,
         account: payload
      }),
      setTempToken: (state: AuthStateT, {payload}: PayloadAction<string>): AuthStateT => ({
         ...state,
         tempToken: payload
      }),
      exit: (state: AuthStateT): AuthStateT => ({
         ...state,
         isAuth: false,
         account: null,
         tempToken: null,
      }),
   }
})
export default auth

export const createAcc = (body: AuthCreateReqBodyT,
                          redirect: (url: string) => void): BaseThunkT => {
   // create user account
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         dispatch(app.actions.setIsLoader(true))

         const resData = await authApi.authCreate(body)
         if (resData.success) {
            redirect("/main/sign_in/auth")
         }
         dispatch(app.actions.setIsLoader(false))
         checkMessage(resData)
      }, dispatch)
   }
}
export const login = (body: AuthLoginReqBodyT, redirect: (url: string) => void): BaseThunkT => {
   // login user
   return async (dispatch, getState) => {
      await commonThunkHandler(async () => {
         dispatch(app.actions.setIsLoader(true))

         const resData = await authApi.authLogin(body)
         if (resData.success) {
            // create cookie and set tokens in it
            // Cookies.set('wpac_id', resData.data.refresh_token, {path: "/"});
            localStorage.setItem("refresh", resData.data.refresh_token)
            // now i can say that user is authorized
            await dispatch(getUserData({}))
            dispatch(auth.actions.setIsAuth(true))
            const userType = getState().user.type
            redirect(userType === UserTypeT.client ? "/dashboard/client" : "/dashboard/profi")
         }
         dispatch(app.actions.setIsLoader(false))
         checkMessage(resData)
      }, dispatch)
   }
}
export const refresh = (onFinish?: () => void): BaseThunkT => {
   // refresh user tokens
   return async (dispatch) => {
      await commonThunkHandler(async () => {

         const refresh = localStorage.getItem("refresh")
         if (!refresh) return

         const resData = await authApi.refresh()
         if (resData.success) {
            // create cookie and set tokens in it
            // Cookies.remove("wpac_id", {path: "/"})
            localStorage.setItem("refresh", resData.data.refresh_token)
            // now i can say that user is authorized
            dispatch(auth.actions.setIsAuth(true))
         }
         if (onFinish) {
            onFinish()
         }
      }, dispatch)
   }
}
export const doSocial = (payload: {type: string}): BaseThunkT => {
   // login throw social networks
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         dispatch(app.actions.setIsLoader(true))

         const resData = await authApi.doSocial(payload)
         if (resData.success) {
            dispatch(auth.actions.setIsAuth(true))
         }
         checkForRefresh(resData, dispatch)
         dispatch(app.actions.setIsLoader(false))
      }, dispatch)
   }
}

export const resetPassword = (body: {account: string},
                              redirect: (url: string) => void,): BaseThunkT => {
   // reset user password request
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         dispatch(app.actions.setIsLoader(true))

         const resData = await authApi.resetPassword(body)
         if (resData.success) {
            dispatch(auth.actions.setAccount(body.account))
            redirect("/main/sign_in/reset/2")
         }
         checkForRefresh(resData, dispatch)
         dispatch(app.actions.setIsLoader(false))
         checkMessage(resData)
      }, dispatch)
   }
}
export const verifyCode = (body: VerifyCodeReqBodyT,
                           redirect: (url: string) => void,
                           resetForm: () => void): BaseThunkT => {
   // verify user code for password reset
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         dispatch(app.actions.setIsLoader(true))

         const resData = await authApi.verifyCode(body)
         if (resData.success) {
            dispatch(auth.actions.setTempToken(resData.data.temp_token))
            redirect("/main/sign_in/reset/3")
         }
         checkForRefresh(resData, dispatch)
         resetForm()
         dispatch(app.actions.setIsLoader(false))
         checkMessage(resData)
      }, dispatch)
   }
}
export const doPassword = (body: DoPasswordReqBodyT, redirect: (url: string) => void,): BaseThunkT => {
   // set new user password after verification
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         dispatch(app.actions.setIsLoader(true))

         const res = await authApi.doPassword(body)
         if (res.success) {
            redirect("/main/sign_in/auth")
         }
         checkForRefresh(res, dispatch)
         checkMessage(res)
         dispatch(app.actions.setIsLoader(false))
      }, dispatch)
   }
}

// types
export type AuthStateT = typeof initialState