import {createSelector} from 'reselect'
import {RootStateT} from "./store";

export const getCategories = (state: RootStateT) => state.search.categories

export const getCategoriesWithLimit = (limit?: number) => createSelector(
   getCategories,
   (items) => limit ? items.slice(0, limit) : items

)