import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {BaseThunkT} from "../store";
import {checkForRefresh, commonThunkHandler} from "../../utils/redux-utils";
import {checkMessage} from "../../utils/notifications";
import {
   ChangeAccountReqBodyT, ChangeNotifyReqBodyT,
   ChangePasswordReqBodyT,
   ChangeSettingsReqBodyT,
   DocT,
   GetDocsReqBodyT,
   GetDocsResDataT, GetSupportMessagesReqBodyT,
   GetUsersReqBodyT, NotifyT, RatingT,
   RemoveDocsReqBodyT, ReviewT, SendSupportMessageReqBodyT, SetSocialReqBodyT, StatT, SupportMessageT,
   userApi
} from "../../api/user-api";
import {transformPricesToObject} from "../../utils/transformators";
import {ObjectId} from "../../api/events-api";
import {FindReqBodyT, searchApi, TagT} from "../../api/search-api";
import {USERS_LIMIT} from "../constants";
import search from "../search-reducer/search-reducer";

const initialState = {
   supportMessages: [] as Array<SupportMessageT>,
   user_id: localStorage.getItem("temp_user_id") as string
}

export const MESSAGES_LIMIT = 3

const support = createSlice({
   name: 'support',
   initialState: initialState,
   reducers: {
      setSupportMessages: (state: SupportStateT, {payload}: PayloadAction<Array<SupportMessageT>>): SupportStateT => ({
         ...state,
         supportMessages: payload
      }),
      setUserId: (state: SupportStateT, {payload}: PayloadAction<string>): SupportStateT => ({
         ...state,
         user_id: payload
      }),
      addSupportMessages: (state: SupportStateT, {payload}: PayloadAction<Array<SupportMessageT>>): SupportStateT => ({
         ...state,
         supportMessages: [...state.supportMessages, ...payload]
      }),
      exit: (state: SupportStateT): SupportStateT => ({
         ...state,
         supportMessages: [],
         user_id: "",
      }),

   }
})
export default support

export const sendSupportMessage = (body: SendSupportMessageReqBodyT,
                                   onSuccess?: () => void): BaseThunkT => {
   // change user account data
   return async (dispatch, getState) => {
      await commonThunkHandler(async () => {
         const resData = await userApi.sendSupportMessage(body)
         if (resData.success) {
            if (!body.user_id && resData.data.user_id) {
               localStorage.setItem("temp_user_id", resData.data.user_id)
               dispatch(support.actions.setUserId(resData.data.user_id))
            }
            if (onSuccess) onSuccess()
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch)
   }
}
export const getSupportMessages = (body: GetSupportMessagesReqBodyT): BaseThunkT => {
   // get messages from support chat
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         const resData = await userApi.getSupportMessages(body)
         if (resData.success) {
            if (resData.data.messages) {
               dispatch(support.actions.setSupportMessages(resData.data.messages))
            }
            if (resData.data.user_id) {
               dispatch(support.actions.setUserId(resData.data.user_id))
            }
         }
         await checkForRefresh(resData, dispatch)
      }, dispatch, false)
   }
}
export const getSupportMessagesInfinity = (body: GetSupportMessagesReqBodyT,
                                           setLoader: (flag: boolean) => void,
                                           setHasMore: (flag: boolean) => void): BaseThunkT => {
   // get messages from support chat
   return async (dispatch) => {
      await commonThunkHandler(async () => {
         setLoader(true)
         const resData = await userApi.getSupportMessages(body)
         if (resData.success) {
            if (resData.data.messages) {
               dispatch(support.actions.addSupportMessages(resData.data.messages))
               if (resData.data.messages.length < MESSAGES_LIMIT) {
                  setHasMore(false)
               }
            }
            if (resData.data.user_id) {
               dispatch(support.actions.setUserId(resData.data.user_id))
            }
         }
         await checkForRefresh(resData, dispatch)
         setLoader(false)
      }, dispatch, false)
   }
}

export type SupportStateT = typeof initialState
